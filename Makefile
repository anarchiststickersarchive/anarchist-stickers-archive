SHELL := /bin/bash

build-frontend:
	cd frontend; docker build -t anarchist-stickers-archive-frontend:latest . --build-arg CACHEBUST=$(shell date +%s); cd ..;

build-proxy:
	cd proxy; docker build -t anarchist-stickers-archive-nginx:1.25.3-alpine . --no-cache; cd ..;

build-ipfs:
	cd ipfs; docker build -t anarchist-stickers-archive-ipfs:0.24.0 . --no-cache; cd ..;

build-backend:
	cd backend; sbt clean docker:publishLocal; cd ..;

check-dependencies:
	cd backend; sbt clean; sbt unusedCompileDependencies; cd ..;

teardown:
	cd ioc/docker; docker-compose down; cd ../../;

up:
	cd ioc/docker; docker-compose --compatibility up -d --force-recreate; cd ../../;

up-frontend:
	cd ioc/docker; docker-compose --compatibility up -d --force-recreate --no-deps frontend; cd ../../;

restart-proxy:
	docker restart anarchist-stickers-archive-nginx;

install: build-frontend teardown build-backend build-proxy up

prune-docker:
	docker image prune -f

prune-redis:
	docker exec anarchist-stickers-archive-redis redis-cli BGREWRITEAOF

stop-elk:
	cd ioc/docker; docker-compose stop elasticsearch kibana filebeat

refresh: prune-docker prune-redis build-frontend up-frontend up restart-proxy publish

publish:
	$(eval ARCHIVE_IPFS_CID=$(shell docker exec anarchist-stickers-archive-ipfs sh -c "ipfs add --pin=true --recursive --nocopy --cid-version 1 /data/assets/archive -Q" | tr -d '\r'))
	echo ARCHIVE_IPFS_CID is $(ARCHIVE_IPFS_CID)
	docker exec anarchist-stickers-archive-ipfs sh -c "ipfs name publish /ipfs/${ARCHIVE_IPFS_CID}";

db-dump:
	docker exec -it anarchist-stickers-archive-db sh -c 'exec mariadb-dump --databases anarchist-sticker-archive-db --user anarchist-sticker-archive-user -p"$${MYSQL_PASSWORD}"' > /opt/anarchist-stickers-archive/doc/dump.sql

build-cli:
	cd cli; cargo build --release; cp target/release/cli ~/.local/bin/asa-cli; cd ..;

cli-process-all: cli-sync cli-scrappe cli-upscale cli-vectorize cli-rasterize cli-stats

cli-categorize:
	asa-cli --env-file cli/.env categorize

cli-rasterize:
	asa-cli --env-file cli/.env rasterize

cli-stats:
	asa-cli --env-file cli/.env stats --sync

cli-sync:
	asa-cli --env-file cli/.env sync

cli-upscale:
	asa-cli --env-file cli/.env upscale

cli-vectorize:
	asa-cli --env-file cli/.env vectorize

cli-scrappe: cli-scrappe-telegram cli-scrappe-instagram

cli-scrappe-instagram:
	asa-cli --env-file cli/.env scrape --source instagram

cli-scrappe-telegram:
	asa-cli --env-file cli/.env scrape --source telegram

