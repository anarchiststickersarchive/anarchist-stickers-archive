worker_processes 1;
 
events { worker_connections 1024; }
 
http {
 
    sendfile on;
 
    upstream backend-upstream {
        server backend:8888;
    }

    upstream ipfs-gateway-upstream {
        server ipfs:8080;
    }

    upstream frontend-upstream {
        server frontend:3000;
    }

    server {
        listen 80;
        server_name anarchiststickersarchive.org;
        server_tokens off;

        location /.well-known/acme-challenge/ {
            root /var/www/certbot;
        }

        location / {
            return 301 https://$host$request_uri;
        }
 
    }

    server {
        listen 443 ssl;
        server_name api.anarchiststickersarchive.org;
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/anarchiststickersarchive.org/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/anarchiststickersarchive.org/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass         http://backend-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }

    server {
        listen 443 ssl;
        server_name gateway.ipfs.anarchiststickersarchive.org;
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/anarchiststickersarchive.org/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/anarchiststickersarchive.org/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass         http://ipfs-gateway-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }

    server {
        listen 443 ssl;
        server_name anarchiststickersarchive.org www.anarchiststickersarchive.org;
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/anarchiststickersarchive.org/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/anarchiststickersarchive.org/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass          http://frontend-upstream;
            proxy_set_header    Host                $http_host;
            proxy_set_header    X-Real-IP           $remote_addr;
            proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        }

        location /api {
            proxy_pass         http://backend-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
 
        location /ipfs-gateway {
            rewrite ^/ipfs-gateway/(.*)$ /$1 break; 
            proxy_pass         http://ipfs-gateway-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }

}
