import os
import re

from telethon.sync import TelegramClient
from dotenv import load_dotenv
from urllib.parse import urlparse


def get_project_path():
    load_dotenv()
    return os.environ.get("PROJECT_PATH")

def download_via_gallery_dl(storing_path, url):
    os.makedirs(storing_path, exist_ok=True)
    command = f"gallery-dl -D {storing_path} {url}"
    subprocess_result = subprocess.run(['zsh', '-c', command], capture_output=True, text=True)
    return subprocess_result

def delete_message(client, message, result):
    if result.returncode == 0:
        client.delete_messages('me', message.id)
    else:
        print(f"result={str(result)}")


def download_telegram_image(message, client):
    project_path = get_project_path()
    channel_handler = message.forward.chat.username if message.forward.chat else "nn"
    storing_path = str(project_path) + '/telegram/@' + str(channel_handler)
    os.makedirs(storing_path, exist_ok=True)
    print(f"channel_handler={channel_handler} storing_path={storing_path}")
    client.download_media(message, storing_path)
    client.delete_messages('me', message.id)


def get_project_path():
    load_dotenv()
    return os.environ.get("PROJECT_PATH")

def extract_tumblr_site_name(url):
    try:
        parsed_url = urlparse(url)
        domain = parsed_url.netloc
        parts = domain.split('.')
        return parts[0]
    except Exception as e:
        print(f"An error occurred: {e}")
        return None


def is_tumblr_url(url):
    # regular expression pattern for a valid tweet URL
    pattern = r'^https://(.+)\.tumblr\.com/post/(.+)'
    if url and re.match(pattern, url):
        return True
    else:
        return False


def download_tumblr_image(url):
    project_path = get_project_path()
    site_name = extract_tumblr_site_name(url)
    storing_path = str(project_path) + '/website/' + str(site_name) + '.tumblr.com'
    return download_via_gallery_dl(storing_path, url)

def download_twitter_media(url):
    project_path = get_project_path()
    tweet_username = twitter_extract_username(url)
    storing_path = str(project_path) + '/twitter/@' + str(tweet_username)
    return download_via_gallery_dl(storing_path, url)


def is_tweet_url(url):
    # regular expression pattern for a valid tweet URL
    pattern = r'^https://(twitter|x)\.com/(.+)/status/(\d+)(.+)$'
    if url and re.match(pattern, url):
        return True
    else:
        return False


def extract_tweet_id(url):
    try:
        result = urlparse(url)
        tweet_id = result.path.split('/')[-1]
        return tweet_id
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

def twitter_extract_username(url):
    try:
        result = urlparse(url)
        username = result.path.split('/')[1]  # get the second element after splitting the path
        return username
    except Exception as e:
        print(f"An error occurred: {e}")
        return None


# ---

load_dotenv()
name = 'asatsm'
telegram_api_id = int(os.environ.get("TELEGRAM_API_ID"))
telegram_api_hash = os.environ.get("TELEGRAM_API_HASH")
telegram_chat = 'me'

with TelegramClient(name, telegram_api_id, telegram_api_hash) as client:
    for message in client.iter_messages(telegram_chat):
        if message.photo and message.forward is not None:
            download_telegram_image(message, client)
        if is_tweet_url(message.text):
            result = download_twitter_media(message.text)
            delete_message(client, message, result)
        if is_tumblr_url(message.text):
            result = download_tumblr_image(message.text)
            delete_message(client, message, result)
