{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  requests = python3Packages.buildPythonPackage rec {
    pname = "requests";
    version = "2.32.3";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-VTZUF3NOsYJVWQqf+euX6eHaho1MzWQCOZ6vaK8gp2A=";
    };
    propagatedBuildInputs = [ python3Packages.urllib3 python3Packages.idna python3Packages.certifi];
  };

  instaloader = python3Packages.buildPythonPackage rec {
    pname = "instaloader";
    version = "4.14.1";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-pBpzcqGPsJaz7VRUaUeYhN6c92jhICDA4OZ8SI2dWZw=";
    };
    propagatedBuildInputs = [ requests ];
  };

  python_dotenv = python3Packages.buildPythonPackage rec {
    pname = "python-dotenv";
    version = "1.0.0";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-qN+WA0qubS1QpOvoIWMmxhw+tkg2d2UE/MpBDlk3o7o=";
    };
  };

  telethon = python3Packages.buildPythonPackage rec {
    pname = "Telethon";
    version = "1.37.0";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-5eQ8/xwbNOL5wrOVIVvrbpvacGtp3vfv/09VsjycQ3Q=";
    };
    propagatedBuildInputs = [ python3Packages.pyaes python3Packages.rsa ];
  };

  tweepy = python3Packages.buildPythonPackage rec {
    pname = "tweepy";
    version = "4.14.0";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-H58XB9aXLebP9sX9kN/mpEnNLg1wvUAEP/qwHgegbIw=";
    };
  };

  pythonEnv = python3.withPackages (ps: [
    instaloader
    python_dotenv
    requests
    telethon
    tweepy
  ]);

in
mkShell {
  buildInputs = [
    git
    zsh
    pythonEnv
    gallery-dl
  ];
  shellHook = ''
      set -a
      . ./resources/telegram-scrapper/.env
      set +a
  '';
}
