use crate::commands::categorize_handler::CategorizeCommandHandler;
use crate::commands::rasterize_handler::RasterizeCommandHandler;
use crate::commands::stats_handler::StatsCommandHandler;
use crate::commands::sync_handler::SyncCommandHandler;
use crate::commands::upscale_handler::UpscaleCommandHandler;
use crate::commands::vectorize_handler::VectorizeCommandHandler;
use crate::dtos::constants;
use crate::dtos::path_mapper_handler::PathMappings;
use crate::services::categorize_service::CategorizeService;
use crate::services::files_service::FilesService;
use crate::services::rasterize_service::RasterizeService;
use crate::services::sshfs_service::SshfsService;
use crate::services::sync_service;
use crate::services::upscale_service::UpscaleService;
use crate::services::vectorize_service::VectorizeService;
use std::env;
use std::error::Error;

pub mod services {
    use crate::boot::di::path_mappings;
    use crate::services::scrape::instagram_service::ScrapeInstagramService;
    use crate::services::scrape::telegram_service::ScrapeTelegramService;
    use rand::prelude::SliceRandom;
    use rand::thread_rng;
    use std::env;
    use std::error::Error;

    pub fn scrape_telegram_service() -> Result<ScrapeTelegramService, Box<dyn Error>> {
        Ok(ScrapeTelegramService {
            telegram_api_id: env::var("TELEGRAM_API_ID").unwrap(),
            telegram_api_hash: env::var("TELEGRAM_API_HASH").unwrap(),
            telegram_chat: env::var("TELEGRAM_CHAT").unwrap(),
        })
    }

    pub fn scrape_instagram_service() -> Result<ScrapeInstagramService, Box<dyn Error>> {
        let mut rng = thread_rng();
        let mut accounts: Vec<String> = env::var("INSTAGRAM_ACCOUNTS")
            .unwrap()
            .split(" ")
            .map(|s| s.to_string())
            .collect();
        accounts.shuffle(&mut rng);
        Ok(ScrapeInstagramService {
            accounts: accounts,
            project_path: format!(
                "{}/instagram",
                path_mappings()?.workbench_path.to_str().unwrap()
            ),
        })
    }
}

pub mod command_handlers {
    use crate::boot::di::services;
    use crate::commands::scrape_handler::{
        ScrapeCommandHandler, ScrapeInstagramCommandHandler, ScrapeTelegramCommandHandler,
    };
    use std::error::Error;

    pub fn scrape() -> Result<ScrapeCommandHandler, Box<dyn Error>> {
        Ok(ScrapeCommandHandler {
            instagram_command_handler: scrape_instagram()?,
            telegram_command_handler: scrape_telegram()?,
        })
    }

    pub fn scrape_instagram() -> Result<ScrapeInstagramCommandHandler, Box<dyn Error>> {
        Ok(ScrapeInstagramCommandHandler {
            service: services::scrape_instagram_service()?,
        })
    }

    pub fn scrape_telegram() -> Result<ScrapeTelegramCommandHandler, Box<dyn Error>> {
        Ok(ScrapeTelegramCommandHandler {
            service: services::scrape_telegram_service()?,
        })
    }
}

pub fn sshfs_service() -> Result<SshfsService, Box<dyn Error>> {
    let service: SshfsService = SshfsService::new();
    service.mount()?;
    Ok(service)
}

pub fn path_mappings() -> Result<PathMappings, Box<dyn Error>> {
    let local_mount_path: String = sshfs_service()?.local_mount_path;
    let path_mappings: Result<PathMappings, Box<dyn Error>> =
        PathMappings::new(Some(local_mount_path));
    path_mappings
}

pub fn categorize_service() -> Result<CategorizeService, Box<dyn Error>> {
    Ok(CategorizeService {})
}

pub fn rasterize_service() -> Result<RasterizeService, Box<dyn Error>> {
    Ok(RasterizeService {
        path_mappings: path_mappings()?,
    })
}

pub fn files_service() -> Result<FilesService, Box<dyn Error>> {
    Ok(FilesService {
        path_mappings: path_mappings()?,
    })
}

pub fn vectorize_service() -> Result<VectorizeService, Box<dyn Error>> {
    Ok(VectorizeService {
        path_mappings: path_mappings()?,
        api_key: env::var("VECTORIZER_IO_API_KEY")?,
        target_size: constants::TARGET_SIZE,
    })
}

pub fn upscale_service() -> Result<UpscaleService, Box<dyn Error>> {
    Ok(UpscaleService {
        path_mappings: path_mappings()?,
    })
}

pub fn upscale_command_handler() -> Result<UpscaleCommandHandler, Box<dyn Error>> {
    Ok(UpscaleCommandHandler {
        files_service: files_service()?,
        upscale_service: upscale_service()?,
    })
}

pub fn categorize_command_handler() -> Result<CategorizeCommandHandler, Box<dyn Error>> {
    Ok(CategorizeCommandHandler {
        categorize_service: categorize_service()?,
        files_service: files_service()?,
        path_mappings: path_mappings()?,
    })
}

pub fn sync_command_handler() -> Result<SyncCommandHandler, Box<dyn Error>> {
    Ok(SyncCommandHandler {
        sync_service: sync_service()?,
    })
}

pub fn rasterize_command_handler() -> Result<RasterizeCommandHandler, Box<dyn Error>> {
    Ok(RasterizeCommandHandler {
        rasterize_service: rasterize_service()?,
        files_service: files_service()?,
    })
}

pub fn vectorize_command_handler() -> Result<VectorizeCommandHandler, Box<dyn Error>> {
    Ok(VectorizeCommandHandler {
        files_service: files_service()?,
        vectorize_service: vectorize_service()?,
    })
}

pub fn sync_service() -> Result<sync_service::SyncService, Box<dyn Error>> {
    Ok(sync_service::SyncService {})
}

pub fn stats_command_handler() -> Result<StatsCommandHandler, Box<dyn Error>> {
    Ok(StatsCommandHandler {
        sync_service: sync_service()?,
    })
}
