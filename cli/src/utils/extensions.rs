
pub fn image_types() -> Vec<&'static str> {
    vec!["jpg", "jpeg", "png", "gif", "webp"]
}