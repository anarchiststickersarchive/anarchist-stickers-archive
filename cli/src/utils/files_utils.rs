use std::path::{Path, PathBuf};

use log::debug;
use path_absolutize::Absolutize;
use rand::{seq::SliceRandom, thread_rng};
use walkdir::WalkDir;

pub fn get_files_in_path_recursive(
    path: &str,
    shuffle: bool,
    filtering_function: Option<&dyn Fn(&Path) -> bool>,
) -> Result<Vec<PathBuf>, std::io::Error> {
    let mut filtered_files: Vec<PathBuf> = WalkDir::new(path)
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
        .filter(|entry| match filtering_function {
            Some(filtering_function) => filtering_function(entry.path()),
            None => true,
        })
        .map(|entry| entry.path().absolutize().unwrap().to_path_buf())
        .collect();

    if shuffle {
        let mut rng = thread_rng();
        filtered_files.shuffle(&mut rng);
    }

    let filtered = filtering_function.is_some();

    debug!(
        "Picked {} files from path={} shuffled?={} filtered?={}",
        filtered_files.len(),
        path,
        shuffle,
        filtered
    );

    Ok(filtered_files)
}
// pub fn get(root_path: &str, extensions: Vec<&str>) -> Result<Vec<String>, std::io::Error> {
//     // Fetch all files recursively
//     let all_files: Vec<PathBuf> = WalkDir::new(root_path)
//         .into_iter()
//         .filter_map(Result::ok)
//         .filter(|entry| entry.path().is_file())
//         .map(|entry| entry.into_path())
//         .collect();
//
//     // Collect SVG files that haven't been rasterized
//     let svg_files = all_files
//         .iter()
//         .filter(|file_path| {
//             // Check if the file has an SVG extension
//             file_path
//                 .extension()
//                 .and_then(|ext| ext.to_str())
//                 .map_or(false, |ext| extensions.contains(&ext))
//         })
//         .filter(|svg_file| {
//             // Get the parent directory and file stem
//             if let (Some(parent), Some(stem)) = (
//                 svg_file.parent(),
//                 svg_file.file_stem().and_then(|s| s.to_str()),
//             ) {
//                 // Check if there is any other file in the same folder with the same stem but different extension
//                 !all_files.iter().any(|other_file| {
//                     other_file.parent() == Some(parent)
//                         && other_file.file_stem().and_then(|s| s.to_str()) == Some(stem)
//                         && other_file.extension().and_then(|ext| ext.to_str()).map_or(
//                             // Exclude the SVG file itself
//                             false,
//                             |ext| !ext.eq_ignore_ascii_case("svg"),
//                         )
//                 })
//             } else {
//                 false
//             }
//         })
//         .map(|path| path.display().to_string())
//         .collect();
//
//     Ok(svg_files)
// }
