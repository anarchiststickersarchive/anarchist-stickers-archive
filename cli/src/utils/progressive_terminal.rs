use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use colored::Colorize;
use std::ops::Add;
use std::{iter::Cycle, time::Duration, vec::IntoIter};

pub fn run<F, T, E>(func: F, config: &ProgressiveTerminalConfig<T>) -> Result<T, E>
where
    F: Fn() -> Result<T, E> + Send + 'static,
    T: Send + 'static,
    E: std::error::Error + Send + 'static,
{
    use std::sync::{mpsc, Arc, Mutex};
    use std::thread;

    let (tx, rx) = mpsc::channel();
    let done_flag = Arc::new(Mutex::new(false));
    let done_flag_clone = done_flag.clone();

    thread::spawn(move || {
        let result = func();
        let _ = tx.send(result);
        let mut done = done_flag_clone.lock().unwrap();
        *done = true;
    });

    let mut spinner: Cycle<IntoIter<&str>> = vec!["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"]
        .into_iter()
        .cycle();

    loop {
        if *done_flag.lock().unwrap() {
            break;
        }

        if let Some(spin_char) = spinner.next() {
            print!("\r{} {}", spin_char.blue(), config.initial_message);
        }
        thread::sleep(Duration::from_millis(40));
    }

    match rx.recv().unwrap() {
        Ok(ok_result) => {
            let white_space_padding = " ".repeat(16);
            print!(
                "\r{}{}{}",
                config
                    .ok_emoji
                    .clone()
                    .map(|emoji| emoji.add(" "))
                    .unwrap_or_default()
                    .green(),
                config
                    .ok_message_supplier
                    .as_ref()
                    .map(|supplier| supplier.as_ref()(&ok_result))
                    .unwrap_or_default(),
                white_space_padding
            );
            println!();
            Ok(ok_result)
        }
        Err(error) => {
            print!(
                "\r{}{} | Cause: {}",
                config
                    .error_emoji
                    .clone()
                    .map(|emoji| emoji.add(" "))
                    .unwrap_or_default()
                    .red(),
                config.error_message_supplier.red(),
                error
            );
            println!();
            Err(error)
        }
    }
}
