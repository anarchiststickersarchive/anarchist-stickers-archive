#[derive(Debug, Clone, Copy)]
pub enum ExtensionType {
    JPG,
    SVG,
}

impl ExtensionType {
    pub fn as_str(&self) -> &'static str {
        match self {
            ExtensionType::JPG => "jpg",
            ExtensionType::SVG => "svg",
        }
    }
}
