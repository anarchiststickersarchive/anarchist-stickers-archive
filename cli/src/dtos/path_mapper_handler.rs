use std::env;
use std::error::Error;
use std::path::{Path, PathBuf};

use crate::dtos::constants::*;
use crate::dtos::path_mapper::*;

// Struct to hold all path mappers and paths
#[derive(Clone)]
pub struct PathMappings {
    pub assets_path: PathBuf,
    pub workbench_path: PathBuf,
    pub upscaled_path: PathBuf,
    pub vector_path: PathBuf,
    pub final_path: PathBuf,
    pub trash_path: PathBuf,
    pub error_root_path: PathBuf,
    pub error_upscaling_path: PathBuf,

    pub upscaled_to_workbench_path_mapper: UpscaledToWorkbenchPathMapper,
    pub upscaled_to_trash_path_mapper: UpscaledToTrashPathMapper,
    pub upscaled_to_final_path_mapper: UpscaledToFinalPathMapper,
    pub upscaled_to_to_vector_path_mapper: UpscaledToToVectorPathMapper,
    pub workbench_to_upscaled_path_mapper: WorkbenchToUpscaledPathMapper,
    pub workbench_to_error_path_mapper: WorkbenchToErrorPathMapper,
    pub workbench_to_trash_path_mapper: WorkbenchToTrashPathMapper,
    pub final_vector_to_final_jpg_path_mapper: FinalVectorToFinalJpgPathMapper,
    pub to_vector_to_trash_path_mapper: ToVectorToTrashPathMapper,
    pub to_vector_to_final_path_mapper: ToVectorToFinalPathMapper,
}

impl PathMappings {
    pub fn new(maybe_assets_path: Option<String>) -> Result<Self, Box<dyn Error>> {
        // Get the root path from the environment variable
        let assets_path_as_string: String =
            maybe_assets_path.unwrap_or(env::var("LOCAL_ASSETS_PATH")?);
        let assets_path = PathBuf::from(&assets_path_as_string);
        let ingestion_path = assets_path.join("ingestion");

        // Define paths using constants
        let workbench_path = ingestion_path.join(WORKBENCH_FOLDER_NAME);
        let upscaled_path = ingestion_path.join(UPSCALED_FOLDER_NAME);
        let vector_path = ingestion_path.join(VECTOR_FOLDER_NAME);
        let final_path = ingestion_path.join(FINAL_FOLDER_NAME);
        let trash_path = ingestion_path.join(TRASH_FOLDER_NAME);
        let error_root_path = ingestion_path.join(ERROR_FOLDER_NAME);
        let error_upscaling_path = ingestion_path.join(UPSCALED_FOLDER_NAME);

        // Initialize path mappers
        let upscaled_to_workbench_path_mapper = UpscaledToWorkbenchPathMapper::new(&ingestion_path);
        let upscaled_to_trash_path_mapper = UpscaledToTrashPathMapper::new(&ingestion_path);
        let upscaled_to_final_path_mapper = UpscaledToFinalPathMapper::new(&ingestion_path);
        let upscaled_to_to_vector_path_mapper = UpscaledToToVectorPathMapper::new(&ingestion_path);
        let workbench_to_upscaled_path_mapper = WorkbenchToUpscaledPathMapper::new(&ingestion_path);
        let workbench_to_error_path_mapper = WorkbenchToErrorPathMapper::new(&ingestion_path);
        let workbench_to_trash_path_mapper = WorkbenchToTrashPathMapper::new(&ingestion_path);
        let final_vector_to_final_jpg_path_mapper =
            FinalVectorToFinalJpgPathMapper::new(&ingestion_path);
        let to_vector_to_trash_path_mapper = ToVectorToTrashPathMapper::new(&ingestion_path);
        let to_vector_to_final_path_mapper = ToVectorToFinalPathMapper::new(&ingestion_path);

        Ok(Self {
            assets_path: assets_path,
            workbench_path,
            upscaled_path,
            vector_path,
            final_path,
            trash_path,
            error_root_path,
            error_upscaling_path,

            upscaled_to_workbench_path_mapper,
            upscaled_to_trash_path_mapper,
            upscaled_to_final_path_mapper,
            upscaled_to_to_vector_path_mapper,
            workbench_to_upscaled_path_mapper,
            workbench_to_error_path_mapper,
            workbench_to_trash_path_mapper,
            final_vector_to_final_jpg_path_mapper,
            to_vector_to_trash_path_mapper,
            to_vector_to_final_path_mapper,
        })
    }

    // Define methods that wrap around the mappers' get methods
    pub fn upscaled_to_workbench_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, Box<dyn Error>> {
        self.upscaled_to_workbench_path_mapper
            .get(source_absolute_path)
    }

    pub fn upscaled_to_trash_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, Box<dyn Error>> {
        self.upscaled_to_trash_path_mapper.get(source_absolute_path)
    }

    pub fn workbench_to_upscaled_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, Box<dyn Error>> {
        self.workbench_to_upscaled_path_mapper
            .get(source_absolute_path)
    }

    pub fn workbench_to_error_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, Box<dyn Error>> {
        self.workbench_to_error_path_mapper
            .get(source_absolute_path)
    }

    pub fn workbench_to_trash_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, Box<dyn Error>> {
        self.workbench_to_trash_path_mapper
            .get(source_absolute_path)
    }

    pub fn final_vector_to_final_jpg_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, std::io::Error> {
        self.final_vector_to_final_jpg_path_mapper
            .get(source_absolute_path)
            .map_err(|e| {
                if let Some(io_error) = e.downcast_ref::<std::io::Error>() {
                    std::io::Error::new(io_error.kind(), io_error.to_string())
                } else {
                    std::io::Error::new(std::io::ErrorKind::Other, e.to_string())
                }
            })
    }

    pub fn upscaled_to_final_path_mapping(
        &self,
        source_absolute_path: &Path,
    ) -> Result<String, Box<dyn Error>> {
        self.upscaled_to_final_path_mapper.get(source_absolute_path)
    }

    pub fn to_vector_to_trash_path_mapping(
        &self,
        source_absolute_path: &Path,
        model: &str,
        number_of_colors: &str,
    ) -> Result<String, Box<dyn Error>> {
        self.to_vector_to_trash_path_mapper
            .get(source_absolute_path, model, number_of_colors)
    }

    pub fn to_vector_to_final_path_mapping(
        &self,
        source_absolute_path: &Path,
        model: &str,
        number_of_colors: &str,
    ) -> Result<String, Box<dyn Error>> {
        self.to_vector_to_final_path_mapper
            .get(source_absolute_path, model, number_of_colors)
    }

    pub fn upscaled_to_to_vector_path_mapping(
        &self,
        source_absolute_path: &Path,
        model: &str,
        number_of_colors: &str,
    ) -> Result<String, Box<dyn Error>> {
        self.upscaled_to_to_vector_path_mapper
            .get(source_absolute_path, model, number_of_colors)
    }
}
