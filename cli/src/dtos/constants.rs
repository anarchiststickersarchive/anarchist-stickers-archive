pub const WORKBENCH_FOLDER_NAME: &str = "workbench";
pub const UPSCALED_FOLDER_NAME: &str = "upscaled";
pub const VECTOR_FOLDER_NAME: &str = "2-vector";
pub const FINAL_FOLDER_NAME: &str = "final";
pub const ERROR_FOLDER_NAME: &str = "error";
pub const TRASH_FOLDER_NAME: &str = "trash";

pub const VECTORIZATION_TYPES: &[&str] = &["clipart", "drawing"];
pub const TARGET_SIZE: u32 = 2500;
