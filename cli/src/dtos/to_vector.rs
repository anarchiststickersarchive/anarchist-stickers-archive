use std::path::PathBuf;

#[derive(Clone, Debug)]
pub struct ToVector {
    pub absolute_path: PathBuf,
    pub vector_type: String,
    pub number_of_colors: String,
}
