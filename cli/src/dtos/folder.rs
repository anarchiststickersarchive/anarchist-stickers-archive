use std::path::{Path, PathBuf};

#[derive(Clone, Debug)]
pub struct Folder {
    pub root_path: PathBuf,
    pub name_path: PathBuf,
    pub absolute_path: PathBuf,
}

impl Folder {
    pub fn new(root_path: &Path, name_path: &str) -> Self {
        let name_path_buf = PathBuf::from(name_path);
        let absolute_path = root_path.join(&name_path_buf);
        Self {
            root_path: root_path.to_path_buf(),
            name_path: name_path_buf,
            absolute_path,
        }
    }
}
