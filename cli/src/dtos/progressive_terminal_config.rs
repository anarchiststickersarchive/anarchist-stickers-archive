pub struct ProgressiveTerminalConfig<T> {
    pub initial_message: String,
    pub ok_emoji: Option<String>,
    pub ok_message_supplier: Option<Box<dyn Fn(&T) -> String>>,
    pub error_emoji: Option<String>,
    pub error_message_supplier: String,
}
