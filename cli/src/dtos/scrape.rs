use std::str::FromStr;

#[derive(Debug, Clone, Copy)]
pub enum ScrapeSource {
    Telegram,
    Instagram,
}

impl FromStr for ScrapeSource {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "telegram" => Ok(ScrapeSource::Telegram),
            "instagram" => Ok(ScrapeSource::Instagram),
            _ => Err("Invalid value: must be 'telegram' or 'instagram'"),
        }
    }
}
