use std::error::Error;
use std::fs;
use std::path::{Path, PathBuf};

use crate::dtos::constants::*;
use crate::dtos::extension_type::ExtensionType;
use crate::dtos::folder::Folder;

#[derive(Clone)]
struct PathMapper {
    source_folder: Folder,
    destination_folder: Folder,
    destination_extension: ExtensionType,
}

impl PathMapper {
    pub fn new(
        source_folder: Folder,
        destination_folder: Folder,
        destination_extension: ExtensionType,
    ) -> Self {
        //println!("source_folder={:?}", source_folder);
        //println!("destination_folder={:?}", destination_folder);
        //println!("destination_extension={:?}", destination_extension);
        Self {
            source_folder,
            destination_folder,
            destination_extension,
        }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        let filename = Self::filename(source_absolute_path)?;
        let filename_no_extension = Self::filename_no_extension(source_absolute_path)?;
        let path_infix = self.path_infix(source_absolute_path, &filename)?;

        let output_folder = self.destination_folder.absolute_path.join(&path_infix);
        fs::create_dir_all(output_folder.clone())?;

        let return_value = output_folder.join(format!(
            "{}.{}",
            filename_no_extension,
            self.destination_extension.as_str()
        ));
        log::debug!(
            "mapper={} input={:?}, output={:?}",
            std::any::type_name::<Self>(),
            source_absolute_path,
            return_value
        );
        Ok(return_value.to_string_lossy().to_string())
    }

    fn extension(source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        let filename = Self::filename(source_absolute_path)?;
        Ok(Path::new(&filename)
            .extension()
            .unwrap_or_default()
            .to_string_lossy()
            .to_string())
    }

    fn filename(source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        Ok(source_absolute_path
            .file_name()
            .ok_or("No filename")?
            .to_string_lossy()
            .to_string())
    }

    fn filename_no_extension(source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        let filename = Self::filename(source_absolute_path)?;
        Ok(Path::new(&filename)
            .file_stem()
            .ok_or("No file stem")?
            .to_string_lossy()
            .to_string())
    }

    fn path_infix(
        &self,
        source_absolute_path: &Path,
        filename: &str,
    ) -> Result<PathBuf, Box<dyn Error>> {
        let source_folder_absolute = &self.source_folder.absolute_path;
        let mut relative_path = source_absolute_path.strip_prefix(source_folder_absolute)?;
        if relative_path.ends_with(filename) {
            relative_path = relative_path.parent().unwrap_or(relative_path);
        }
        Ok(relative_path.to_path_buf())
    }
}

#[derive(Clone)]
pub struct WorkbenchToUpscaledPathMapper {
    mapper: PathMapper,
}

impl WorkbenchToUpscaledPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let workbench_folder = Folder::new(root_path, WORKBENCH_FOLDER_NAME);
        let upscaled_folder = Folder::new(root_path, UPSCALED_FOLDER_NAME);
        let mapper = PathMapper::new(workbench_folder, upscaled_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}

#[derive(Clone)]
pub struct WorkbenchToErrorPathMapper {
    mapper: PathMapper,
}

impl WorkbenchToErrorPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let workbench_folder = Folder::new(root_path, WORKBENCH_FOLDER_NAME);
        let error_folder = Folder::new(root_path, ERROR_FOLDER_NAME);
        let mapper = PathMapper::new(workbench_folder, error_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}

#[derive(Clone)]
pub struct WorkbenchToTrashPathMapper {
    mapper: PathMapper,
}

impl WorkbenchToTrashPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let workbench_folder = Folder::new(root_path, WORKBENCH_FOLDER_NAME);
        let trash_folder = Folder::new(root_path, TRASH_FOLDER_NAME);
        let mapper = PathMapper::new(workbench_folder, trash_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}

#[derive(Clone)]
pub struct UpscaledToWorkbenchPathMapper {
    mapper: PathMapper,
}

impl UpscaledToWorkbenchPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let upscaled_folder = Folder::new(root_path, UPSCALED_FOLDER_NAME);
        let workbench_folder = Folder::new(root_path, WORKBENCH_FOLDER_NAME);
        let mapper = PathMapper::new(upscaled_folder, workbench_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}

#[derive(Clone)]
pub struct UpscaledToTrashPathMapper {
    mapper: PathMapper,
}

impl UpscaledToTrashPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let upscaled_folder = Folder::new(root_path, UPSCALED_FOLDER_NAME);
        let trash_folder = Folder::new(root_path, TRASH_FOLDER_NAME);
        let mapper = PathMapper::new(upscaled_folder, trash_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}

#[derive(Clone)]
pub struct UpscaledToFinalPathMapper {
    mapper: PathMapper,
}

impl UpscaledToFinalPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let upscaled_folder = Folder::new(root_path, UPSCALED_FOLDER_NAME);
        let final_folder = Folder::new(root_path, FINAL_FOLDER_NAME);
        let mapper = PathMapper::new(upscaled_folder, final_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}

#[derive(Clone)]
pub struct UpscaledToToVectorPathMapper {
    mapper: PathMapper,
}

impl UpscaledToToVectorPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let upscaled_folder = Folder::new(root_path, UPSCALED_FOLDER_NAME);
        let vector_folder = Folder::new(root_path, VECTOR_FOLDER_NAME);
        let mapper = PathMapper::new(upscaled_folder, vector_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(
        &self,
        source_absolute_path: &Path,
        model: &str,
        number_of_colors: &str,
    ) -> Result<String, Box<dyn Error>> {
        let filename = PathMapper::filename(source_absolute_path)?;
        let filename_no_extension = PathMapper::filename_no_extension(source_absolute_path)?;
        let extension = PathMapper::extension(source_absolute_path)?;
        let path_infix = self.mapper.path_infix(source_absolute_path, &filename)?;

        let output_folder = self
            .mapper
            .source_folder
            .root_path
            .join(&self.mapper.destination_folder.name_path)
            .join(format!("{}-{}/", model, number_of_colors))
            .join(&path_infix);
        fs::create_dir_all(&output_folder)?;

        let return_value = output_folder.join(format!("{}.{}", filename_no_extension, extension));
        log::debug!(
            "mapper={} input={:?}, output={:?}",
            std::any::type_name::<Self>(),
            source_absolute_path,
            return_value
        );

        Ok(return_value.to_string_lossy().to_string())
    }
}

#[derive(Clone)]
pub struct ToVectorToTrashPathMapper {
    mapper: PathMapper,
}

impl ToVectorToTrashPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let vector_folder = Folder::new(root_path, VECTOR_FOLDER_NAME);
        let trash_folder = Folder::new(root_path, TRASH_FOLDER_NAME);
        let mapper = PathMapper::new(vector_folder, trash_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(
        &self,
        source_absolute_path: &Path,
        model: &str,
        number_of_colors: &str,
    ) -> Result<String, Box<dyn Error>> {
        let filename = PathMapper::filename(source_absolute_path)?;
        let filename_no_extension = PathMapper::filename_no_extension(source_absolute_path)?;
        let extension = PathMapper::extension(source_absolute_path)?;
        let path_infix = self.mapper.path_infix(source_absolute_path, &filename)?;

        let prefix_to_remove = format!("{}-{}/", model, number_of_colors);
        let path_infix_str = path_infix.to_string_lossy();
        let path_infix_str = path_infix_str
            .strip_prefix(&prefix_to_remove)
            .unwrap_or(&path_infix_str);
        let path_infix = PathBuf::from(path_infix_str);

        let output_folder = self
            .mapper
            .source_folder
            .root_path
            .join(&self.mapper.destination_folder.name_path)
            .join(&path_infix);
        fs::create_dir_all(&output_folder)?;
        let return_value = output_folder.join(format!("{}.{}", filename_no_extension, extension));
        Ok(return_value.to_string_lossy().to_string())
    }
}

#[derive(Clone)]
pub struct ToVectorToFinalPathMapper {
    mapper: PathMapper,
}

impl ToVectorToFinalPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let vector_folder = Folder::new(root_path, VECTOR_FOLDER_NAME);
        let final_folder = Folder::new(root_path, FINAL_FOLDER_NAME);
        let mapper = PathMapper::new(vector_folder, final_folder, ExtensionType::SVG);
        Self { mapper }
    }

    pub fn get(
        &self,
        source_absolute_path: &Path,
        model: &str,
        number_of_colors: &str,
    ) -> Result<String, Box<dyn Error>> {
        let filename = PathMapper::filename(source_absolute_path)?;
        let filename_no_extension = PathMapper::filename_no_extension(source_absolute_path)?;
        let path_infix = self.mapper.path_infix(source_absolute_path, &filename)?;

        let prefix_to_remove = format!("{}-{}/", model, number_of_colors);
        let path_infix_str = path_infix.to_string_lossy();
        let path_infix_str = path_infix_str
            .strip_prefix(&prefix_to_remove)
            .unwrap_or(&path_infix_str);
        let path_infix = PathBuf::from(path_infix_str);

        let output_folder = self
            .mapper
            .source_folder
            .root_path
            .join(&self.mapper.destination_folder.name_path)
            .join(&path_infix);
        fs::create_dir_all(&output_folder)?;
        let return_value = output_folder.join(format!(
            "{}.{}",
            filename_no_extension,
            self.mapper.destination_extension.as_str()
        ));
        Ok(return_value.to_string_lossy().to_string())
    }
}

#[derive(Clone)]
pub struct FinalVectorToFinalJpgPathMapper {
    mapper: PathMapper,
}

impl FinalVectorToFinalJpgPathMapper {
    pub fn new(root_path: &Path) -> Self {
        let final_folder = Folder::new(root_path, FINAL_FOLDER_NAME);
        let mapper = PathMapper::new(final_folder.clone(), final_folder, ExtensionType::JPG);
        Self { mapper }
    }

    pub fn get(&self, source_absolute_path: &Path) -> Result<String, Box<dyn Error>> {
        self.mapper.get(source_absolute_path)
    }
}
