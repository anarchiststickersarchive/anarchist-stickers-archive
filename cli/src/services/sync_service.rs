use std::borrow::Cow;
use std::path::Path;
use std::{env, process::Command};

#[derive(Clone)]
pub struct SyncService;
impl SyncService {
    pub fn run(&self) -> Result<Vec<String>, std::io::Error> {
        let remote_assets_path = env::var("REMOTE_ASSETS_PATH").unwrap();
        let remote_assets_user = env::var("REMOTE_USER").unwrap();
        let local_assets_parent_path = Self::get_local_assets_parent_path();
        let mut binding = Command::new("rsync");
        let rsync_sub_cmd: String = format!(
            "{}@{}:{}",
            remote_assets_user,
            &env::var("REMOTE_HOST").unwrap(),
            remote_assets_path
        );
        binding
            .arg("-atzv")
            .arg("--delete")
            .arg(rsync_sub_cmd)
            .arg(local_assets_parent_path)
            .output()
            .map(|output| {
                let stdout: Cow<str> = String::from_utf8_lossy(&output.stdout);
                stdout
                    .lines()
                    .filter(|line| {
                        (line.starts_with("assets") || line.starts_with("deleting"))
                            && !line.ends_with("/")
                    })
                    .map(|line| line.to_string())
                    .collect()
            })
    }

    fn get_local_assets_parent_path() -> String {
        Path::new(&env::var("LOCAL_ASSETS_PATH").unwrap())
            .parent()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string()
    }
}
