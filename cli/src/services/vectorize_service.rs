use crate::dtos::path_mapper_handler::PathMappings;
use crate::dtos::to_vector::ToVector;
use image::{DynamicImage, GenericImageView};
use reqwest::blocking::Client;
use reqwest::header::HeaderMap;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};

#[derive(Clone)]
pub struct VectorizeService {
    pub path_mappings: PathMappings,
    pub api_key: String,
    pub target_size: u32,
}

impl VectorizeService {
    pub fn run(&self, to_vector: ToVector) -> Result<(), Box<dyn Error>> {
        let image_output_absolute_path: String =
            self.path_mappings.to_vector_to_final_path_mapper.get(
                Path::new(&to_vector.absolute_path.clone()),
                to_vector.vector_type.as_str(),
                to_vector.number_of_colors.as_str(),
            )?;
        if Path::new(&image_output_absolute_path).exists() {
            self.move_to_trash(&to_vector)?;
        } else {
            let img: DynamicImage = image::open(&to_vector.absolute_path)?;
            let (width, height) = img.dimensions();
            let (desired_width, desired_height) = self.get_desired_dimensions(width, height);
            let binary_response = self.request_image_to_svg(
                to_vector.absolute_path.clone(),
                desired_width,
                desired_height,
                to_vector.vector_type.clone(),
                to_vector.number_of_colors.clone(),
            )?;
            self.save_binary(image_output_absolute_path.clone(), &binary_response)?;
            self.move_to_trash(&to_vector)?;
        }
        Ok(())
    }

    fn move_to_trash(&self, to_vector: &ToVector) -> Result<(), Box<dyn Error>> {
        let trash_destination_path: &String =
            &self.path_mappings.to_vector_to_trash_path_mapper.get(
                Path::new(&to_vector.absolute_path.clone()),
                to_vector.vector_type.as_str(),
                to_vector.number_of_colors.as_str(),
            )?;
        fs::rename(to_vector.absolute_path.clone(), trash_destination_path)?;
        Ok(())
    }

    fn save_binary(
        &self,
        image_output_absolute_path: String,
        binary_response: &[u8],
    ) -> std::io::Result<()> {
        let mut file = File::create(image_output_absolute_path)?;
        file.write_all(binary_response)?;
        Ok(())
    }

    fn request_image_to_svg(
        &self,
        image_input_absolute_path: PathBuf,
        desired_width: u32,
        desired_height: u32,
        model: String,
        colors: String,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let client = Client::new();
        let mut headers = HeaderMap::new();
        headers.insert(
            "X-CREDITS-CODE",
            reqwest::header::HeaderValue::from_str(&self.api_key)?,
        );

        let form = reqwest::blocking::multipart::Form::new()
            .text("format", "svg")
            .text("colors", colors.clone())
            .text("model", model.clone())
            .text("algorithm", "auto")
            .text("details", "auto")
            .text("antialiasing", "off")
            .text("minarea", "5")
            .text("colormergefactor", "5")
            .text("unit", "auto")
            .text("width", desired_width.to_string())
            .text("height", desired_height.to_string())
            .text("roundness", "default")
            .file("image", &image_input_absolute_path)?;

        let response = client
            .post("https://api.vectorizer.io/v4.0/vectorize")
            .headers(headers)
            .multipart(form)
            .send()?;

        if response.status().is_success() {
            Ok(response.bytes()?.to_vec())
        } else {
            eprintln!(
                "** Failed request. status_code={}, image_input_absolute_path={:?}, request.url={}",
                response.status(),
                image_input_absolute_path,
                response.url()
            );
            Err("Failed to vectorize image".into())
        }
    }

    fn get_desired_dimensions(&self, width: u32, height: u32) -> (u32, u32) {
        if width < height {
            (self.target_size, self.target_size * height / width)
        } else {
            (self.target_size * width / height, self.target_size)
        }
    }
}
