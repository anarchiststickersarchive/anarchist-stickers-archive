use crate::dtos::path_mapper_handler::PathMappings;
use resvg::usvg::{Options, Tree};
use std::{fs, path::Path};
use tiny_skia::{Pixmap, Transform};

#[derive(Clone)]
pub struct RasterizeService {
    pub path_mappings: PathMappings,
}
impl RasterizeService {
    pub fn run(&self, svg_absolute_path: &String) -> Result<String, std::io::Error> {
        self.path_mappings
            .final_vector_to_final_jpg_path_mapping(Path::new(svg_absolute_path))
            .and_then(|jpg_output_absolute_path| {
                let tree = self.load_svg(svg_absolute_path)?;
                let image = self.render_svg(&tree)?;
                self.save_image(&image, jpg_output_absolute_path.as_str())
            })
            .map(|_| svg_absolute_path.to_string())
    }

    fn load_svg(&self, file_path: &str) -> Result<Tree, std::io::Error> {
        let opt = Options::default();
        let svg_data = fs::read(file_path)?;
        let tree = Tree::from_data(&svg_data, &opt).unwrap();
        Ok(tree)
    }

    fn render_svg(&self, tree: &Tree) -> Result<Pixmap, std::io::Error> {
        let pixmap_size = tree.size().to_int_size();
        let mut pixmap: Pixmap = Pixmap::new(pixmap_size.width(), pixmap_size.height()).unwrap();
        let render_ts = Transform::default();
        resvg::render(&tree, render_ts, &mut pixmap.as_mut());
        Ok(pixmap)
    }

    fn save_image(&self, image: &Pixmap, output_path: &str) -> Result<(), std::io::Error> {
        Ok(image.save_png(output_path)?)
    }
}
