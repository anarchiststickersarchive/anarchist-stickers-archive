use crate::dtos::path_mapper_handler::PathMappings;
use crate::dtos::to_vector::ToVector;
use crate::utils::{extensions, files_utils};
use std::io::Error;
use std::path::Path;
use std::path::PathBuf;

#[derive(Clone)]
pub struct FilesService {
    pub path_mappings: PathMappings,
}

impl FilesService {
    pub fn get_workbenched(&self) -> Result<Vec<PathBuf>, Error> {
        files_utils::get_files_in_path_recursive(
            &self
                .path_mappings
                .workbench_path
                .to_string_lossy()
                .to_string()
                .as_str(),
            true,
            Some(&|path: &Path| {
                path.extension()
                    .and_then(|ext| ext.to_str())
                    .map_or(false, |ext| extensions::image_types().contains(&ext))
            }),
        )
    }

    pub fn get_upscaled(&self) -> Result<Vec<PathBuf>, std::io::Error> {
        files_utils::get_files_in_path_recursive(
            &self
                .path_mappings
                .upscaled_path
                .to_string_lossy()
                .to_string()
                .as_str(),
            true,
            None,
        )
    }

    pub fn get_to_vector(&self) -> Result<Vec<ToVector>, std::io::Error> {
        files_utils::get_files_in_path_recursive(
            self.path_mappings
                .vector_path
                .to_string_lossy()
                .to_string()
                .as_str(),
            false,
            None,
        )
        .map(|paths| {
            paths
                .into_iter()
                .flat_map(|path| {
                    let (vector_type, number_of_colors) =
                        self.get_vector_type_and_number_of_colors(path.clone())?;
                    Ok::<ToVector, std::io::Error>(ToVector {
                        absolute_path: path,
                        vector_type,
                        number_of_colors,
                    })
                })
                .collect()
        })
    }

    pub fn get_svgs_not_rasterize(&self) -> Result<Vec<String>, std::io::Error> {
        files_utils::get_files_in_path_recursive(
            self.path_mappings
                .final_path
                .to_string_lossy()
                .to_string()
                .as_str(),
            false,
            Some(&|path: &Path| {
                path.extension()
                    .and_then(|ext| ext.to_str())
                    .map_or(false, |ext| ext.eq_ignore_ascii_case("svg"))
            }),
        )
        .map(|paths| {
            paths
                .iter()
                .filter(|path: &&PathBuf| {
                    let output_path_str: String = self
                        .path_mappings
                        .final_vector_to_final_jpg_path_mapping(path)
                        .unwrap();
                    let output_path: &Path = Path::new(&output_path_str);
                    let exists = output_path.exists();
                    !exists
                })
                .flat_map(|path| path.clone().into_os_string().into_string())
                .collect()
        })
    }

    fn get_vector_type_and_number_of_colors(
        &self,
        path: PathBuf,
    ) -> Result<(String, String), Error> {
        let vector_path: PathBuf = self.path_mappings.vector_path.clone();

        let relative_path: &Path = path.strip_prefix(&vector_path).unwrap();
        let dirname: &Path = self.get_dirname(relative_path);

        let binding = dirname.to_string_lossy();
        let parts: Vec<&str> = binding.split('-').collect();
        let vector_type = parts.get(0).unwrap().to_string();
        let number_of_colors = parts.get(1).unwrap().to_string();

        Ok((vector_type, number_of_colors))
    }

    fn get_dirname<'a>(&self, relative_path: &'a Path) -> &'a Path {
        let predicate: bool = relative_path
            .parent()
            .is_some_and(|parent| parent.ends_with("nn"));
        if predicate {
            relative_path.parent().unwrap().parent().unwrap()
        } else {
            relative_path
                .parent()
                .unwrap()
                .parent()
                .unwrap()
                .parent()
                .unwrap()
        }
    }
}
