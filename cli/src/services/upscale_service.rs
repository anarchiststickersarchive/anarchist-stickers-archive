use crate::dtos::path_mapper_handler::PathMappings;
use image;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io::BufWriter;
use std::num::NonZeroU32;
use std::path::{Path, PathBuf};

use image::{DynamicImage, ExtendedColorType, ImageEncoder, ImageReader, RgbImage};

use crate::dtos::constants::TARGET_SIZE;
use fast_image_resize::images::Image;
use fast_image_resize::{PixelType, Resizer};
use image::codecs::jpeg::JpegEncoder;

#[derive(Clone)]
pub struct UpscaleService {
    pub path_mappings: PathMappings,
}

impl UpscaleService {
    pub fn run(&self, input_path: &PathBuf) -> Result<(), Box<dyn Error>> {
        let output_path_str: &String = &self
            .path_mappings
            .workbench_to_upscaled_path_mapping(input_path)?;
        let output_path: &PathBuf = &Path::new(output_path_str).to_path_buf();
        self.run_inner(input_path, output_path)?;
        self.move_to_trash(input_path)
    }

    fn run_inner(&self, input_path: &PathBuf, output_path: &PathBuf) -> Result<(), Box<dyn Error>> {
        // Read source image from file
        let src_image: DynamicImage = ImageReader::open(input_path).unwrap().decode().unwrap();

        // Convert DynamicImage to a buffer
        let mut src_image_buffer: RgbImage = src_image.to_rgb8();
        let src_width = src_image.width() as usize;
        let src_height = src_image.height() as usize;

        // Prepare source image for resizing
        let src_image_view: Image = Image::from_slice_u8(
            NonZeroU32::new(src_width as u32).unwrap().get(),
            NonZeroU32::new(src_height as u32).unwrap().get(),
            src_image_buffer.as_flat_samples_mut().samples,
            PixelType::U8x3,
        )
        .unwrap();

        // Create container for data of destination image
        let height: u32 = src_image.height();
        let width: u32 = src_image.width();
        let target_size: u32 = TARGET_SIZE;
        let (desired_width, desired_height) = {
            if width < height {
                (target_size, target_size * height / width)
            } else {
                (target_size * width / height, target_size)
            }
        };

        let mut dst_image: Image = Image::new(desired_width, desired_height, PixelType::U8x3);

        // Create Resizer instance and resize source image
        // into buffer of destination image
        let mut resizer = Resizer::new();
        resizer
            .resize(&src_image_view, &mut dst_image, None)
            .unwrap();

        // Write destination image as PNG-file
        let file = File::create(output_path).unwrap();
        let mut result_buf = BufWriter::new(file);
        JpegEncoder::new(&mut result_buf)
            .write_image(
                dst_image.buffer(),
                desired_width,
                desired_height,
                ExtendedColorType::Rgb8,
                // src_image_view.color().into(),
            )
            .unwrap();
        Ok(())
    }

    fn move_to_trash(&self, workbench_image: &PathBuf) -> Result<(), Box<dyn Error>> {
        let trash_destination_path: &String = &self
            .path_mappings
            .workbench_to_trash_path_mapper
            .get(workbench_image)?;
        fs::rename(workbench_image, trash_destination_path)?;
        Ok(())
    }
}
