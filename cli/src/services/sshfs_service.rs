use std::{env, fs, process::Command};

pub struct SshfsService {
    pub local_mount_path: String,
    remote_mount_path: String,
    remote_user: String,
    remote_host: String,
}
impl SshfsService {
    pub fn new() -> Self {
        Self {
            local_mount_path: Self::get_local_mount_path(),
            remote_mount_path: env::var("REMOTE_ASSETS_PATH").unwrap(),
            remote_user: env::var("REMOTE_USER").unwrap(),
            remote_host: env::var("REMOTE_HOST").unwrap(),
        }
    }

    pub fn mount(&self) -> Result<(), std::io::Error> {
        self.umount_and_forget()
            .and_then(|_| self.make_dir(self.local_mount_path.clone()))
            .and_then(|_| self.exec_sshfs())
            .inspect(|_| self.check_mount().unwrap())
            .inspect_err(|e| println!("Rasterize mount error: {:?}", e))
    }

    pub fn umount(&self) -> Result<(), std::io::Error> {
        Command::new("umount")
            .args([self.local_mount_path.as_str()])
            .output()
            .map(|_| ())
    }

    fn umount_and_forget(&self) -> Result<(), std::io::Error> {
        let _ = self.umount();
        Ok(())
    }

    fn get_local_mount_path() -> String {
        "/tmp/anarchist-stickers-archive/sshfs-mount".to_string()
    }

    fn make_dir(&self, mount_path: String) -> Result<(), std::io::Error> {
        Command::new("mkdir")
            .args(["-p", mount_path.as_str()])
            .output()
            .map(|_| ())
    }

    fn exec_sshfs(&self) -> Result<(), std::io::Error> {
        let host: String = format!(
            "{}@{}:{}",
            self.remote_user, self.remote_host, self.remote_mount_path
        );
        Command::new("sshfs")
            .args([host, self.local_mount_path.clone()])
            .output()
            .map(|_| ())
    }

    fn check_mount(&self) -> Result<(), std::io::Error> {
        let files_in_path = fs::read_dir(self.local_mount_path.clone())?;
        let number_of_files = files_in_path.count();
        if number_of_files == 0 {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("No files found in {:?}", self.local_mount_path),
            ))
        } else {
            Ok(())
        }
    }
}
