use std::borrow::Cow;
use std::io::{Error, ErrorKind};
use std::path::Path;
use std::process::Command;

#[derive(Clone)]
pub struct ScrapeInstagramService {
    pub accounts: Vec<String>,
    pub project_path: String,
}
impl ScrapeInstagramService {

    pub fn run(&self, account_name: &String) -> Result<Vec<String>, Error> {
        let nix_file = self.absolute_path("resources/nix/python_env.nix");

        let command: &String = &format!("\
            instaloader {} \
                --no-videos \
                --no-captions \
                --no-metadata-json \
                --no-compress-json \
                --latest-stamps latest-stamps.ini \
                --abort-on 401 \
                --login anarchiststickersarchive \
                --sessionfile ~/.config/instaloader/session-anarchiststickersarchive \
            ", account_name
        );

        Command::new("nix-shell")
            .current_dir(self.project_path.clone())
            .args(&[
                &nix_file,
                "--command",
                command,
            ])
            .output()
            .map_err(|e| Error::new(
                ErrorKind::Other,
                format!("Failed to execute process: {}", e),
            ))
            .map(|output| {
                let stdout: Cow<str> = String::from_utf8_lossy(&output.stdout);
                let output_as_lines: Vec<String> = stdout
                    .lines()
                    .map(|line| line.to_string())
                    .collect();
                (output, output_as_lines)
            })
            .and_then(|output_pair| {
                if output_pair.0.status.success() {
                    Ok(output_pair.1)
                } else {
                    let message = format!("Failed to execute process: {}", String::from_utf8_lossy(&output_pair.0.stderr.clone()));
                    let error: Error = Error::new(ErrorKind::Other, message);
                    Err(error)
                }
            })
    }

    pub fn get_accounts(&self) -> Result<Vec<String>, Error> {
        Ok(self.accounts.clone())
    }

    fn absolute_path(&self,relative: &str) -> String {
        Path::new(env!("CARGO_MANIFEST_DIR"))
            .join(relative)
            .to_str()
            .expect("Path is not valid UTF-8")
            .to_owned()
    }
}
