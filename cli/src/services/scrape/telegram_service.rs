use std::borrow::Cow;
use std::env;
use std::io::{Error, ErrorKind};
use std::path::Path;
use std::process::Command;

#[derive(Clone)]
pub struct ScrapeTelegramService {
    pub telegram_api_id: String,
    pub telegram_api_hash: String,
    pub telegram_chat: String,
}

impl ScrapeTelegramService {
    pub fn run(&self) -> Result<Vec<String>, Error> {
        let nix_file = self.absolute_path("resources/nix/python_env.nix");
        let python_script = self.absolute_path("resources/telegram-scrapper/main.py");

        Command::new("nix-shell")
            .args(&[
                &nix_file,
                "--command",
                &format!("python {}", python_script),
            ])
            .output()
            .map_err(|e| Error::new(
                ErrorKind::Other,
                format!("Failed to execute process: {}", e),
            ))
            .map(|output| {
                let stdout: Cow<str> = String::from_utf8_lossy(&output.stdout);
                let output_as_lines: Vec<String> = stdout
                    .lines()
                    .map(|line| line.to_string())
                    .collect();
                (output, output_as_lines)
            })
            .and_then(|output_pair| {
                if output_pair.0.status.success() {
                    Ok(output_pair.1)
                } else {
                    let message = format!("Failed to execute process: {}", String::from_utf8_lossy(&output_pair.0.stderr.clone()));
                    let error: Error = Error::new(ErrorKind::Other, message);
                    Err(error)
                }
            })
    }

    fn absolute_path(&self,relative: &str) -> String {
        Path::new(env!("CARGO_MANIFEST_DIR"))
            .join(relative)
            .to_str()
            .expect("Path is not valid UTF-8")
            .to_owned()
    }
}
