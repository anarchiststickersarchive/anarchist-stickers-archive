use crate::commands::stats_handler::CommandHandlerTrait;
use crate::services::categorize_service::CategorizeService;
use crate::services::files_service::FilesService;

use crate::dtos::path_mapper_handler::PathMappings;
use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use crate::utils::progressive_terminal;
use cliclack::{input, select};
use image::image_dimensions;
use std::io::Error;
use std::path::{Path, PathBuf};
use std::process::{exit, Command};
use std::{fmt, fs};

#[derive(Debug, PartialEq, Clone, Eq)]
enum Action {
    Vectorize,
    Final,
    Remove,
    Skip,
    Problem,
    Quit,
}

#[derive(Debug, PartialEq, Clone, Eq)]
enum VectorizeAction {
    Clipart,
    Photo,
    Drawing,
}
impl fmt::Display for VectorizeAction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            VectorizeAction::Clipart => write!(f, "clipart"),
            VectorizeAction::Photo => write!(f, "photo"),
            VectorizeAction::Drawing => write!(f, "drawing"),
        }
    }
}

pub struct CategorizeCommandHandler {
    pub categorize_service: CategorizeService,
    pub files_service: FilesService,
    pub path_mappings: PathMappings,
}

impl CommandHandlerTrait for CategorizeCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let _ = self
            .get_upscaled_images()
            .inspect(|upscaled_paths: &Vec<PathBuf>| {
                println!();
                for upscaled_path in upscaled_paths.clone() {
                    let _ = self.categorize_one(&upscaled_path);
                }
            });
    }
}

impl CategorizeCommandHandler {
    fn categorize_one(&self, image_path: &PathBuf) -> Result<(), Error> {
        println!();
        let _ = &self.open_image_with_default_app(&image_path);

        let action: Action = match select(self.get_initial_message(image_path))
            .item(Action::Vectorize, "Vectorize", "Add an SVG")
            .item(Action::Final, "Final", "As-is")
            .item(Action::Remove, "Remove", "Discard")
            .item(Action::Skip, "Skip", "Skip for now")
            .item(Action::Problem, "Problem", "All black?")
            .item(Action::Quit, "Quit", "Bye bye")
            .interact()?
        {
            Action::Vectorize => {
                let model: VectorizeAction = select("Vector type?")
                    .item(
                        VectorizeAction::Clipart,
                        "Clipart - Few colors",
                        "Clipart - Few colors",
                    )
                    .item(
                        VectorizeAction::Photo,
                        "Photo - Many colors",
                        "Photo - Many colors",
                    )
                    .item(
                        VectorizeAction::Drawing,
                        "Drawing - Black&White",
                        "Drawing - Black&White",
                    )
                    .interact()?;

                let colors: String = input("Colors (2-32 or auto):")
                    .default_input("auto")
                    .interact()?;

                let output_path: &String = &self
                    .path_mappings
                    .upscaled_to_to_vector_path_mapping(
                        &image_path,
                        model.to_string().as_str(),
                        &colors,
                    )
                    .unwrap();
                let _ = &self.move_to_destination(&image_path, &output_path)?;

                Action::Vectorize
            }
            Action::Final => {
                let output_path: &String = &self
                    .path_mappings
                    .upscaled_to_final_path_mapping(&image_path)
                    .unwrap();
                let _ = &self.move_to_destination(&image_path, &output_path)?;
                Action::Final
            }
            Action::Remove => {
                let _ = &self.move_to_trash(&image_path)?;
                Action::Remove
            }
            Action::Problem => {
                let _ = &self.handle_problem_image(&image_path)?;
                Action::Problem
            }
            Action::Quit => {
                exit(0);
            }
            action => action,
        };
        let _ = &self.cleanup_workbench(&image_path)?;
        if action == Action::Quit {
            exit(0);
        }
        Ok(())
    }

    fn get_initial_message(&self, image_path: &PathBuf) -> String {
        format!(
            "What to do with '{}/{}/{}' ({}x{})?",
            image_path
                .parent()
                .unwrap()
                .parent()
                .unwrap()
                .file_name()
                .unwrap()
                .to_string_lossy()
                .to_string(),
            image_path
                .parent()
                .unwrap()
                .file_name()
                .unwrap()
                .to_string_lossy()
                .to_string(),
            image_path
                .file_name()
                .unwrap()
                .to_string_lossy()
                .to_string(),
            image_dimensions(image_path).unwrap().0,
            image_dimensions(image_path).unwrap().1
        )
    }

    fn move_to_destination(
        &self,
        image_path: &&PathBuf,
        output_path: &&String,
    ) -> Result<(), Error> {
        println!("Moving {} to {}", image_path.to_str().unwrap(), output_path);
        fs::rename(&image_path, &output_path)
    }

    fn open_image_with_default_app(&self, path: &Path) {
        Command::new("open")
            .arg(path)
            .spawn()
            .expect("Failed to open image");
    }

    fn cleanup_workbench(&self, path: &Path) -> Result<(), Error> {
        let workbench_output_path_as_str = self
            .path_mappings
            .upscaled_to_workbench_path_mapping(&path)
            .unwrap();
        let workbench_output_path: &Path = Path::new(&workbench_output_path_as_str);
        if workbench_output_path.exists() {
            let trash_output_path: &String = &self
                .path_mappings
                .workbench_to_trash_path_mapping(&workbench_output_path)
                .unwrap();
            println!("Moved {} to trash", workbench_output_path_as_str);
            fs::rename(&workbench_output_path, &trash_output_path)
        } else {
            Ok(())
        }
    }

    fn move_to_trash(&self, image_path: &Path) -> Result<(), Error> {
        let output_path: &String = &self
            .path_mappings
            .upscaled_to_trash_path_mapping(&image_path)
            .unwrap();
        println!("Moved {} to trash", image_path.to_str().unwrap());
        fs::rename(&image_path, &output_path)
    }

    fn get_upscaled_images(&self) -> Result<Vec<PathBuf>, Error> {
        let config = ProgressiveTerminalConfig::<Vec<PathBuf>> {
            initial_message: "Fetching upscaled images".to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|svgs: &Vec<PathBuf>| {
                format!("Fetched {:?} images pending categorization", svgs.len()).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not fetch images to categorize".to_string(),
        };

        let files_service_clone = self.files_service.clone();
        let function = move || files_service_clone.get_upscaled();
        progressive_terminal::run(function, &config)
    }

    fn handle_problem_image(&self, _path: &Path) -> Result<(), Error> {
        unimplemented!()
    }
}
