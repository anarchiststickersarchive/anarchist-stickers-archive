use crate::dtos::scrape::ScrapeSource;
use clap::Arg;
use clap::Command;

pub fn env_file_value() -> String {
    application()
        .get_matches()
        .get_one::<String>("env-file")
        .unwrap()
        .to_string()
}

pub fn application() -> Command {
    Command::new("asa")
        .about("Anarchist Stickers Archive (ASA) cli utility.")
        .arg(env_file_arg())
        .subcommand(categorize_command_handler())
        .subcommand(rasterize_command_handler())
        .subcommand(stats_command_handler())
        .subcommand(sync_command_handler())
        .subcommand(vectorize_command_handler())
        .subcommand(upscale_command_handler())
        .subcommand(scrape_command_handler())
}

fn upscale_command_handler() -> Command {
    Command::new("upscale").about("Upscale the image")
}

fn categorize_command_handler() -> Command {
    Command::new("categorize").about("Place a sticker candidate into the correct category/folder.")
}

fn sync_command_handler() -> Command {
    Command::new("sync").about("Sync the remote original to a local storage")
}

fn rasterize_command_handler() -> Command {
    Command::new("rasterize")
        .about("Any SVG vector file in the final folder gets rasterized into a JPG.")
}

fn stats_command_handler() -> Command {
    Command::new("stats")
        .arg(sync_flag_arg())
        .about("Shows statistics about the archive.")
}

fn vectorize_command_handler() -> Command {
    Command::new("vectorize")
        .arg(sync_flag_arg())
        .about("Any JPG image in the 2-vector folder gets vectorized into a SVG via Paddle.")
}

fn scrape_command_handler() -> Command {
    Command::new("scrape")
        .arg(scrape_source_arg())
        .about("Scrape images from different sources. Scrapes every source by default unless one is specified.")
}

fn scrape_source_arg() -> Arg {
    Arg::new("source")
        .long("source")
        .short('s')
        .value_parser(clap::value_parser!(ScrapeSource))
        .required(false)
        .help("Sets the source used whilst scrapping. Valid values: 'instagram' or 'telegram'")
}

fn env_file_arg() -> Arg {
    Arg::new("env-file")
        .long("env-file")
        .short('e')
        .help("Sets a custom environment file")
        .default_value(".env")
}

fn sync_flag_arg() -> Arg {
    Arg::new("sync")
        .long("sync")
        .action(clap::ArgAction::SetTrue)
        .help("When set, synchronizes the local copy before running the stats.")
}
