use crate::commands::stats_handler::CommandHandlerTrait;
use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use crate::services::sync_service::SyncService;
use crate::utils::progressive_terminal;
use colored::Colorize;
use std::io::Error;

pub struct AssetPath {
    pub short_name: String,
    pub full_path: String,
}

pub struct SyncCommandHandler {
    pub sync_service: SyncService,
}

impl CommandHandlerTrait for SyncCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let config = ProgressiveTerminalConfig::<Vec<String>> {
            initial_message: "Syncing".to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|svgs: &Vec<String>| {
                format!("Synced {:?} images", svgs.len()).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not sync images".to_string(),
        };

        let sync_service_clone = self.sync_service.clone();
        let function = move || sync_service_clone.run();
        let synced_images: Result<Vec<String>, Error> =
            progressive_terminal::run(function, &config);
        println!();
        let _ = synced_images.inspect(|synced_images| {
            for synced_image in synced_images {
                let check_mark = "\u{2713}";
                println!("{} {}", check_mark.green(), synced_image.to_string());
            }
        });
    }
}
