use crate::commands::stats_handler::CommandHandlerTrait;
use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use crate::dtos::to_vector::ToVector;
use crate::services::files_service::FilesService;
use crate::services::vectorize_service::VectorizeService;
use crate::utils::progressive_terminal;
use std::io::{Error, ErrorKind};

pub struct VectorizeCommandHandler {
    pub files_service: FilesService,
    pub vectorize_service: VectorizeService,
}

impl CommandHandlerTrait for VectorizeCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let _ = self
            .get_to_vector_files()
            .inspect(|to_vectors: &Vec<ToVector>| {
                println!();
                for to_vector in to_vectors.clone() {
                    self.vectorize_one(to_vector);
                }
            });
    }
}

impl VectorizeCommandHandler {
    fn vectorize_one(&self, to_vector: ToVector) {
        let config: ProgressiveTerminalConfig<ToVector> = Self::get_vector_config(&to_vector);
        let vectorize_service_clone = self.vectorize_service.clone();
        let function = {
            let to_vector_clone: ToVector = to_vector.clone();
            move || {
                vectorize_service_clone
                    .run(to_vector.clone())
                    .map_err(|e| Error::new(ErrorKind::Other, format!("{}", e)))?;
                Ok(to_vector_clone.clone())
            }
        };
        let _: Result<ToVector, Error> = progressive_terminal::run(function, &config);
    }

    fn get_vector_config(to_vector: &ToVector) -> ProgressiveTerminalConfig<ToVector> {
        ProgressiveTerminalConfig::<ToVector> {
            initial_message: format!(
                "Vectorizing {}",
                to_vector.absolute_path.clone().to_string_lossy()
            ),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|to_vector: &ToVector| {
                format!(
                    "Vectorized {}",
                    to_vector.absolute_path.clone().to_string_lossy()
                )
                .to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: to_vector
                .absolute_path
                .clone()
                .to_string_lossy()
                .to_string(),
        }
    }

    fn get_to_vector_files(&self) -> Result<Vec<ToVector>, Error> {
        let config = ProgressiveTerminalConfig::<Vec<ToVector>> {
            initial_message: "Fetching files to vectorize...".to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|to_vectors: &Vec<ToVector>| {
                format!(
                    "Fetched {:?} images pending vectorization",
                    to_vectors.len()
                )
                .to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not fetch files to vectorize".to_string(),
        };

        let files_service_clone = self.files_service.clone();
        let fetch_vectorize_files_fn = move || files_service_clone.get_to_vector();
        progressive_terminal::run(fetch_vectorize_files_fn, &config)
    }
}
