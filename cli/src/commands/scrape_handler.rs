use crate::commands::stats_handler::CommandHandlerTrait;
use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use crate::dtos::scrape::ScrapeSource;
use crate::services::scrape::instagram_service::ScrapeInstagramService;
use crate::services::scrape::telegram_service::ScrapeTelegramService;
use crate::utils::progressive_terminal;
use colored::Colorize;
use std::io::{Error, ErrorKind};

#[derive(Clone)]
pub struct ScrapeInstagramCommandHandler {
    pub service: ScrapeInstagramService,
}

impl CommandHandlerTrait for ScrapeInstagramCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let _ = self
            .service
            .get_accounts()
            .unwrap()
            .iter()
            .fold(Ok("".to_string()), |acc, account| {
                acc.and_then(|_| self.run_one(account.clone()))
            });
    }
}

impl ScrapeInstagramCommandHandler {
    fn run_one(&self, account_name: String) -> Result<String, Error> {
        let config = ProgressiveTerminalConfig::<String> {
            initial_message: format!("Scraping {}", account_name),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|account_name: &String| {
                format!("Scrapped {:?}", account_name)
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not scrape account".to_string(),
        };

        let service_clone = self.service.clone();
        let function = {
            let account_name_clone = account_name.clone();
            move || {
                service_clone
                    .run(&account_name.clone())
                    .map_err(|e| Error::new(ErrorKind::Other, format!("{}", e)))?;
                Ok(account_name_clone.clone())
            }
        };
        progressive_terminal::run(function, &config)
    }
}

#[derive(Clone)]
pub struct ScrapeTelegramCommandHandler {
    pub service: ScrapeTelegramService,
}

impl CommandHandlerTrait for ScrapeTelegramCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let config = ProgressiveTerminalConfig::<Vec<String>> {
            initial_message: "Scrapping telegram".to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|svgs: &Vec<String>| {
                format!("Scrapped {:?} images", svgs.len()).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not scrape images from telegram".to_string(),
        };

        let sync_service_clone = self.service.clone();
        let function = move || sync_service_clone.run();
        let scrapped_images: Result<Vec<String>, Error> =
            progressive_terminal::run(function, &config);
        println!();
        let _ = scrapped_images.inspect(|synced_images| {
            for synced_image in synced_images {
                let check_mark = "\u{2713}";
                println!("{} {}", check_mark.green(), synced_image.to_string());
            }
        });
    }
}

pub struct ScrapeCommandHandler {
    pub instagram_command_handler: ScrapeInstagramCommandHandler,
    pub telegram_command_handler: ScrapeTelegramCommandHandler,
}

impl CommandHandlerTrait for ScrapeCommandHandler {
    fn run(&self, matches: &clap::ArgMatches) {
        let maybe_source: Option<&ScrapeSource> = matches.get_one::<ScrapeSource>("source");
        match maybe_source {
            None => {
                self.telegram_command_handler.run(&matches.clone());
                self.instagram_command_handler.run(&matches.clone());
            }
            Some(ScrapeSource::Telegram) => self.telegram_command_handler.run(matches),
            Some(ScrapeSource::Instagram) => self.instagram_command_handler.run(matches),
        }
    }
}
