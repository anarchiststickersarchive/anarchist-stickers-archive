use std::{env, format, path::Path, println};

use crate::services::sync_service::SyncService;
use crate::utils::extensions;
use crate::utils::files_utils::get_files_in_path_recursive;
use capitalize::Capitalize;
use colored::Colorize;

pub struct AssetPath {
    pub short_name: String,
    pub full_path: String,
}

pub trait CommandHandlerTrait {
    fn run(&self, matches: &clap::ArgMatches);
}
pub struct StatsCommandHandler {
    pub sync_service: SyncService,
}

impl CommandHandlerTrait for StatsCommandHandler {
    fn run(&self, matches: &clap::ArgMatches) {
        let is_sync: bool = matches.contains_id("sync") && matches.get_flag("sync");
        if is_sync {
            let _ = &self.sync_service.run();
        }

        let root_path = env::var("LOCAL_ASSETS_PATH").unwrap_or_else(|_| ".".to_string());
        let filter_predicate: Option<&dyn Fn(&Path) -> bool> = Some(&|file_path: &Path| {
            file_path
                .extension()
                .and_then(|ext| ext.to_str())
                .map(|ext_str| {
                    let contained = extensions::image_types().iter().any(|e| ext_str.eq_ignore_ascii_case(e));
                    contained
                })
                .unwrap_or(false)
        });

        println!("Summary of assets:");
        for asset_directory_name in [
            "workbench",
            "upscaled",
            "2-vector",
            "error",
            "final",
            "trash",
        ] {
            let asset_path = AssetPath {
                short_name: asset_directory_name.to_string(),
                full_path: format!("{}/ingestion/{}", root_path, asset_directory_name),
            };

            let images_at_path: Result<Vec<std::path::PathBuf>, std::io::Error> =
                get_files_in_path_recursive(&asset_path.full_path, false, filter_predicate);

            println!(
                "+ {}: {}",
                asset_path.short_name.capitalize().blue(),
                images_at_path.unwrap().len()
            );
        }
        let archive_asset_path = AssetPath {
            short_name: "archive".to_string(),
            full_path: format!("{}/archive", root_path),
        };
        let images_at_path =
            get_files_in_path_recursive(&archive_asset_path.full_path, false, filter_predicate);

        println!(
            "+ {}: {}",
            archive_asset_path.short_name.capitalize().blue(),
            images_at_path.unwrap().len()
        );
    }
}
