pub mod categorize_handler;
pub mod rasterize_handler;
pub mod scrape_handler;
pub mod spec;
pub mod stats_handler;
pub mod sync_handler;
pub mod upscale_handler;
pub mod vectorize_handler;
