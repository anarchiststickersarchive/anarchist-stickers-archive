use crate::commands::stats_handler::CommandHandlerTrait;
use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use crate::services::files_service::FilesService;
use crate::services::upscale_service::UpscaleService;
use crate::utils::progressive_terminal;
use std::io::{Error, ErrorKind};
use std::path::PathBuf;

pub struct UpscaleCommandHandler {
    pub files_service: FilesService,
    pub upscale_service: UpscaleService,
}

impl CommandHandlerTrait for UpscaleCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let _ = self
            .get_to_upscale_files()
            .inspect(|to_upscale_list: &Vec<PathBuf>| {
                println!();
                for to_upscale_one in to_upscale_list.clone() {
                    self.upscale_one(to_upscale_one);
                }
            });
    }
}

impl UpscaleCommandHandler {
    fn upscale_one(&self, to_upscale: PathBuf) {
        let config: ProgressiveTerminalConfig<PathBuf> =
            self.get_upscale_one_config(to_upscale.clone());
        let upscale_service_clone = self.upscale_service.clone();
        let function = {
            let to_upscale_clone: PathBuf = to_upscale.clone();
            move || {
                upscale_service_clone
                    .run(&to_upscale.clone())
                    .map_err(|e| Error::new(ErrorKind::Other, format!("{}", e)))?;
                Ok(to_upscale_clone.clone())
            }
        };
        let _: Result<PathBuf, Error> = progressive_terminal::run(function, &config);
    }

    fn get_upscale_one_config(&self, to_upscale: PathBuf) -> ProgressiveTerminalConfig<PathBuf> {
        let to_upscale_string = to_upscale.to_string_lossy().to_string();

        ProgressiveTerminalConfig::<PathBuf> {
            initial_message: format!("Upscaling {}", to_upscale_string.clone()).to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(move |to_upscale: &PathBuf| {
                format!("Upscaling {}", to_upscale.to_string_lossy().to_string()).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: to_upscale_string,
        }
    }

    fn get_to_upscale_files(&self) -> Result<Vec<PathBuf>, Error> {
        let config = ProgressiveTerminalConfig::<Vec<PathBuf>> {
            initial_message: "Fetching files to upscale...".to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|to_vectors: &Vec<PathBuf>| {
                format!("Fetched {:?} images pending upscaling", to_vectors.len()).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not fetch files to upscale".to_string(),
        };

        let files_service_clone = self.files_service.clone();
        let fetch_vectorize_files_fn = move || files_service_clone.get_workbenched();
        progressive_terminal::run(fetch_vectorize_files_fn, &config)
    }
}
