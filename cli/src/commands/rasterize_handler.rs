use crate::commands::stats_handler::CommandHandlerTrait;
use crate::dtos::progressive_terminal_config::ProgressiveTerminalConfig;
use crate::services::files_service::FilesService;
use crate::services::rasterize_service::RasterizeService;
use crate::utils::progressive_terminal;
use std::io::Error;

pub struct RasterizeCommandHandler {
    pub rasterize_service: RasterizeService,
    pub files_service: FilesService,
}

impl CommandHandlerTrait for RasterizeCommandHandler {
    fn run(&self, _matches: &clap::ArgMatches) {
        let _ = self
            .get_svgs_not_rasterize()
            .inspect(|svg_paths: &Vec<String>| {
                println!();
                for svg_path in svg_paths.clone() {
                    self.rasterize_one(svg_path);
                }
            });
    }
}
impl RasterizeCommandHandler {
    fn get_svgs_not_rasterize(&self) -> Result<Vec<String>, Error> {
        let config = ProgressiveTerminalConfig::<Vec<String>> {
            initial_message: "Fetching SVGs".to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|svgs: &Vec<String>| {
                format!("Fetched {:?} SVGs pending rasterization", svgs.len()).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: "Could not fetch SVGs to rasterize".to_string(),
        };

        let files_service_clone = self.files_service.clone();
        let function = move || files_service_clone.get_svgs_not_rasterize();
        progressive_terminal::run(function, &config)
    }

    fn rasterize_one(&self, svg_path: String) {
        let config = ProgressiveTerminalConfig::<String> {
            initial_message: format!("Rasterizing {}", svg_path.to_string()).to_string(),
            ok_emoji: Some("\u{2713}".to_string()),
            ok_message_supplier: Some(Box::new(|path: &String| {
                format!("Rasterized {}", path).to_string()
            })),
            error_emoji: Some("x".to_string()),
            error_message_supplier: format!("Could not rasterize {}", svg_path),
        };

        let rasterize_service_clone = self.rasterize_service.clone();
        let function = move || rasterize_service_clone.run(&svg_path.to_string());
        let _ = progressive_terminal::run(function, &config);
    }
}
