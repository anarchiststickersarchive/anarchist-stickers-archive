use boot::di;
use commands::stats_handler::CommandHandlerTrait;

mod boot;
mod commands;
mod dtos;
mod services;
mod utils;

fn main() {
    init_environment();
    match commands::spec::application().get_matches().subcommand() {
        Some(("categorize", matches)) => di::categorize_command_handler().unwrap().run(matches),
        Some(("stats", matches)) => di::stats_command_handler().unwrap().run(matches),
        Some(("scrape", matches)) => di::command_handlers::scrape().unwrap().run(matches),
        Some(("sync", matches)) => di::sync_command_handler().unwrap().run(matches),
        Some(("rasterize", matches)) => di::rasterize_command_handler().unwrap().run(matches),
        Some(("upscale", matches)) => di::upscale_command_handler().unwrap().run(matches),
        Some(("vectorize", matches)) => di::vectorize_command_handler().unwrap().run(matches),
        _ => println!("No known subcommand provided"),
    }
}

fn init_environment() {
    let env_file: String = commands::spec::env_file_value();
    dotenvy::from_filename(env_file).ok();
}
