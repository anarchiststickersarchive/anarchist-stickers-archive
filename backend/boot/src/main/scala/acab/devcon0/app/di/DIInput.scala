package acab.devcon0.app.di

import acab.devcon0.domain.dtos.{PublicationPlatforms, TelegramUserId}
import acab.devcon0.input.http.{BookmarkStickersRoute, Routes, RssRoute, StickersRoute}
import acab.devcon0.input.scheduler.SchedulerWrapper
import acab.devcon0.input.telegrambot._
import akka.actor.{ActorSystem, Scheduler}

object DIInput {
  import DIConfiguration._
  import DIDomain._
  import DITelegramBase._

  // Input > Scheduler
  private val schedulerActorSystem: ActorSystem = ActorSystem("akka-scheduler-system")
  val scheduler: Scheduler = schedulerActorSystem.scheduler
  val schedulerWrapper: SchedulerWrapper =
    new SchedulerWrapper(
      scheduler = scheduler,
      configuration = configuration,
      publishFacadeCommand = publishStickerCommandFacade,
      stickersService = stickersService
    )

  // Input > TelegramBot
  val indexRoutes: IndexRoutes = new IndexRoutes(
    indexAllQueryHandler = indexAllQueryHandler,
    indexUnpublishedQueryHandler = indexUnpublishedQueryHandler,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id)),
    indexStatsQueryHandler = indexStatsQueryHandler
  )
  val searchRoutes: SearchRoutes = new SearchRoutes(
    searchStickerQueryHandler = searchStickerQueryHandler,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )
  val previewRoutes: PreviewStickerRoutes =
    new PreviewStickerRoutes(
      previewStickerCommandHandler = previewStickerCommandHandler,
      previewStickerSocialMediaPlainCommandHandler = previewStickerSocialMediaPlainCommandHandler,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
    )
  val publishRoutes: PublishStickerRoutes =
    new PublishStickerRoutes(
      publishFacadeCommand = publishStickerCommandFacade,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id)),
      defaultPublicationPlatforms =
        configuration.publicationPlatforms.map(value => PublicationPlatforms.withName(value))
    )
  val acceptRoutes: AcceptDraftStickerRoutes =
    new AcceptDraftStickerRoutes(
      draftStickerAcceptCommandHandler = draftStickerAcceptCommandHandler,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
    )
  val rejectDraftStickerRoutes: RejectDraftStickerRoutes =
    new RejectDraftStickerRoutes(
      draftStickerRejectCommandHandler = draftStickerRejectCommandHandler,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
    )
  val reviewDraftStickerRoutes: ReviewDraftStickerRoutes =
    new ReviewDraftStickerRoutes(
      previewDraftStickerCommandHandler = previewDraftStickerCommandHandler,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
    )
  val addStickerRoutes: AddStickerRoutes = new AddStickerRoutes(
    telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
    telegramAddFSMExistsQueryHandler = telegramAddFSMExistsQueryHandler,
    telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
    telegramFSMDeleteCommandHandler = telegramFSMDeleteCommandHandler,
    newDraftStickerCommandHandler = newDraftStickerCommandHandler,
    telegramAddFSMQueryHandler = telegramAddFSMQueryHandler,
    draftStickerUpsertTextFieldCommandHandler = draftStickerUpsertTextFieldCommandHandler,
    draftStickerUpsertBinaryCommandHandler = DraftStickerUpsertBinaryCommandHandler,
    draftStickerAcceptCommandHandler = draftStickerAcceptCommandHandler,
    draftStickerAbortCommandHandler = draftStickerAbortCommandHandler,
    previewStickerCommandHandler = previewStickerCommandHandler,
    previewDraftStickerCommandHandler = previewDraftStickerCommandHandler,
    telegramService = telegramService,
    refreshIngestionFilesListCommandHandler = refreshIngestionFilesListCommandHandler,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val addStickerBasicDataAtOnceRoutes: AddStickerBasicDataAtOnceRoutes = new AddStickerBasicDataAtOnceRoutes(
    telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
    telegramAddFSMExistsQueryHandler = telegramAddFSMExistsQueryHandler,
    telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
    newDraftStickerCommandHandler = newDraftStickerCommandHandler,
    draftStickerUpsertTextFieldCommandHandler = draftStickerUpsertTextFieldCommandHandler,
    draftStickerUpsertBinaryCommandHandler = DraftStickerUpsertBinaryCommandHandler,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val addStickerFromIngestionFinalRoutes: AddStickerFromIngestionFinalRoutes = new AddStickerFromIngestionFinalRoutes(
    telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
    telegramAddFSMExistsQueryHandler = telegramAddFSMExistsQueryHandler,
    telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
    newDraftStickerCommandHandler = newDraftStickerCommandHandler,
    draftStickerUpsertTextFieldCommandHandler = draftStickerUpsertTextFieldCommandHandler,
    draftStickerUpsertBinaryCommandHandler = DraftStickerUpsertBinaryCommandHandler,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id)),
    ingestionQueueRasterizeQueryHandler = ingestionQueueRasterizeQueryHandler,
    ingestionQueueVectorQueryHandler = ingestionQueueVectorQueryHandler
  )

  val addStickerNewRoutes: AddStickerNewRoutes = new AddStickerNewRoutes(
    telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
    telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
    telegramFSMDeleteCommandHandler = telegramFSMDeleteCommandHandler,
    draftStickerNewCommandHandler = newDraftStickerCommandHandler,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val editStickerRoutes: EditStickerRoutes =
    new EditStickerRoutes(
      stickerUpsertTextFieldCommandHandler = stickerUpsertTextFieldCommandHandler,
      previewStickerCommandHandler = previewStickerCommandHandler,
      stickerUpsertBinaryCommandHandler = stickerUpsertBinaryCommandHandler,
      telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
      telegramFSMDeleteCommandHandler = telegramFSMDeleteCommandHandler,
      telegramEditFSMExistsQueryHandler = telegramEditFSMExistsQueryHandler,
      telegramEditFSMQueryHandler = telegramEditFSMQueryHandler,
      telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
    )
  val editStickerInitRoutes: EditStickerInitRoutes =
    new EditStickerInitRoutes(
      telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
      telegramFSMDeleteCommandHandler = telegramFSMDeleteCommandHandler,
      telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
      telegramService = telegramService,
      adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
    )

  val bookmarkStickerInitRoutes: BookmarkStickerInitRoutes = new BookmarkStickerInitRoutes(
    telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
    telegramFSMDeleteCommandHandler = telegramFSMDeleteCommandHandler,
    telegramBookmarkFSMExistsQueryHandler = telegramBookmarkFSMExistsQueryHandler,
    telegramBookmarkFSMQueryHandler = telegramBookmarkFSMQueryHandler,
    telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
    telegramService = telegramService,
    stickerIndexBookmarkQueryHandler = stickerIndexBookmarkQueryHandler,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val bookmarkStickerAfterInitRoutes: BookmarkStickerAfterInitRoutes = new BookmarkStickerAfterInitRoutes(
    telegramFSMUpdateCommandHandler = telegramFSMUpdateCommandHandler,
    telegramFSMDeleteCommandHandler = telegramFSMDeleteCommandHandler,
    telegramBookmarkFSMExistsQueryHandler = telegramBookmarkFSMExistsQueryHandler,
    telegramBookmarkFSMQueryHandler = telegramBookmarkFSMQueryHandler,
    telegramFSMExistsAnyQueryHandler = telegramFSMExistsAnyQueryHandler,
    telegramService = telegramService,
    stickerIndexBookmarkQueryHandler = stickerIndexBookmarkQueryHandler,
    stickerAddBookmarkCommandHandler = stickerAddBookmarkCommandHandler,
    stickerDeleteBookmarkCommandHandler = stickerDeleteBookmarkCommandHandler,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val deleteStickerRoutes: DeleteStickerRoutes = new DeleteStickerRoutes(
    stickerDeleteCommandHandler = stickerDeleteCommandHandler,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val refreshCommandRoutes: RefreshCommandRoutes = new RefreshCommandRoutes(
    configuration = configuration,
    telegramService = telegramService,
    adminUsers = List(TelegramUserId(configuration.telegram.adminUserId.id))
  )

  val telegramRoutesFacade: TelegramRoutesFacade =
    new TelegramRoutesFacade(
      config = configuration,
      client = rawTelegramClient,
      previewStickerRoutes = previewRoutes,
      publishStickerRoutes = publishRoutes,
      indexRoutes = indexRoutes,
      searchRoutes = searchRoutes,
      editStickerRoutes = editStickerRoutes,
      editStickerInitRoutes = editStickerInitRoutes,
      addStickerRoutes = addStickerRoutes,
      addStickerNewRoutes = addStickerNewRoutes,
      addStickerBasicDataAtOnceRoutes = addStickerBasicDataAtOnceRoutes,
      acceptRoutes = acceptRoutes,
      rejectDraftStickerRoutes = rejectDraftStickerRoutes,
      reviewDraftStickerRoutes = reviewDraftStickerRoutes,
      bookmarkStickerInitRoutes = bookmarkStickerInitRoutes,
      bookmarkStickerAfterInitRoutes = bookmarkStickerAfterInitRoutes,
      deleteStickerRoutes = deleteStickerRoutes,
      addStickerFromIngestionFinalRoutes = addStickerFromIngestionFinalRoutes,
      refreshCommandRoutes = refreshCommandRoutes
    )

  // HTTP API routes
  val stickersRoute: StickersRoute = new StickersRoute(
    queryHandler = indexByPageQueryHandler,
    indexAllQueryHandler = indexAllQueryHandler,
    indexByIdQueryHandler = indexByIdQueryHandler
  )

  val bookmarkStickersRoute: BookmarkStickersRoute = new BookmarkStickersRoute(
    queryHandler = aggregatedStickerIndexBookmarkQueryHandler
  )

  val rssRoute: RssRoute = new RssRoute(queryHandler = indexByPageQueryHandler)

  val routes: Routes = new Routes(
    stickersRoute = stickersRoute,
    bookmarkStickersRoute = bookmarkStickersRoute,
    rssRoute = rssRoute
  )
}
