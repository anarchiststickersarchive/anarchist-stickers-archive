package acab.devcon0.app.di

import acab.devcon0.configuration.RedisConfiguration
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.clients.ScalajHttpClient
import com.bot4s.telegram.future.GlobalExecutionContext
import com.redis.RedisClientPool

import scala.concurrent.Future

object DITelegramBase extends GlobalExecutionContext {
  import DIConfiguration._

  // Client
  val rawTelegramClient: RequestHandler[Future] = new ScalajHttpClient(token = configuration.telegram.token)
  // Redis client
  val telegramFSMDB = 0
  val draftStickerDB = 1
  private val redisConfiguration: RedisConfiguration = configuration.redis
  val telegramFSMRedisClientPool: RedisClientPool = new RedisClientPool(
    host = redisConfiguration.host,
    port = redisConfiguration.port,
    database = telegramFSMDB
  )
  val draftStickerRedisClientPool: RedisClientPool = new RedisClientPool(
    host = redisConfiguration.host,
    port = redisConfiguration.port,
    database = draftStickerDB
  )
}
