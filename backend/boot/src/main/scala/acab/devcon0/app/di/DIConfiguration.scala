package acab.devcon0.app.di

import acab.devcon0.configuration.{BotConfigFactory, Configuration}

object DIConfiguration {
  private val botConfigFactory: BotConfigFactory = new BotConfigFactory()
  val configuration: Configuration = botConfigFactory.build()
}
