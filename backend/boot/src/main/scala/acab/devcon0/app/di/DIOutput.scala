package acab.devcon0.app.di

import acab.devcon0.domain.ports.output._
import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import acab.devcon0.output.client._
import acab.devcon0.output.repository._
import com.danielasfregola.twitter4s.TwitterRestClient
import sttp.client3.{HttpURLConnectionBackend, Identity, SttpBackend}

object DIOutput {
  import DIConfiguration._
  import DITelegramBase._
  private val backend: SttpBackend[Identity, Any] = HttpURLConnectionBackend()

  // Client
  val twitterRestClient: TwitterRestClient = TwitterRestClient()
  val twitterClient: TwitterClient = new TwitterClientImpl(twitterRestClient = twitterRestClient)
  val telegramClient: TelegramClient =
    new TelegramClientImpl(rawTelegramClient, configuration.telegram.token)
  val ipfsClient: IpfsClient = new IpfsClientImpl(configuration = configuration, backend = backend)
  val vectorizerClient: VectorizerClient = new VectorizerClientImpl(
    apiKey = configuration.vectorizerConfiguration.apiKey,
    backend = backend
  )
  val mastodonClient: MastodonClient = new MastodonClientImpl(configuration = configuration.mastodon, backend = backend)
  val blueskyClient: BlueskyClient = new BlueskyClientImpl(configuration = configuration.bluesky, backend = backend)
  val pixelfedClient: PixelfedClient = new PixelfedClientImpl(configuration = configuration.pixelfed, backend = backend)
  // Repository
  val tagsRepository: TagsRepository = new TagsRepository()
  val stickersRepository: StickersRepositoryImpl = new StickersRepositoryImpl()
  val publicationsRepository: PublicationsRepository = new PublicationsRepositoryImpl()
  val bookmarksRepository: BookmarksRepository = new BookmarksRepositoryImpl()
  val stickerLinksRepository: StickerLinksRepository = new StickerLinksRepositoryImpl()
  val stickerDirectoryRelationsRepository: StickerDirectoryRelationsRepositoryImpl =
    new StickerDirectoryRelationsRepositoryImpl()
  val telegramFSMRepository: FiniteStateMachineRepository[TelegramFSMId] = new TelegramFSMRepository(
    redisClientPool = telegramFSMRedisClientPool
  )
  val draftStickersRepository: DraftStickersRepository = new DraftStickersRepositoryImpl(
    redisClientPool = draftStickerRedisClientPool
  )
}
