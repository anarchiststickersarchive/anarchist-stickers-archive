package acab.devcon0.app.di

import acab.devcon0.domain.adapters.addsticker._
import acab.devcon0.domain.adapters.bookmarksticker.{
  AggregatedStickerIndexBookmarkQueryHandlerImpl,
  StickerAddBookmarkCommandHandlerImpl,
  StickerDeleteBookmarkCommandHandlerImpl,
  StickerIndexBookmarkQueryHandlerImpl
}
import acab.devcon0.domain.adapters.deletesticker.StickerDeleteCommandHandlerImpl
import acab.devcon0.domain.adapters.editsticker.{
  StickerUpsertBinaryCommandHandlerImpl,
  StickerUpsertTextFieldCommandHandlerImpl
}
import acab.devcon0.domain.adapters.index._
import acab.devcon0.domain.adapters.ingestion.{
  IngestionQueueRasterizeQueryHandlerImpl,
  IngestionQueueVectorQueryHandlerImpl,
  RefreshIngestionFilesListCommandHandlerImpl
}
import acab.devcon0.domain.adapters.previewsticker.{
  PreviewDraftStickerCommandHandlerImpl,
  PreviewStickerCommandHandlerImpl,
  PreviewStickerSocialMediaPlainCommandHandlerImpl
}
import acab.devcon0.domain.adapters.publish._
import acab.devcon0.domain.adapters.searchsticker.SearchStickerQueryHandlerImpl
import acab.devcon0.domain.adapters.telegramfsm._
import acab.devcon0.domain.dtos.PublicationPlatforms
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.ports.input.bookmarksticker.{
  AggregatedStickerIndexBookmarkQueryHandler,
  StickerAddBookmarkCommandHandler,
  StickerDeleteBookmarkCommandHandler,
  StickerIndexBookmarkQueryHandler
}
import acab.devcon0.domain.ports.input.deletesticker.StickerDeleteCommandHandler
import acab.devcon0.domain.ports.input.editsticker.{
  StickerUpsertBinaryCommandHandler,
  StickerUpsertTextFieldCommandHandler
}
import acab.devcon0.domain.ports.input.index._
import acab.devcon0.domain.ports.input.ingestion.{
  IngestionQueueRasterizeQueryHandler,
  IngestionQueueVectorQueryHandler,
  RefreshIngestionFilesListCommandHandler
}
import acab.devcon0.domain.ports.input.preview.{
  PreviewDraftStickerCommandHandler,
  PreviewStickerSocialMediaPlainCommandHandler
}
import acab.devcon0.domain.ports.input.searchsticker.SearchStickerQueryHandler
import acab.devcon0.domain.ports.input.telegramfsm.{
  TelegramFSMDeleteCommandHandler,
  TelegramFSMExistsAnyQueryHandler,
  TelegramFSMExistsQueryHandler,
  TelegramFSMUpdateCommandHandler
}
import acab.devcon0.domain.service._

object DIDomain {
  import DIConfiguration._
  import DIOutput._
  // Domain > Service
  val squarePhotoHandler: SquarePhotoHandler = new SquarePhotoHandler() {}
  val ingestionFinalPickService: IngestionQueueService = new IngestionQueueServiceImpl(
    configuration = configuration
  )
  val twitterService: TwitterService = new TwitterService(twitterClient)
  val mastodonService: MastodonService = new MastodonServiceImpl(mastodonClient)
  val blueskyService: BlueskyService = new BlueskyServiceImpl(blueskyClient)
  val pixelfedService: PixelfedService = new PixelfedServiceImpl(pixelfedClient)
  val ipfsService: IpfsService = new IpfsService(ipfsClient = ipfsClient)
  val telegramService: TelegramService = new TelegramServiceImpl(telegramClient = telegramClient)
  val sourceService: SourceService = new SourceServiceImpl()
  val captionService: CaptionService =
    new CaptionServiceImpl(configuration = configuration, sourceService = sourceService)
  val stickerDirectoryRelationsService: StickerDirectoryRelationsService = new StickerDirectoryRelationsService(
    repository = stickerDirectoryRelationsRepository
  )
  val stickersService: StickersService = new StickersService(repository = stickersRepository)
  val stickerLinksService: StickerLinksService = new StickerLinksService(
    repository = stickerLinksRepository
  )
  val publicationsService: PublicationsService = new PublicationsService(
    repository = publicationsRepository
  )
  val bookmarksService: BookmarksService = new BookmarksServiceImpl(
    repository = bookmarksRepository
  )
  val aggregatedStickersService: AggregatedStickersService = new AggregatedStickersService(
    configuration = configuration,
    stickersService = stickersService,
    stickerLinksService = stickerLinksService,
    stickerDirectoryRelationsService = stickerDirectoryRelationsService
  )
  val draftStickersService: DraftStickersService = new DraftStickersService(
    repository = draftStickersRepository
  )
  val telegramFSMService: TelegramFSMService = new TelegramFSMService(repository = telegramFSMRepository)

  // Domain
  // Domain > Adapters
  // Domain > Adapters > Queries
  val ingestionQueueRasterizeQueryHandler: IngestionQueueRasterizeQueryHandler =
    new IngestionQueueRasterizeQueryHandlerImpl(
      ingestionQueueService = ingestionFinalPickService
    )
  val ingestionQueueVectorQueryHandler: IngestionQueueVectorQueryHandler =
    new IngestionQueueVectorQueryHandlerImpl(
      ingestionFinalPickService = ingestionFinalPickService
    )
  val searchStickerQueryHandler: SearchStickerQueryHandler = new SearchStickerQueryHandlerImpl(
    stickersService = stickersService
  )
  val indexStatsQueryHandler: IndexStatsQueryHandler = new IndexStatsQueryHandlerImpl(
    stickersService = stickersService,
    ingestionFinalPickService = ingestionFinalPickService
  )
  val indexAllQueryHandler: IndexAllQueryHandler = new IndexAllQueryHandlerImpl(stickersService = stickersService)
  val indexByIdQueryHandler: IndexByIdQueryHandler = new IndexByIdQueryHandlerImpl(
    aggregatedStickersService = aggregatedStickersService
  )
  val indexByPageQueryHandler: IndexByPageQueryHandler = new IndexByPageQueryHandlerImpl(
    aggregatedStickersService = aggregatedStickersService
  )
  val indexUnpublishedQueryHandler: IndexUnpublishedQueryHandler = new IndexUnpublishedQueryHandlerImpl(
    stickersService = stickersService
  )
  val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler = new TelegramFSMExistsAnyQueryHandlerImpl(
    telegramFSMService = telegramFSMService
  )
  val telegramAddFSMExistsQueryHandler: TelegramFSMExistsQueryHandler = new TelegramAddFSMExistsQueryHandler(
    telegramFSMService = telegramFSMService
  )
  val telegramEditFSMExistsQueryHandler: TelegramFSMExistsQueryHandler = new TelegramEditFSMExistsQueryHandler(
    telegramFSMService = telegramFSMService
  )
  val telegramAddFSMQueryHandler: TelegramAddFSMQueryHandler = new TelegramAddFSMQueryHandler(
    telegramFSMService = telegramFSMService
  )
  val telegramEditFSMQueryHandler: TelegramEditFSMQueryHandler = new TelegramEditFSMQueryHandler(
    telegramFSMService = telegramFSMService
  )
  val telegramBookmarkFSMExistsQueryHandler: TelegramFSMExistsQueryHandler = new TelegramBookmarkFSMExistsQueryHandler(
    telegramFSMService = telegramFSMService
  )
  val telegramBookmarkFSMQueryHandler: TelegramBookmarkFSMQueryHandler = new TelegramBookmarkFSMQueryHandler(
    telegramFSMService = telegramFSMService
  )
  val stickerIndexBookmarkQueryHandler: StickerIndexBookmarkQueryHandler = new StickerIndexBookmarkQueryHandlerImpl(
    stickersService = stickersService,
    bookmarksService = bookmarksService
  )
  val aggregatedStickerIndexBookmarkQueryHandler: AggregatedStickerIndexBookmarkQueryHandler =
    new AggregatedStickerIndexBookmarkQueryHandlerImpl(
      aggregatedStickersService = aggregatedStickersService,
      bookmarksService = bookmarksService
    )
  val stickerAddBookmarkCommandHandler: StickerAddBookmarkCommandHandler = new StickerAddBookmarkCommandHandlerImpl(
    bookmarksService = bookmarksService
  )
  val stickerDeleteBookmarkCommandHandler: StickerDeleteBookmarkCommandHandler =
    new StickerDeleteBookmarkCommandHandlerImpl(
      bookmarksService = bookmarksService
    )
  // Input > Adapters > Commands
  val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler =
    new TelegramFSMUpdateCommandHandlerImpl(telegramFSMService: TelegramFSMService)
  val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler =
    new TelegramFSMDeleteCommandHandlerImpl(telegramFSMService: TelegramFSMService)
  val newDraftStickerCommandHandler: DraftStickerNewCommandHandler =
    new DraftStickerNewCommandHandlerImpl(draftStickersService = draftStickersService)
  val previewStickerCommandHandler: PreviewStickerCommandHandlerImpl = new PreviewStickerCommandHandlerImpl(
    captionService = captionService,
    aggregatedStickersService = aggregatedStickersService,
    telegramService = telegramService
  )
  val previewStickerSocialMediaPlainCommandHandler: PreviewStickerSocialMediaPlainCommandHandler =
    new PreviewStickerSocialMediaPlainCommandHandlerImpl(
      captionService = captionService,
      aggregatedStickersService = aggregatedStickersService,
      telegramService = telegramService,
      squarePhotoHandler = squarePhotoHandler
    )
  val refreshIngestionFilesListCommandHandler: RefreshIngestionFilesListCommandHandler =
    new RefreshIngestionFilesListCommandHandlerImpl(
      ingestionFinalPickService = ingestionFinalPickService
    )
  val previewDraftStickerCommandHandler: PreviewDraftStickerCommandHandler = new PreviewDraftStickerCommandHandlerImpl(
    captionService = captionService,
    draftStickersService = draftStickersService,
    telegramService = telegramService
  )
  val publishToTelegramCommand: PublishToTelegramCommand =
    new PublishToTelegramCommand(
      configuration = configuration,
      telegramService = telegramService,
      captionService = captionService
    )
  val publishToTwitterCommand: PublishToTwitterCommand =
    new PublishToTwitterCommand(captionService = captionService, twitterService = twitterService)
  val publishToMastodonCommand: PublishToMastodonCommand =
    new PublishToMastodonCommand(captionService = captionService, mastodonService = mastodonService)
  val publishToPixelfedCommand: PublishToPixelfedCommand =
    new PublishToPixelfedCommand(captionService = captionService, pixelfedService = pixelfedService)
  val publishToBlueskyCommand: PublishToBlueskyCommand =
    new PublishToBlueskyCommand(captionService = captionService, blueskyService = blueskyService)
  val publishToInstagramViaTelegramCommand: PublishToInstagramViaTelegramCommand =
    new PublishToInstagramViaTelegramCommand(
      captionService = captionService,
      configuration = configuration,
      telegramService = telegramService
    )
  val publishStickerCommandFacade: PublishStickerCommandFacadeImpl = new PublishStickerCommandFacadeImpl(
    configuration = configuration,
    captionService = captionService,
    aggregatedStickersService = aggregatedStickersService,
    publicationsService = publicationsService,
    publishToPlatformCommands = Map(
      PublicationPlatforms.TELEGRAM -> publishToTelegramCommand,
      PublicationPlatforms.TWITTER -> publishToTwitterCommand,
      PublicationPlatforms.INSTAGRAM -> publishToInstagramViaTelegramCommand,
      PublicationPlatforms.MASTODON -> publishToMastodonCommand,
      PublicationPlatforms.PIXELFED -> publishToPixelfedCommand,
      PublicationPlatforms.BLUESKY -> publishToBlueskyCommand
    ),
    telegramService = telegramService
  )
  val stickerUpsertBinaryCommandHandler: StickerUpsertBinaryCommandHandler =
    new StickerUpsertBinaryCommandHandlerImpl(
      configuration = configuration,
      stickerLinksService = stickerLinksService,
      stickerDirectoryRelationsService = stickerDirectoryRelationsService,
      ipfsService = ipfsService,
      telegramService = telegramService,
      vectorizerClient = vectorizerClient,
      aggregatedStickersService = aggregatedStickersService
    )
  val stickerUpsertTextFieldCommandHandler: StickerUpsertTextFieldCommandHandler =
    new StickerUpsertTextFieldCommandHandlerImpl(
      stickersService = stickersService
    )
  val draftStickerUpsertTextFieldCommandHandler: DraftStickerUpsertTextFieldCommandHandler =
    new DraftStickerUpsertTextFieldCommandHandlerImpl(
      draftStickersService = draftStickersService
    )
  val DraftStickerUpsertBinaryCommandHandler: DraftStickerUpsertBinaryCommandHandler =
    new DraftStickerUpsertBinaryCommandHandlerImpl(
      draftStickersService = draftStickersService,
      telegramService = telegramService,
      vectorizerClient = vectorizerClient
    )
  val draftStickerAcceptCommandHandler: DraftStickerAcceptCommandHandler = new DraftStickerAcceptCommandHandlerImpl(
    draftStickersService = draftStickersService,
    configuration = configuration,
    stickersService = stickersService,
    stickerLinksService = stickerLinksService,
    stickerDirectoryRelationsService = stickerDirectoryRelationsService,
    ipfsService = ipfsService
  )
  val draftStickerAbortCommandHandler: DraftStickerAbortCommandHandler = new DraftStickerAbortCommandHandlerImpl(
    draftStickersService = draftStickersService,
    ingestionFinalPickService = ingestionFinalPickService,
    telegramFSMService = telegramFSMService
  )
  val draftStickerRejectCommandHandler: DraftStickerRejectCommandHandler = new DraftStickerRejectCommandHandlerImpl(
    draftStickersService = draftStickersService
  )
  val stickerDeleteCommandHandler: StickerDeleteCommandHandler = new StickerDeleteCommandHandlerImpl(
    stickersService = stickersService,
    configuration = configuration,
    stickerLinksService = stickerLinksService,
    stickerDirectoryRelationsService = stickerDirectoryRelationsService,
    publicationsService = publicationsService
  )
}
