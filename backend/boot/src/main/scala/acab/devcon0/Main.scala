package acab.devcon0

import acab.devcon0.app.di.{DIConfiguration, DIInput}
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.codecs.CirceCodecs.Encoders._
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import com.typesafe.scalalogging.LazyLogging
import io.circe.syntax._
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.Location
import scalikejdbc.config.DBs

import scala.concurrent.{ExecutionContextExecutor, Future}
import cats.data.OptionT

object Main extends App with LazyLogging {

  val configurationAsString: String = getConfigurationAsString(DIConfiguration.configuration)
  logger.info(s"Configuration=$configurationAsString")

  performFlywayMigrations(DIConfiguration.configuration)
  DBs.setupAll()
  DIInput.telegramRoutesFacade.run()
  DIInput.schedulerWrapper.publications()

  implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "my-system")
  implicit val executionContext: ExecutionContextExecutor = system.executionContext
  val bindingFuture: Future[Http.ServerBinding] = Http()
    .newServerAt("0.0.0.0", DIConfiguration.configuration.http.port)
    .bind(DIInput.routes.routes)

  private def performFlywayMigrations(configuration: Configuration): Unit = {
    Flyway
      .configure()
      .dataSource(
        configuration.flywayDataSource.url,
        configuration.flywayDataSource.user,
        configuration.flywayDataSource.password
      )
      .locations(new Location("classpath:db/migrations"))
      .load()
      .migrate()
  }

  private def getConfigurationAsString(configuration: Configuration): String = {
    EncoderOps[Configuration](configuration).asJson.toString()
  }
}
