create table BOOKMARKS
(
    STICKER_ID         int                     not null,
    USER_ID            varchar(128)            not null,
    USER_PLATFORM      varchar(128)            not null,
    CREATION_TIMESTAMP timestamp default NOW() not null,
    constraint BOOKMARKS_pk
        primary key (STICKER_ID, USER_ID, USER_PLATFORM),
    constraint BOOKMARKS_STICKERS_ID_fk
        foreign key (STICKER_ID) references STICKERS (ID)
);

