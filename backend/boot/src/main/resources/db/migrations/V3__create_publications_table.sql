create table PUBLICATIONS
(
    ID int auto_increment,
    STICKER_ID int not null,
    PLATFORM VARCHAR(128) not null,
    CREATION_TIMESTAMP TIMESTAMP default NOW() not null,
    constraint PUBLICATIONS_pk
        primary key (ID),
    constraint PUBLICATIONS_PUBLISHING_QUEUE_ID_fk
        foreign key (STICKER_ID) references STICKERS (ID)
);

create unique index PUBLICATIONS_PUBLISHING_QUEUE_ID_PLATFORM_uindex
    on PUBLICATIONS (STICKER_ID, PLATFORM);

