create table TAGS
(
    ID int auto_increment,
    STICKER_ID INT NOT NULL,
    VALUE VARCHAR(128) not null,
    CREATION_TIMESTAMP TIMESTAMP default NOW() null,
    constraint TAGS_pk
        primary key (ID),
    CONSTRAINT `TAGS_STICKERS_ID_FK`
        FOREIGN KEY (STICKER_ID) REFERENCES STICKERS(ID)
);

