package acab.devcon0.commons

import scala.concurrent.Future

object FunctionUtils {
  def flushResult[T]: T => Unit = { _ => () }
  def flushResultToFuture[T]: T => Future[Unit] = { _ => Future.successful() }
}
