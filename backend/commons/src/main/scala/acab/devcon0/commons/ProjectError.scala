package acab.devcon0.commons

class ProjectError(message: String) extends Throwable {
  override def getMessage: String = message
}

object ProjectError {
  type NotFound = ProjectError
}
