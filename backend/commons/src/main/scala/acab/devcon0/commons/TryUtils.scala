package acab.devcon0.commons

import acab.devcon0.commons.ProjectError.NotFound
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object TryUtils {
  def peekException[T](logger: Logger)(throwable: Throwable): Try[T] = {
    logger.error(s"throwable=$throwable")
    Failure(throwable)
  }

  def doNothing[T](): T => Try[T] = { value => Success(value) }

  def foldToFuture[T](value: T): Future[T] = Future.successful(value)

  def foldToFutureFailed[T](throwable: Throwable): Future[T] = Future.failed(throwable)

  def forceOptionExistence[T, I](identity: Option[I] = None): Option[T] => Try[T] = {
    case Some(value)                => Success(value)
    case None if identity.isDefined => Failure(new NotFound(s"Expected data was not found. id=${identity.get}"))
    case None                       => Failure(new NotFound(s"Expected data was not found"))
  }
}
