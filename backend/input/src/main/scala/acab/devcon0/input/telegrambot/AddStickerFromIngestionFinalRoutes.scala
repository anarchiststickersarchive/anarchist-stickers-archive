package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.dtos.{Sources, StickerLinkTypes, TelegramUserId}
import acab.devcon0.domain.fsm.addsticker._
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.ports.input.ingestion.{IngestionQueueRasterizeQuery, IngestionQueueRasterizeQueryHandler, IngestionQueueVectorQuery, IngestionQueueVectorQueryHandler}
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{ArgumentParser, RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.{InputFile, Message}

import java.io.File
import java.util.UUID
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class AddStickerFromIngestionFinalRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramAddFSMExistsQueryHandler: TelegramFSMExistsQueryHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val draftStickerUpsertTextFieldCommandHandler: DraftStickerUpsertTextFieldCommandHandler,
    val newDraftStickerCommandHandler: DraftStickerNewCommandHandler,
    val draftStickerUpsertBinaryCommandHandler: DraftStickerUpsertBinaryCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser],
    val ingestionQueueRasterizeQueryHandler: IngestionQueueRasterizeQueryHandler,
    val ingestionQueueVectorQueryHandler: IngestionQueueVectorQueryHandler
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    addStickerMessageRoute()
  )

  private def addStickerMessageRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/ingest",
      authorityFilter = RoutesAuthorizationUtils.isAuthorized(adminUsers),
      action = addStickerMessageAction(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "addStickerMessageFromIngestionFinalRoute")
    )
  }

  private def addStickerMessageAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val existsSomeFsmForThisUserValue: Try[Boolean] = existsSomeFsmForThisUser(userId)
    val existsMatchingFsmForThisUserValue: Try[Boolean] = existsMatchingFSMForThisUser(userId)
    val args: Args = ArgumentParser.parse(message)
    val maybeSource: Option[String] = Try(args.head).toOption
    val safeMaybeRasterized: Try[Option[File]] =
      ingestionQueueRasterizeQueryHandler.handle(IngestionQueueRasterizeQuery(maybeSource))

    (existsSomeFsmForThisUserValue, existsMatchingFsmForThisUserValue, safeMaybeRasterized) match {
      case (Failure(exception), _, _)                                  => handleExceptionCase(exception)
      case (_, Failure(exception), _)                                  => handleExceptionCase(exception)
      case (_, _, Failure(exception))                                  => handleExceptionCase(exception)
      case (Success(true), Success(false), _)                          => handleOtherFSMTakingCareCase()
      case (Success(false), Success(false), Success(Some(rasterized))) => handleBasicDataAtOnceCase(message, rasterized)
      case (Success(false), Success(false), Success(None))             => handleNoRasterized(message)
      case (_, _, _)                                                   => Future.successful()
    }
  }

  private def handleNoRasterized(message: Message): Future[Unit] = {
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    sendMessage(
      userId = userId,
      text = s"No candidates for ingestion"
    )
    Future.successful()
  }

  private def handleBasicDataAtOnceCase(message: Message, rasterized: File): Future[Unit] = {
    val maybeVector: Option[File] = getOptionalVector(rasterized)
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    sendMessage(
      userId = userId,
      text = s"Up to insert sticker=${rasterized.getAbsolutePath} with vector=${maybeVector.isDefined}"
    )
    upsertDraftSticker(userId)
      .flatMap(draftStickerId => upsertRasterized(draftStickerId, rasterized))
      .flatMap(draftStickerId => upsertVector(draftStickerId, maybeVector))
      .flatMap(draftStickerId => upsertSource(rasterized, draftStickerId))
      .flatMap(draftStickerId => upsertFSMForAdditionFromIngestionFinal(userId, draftStickerId))
      .flatMap(_ => reply(userId = userId, text = UIMessages.afterIngestionFromFinalAction, rasterized = rasterized))
      .fold(
        throwable => {
          sendMessage(userId = userId, s"Something went wrong during the ingestion. throwable=$throwable")
          TryUtils.foldToFutureFailed(throwable)
        },
        TryUtils.foldToFuture
      )
  }

  private def getOptionalVector(rasterized: File): Option[File] = {
    ingestionQueueVectorQueryHandler
      .handle(IngestionQueueVectorQuery(rasterized))
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
      .flatMap(TryUtils.forceOptionExistence())
      .toOption
  }

  def upsertRasterized(draftStickerId: DraftStickerId, rasterized: File): Try[DraftStickerId] = {
    val rasterizedPath = rasterized.toPath
    for {
      command <- Try(
        DraftStickerUpsertBinaryTelegramCommand(
          draftStickerId = draftStickerId,
          stickerLinkType = StickerLinkTypes.ORIGINAL,
          eitherTelegramFileIdOrAbsoluteFilename = Right(rasterizedPath.toAbsolutePath.toString),
          extension = rasterized.getName.split('.').last
        )
      )
      commandResult = draftStickerUpsertBinaryCommandHandler.handle(command)
      processResult <- DraftStickerUpsertBinaryEventProcessor.apply(commandResult, logger)
    } yield {
      processResult
    }
  }

  def upsertVector(draftStickerId: DraftStickerId, maybeVector: Option[File]): Try[DraftStickerId] = {
    maybeVector
      .map(vector => {
        val vectorPath = vector.toPath
        val command = DraftStickerUpsertBinaryTelegramCommand(
          draftStickerId = draftStickerId,
          stickerLinkType = StickerLinkTypes.VECTOR,
          eitherTelegramFileIdOrAbsoluteFilename = Right(vectorPath.toAbsolutePath.toString),
          extension = vector.getName.split('.').last
        )
        val commandResult = draftStickerUpsertBinaryCommandHandler.handle(command)
        val result = DraftStickerUpsertBinaryEventProcessor.apply(commandResult, logger)
        return result
      })
      .fold(Try(draftStickerId))({ result: Try[DraftStickerId] => result })
  }

  private def upsertSource(rasterized: File, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    getSourceFromFile(rasterized)
      .map(tokenizedSource => {
        val cmd = DraftStickerUpsertSourceCommand(draftStickerId, tokenizedSource)
        val commandResult: Try[DraftStickerUpsertTextFieldEvent[_]] =
          draftStickerUpsertTextFieldCommandHandler.handle(cmd)
        DraftStickerUpsertTextFieldEventProcessor.apply(commandResult, logger)
      })
      .fold(Try(draftStickerId))({ result: Try[DraftStickerId] => result })

  }

  private def getSourceFromFile(file: File): Option[String] = {
    val acceptedSources = Sources.values.toList.map(_.toString).map(_.toLowerCase)
    val parts = file.getPath.split(File.separator).reverse
    if (parts(1) != "nn" && acceptedSources.contains(parts(2).toLowerCase)) {
      val userHandler = parts(1)
      val sourceType = parts(2)
      Some(s"$sourceType:$userHandler")
    } else {
      None
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def existsMatchingFSMForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramAddFSMExistsQueryHandler.handle(TelegramFSMExistsQuery(id = userId))
  }

  private def upsertFSMForAdditionFromIngestionFinal(
      userId: TelegramUserId,
      draftStickerId: DraftStickerId
  ): Try[TelegramFSMId] = {
    val data: AddStickerFSMData = AddStickerFSMData(draftStickerId = draftStickerId)
    val instance: TelegramAddFSMInstance =
      TelegramAddFSMInstance(id = userId, state = AwaitingName, data = data)
    val cmdResult = telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
    TelegramFSMUpdateEventProcessor.apply(cmdResult, logger)
  }

  private def upsertDraftSticker(userId: TelegramUserId): Try[DraftStickerId] = {
    val command = DraftStickerNewCommand(draftStickerId = UUID.randomUUID(), submittingUser = userId)
    val commandResult = newDraftStickerCommandHandler.handle(command)
    val result = DraftStickerNewEventProcessor.apply(commandResult, logger)
    result
  }

  private def handleExceptionCase(exception: Throwable): Future[Unit] = {
    logger.error("AddStickerFromIngestionFinalRoutes", exception)
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def sendMessage(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }

  private def reply(userId: TelegramUserId, text: String, rasterized: File): Try[Unit] = {
    telegramService.sendPhoto(to = userId, text = text, inputFile = InputFile.Path(rasterized.toPath))
  }
}
