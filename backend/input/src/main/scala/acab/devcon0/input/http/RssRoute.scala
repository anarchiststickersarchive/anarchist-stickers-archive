package acab.devcon0.input.http

import acab.devcon0.domain.dtos.{AggregatedSticker, StickerLinkTypes}
import acab.devcon0.domain.ports.input.index.{IndexByPageQuery, IndexByPageQueryHandler}
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

class RssRoute(queryHandler: IndexByPageQueryHandler) {

  private def responseHeader(content: String): String =
    s"""<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/" >
      |      <channel>
      |        <title>Anarchist Stickers Archive</title>
      |        <link>https://anarchiststickersarchive.org/</link>
      |        <description>An archive. Of Stickers. From the anarchist surroundings</description>
      |        ${content}
      |      </channel>
      |    </rss>
      |""".stripMargin

  private def itemsResponse(stickers: Seq[AggregatedSticker]): String = {
    stickers.map(itemResponse).mkString
  }

  private def itemResponse(aggregatedSticker: AggregatedSticker): String = {
    val ipfsCID = aggregatedSticker.links.find(link => link.`type`.equals(StickerLinkTypes.ORIGINAL)).get.ipfsCID
    val maybeNotes = aggregatedSticker.sticker.notes.map(value => s"Notes: $value").getOrElse("")
    val maybeSource = aggregatedSticker.sticker.source.map(value => s"Source: $value").getOrElse("")
    val maybeLanguage = aggregatedSticker.sticker.language.map(value => s"Language: ${value.name}").getOrElse("")
    s"""
        <item>
          <title>${aggregatedSticker.sticker.name}</title>
          <link>https://anarchiststickersarchive.org/stickers/${aggregatedSticker.sticker.id.get}</link>
          <media:content 
            medium="image" 
            url="https://gateway.ipfs.anarchiststickersarchive.org/ipfs/$ipfsCID" 
            width="500" 
          />
          <description>$maybeNotes \n $maybeSource \n $maybeLanguage</description>
        </item>
    """.stripMargin
  }

  val route: Route = get {
    queryHandler
      .handle(IndexByPageQuery(0, 10000, None))
      .fold(
        throwable => {
          complete(HttpResponse(BadRequest, entity = s"Something went wrong. throwable=${throwable.getMessage}"))
        },
        (stickers: Seq[AggregatedSticker]) => {
          complete(responseHeader(itemsResponse(stickers.sortWith(_.sticker.id.get > _.sticker.id.get).take(100))))
        }
      )
  }
}
