package acab.devcon0.input.scheduler

import acab.devcon0.commons.TryUtils
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.adapters.publish.PublishStickerCommandFacadeImpl
import acab.devcon0.domain.dtos.PublicationPlatforms
import acab.devcon0.domain.service.StickersService
import akka.actor.Scheduler
import com.typesafe.scalalogging.LazyLogging

import java.time.temporal.ChronoUnit
import java.time.{Duration, LocalDate, LocalDateTime, LocalTime}
import scala.concurrent.ExecutionContext.Implicits.global

class SchedulerWrapper(
    scheduler: Scheduler,
    configuration: Configuration,
    publishFacadeCommand: PublishStickerCommandFacadeImpl,
    stickersService: StickersService
) extends LazyLogging {

  def publications(): Unit = {

    val postingFrequencyHours: Long = configuration.postingFrequencyHours
    val initialDelay: Duration = getInitialDelay
    val interval = Duration.of(postingFrequencyHours, ChronoUnit.HOURS)
    logger.info(
      s"" +
        s"Next logging time at for general scheduler in about $initialDelay seconds. " +
        s"Then, publishing every $interval"
    )
    schedule(
      initialDelay = initialDelay,
      interval = interval,
      runnable = () => {
        stickersService.Sugar.getRandomNotPublishedAnywhere
          .flatMap(TryUtils.forceOptionExistence())
          .flatMap(sticker => {
            publishFacadeCommand
              .apply(
                stickerId = sticker.id,
                publicationPlatforms =
                  configuration.publicationPlatforms.map(value => PublicationPlatforms.withName(value))
              )
          })
      }
    )
  }

  private def schedule(initialDelay: Duration, interval: Duration, runnable: Runnable): Unit = {
    scheduler.scheduleAtFixedRate(
      initialDelay = initialDelay,
      interval = interval,
      runnable = runnable,
      executor = global
    )
  }

  private def getNextPostingTime: LocalDateTime = {
    val now = LocalDateTime.now()
    val postingFrequencyHours: Long = configuration.postingFrequencyHours
    val postingInitialDelayHours = configuration.postingInitialDelayHours
    val firstTimeForPostingInTheDay: LocalDateTime = LocalDate.now().atStartOfDay().plusHours(postingInitialDelayHours)
    var nextPostingTime: LocalDateTime = firstTimeForPostingInTheDay

    while (nextPostingTime.isBefore(now)) {
      nextPostingTime = nextPostingTime.plusHours(postingFrequencyHours)
    }
    logger.info(s"nextPostingTime=$nextPostingTime")
    nextPostingTime
  }

  private def getInitialDelay: Duration = {
    val nextPostingTime: LocalDateTime = getNextPostingTime
    val initialDelayInSeconds: Long = ChronoUnit.SECONDS.between(LocalDateTime.now(), nextPostingTime)
    Duration.of(initialDelayInSeconds, ChronoUnit.SECONDS)
  }
}
