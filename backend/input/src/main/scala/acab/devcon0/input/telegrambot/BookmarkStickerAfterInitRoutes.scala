package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.adapters.telegramfsm.TelegramBookmarkFSMQueryHandler
import acab.devcon0.domain.dtos.BookmarkUserPlatforms.TELEGRAM
import acab.devcon0.domain.dtos.TypeAliases.StickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.fsm.bookmarksticker._
import acab.devcon0.domain.ports.input.bookmarksticker._
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, TelegramRouteExecutionUtils, UIMessages}
import com.bot4s.telegram.models.{ChatId, Message}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class BookmarkStickerAfterInitRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler,
    val telegramBookmarkFSMExistsQueryHandler: TelegramFSMExistsQueryHandler,
    val telegramBookmarkFSMQueryHandler: TelegramBookmarkFSMQueryHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val telegramService: TelegramService,
    val stickerIndexBookmarkQueryHandler: StickerIndexBookmarkQueryHandler,
    val stickerAddBookmarkCommandHandler: StickerAddBookmarkCommandHandler,
    val stickerDeleteBookmarkCommandHandler: StickerDeleteBookmarkCommandHandler,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    bookmarkStickerInitCommandRoute()
  )

  private def bookmarkStickerInitCommandRoute(): DeclarativeRoute = {
    AuthorizedMessageRoute(
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = bookmarkStickerMessageAction(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "BookmarkStickerAfterInitRoutes")
    )
  }

  private def bookmarkStickerMessageAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val existsSomeFsmForThisUserValue: Try[Boolean] = existsSomeFsmForThisUser(userId)
    val existsMatchingFsmForThisUserValue: Try[Boolean] = existsMatchingFSMForThisUser(userId)
    val isAllBasicDataAtOnce: Try[Boolean] = doesMessageHasAllBasicData(message)
    (existsSomeFsmForThisUserValue, existsMatchingFsmForThisUserValue, isAllBasicDataAtOnce) match {
      case (Failure(exception), _, _)                    => handleExceptionCase(userId, exception)
      case (_, Failure(exception), _)                    => handleExceptionCase(userId, exception)
      case (Success(true), Success(false), _)            => handleOtherFSMTakingCareCase()
      case (Success(true), Success(true), Success(true)) => handleAnyMessageWhenFSMRunningCase(message)
      case (_, _, _)                                     => Future.successful()
    }
  }

  private def handleAnyMessageWhenFSMRunningCase(message: Message): Future[Unit] = {
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val fsmInstance: Try[TelegramBookmarkFSMInstance] = telegramBookmarkFSMQueryHandler.handle(TelegramFSMQuery(userId))
    val fsmState: Try[BookmarkStickerFSMState] = fsmInstance.map(_.state)
    val maybeEvent: Option[BookmarkStickerFSMEvent] = parseEvent(message)
    val bookmarkUserId = BookmarkUserId(userId.id, BookmarkUserPlatforms.TELEGRAM)
    (fsmInstance, fsmState, maybeEvent) match {
      case (Failure(exception), _, _) => handleExceptionCase(userId, exception)
      case (_, Failure(exception), _) => handleExceptionCase(userId, exception)
      case (Success(_), Success(_), Some(Exit)) =>
        telegramFSMDeleteCommandHandler
          .handle(TelegramFSMDeleteCommand(userId))
          .flatMap(_ => reply(userId = userId, text = UIMessages.bookmarkStickerAbort))
        Future.successful()
      case (Success(_), Success(AwaitingSubCommand), Some(Add)) =>
        putFSMOnState(userId, AwaitingNewStickerIds)
          .flatMap(_ => reply(userId = userId, text = UIMessages.bookmarkOnAdd))
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case (Success(_), Success(AwaitingSubCommand), Some(Delete)) =>
        putFSMOnState(userId, AwaitingStickerIdsToRemove)
          .flatMap(_ => reply(userId = userId, text = UIMessages.bookmarkOnDelete))
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case (Success(_), Success(AwaitingNewStickerIds), Some(StickerIdsSubmitted)) =>
        addStickerIds(message)
          .flatMap(_ => putFSMOnState(userId, AwaitingSubCommand))
          .flatMap(_ => sendAllBookmarks(bookmarkUserId = bookmarkUserId, chatId = message.source))
          .flatMap(_ => reply(userId = userId, text = UIMessages.afterBookmarkParentCommand))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(_), Success(AwaitingStickerIdsToRemove), Some(StickerIdsSubmitted)) =>
        deleteStickerIds(message)
          .flatMap(_ => putFSMOnState(userId, AwaitingSubCommand))
          .flatMap(_ => sendAllBookmarks(bookmarkUserId = bookmarkUserId, chatId = message.source))
          .flatMap(_ => reply(userId = userId, text = UIMessages.afterBookmarkParentCommand))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
    }
  }

  private def addStickerIds(message: Message): Try[Seq[StickerId]] = {
    val userId: TelegramUserId = TelegramUserId(
      ChatId(message.source).toEither.fold(long => long.toString, string => string)
    )
    val stickerIds: Seq[Long] = message.text.getOrElse("").filterNot(_.isWhitespace).split(',').toList.map(_.toLong)
    val bookmarkUserId = BookmarkUserId(userId.id, TELEGRAM)
    val cmd: StickerAddBookmarkCommand = StickerAddBookmarkCommand(stickerIds, bookmarkUserId)
    val commandResult: Try[StickerAddBookmarkEvent[_]] = stickerAddBookmarkCommandHandler.handle(cmd)
    StickerAddBookmarkEventProcessor.apply(commandResult, logger)
  }

  private def deleteStickerIds(message: Message): Try[Seq[StickerId]] = {
    val userId: TelegramUserId = TelegramUserId(
      ChatId(message.source).toEither.fold(long => long.toString, string => string)
    )
    val stickerIds: Seq[Long] = message.text.getOrElse("").filterNot(_.isWhitespace).split(',').toList.map(_.toLong)
    val bookmarkUserId = BookmarkUserId(userId.id, TELEGRAM)
    val cmd: StickerDeleteBookmarkCommand =
      StickerDeleteBookmarkCommand(stickerIds, bookmarkUserId)
    val commandResult: Try[StickerDeleteBookmarkEvent[_]] = stickerDeleteBookmarkCommandHandler.handle(cmd)
    StickerDeleteBookmarkEventProcessor.apply(commandResult, logger)
  }

  private def putFSMOnState(
      userId: TelegramUserId,
      state: BookmarkStickerFSMState
  ): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = TelegramBookmarkFSMInstance(
      id = userId,
      state = state,
      data = BookmarkStickerFSMData()
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def parseEvent(message: Message): Option[BookmarkStickerFSMEvent] = {
    message.text match {
      case Some(".") | Some(".cancel") => Some(Exit)
      case Some("a") | Some("add")     => Some(Add)
      case Some("d") | Some("delete")  => Some(Delete)
      case Some(_)                     => Some(StickerIdsSubmitted)
      case None                        => None
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def existsMatchingFSMForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramBookmarkFSMExistsQueryHandler.handle(TelegramFSMExistsQuery(id = userId))
  }

  private def doesMessageHasAllBasicData(message: Message): Try[Boolean] = Try {
    message.text.isDefined
  }

  private def handleExceptionCase(userId: TelegramUserId, exception: Throwable): Future[Unit] = {
    logger.error("bookmarkStickerMessageAction", exception)
    telegramFSMDeleteCommandHandler.handle(TelegramFSMDeleteCommand(userId))
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }

  private def sendAllBookmarks(bookmarkUserId: BookmarkUserId, chatId: ChatId): Try[Unit] = {
    getBookmarksMessage(bookmarkUserId)
      .map(replyMessage => sendMessage(replyMessage, chatId))
  }

  private def getBookmarksMessage(bookmarkUserId: BookmarkUserId): Try[Seq[String]] = {
    stickerIndexBookmarkQueryHandler
      .handle(StickerIndexBookmarkQuery(bookmarkUserId))
      .map(stickers => stringifyStickerList(stickers))
      .map(stickersIndex => stickersIndex.map(_.mkString("\n")))
  }

  private def stringifyStickerList(stickers: Seq[Sticker]): Seq[Seq[String]] = {
    stickers.map(sticker => sticker.id.get + " - " + sticker.name).grouped(100).toList
  }

  private def sendMessage(repliesMessage: Seq[String], chatId: ChatId): Unit = {
    if (repliesMessage.isEmpty) {
      val to: TelegramUserId = TelegramUserId(chatId.toEither.fold(long => long.toString, string => string))
      telegramService.sendMessage(to = to, text = "No bookmarks present in your library so far")
    } else {
      repliesMessage.foreach(replyMessage => {
        val to: TelegramUserId = TelegramUserId(chatId.toEither.fold(long => long.toString, string => string))
        telegramService.sendMessage(to = to, text = replyMessage)
      })
    }
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}
