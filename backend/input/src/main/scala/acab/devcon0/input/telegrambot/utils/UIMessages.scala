package acab.devcon0.input.telegrambot.utils

import acab.devcon0.domain.dtos.TypeAliases.{DraftStickerId, StickerId}

case object UIMessages {
  def afterAddCommand(draftStickerId: DraftStickerId): String = s"Yes, let's add a new sticker.\n" +
    s"The internal ID of your draft sticker is $draftStickerId.\n" +
    s"You can abort the process at any time by typing .cancel .\n" +
    s"Now, send me the sticker in uncompress way (file)."
  val afterBookmarkParentCommand: String =
    "Above you can see the stickers you bookmarked so far.\n" +
      "Use any of the following commands to edit your bookmark list:\n" +
      "+ (.)cancel: to exit this flow.\n" +
      "+ (a)dd: to add stickers to your bookmarks.\n" +
      "+ (d)elete: to delete stickers to your bookmarks."

  val afterAllBasicDataAtOnceAction: String =
    "At this moment the sticker can be published since I have sufficient data.\n" +
      "Available options, type any to continue:\n" +
      "+ (.)cancel: to abort the adding process.\n" +
      "+ (s)ave: to publish the sticker.\n" +
      "+ (l)anguage: specify the language of the text in the sticker.\n" +
      "+ (n)otes: specify the notes. You may include an English translation.\n" +
      "+ (so)urce: specify the source. You may include an author reference.\n" +
      "+ (dv) doVectorize: On-demand vectorization.\n" +
      "+ add(v)ector: add a vector (SVG) version."

  val afterIngestionFromFinalAction: String = "I need a title. Submit it:"

  val afterEditCommand: String =
    "Type any of the following options:\n" +
      "+ (.)cancel: to finish the edition process.\n" +
      "+ (l)anguage: edit/specify the language of the text in the sticker.\n" +
      "+ (n)ame: edit the name.\n" +
      "+ (no)otes: specify the notes. You may include an English translation.\n" +
      "+ (s)source: specify the source. You may include an author reference.\n" +
      "+ (v)ector: add/edit a vector (SVG) version.\n" +
      "+ (dv) doVectorize: On-demand vectorization.\n" +
      "+ (o)original: edit the original version."

  val editStickerAbort: String = "Aborting sticker edition process. Maybe start over?"
  val addStickerAbort: String = "Aborting draft sticker publishing process. Maybe start over?"
  val addStickerSkip: String = "Skipping draft sticker publishing process. Maybe start over?"
  val bookmarkStickerAbort: String = "Exiting the bookmark flow"
  val bookmarkOnAdd: String =
    "Send the list of sticker ids you want to bookmark in a comma-separated list.\n" +
      "For example: 1,2,3. Or: 1, 2, 3. Spaces will be ignored.\n" +
      "If you want, you can also type .cancel"
  val bookmarkOnDelete: String =
    "Send the list of sticker ids you want to delete from your bookmark list in a comma-separated list.\n" +
      "For example: 1,2,3. Or: 1, 2, 3. Spaces will be ignored.\n" +
      "If you want, you can also type .cancel"
  val afterAcceptingDraftSticker: String = "Your sticker got published.\n" +
    "Check it below.\n" +
    "Remember you can edit it if there's incorrect data."
  val afterSuggestingDraftSticker: String = "Your sticker got submitted. " +
    "The admins will review it to warranty data & metadata quality.\n" +
    "Check the index from time to time to see if your submission was accepted.\n" +
    "Check a preview of what you've just submitted:\n"

  val submissionAccepted: String = "Submitted sticker was accepted."
  val submissionRejected: String = "Submitted sticker was rejected."

  val onUpsertNotes: String =
    "Type some notes like translated caption to english or give some context.\n" +
      "If you want, you can also type .cancel"
  val onUpsertSource: String =
    "Type some information about the author or the source.\n" +
      "If you want, you can also type .cancel"
  val onUpsertName: String =
    "Type a new name for the sticker.\n" +
      "If you want, you can also type .cancel."
  val onVectorizeOnDemandRequest: String = "Sending the sticker for vectorization, this might take a bit (~20secs)"
  def onSuccessfulVectorizeOnDemand(stickerId: StickerId): String = s"" +
    s"The sticker (id=$stickerId) was vectorized successfully, check it now."
  val onSuccessfulVectorizeOnDemand: String = "Vectorization on demand was successful."
  val onUpsertLanguage: String = "Type the language in 2 digits format or type .cancel"
  val onUpsertVector: String = "Send the (SVG) sticker in uncompress way (file) or type .cancel"
  val onUpsertOriginalSticker: String = "Done. Now, What's the title of the sticker?"
  val onUpsertOriginal: String = "Done. Now, send me the sticker in uncompress way (file)."
  val publishingFailed: String = "Publishing failed, check the logs"
  val submissionFailed: String = "Submission failed, maybe file extensions are not accepted?"
  val publishingSuccessful: String = "Publishing was successful"
  val refreshCommandSuccessful: String = "Refreshing command succeeded. Wait 10 minutes before checking the website."
  def deletingSuccessful(stickerId: StickerId): String = s"Deleting of sticker=${stickerId} was successful."
}
