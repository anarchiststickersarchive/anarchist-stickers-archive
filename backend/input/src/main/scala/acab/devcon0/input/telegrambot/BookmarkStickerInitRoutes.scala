package acab.devcon0.input.telegrambot

import acab.devcon0.domain.adapters.telegramfsm.TelegramBookmarkFSMQueryHandler
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.fsm.bookmarksticker
import acab.devcon0.domain.fsm.bookmarksticker._
import acab.devcon0.domain.ports.input.bookmarksticker._
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, TelegramRouteExecutionUtils, UIMessages}
import com.bot4s.telegram.models.{ChatId, Message}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class BookmarkStickerInitRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler,
    val telegramBookmarkFSMExistsQueryHandler: TelegramFSMExistsQueryHandler,
    val telegramBookmarkFSMQueryHandler: TelegramBookmarkFSMQueryHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val telegramService: TelegramService,
    val stickerIndexBookmarkQueryHandler: StickerIndexBookmarkQueryHandler,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    bookmarkStickerInitCommandRoute()
  )

  private def bookmarkStickerInitCommandRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/bookmark",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = bookmarkStickerMessageAction(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "BookmarkStickerInitRoute")
    )
  }

  private def bookmarkStickerMessageAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)

    existsSomeFsmForThisUser(userId) match {
      case Failure(exception) => handleExceptionCase(userId, exception)
      case Success(true)      => handleOtherFSMTakingCareCase()
      case Success(false)     => handleBookmarkStickerCommandSuccessCase(message.source)
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def handleBookmarkStickerCommandSuccessCase(chatId: ChatId): Future[Unit] = {
    val userId: TelegramUserId = TelegramUserId(chatId.toEither.fold(long => long.toString, string => string))
    val executionResult: Try[Unit] = for {
      _ <- sendAllBookmarks(BookmarkUserId(userId.id, BookmarkUserPlatforms.TELEGRAM), chatId = chatId)
      initFSMResult <- initFSM(userId).flatMap(_ =>
        reply(userId = userId, text = UIMessages.afterBookmarkParentCommand)
      )
    } yield initFSMResult
    postProcessExecutionResult(executionResult)
  }

  private def sendAllBookmarks(bookmarkUserId: BookmarkUserId, chatId: ChatId): Try[Unit] = {
    getBookmarksMessage(bookmarkUserId)
      .map(replyMessage => sendMessage(replyMessage, chatId))
  }

  private def getBookmarksMessage(bookmarkUserId: BookmarkUserId): Try[Seq[String]] = {
    stickerIndexBookmarkQueryHandler
      .handle(StickerIndexBookmarkQuery(bookmarkUserId))
      .map(stickers => stringifyStickerList(stickers))
      .map(stickersIndex => stickersIndex.map(_.mkString("\n")))
  }

  private def stringifyStickerList(stickers: Seq[Sticker]): Seq[Seq[String]] = {
    stickers.map(sticker => sticker.id.get + " - " + sticker.name).grouped(100).toList
  }

  private def sendMessage(repliesMessage: Seq[String], chatId: ChatId): Unit = {
    if (repliesMessage.isEmpty) {
      val to: TelegramUserId = TelegramUserId(chatId.toEither.fold(long => long.toString, string => string))
      telegramService.sendMessage(to = to, text = "No bookmarks present in your library so far")
    } else {
      repliesMessage.foreach(replyMessage => {
        val to: TelegramUserId = TelegramUserId(chatId.toEither.fold(long => long.toString, string => string))
        telegramService.sendMessage(to = to, text = replyMessage)
      })
    }
  }

  private def initFSM(userId: TelegramUserId): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = bookmarksticker.TelegramBookmarkFSMInstance(
      id = userId,
      state = AwaitingSubCommand,
      data = BookmarkStickerFSMData()
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def handleExceptionCase(userId: TelegramUserId, exception: Throwable): Future[Unit] = {
    logger.error("bookmarkStickerMessageAction", exception)
    telegramFSMDeleteCommandHandler.handle(TelegramFSMDeleteCommand(userId))
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}
