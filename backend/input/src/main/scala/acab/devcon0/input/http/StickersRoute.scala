package acab.devcon0.input.http

import acab.devcon0.domain.dtos.{AggregatedSticker, Sticker}
import acab.devcon0.domain.ports.input.index.{
  IndexAllQuery,
  IndexAllQueryHandler,
  IndexByIdQuery,
  IndexByIdQueryHandler,
  IndexByPageQuery,
  IndexByPageQueryHandler
}
import acab.devcon0.input.http.CirceCodecs.Encoders.{
  aggregatedStickerGetResponse2Encoder,
  stickersCountGetResponseEncoder
}
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import io.circe.Encoder._
import io.circe.syntax._

class StickersRoute(
    queryHandler: IndexByPageQueryHandler,
    indexAllQueryHandler: IndexAllQueryHandler,
    indexByIdQueryHandler: IndexByIdQueryHandler
) {

  val indexRoute: Route = get {
    parameters("page".as[Int].withDefault(0), "pageSize".as[Int].withDefault(100), "q".as[String].optional) {
      (page, pageSize, searchTerm) =>
        queryHandler
          .handle(IndexByPageQuery(page, pageSize, searchTerm))
          .fold(
            throwable => {
              complete(HttpResponse(BadRequest, entity = s"Something went wrong. throwable=${throwable.getMessage}"))
            },
            (stickers: Seq[AggregatedSticker]) => {
              val response: Seq[AggregatedStickerGetResponse2] = stickers.map(aggregatedSticker =>
                AggregatedStickerGetResponse2(
                  id = aggregatedSticker.sticker.id,
                  name = aggregatedSticker.sticker.name,
                  notes = aggregatedSticker.sticker.notes,
                  source = aggregatedSticker.sticker.source,
                  language = aggregatedSticker.sticker.language.map(languageCode =>
                    StickerLanguage(
                      iso639 = languageCode.value,
                      name = languageCode.name,
                      nativeNames = languageCode.native
                    )
                  ),
                  creationTimestamp = aggregatedSticker.sticker.creationTimestamp,
                  links = aggregatedSticker.links.map(link => link.`type` -> link).toMap,
                  stickerDirectoryRelation = aggregatedSticker.stickerDirectoryRelation
                )
              )
              complete(response.asJson.toString())
            }
          )
    }
  }

  val countRoute: Route = path("_count") {
    get {
      indexAllQueryHandler
        .handle(IndexAllQuery())
        .fold(
          throwable => {
            complete(HttpResponse(BadRequest, entity = s"Something went wrong. throwable=${throwable.getMessage}"))
          },
          (stickers: Seq[Sticker]) => {
            val response: StickersCountGetResponse = StickersCountGetResponse(count = stickers.length)
            complete(response.asJson.toString())
          }
        )

    }
  }

  val idRoute: Route = path(LongNumber) { stickerId: Long =>
    get {
      indexByIdQueryHandler
        .handle(IndexByIdQuery(stickerId))
        .fold(
          throwable => {
            complete(HttpResponse(BadRequest, entity = s"Something went wrong. throwable=${throwable.getMessage}"))
          },
          (aggregatedSticker: AggregatedSticker) => {
            val response: AggregatedStickerGetResponse2 =
              AggregatedStickerGetResponse2(
                id = aggregatedSticker.sticker.id,
                name = aggregatedSticker.sticker.name,
                notes = aggregatedSticker.sticker.notes,
                source = aggregatedSticker.sticker.source,
                language = aggregatedSticker.sticker.language.map(languageCode =>
                  StickerLanguage(
                    iso639 = languageCode.value,
                    name = languageCode.name,
                    nativeNames = languageCode.native
                  )
                ),
                creationTimestamp = aggregatedSticker.sticker.creationTimestamp,
                links = aggregatedSticker.links.map(link => link.`type` -> link).toMap,
                stickerDirectoryRelation = aggregatedSticker.stickerDirectoryRelation
              )
            complete(response.asJson.toString())
          }
        )
    }
  }
}
