package acab.devcon0.input.telegrambot

import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.ports.input.searchsticker.{SearchStickerQuery, SearchStickerQueryHandler}
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{ArgumentParser, RoutesAuthorizationUtils, TelegramRouteExecutionUtils}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.{Success, Try}

class SearchRoutes(
    val searchStickerQueryHandler: SearchStickerQueryHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    searchRoute()
  )

  private def searchRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/search",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = searchAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "searchRoute")
    )
  }

  private def searchAction: Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    val executionResult: Try[Unit] = searchStickerQueryHandler
      .handle(SearchStickerQuery(searchTerm = args.mkString(" ")))
      .map(stickersSearchResults =>
        stickersSearchResults.map(sticker => sticker.id.get + " - " + sticker.name).grouped(100).toList
      )
      .map(stickersSearchResultsGrouped => stickersSearchResultsGrouped.map(_.mkString("\n")))
      .map(stickersSearchResultsGroupedAndFormatted =>
        stickersSearchResultsGroupedAndFormatted.map(sticker => {
          telegramService.sendMessage(TelegramUserId(message.source.toString), sticker)
        })
      )
      .flatMap(listOfTries =>
        listOfTries.find(triedExecution => triedExecution.isFailure) match {
          case Some(failedTry) => failedTry
          case None            => Success(())
        }
      )
    postProcessExecutionResult(executionResult)
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}
