package acab.devcon0.input.http

import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerName, StickerNotes, StickerSource}
import acab.devcon0.domain.dtos.{Sticker, StickerDirectoryRelation, StickerLink}

import java.time.LocalDateTime

case class AggregatedStickerGetResponse(
    sticker: Sticker,
    links: Seq[StickerLink],
    stickerDirectoryRelation: StickerDirectoryRelation
)

case class StickerLanguage(iso639: String, name: String, nativeNames: List[String])

case class AggregatedStickerGetResponse2(
    id: MaybeStickerId = None,
    name: StickerName,
    notes: StickerNotes = None,
    source: StickerSource = None,
    language: Option[StickerLanguage] = None,
    creationTimestamp: LocalDateTime,
    links: Map[StickerLinkType, StickerLink],
    stickerDirectoryRelation: StickerDirectoryRelation
)

case class StickersCountGetResponse(count: Int)
