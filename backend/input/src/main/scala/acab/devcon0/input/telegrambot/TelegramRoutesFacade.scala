package acab.devcon0.input.telegrambot

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.api.declarative.whenOrElse

import scala.concurrent.Future

/* Responsible for auth & params parsing */
/** Telegram list of commands, update & set when modified
  * index - list stickers by id
  * indexnonpublished - list unpublished stickers by id
  * indexstats - provides some stats about the index
  * search - look up terms at the title, authors and translated captions
  * preview - preview by id
  * add - add a new sticker
  * publish - publish by id (Only for authorized users)
  * accept - publish by draft id (Only for authorized users)
  * reject - rejects by draft id (Only for authorized users)
  * review - review by draft id (Only for authorized users)
  * edit - edit by id an existing sticker (Only for authorized users)
  * delete - delete by id an existing sticker (Only for authorized users)
  */
class TelegramRoutesFacade(
    val config: Configuration,
    override val client: RequestHandler[Future],
    val previewStickerRoutes: PreviewStickerRoutes,
    val publishStickerRoutes: PublishStickerRoutes,
    val indexRoutes: IndexRoutes,
    val searchRoutes: SearchRoutes,
    val editStickerRoutes: EditStickerRoutes,
    val editStickerInitRoutes: EditStickerInitRoutes,
    val addStickerRoutes: AddStickerRoutes,
    val addStickerNewRoutes: AddStickerNewRoutes,
    val addStickerBasicDataAtOnceRoutes: AddStickerBasicDataAtOnceRoutes,
    val acceptRoutes: AcceptDraftStickerRoutes,
    val rejectDraftStickerRoutes: RejectDraftStickerRoutes,
    val reviewDraftStickerRoutes: ReviewDraftStickerRoutes,
    val bookmarkStickerInitRoutes: BookmarkStickerInitRoutes,
    val bookmarkStickerAfterInitRoutes: BookmarkStickerAfterInitRoutes,
    val deleteStickerRoutes: DeleteStickerRoutes,
    val addStickerFromIngestionFinalRoutes: AddStickerFromIngestionFinalRoutes,
    val refreshCommandRoutes: RefreshCommandRoutes
) extends RouteFacade {

  implicit val adminUsers: Seq[AuthorizedUser] = List(TelegramUserId(config.telegram.adminUserId.id))
  private val routes: Seq[DeclarativeRoute] = editStickerRoutes.routes ++
    editStickerInitRoutes.routes ++
    addStickerRoutes.routes ++
    addStickerNewRoutes.routes ++
    addStickerBasicDataAtOnceRoutes.routes ++
    indexRoutes.routes ++
    previewStickerRoutes.routes ++
    publishStickerRoutes.routes ++
    searchRoutes.routes ++
    acceptRoutes.routes ++
    rejectDraftStickerRoutes.routes ++
    reviewDraftStickerRoutes.routes ++
    bookmarkStickerInitRoutes.routes ++
    bookmarkStickerAfterInitRoutes.routes ++
    deleteStickerRoutes.routes ++
    addStickerFromIngestionFinalRoutes.routes ++
    refreshCommandRoutes.routes

  routes.foreach {
    case AuthorizedCommandRoute(command, action, authorityFilter, unauthorizedAccessAction) =>
      whenOrElse(onCommand(command), authorityFilter)(action)(unauthorizedAccessAction)
    case AuthorizedMessageRoute(action, authorityFilter, unauthorizedAccessAction) =>
      whenOrElse(onMessage, authorityFilter)(action)(unauthorizedAccessAction)
    case PublicCommandDeclarativeRoute(command, action) =>
      onCommand(command)(action)
    case PublicActionDeclarativeRoute(action) =>
      onMessage(action)
  }
}
