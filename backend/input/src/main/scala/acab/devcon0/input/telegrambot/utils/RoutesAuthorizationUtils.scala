package acab.devcon0.input.telegrambot.utils

import acab.devcon0.commons.FunctionUtils
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import com.bot4s.telegram.api.declarative.{Action, Filter}
import com.bot4s.telegram.models.Message

import scala.concurrent.Future

object RoutesAuthorizationUtils {

  def isAuthorized(implicit adminUsers: Seq[AuthorizedUser]): Filter[Message] = { message =>
    isAuthorizedUser(message, adminUsers)
  }

  def isAuthorizedOrPrivateChat(implicit adminUsers: Seq[AuthorizedUser]): Filter[Message] = { message =>
    isAuthorizedUser(message, adminUsers) || isPrivateChat(message)
  }

  def unauthorizedAccessAction(telegramService: TelegramService, who: String): Action[Future, Message] = {
    implicit message =>
      notifyUnauthorizedAccess(telegramService, who, TelegramUserId(message.source.toString))
  }

  def notifyUnauthorizedAccess(telegramService: TelegramService, who: String, userId: TelegramUserId): Future[Unit] = {
    telegramService
      .sendMessage(userId, s"Unauthorized access ($who)")
      .fold(FunctionUtils.flushResultToFuture, FunctionUtils.flushResultToFuture)
  }

  private def isAuthorizedUser(message: Message, adminUsers: Seq[AuthorizedUser]): Boolean = {
    adminUsers.map(_.id).contains(message.chat.id.toString)
  }

  private def isPrivateChat(message: Message): Boolean = {
    message.from.map(_.id).contains(message.source)
  }
}
