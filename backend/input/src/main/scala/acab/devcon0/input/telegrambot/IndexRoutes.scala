package acab.devcon0.input.telegrambot

import acab.devcon0.domain.dtos.{Sticker, TelegramUserId}
import acab.devcon0.domain.ports.input.index._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, TelegramRouteExecutionUtils}
import com.bot4s.telegram.models.{ChatId, Message}

import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

class IndexRoutes(
    val indexAllQueryHandler: IndexAllQueryHandler,
    val indexUnpublishedQueryHandler: IndexUnpublishedQueryHandler,
    val indexStatsQueryHandler: IndexStatsQueryHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    indexRoute(),
    indexNonPublishedRoute(),
    indexStatsRoute()
  )

  private def indexRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/index",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = indexAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "indexRoute")
    )
  }

  private def indexNonPublishedRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/indexNonPublished",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = indexNonPublishedAction,
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "indexNonPublishedRoute")
    )
  }

  private def indexStatsRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/indexStats",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = indexStatsAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "indexStatsRoute")
    )
  }

  private def indexAction: Message => Future[Unit] = { message: Message =>
    val executionResult: Try[Unit] = getAllReplyMessage
      .map(replyMessage => sendMessage(replyMessage, message.source))
    postProcessExecutionResult(executionResult)
  }

  private def getAllReplyMessage: Try[Seq[String]] = {
    indexAllQueryHandler
      .handle(IndexAllQuery())
      .map(stickers => stringifyStickerList(stickers))
      .map(stickersIndex => stickersIndex.map(_.mkString("\n")))
  }

  private def indexNonPublishedAction: Message => Future[Unit] = { message: Message =>
    val executionResult: Try[Unit] = getNonPublishedAnywhereReplyMessage
      .map(replyMessage => sendMessage(replyMessage, message.source))
    postProcessExecutionResult(executionResult)
  }

  private def getNonPublishedAnywhereReplyMessage: Try[Seq[String]] = {
    indexUnpublishedQueryHandler
      .handle(IndexUnpublishedQuery())
      .map(stickers => stringifyStickerList(stickers))
      .map(stickersIndex => stickersIndex.map(_.mkString("\n")))
  }

  private def indexStatsAction: Message => Future[Unit] = { message: Message =>
    val executionResult: Try[Unit] = indexStatsQueryHandler
      .handle(IndexStatsQuery())
      .flatMap(queryResponse =>
        telegramService.sendMessage(to = TelegramUserId(message.source.toString), text = queryResponse)
      )
    postProcessExecutionResult(executionResult)
  }

  private def stringifyStickerList(stickers: Seq[Sticker]): Seq[Seq[String]] = {
    stickers.map(sticker => sticker.id.get + " - " + sticker.name).grouped(100).toList
  }

  private def sendMessage(repliesMessage: Seq[String], chatId: ChatId): Unit = {
    repliesMessage.foreach(replyMessage => {
      val to: TelegramUserId = TelegramUserId(chatId.toEither.fold(long => long.toString, string => string))
      telegramService.sendMessage(to = to, text = replyMessage)
    })
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}
