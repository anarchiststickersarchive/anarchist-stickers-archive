package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.adapters.telegramfsm.TelegramEditFSMQueryHandler
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLinkId}
import acab.devcon0.domain.dtos.{StickerLinkTypes, TelegramUserId}
import acab.devcon0.domain.fsm.editsticker._
import acab.devcon0.domain.ports.input.editsticker._
import acab.devcon0.domain.ports.input.preview.{PreviewStickerCommand, PreviewStickerCommandHandler, PreviewStickerEvent, PreviewStickerEventProcessor}
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class EditStickerRoutes(
    val stickerUpsertTextFieldCommandHandler: StickerUpsertTextFieldCommandHandler,
    val previewStickerCommandHandler: PreviewStickerCommandHandler,
    val stickerUpsertBinaryCommandHandler: StickerUpsertBinaryCommandHandler,
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler,
    val telegramEditFSMExistsQueryHandler: TelegramFSMExistsQueryHandler,
    val telegramEditFSMQueryHandler: TelegramEditFSMQueryHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    editStickerMessageRoute()
  )

  private def editStickerMessageRoute(): DeclarativeRoute = {
    PublicActionDeclarativeRoute(
      action = editStickerMessageAction
    )
  }

  private def editStickerMessageAction: Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    lazy val existsSomeFsmForThisUserValue: Try[Boolean] = existsSomeFsmForThisUser(userId)
    lazy val existsMatchingFsmForThisUserValue: Try[Boolean] = existsMatchingFSMForThisUser(userId)
    (existsSomeFsmForThisUserValue, existsMatchingFsmForThisUserValue) match {
      case (Failure(exception), _)         => handleExceptionCase(exception)
      case (_, Failure(exception))         => handleExceptionCase(exception)
      case (Success(true), Success(false)) => handleOtherFSMTakingCareCase()
      case (Success(true), Success(true))  => handleAnyMessageWhenFSMRunningCase(message)
      case (_, _)                          => Future.successful()
    }
    Future.successful()
  }

  private def previewEditingStickerWithFollowUpMessage(userId: TelegramUserId, stickerId: MaybeStickerId): Try[Unit] = {
    previewEditingSticker(userId, stickerId)
      .flatMap(_ => reply(userId = userId, text = UIMessages.afterEditCommand))
  }

  private def previewEditingSticker(userId: TelegramUserId, stickerId: MaybeStickerId): Try[Unit] = {
    val command: PreviewStickerCommand = PreviewStickerCommand(stickerId = stickerId, telegramUserId = userId)
    val commandResult: Try[PreviewStickerEvent[_]] = previewStickerCommandHandler.handle(command)
    PreviewStickerEventProcessor
      .apply(commandResult, logger)
      .map(_ => ())
  }

  private def handleAnyMessageWhenFSMRunningCase(message: Message): Future[Unit] = {
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    if (!adminUsers.contains(userId)) {
      RoutesAuthorizationUtils.notifyUnauthorizedAccess(telegramService, "handleAnyMessageWhenFSMRunningCase", userId)
    } else {
      lazy val fsmInstance: Try[TelegramEditFSMInstance] = telegramEditFSMQueryHandler.handle(TelegramFSMQuery(userId))
      lazy val fsmState: Try[EditStickerFSMState] = fsmInstance.map(_.state)
      lazy val maybeEvent: Option[EditStickerFSMEvent] = parseEvent(message)
      (fsmInstance, fsmState, maybeEvent) match {
        case (Failure(exception), _, _) => handleExceptionCase(exception)
        case (_, Failure(exception), _) => handleExceptionCase(exception)
        case (Success(_), _, Some(Cancel)) =>
          telegramFSMDeleteCommandHandler
            .handle(TelegramFSMDeleteCommand(userId))
            .flatMap(_ => reply(userId = userId, text = UIMessages.editStickerAbort))
          Future.successful()
        case (Success(instance), Success(AwaitingNameSubmission), Some(TextSubmitted)) =>
          upsertName(message, instance.data.stickerId)
            .flatMap(_ => upsertFSMForAwaitingForFieldToEdit(userId, instance.data.stickerId))
            .flatMap(_ => previewEditingStickerWithFollowUpMessage(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingNotesSubmission), Some(TextSubmitted)) =>
          upsertNotes(message, instance.data.stickerId)
            .flatMap(_ => upsertFSMForAwaitingForFieldToEdit(userId, instance.data.stickerId))
            .flatMap(_ => previewEditingStickerWithFollowUpMessage(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingSourceSubmission), Some(TextSubmitted)) =>
          upsertSource(message, instance.data.stickerId)
            .flatMap(_ => upsertFSMForAwaitingForFieldToEdit(userId, instance.data.stickerId))
            .flatMap(_ => previewEditingStickerWithFollowUpMessage(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingLanguageSubmission), Some(TextSubmitted)) =>
          upsertLanguage(message, instance.data.stickerId)
            .flatMap(_ => upsertFSMForAwaitingForFieldToEdit(userId, instance.data.stickerId))
            .flatMap(_ => previewEditingStickerWithFollowUpMessage(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingVectorSubmission), Some(DocumentSubmitted)) =>
          upsertLink(
            stickerId = instance.data.stickerId,
            stickerLinkType = StickerLinkTypes.VECTOR,
            telegramFileId = message.document.get.fileId,
            extension = message.document.get.fileName.get.split('.').last
          ).flatMap(_ => upsertFSMForAwaitingForFieldToEdit(userId, instance.data.stickerId))
            .flatMap(_ => previewEditingStickerWithFollowUpMessage(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingOriginalSubmission), Some(DocumentSubmitted)) =>
          upsertLink(
            stickerId = instance.data.stickerId,
            stickerLinkType = StickerLinkTypes.ORIGINAL,
            telegramFileId = message.document.get.fileId,
            extension = message.document.get.fileName.get.split('.').last
          ).flatMap(_ => upsertFSMForAwaitingForFieldToEdit(userId, instance.data.stickerId))
            .flatMap(_ => previewEditingStickerWithFollowUpMessage(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(VectorizeOnDemand)) =>
          reply(
            userId = userId,
            text = UIMessages.onVectorizeOnDemandRequest
          )
          upsertVectorOnDemandCommand(
            stickerId = instance.data.stickerId
          ).flatMap(_ =>
            reply(
              userId = userId,
              text = UIMessages.onSuccessfulVectorizeOnDemand(instance.data.stickerId.get)
            )
          ).flatMap(_ => previewEditingSticker(userId, instance.data.stickerId))
            .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(EditName)) =>
          putFSMOnState(userId, instance.data.stickerId, AwaitingNameSubmission)
            .flatMap(_ =>
              reply(
                userId = userId,
                text = UIMessages.onUpsertName
              )
            )
            .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(EditNotes)) =>
          putFSMOnState(userId, instance.data.stickerId, AwaitingNotesSubmission)
            .flatMap(_ =>
              reply(
                userId = userId,
                text = UIMessages.onUpsertNotes
              )
            )
            .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(EditSource)) =>
          putFSMOnState(userId, instance.data.stickerId, AwaitingSourceSubmission)
            .flatMap(_ =>
              reply(
                userId = userId,
                text = UIMessages.onUpsertSource
              )
            )
            .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(EditLanguage)) =>
          putFSMOnState(userId, instance.data.stickerId, AwaitingLanguageSubmission)
            .flatMap(_ =>
              reply(
                userId = userId,
                text = UIMessages.onUpsertLanguage
              )
            )
            .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(EditVector)) =>
          putFSMOnState(userId, instance.data.stickerId, AwaitingVectorSubmission)
            .flatMap(_ =>
              reply(
                userId = userId,
                text = UIMessages.onUpsertVector
              )
            )
            .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
        case (Success(instance), Success(AwaitingForFieldToEdit), Some(EditOriginal)) =>
          putFSMOnState(userId, instance.data.stickerId, AwaitingOriginalSubmission)
            .flatMap(_ =>
              reply(
                userId = userId,
                text = UIMessages.onUpsertOriginal
              )
            )
            .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      }
    }
  }

  private def parseEvent(message: Message): Option[EditStickerFSMEvent] = {
    message.document match {
      case Some(_) => Some(DocumentSubmitted)
      case None =>
        message.caption.orElse(message.text).map(_.toLowerCase()) match {
          case Some(".") | Some(".cancel")      => Some(Cancel)
          case Some("n") | Some("name")         => Some(EditName)
          case Some("l") | Some("language")     => Some(EditLanguage)
          case Some("no") | Some("notes")       => Some(EditNotes)
          case Some("so") | Some("source")      => Some(EditSource)
          case Some("v") | Some("vector")       => Some(EditVector)
          case Some("dv") | Some("doVectorize") => Some(VectorizeOnDemand)
          case Some("o") | Some("original")     => Some(EditOriginal)
          case Some(_)                          => Some(TextSubmitted)
          case None                             => None
        }
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def existsMatchingFSMForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramEditFSMExistsQueryHandler.handle(TelegramFSMExistsQuery(id = userId))
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def handleExceptionCase(exception: Throwable): Future[Unit] = {
    logger.error("addStickerMessageAction", exception)
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }

  private def upsertName(message: Message, stickerId: MaybeStickerId): Try[MaybeStickerId] = {
    val cmd = StickerUpsertNameCommand(stickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertNotes(message: Message, stickerId: MaybeStickerId): Try[MaybeStickerId] = {
    val cmd = StickerUpsertNotesCommand(stickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertSource(message: Message, stickerId: MaybeStickerId): Try[MaybeStickerId] = {
    val cmd = StickerUpsertSourceCommand(stickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertLanguage(message: Message, stickerId: MaybeStickerId): Try[MaybeStickerId] = {
    val cmd = StickerUpsertLanguageCommand(stickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertDraftStickerTextField(cmd: StickerUpsertTextFieldCommand): Try[MaybeStickerId] = {
    val commandResult: Try[StickerUpsertTextFieldEvent[_]] = stickerUpsertTextFieldCommandHandler.handle(cmd)
    StickerUpsertTextFieldEventProcessor.apply(commandResult, logger)
  }

  private def putFSMOnState(
      userId: TelegramUserId,
      stickerId: MaybeStickerId,
      state: EditStickerFSMState
  ): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = TelegramEditFSMInstance(
      id = userId,
      state = state,
      data = EditStickerFSMData(stickerId = stickerId)
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def upsertFSMForAwaitingForFieldToEdit(
      userId: TelegramUserId,
      stickerId: MaybeStickerId
  ): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = TelegramEditFSMInstance(
      id = userId,
      state = AwaitingForFieldToEdit,
      data = EditStickerFSMData(stickerId = stickerId)
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def upsertLink(
      stickerId: MaybeStickerId,
      stickerLinkType: StickerLinkType,
      telegramFileId: String,
      extension: String
  ): Try[StickerLinkId] = {
    val command: StickerUpsertBinaryCommand = StickerUpsertBinaryTelegramCommand(
      stickerId = stickerId,
      stickerLinkType = stickerLinkType,
      telegramFileId = telegramFileId,
      extension = extension
    )
    val commandResult: Try[StickerUpsertBinaryEvent[_]] = stickerUpsertBinaryCommandHandler.handle(command)
    StickerUpsertBinaryEventProcessor.apply(commandResult, logger)
  }

  private def upsertVectorOnDemandCommand(stickerId: MaybeStickerId): Try[StickerLinkId] = {
    val command: StickerUpsertBinaryCommand = StickerUpsertVectorOnDemandCommand(stickerId)
    val commandResult: Try[StickerUpsertBinaryEvent[_]] = stickerUpsertBinaryCommandHandler.handle(command)
    StickerUpsertBinaryEventProcessor.apply(commandResult, logger)
  }
}
