package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.deletesticker.StickerDeleteCommandImplicits.StickerDeleteCommandEventFlattenOps
import acab.devcon0.domain.ports.input.deletesticker.{StickerDeleteCommand, StickerDeleteCommandHandler}
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{ArgumentParser, RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class DeleteStickerRoutes(
    val stickerDeleteCommandHandler: StickerDeleteCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    deleteStickerMessageRoute()
  )

  private def deleteStickerMessageRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/delete",
      authorityFilter = RoutesAuthorizationUtils.isAuthorized(adminUsers),
      action = deleteAction(),
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "deleteRoute")
    )
  }

  private def deleteAction(): Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    val stickerId: MaybeStickerId = args.headOption.map(_.toLong)
    val command = StickerDeleteCommand(stickerId.get)
    val commandResult: Try[Sticker] = stickerDeleteCommandHandler.handle(command).flattenEvents
    commandResult match {
      case Success(sticker) =>
        reply(
          TelegramUserId(message.source.toString),
          UIMessages.deletingSuccessful(sticker.id.get)
        ).fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case Failure(throwable) => notifyException(message)(throwable)
    }
  }

  private def notifyException(message: Message): Throwable => Future[Unit] = { throwable =>
    {
      logger.error(s"throwable=$throwable")
      reply(userId = TelegramUserId(message.source.toString), text = UIMessages.publishingFailed)
        .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
    }
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }
}
