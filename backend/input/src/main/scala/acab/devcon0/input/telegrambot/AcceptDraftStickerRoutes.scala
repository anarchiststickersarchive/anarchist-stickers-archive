package acab.devcon0.input.telegrambot

import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.ports.input.addsticker.DraftStickerAcceptCommandImplicits.DraftStickerAcceptCommandEventFlattenOps
import acab.devcon0.domain.ports.input.addsticker.{DraftStickerAcceptCommand, DraftStickerAcceptCommandHandler}
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{
  ArgumentParser,
  RoutesAuthorizationUtils,
  TelegramRouteExecutionUtils,
  UIMessages
}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import java.util.UUID
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

class AcceptDraftStickerRoutes(
    val draftStickerAcceptCommandHandler: DraftStickerAcceptCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    acceptRoute()
  )

  private def acceptRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/accept",
      authorityFilter = RoutesAuthorizationUtils.isAuthorized(adminUsers),
      action = acceptAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "acceptRoute")
    )
  }

  def acceptAction: Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    postProcessExecutionResult(executionResult = for {
      arguments <- AcceptByIdRouteArguments.parser(args)
      command = DraftStickerAcceptCommand(draftStickerId = arguments.draftStickerId)
      commandResult <- draftStickerAcceptCommandHandler.handle(command).flattenEvents
      notificationResult <- telegramService.sendMessage(
        to = TelegramUserId(message.source.toString),
        text = UIMessages.submissionAccepted
      )
    } yield notificationResult)
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}

object AcceptByIdRouteArguments {
  def parser(args: Args): Try[AcceptByIdRouteArguments] = Try {
    val draftStickerId: DraftStickerId = args.headOption.map(UUID.fromString).get
    AcceptByIdRouteArguments(draftStickerId)

  }
}
case class AcceptByIdRouteArguments(draftStickerId: DraftStickerId)
