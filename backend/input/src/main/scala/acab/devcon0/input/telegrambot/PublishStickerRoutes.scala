package acab.devcon0.input.telegrambot

import acab.devcon0.domain.adapters.publish.PublishStickerCommandFacadeImpl
import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.dtos.{PublicationPlatforms, TelegramUserId}
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{
  ArgumentParser,
  RoutesAuthorizationUtils,
  TelegramRouteExecutionUtils,
  UIMessages
}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

class PublishStickerRoutes(
    val publishFacadeCommand: PublishStickerCommandFacadeImpl,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser],
    val defaultPublicationPlatforms: Seq[PublicationPlatform]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    publishRoute()
  )

  private def publishRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/publish",
      authorityFilter = RoutesAuthorizationUtils.isAuthorized(adminUsers),
      action = publishAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "publishRoute")
    )
  }

  def publishAction: Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    val arguments: PublishByIdRouteArguments = PublishByIdRouteArguments.parser(args)
    val publicationPlatforms: Seq[PublicationPlatform] =
      arguments.maybePublicationPlatforms.getOrElse(defaultPublicationPlatforms)
    val executionResult: Try[Unit] = publishFacadeCommand
      .apply(arguments.stickerId, publicationPlatforms)
      .flatMap(_ =>
        telegramService.sendMessage(TelegramUserId(message.source.toString), UIMessages.publishingSuccessful)
      )
    postProcessExecutionResult(executionResult)
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}

object PublishByIdRouteArguments {
  def parser(args: Args): PublishByIdRouteArguments = {
    val stickerId: MaybeStickerId = args.headOption.map(_.toLong)
    val maybePublicationPlatforms: Option[Seq[PublicationPlatform]] = Try {
      args.tail.toList.map(PublicationPlatforms.withName)
    }.toOption.filterNot(_.isEmpty)
    PublishByIdRouteArguments(stickerId, maybePublicationPlatforms)

  }
}
case class PublishByIdRouteArguments(
    stickerId: MaybeStickerId,
    maybePublicationPlatforms: Option[Seq[PublicationPlatform]]
)
