package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.fsm.addsticker
import acab.devcon0.domain.fsm.addsticker._
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.models.Message

import java.util.UUID
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class AddStickerNewRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val draftStickerNewCommandHandler: DraftStickerNewCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    addStickerCommandRoute()
  )

  private def addStickerCommandRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/add",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = addStickerCommandAction(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "addStickerCommandRoute")
    )
  }

  private def addStickerCommandAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)

    existsSomeFsmForThisUser(userId) match {
      case Failure(exception) => handleExceptionCase(userId, exception)
      case Success(true)      => handleOtherFSMTakingCareCase()
      case Success(false) =>
        upsertDraftSticker(userId) match {
          case Success(draftStickerId) => handleAddStickerCommandSuccessCase(userId, draftStickerId)
          case Failure(exception)      => handleExceptionCase(userId, exception)
        }
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def initFSM(userId: TelegramUserId, draftStickerId: DraftStickerId): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = addsticker.TelegramAddFSMInstance(
      id = userId,
      state = AwaitingOriginalSticker,
      data = AddStickerFSMData(draftStickerId = draftStickerId)
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def upsertDraftSticker(userId: TelegramUserId): Try[DraftStickerId] = {
    val command = DraftStickerNewCommand(draftStickerId = UUID.randomUUID(), submittingUser = userId)
    val commandResult: Try[DraftStickerNewEvent[_]] = draftStickerNewCommandHandler.handle(command)
    DraftStickerNewEventProcessor.apply(commandResult, logger)
  }

  private def handleAddStickerCommandSuccessCase(
      userId: TelegramUserId,
      draftStickerId: DraftStickerId
  ): Future[Unit] = {
    initFSM(userId, draftStickerId)
      .flatMap(_ => reply(userId = userId, text = UIMessages.afterAddCommand(draftStickerId)))
      .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
  }

  private def handleExceptionCase(userId: TelegramUserId, exception: Throwable): Future[Unit] = {
    logger.error("addStickerMessageAction", exception)
    telegramFSMDeleteCommandHandler.handle(TelegramFSMDeleteCommand(userId))
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }
}
