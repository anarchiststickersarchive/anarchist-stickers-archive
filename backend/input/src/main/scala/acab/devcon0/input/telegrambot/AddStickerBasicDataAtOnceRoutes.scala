package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.dtos.{StickerLinkTypes, TelegramFileId, TelegramUserId}
import acab.devcon0.domain.fsm.addsticker._
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.models.Message

import java.util.UUID
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class AddStickerBasicDataAtOnceRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramAddFSMExistsQueryHandler: TelegramFSMExistsQueryHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val draftStickerUpsertTextFieldCommandHandler: DraftStickerUpsertTextFieldCommandHandler,
    val newDraftStickerCommandHandler: DraftStickerNewCommandHandler,
    val draftStickerUpsertBinaryCommandHandler: DraftStickerUpsertBinaryCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    addStickerMessageRoute()
  )

  private def addStickerMessageRoute(): DeclarativeRoute = {
    AuthorizedMessageRoute(
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = addStickerMessageAction(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "addStickerMessageRoute")
    )
  }

  private def addStickerMessageAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val existsSomeFsmForThisUserValue: Try[Boolean] = existsSomeFsmForThisUser(userId)
    val existsMatchingFsmForThisUserValue: Try[Boolean] = existsMatchingFSMForThisUser(userId)
    val isAllBasicDataAtOnce: Try[Boolean] = doesMessageHasAllBasicData(message)
    (existsSomeFsmForThisUserValue, existsMatchingFsmForThisUserValue, isAllBasicDataAtOnce) match {
      case (Failure(exception), _, _)                      => handleExceptionCase(exception)
      case (_, Failure(exception), _)                      => handleExceptionCase(exception)
      case (Success(true), Success(false), _)              => handleOtherFSMTakingCareCase()
      case (Success(false), Success(false), Success(true)) => handleBasicDataAtOnceCase(message)
      case (_, _, _)                                       => Future.successful()
    }
  }

  private def handleBasicDataAtOnceCase(message: Message): Future[Unit] = {
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val triedDraftStickerUpsert = upsertDraftSticker(userId)
      .flatMap(draftStickerId => upsertName(message, draftStickerId))
      .flatMap(draftStickerId => upsertLink(message, draftStickerId))
      .flatMap(draftStickerId => upsertFSMForBasicDataCase(userId, draftStickerId))
      .flatMap(_ => reply(userId = userId, text = UIMessages.afterAllBasicDataAtOnceAction))
    triedDraftStickerUpsert match {
      case Success(_)         => TryUtils.foldToFuture()
      case Failure(exception) => notifyException(message)(exception)
    }
  }

  private def upsertName(message: Message, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val cmd = DraftStickerUpsertNameCommand(draftStickerId, message.caption.get)
    val commandResult: Try[DraftStickerUpsertTextFieldEvent[_]] = draftStickerUpsertTextFieldCommandHandler.handle(cmd)
    DraftStickerUpsertTextFieldEventProcessor.apply(commandResult, logger)
  }

  def upsertLink(message: Message, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val command = DraftStickerUpsertBinaryTelegramCommand(
      draftStickerId = draftStickerId,
      stickerLinkType = StickerLinkTypes.ORIGINAL,
      eitherTelegramFileIdOrAbsoluteFilename = Left(TelegramFileId(message.document.get.fileId)),
      extension = message.document.get.fileName.get.split('.').last
    )
    val commandResult = draftStickerUpsertBinaryCommandHandler.handle(command)
    DraftStickerUpsertBinaryEventProcessor.apply(commandResult, logger)
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def existsMatchingFSMForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramAddFSMExistsQueryHandler.handle(TelegramFSMExistsQuery(id = userId))
  }

  private def doesMessageHasAllBasicData(message: Message): Try[Boolean] = Try {
    message.document.isDefined && message.caption.isDefined
  }

  private def upsertFSMForBasicDataCase(userId: TelegramUserId, draftStickerId: DraftStickerId): Try[TelegramFSMId] = {
    val data: AddStickerFSMData = AddStickerFSMData(draftStickerId = draftStickerId)
    val instance: TelegramAddFSMInstance =
      TelegramAddFSMInstance(id = userId, state = BasicDataGathered, data = data)
    val cmdResult = telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
    TelegramFSMUpdateEventProcessor.apply(cmdResult, logger)
  }

  private def upsertDraftSticker(userId: TelegramUserId): Try[DraftStickerId] = {
    val command = DraftStickerNewCommand(draftStickerId = UUID.randomUUID(), submittingUser = userId)
    val commandResult = newDraftStickerCommandHandler.handle(command)
    DraftStickerNewEventProcessor.apply(commandResult, logger)
  }

  private def handleExceptionCase(exception: Throwable): Future[Unit] = {
    logger.error("AddStickerBasicDataAtOnceRoutes", exception)
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }

  private def notifyException(message: Message): Throwable => Future[Unit] = { throwable =>
    {
      logger.error(s"throwable=$throwable")
      reply(userId = TelegramUserId(message.source.toString), text = UIMessages.submissionFailed)
        .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
    }
  }
}
