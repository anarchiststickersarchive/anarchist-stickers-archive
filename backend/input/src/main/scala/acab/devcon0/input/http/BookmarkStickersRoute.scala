package acab.devcon0.input.http

import acab.devcon0.domain.dtos.{AggregatedSticker, BookmarkUserId, BookmarkUserPlatforms}
import acab.devcon0.domain.ports.input.bookmarksticker.{
  AggregatedStickerIndexBookmarkQuery,
  AggregatedStickerIndexBookmarkQueryHandler
}
import acab.devcon0.input.http.CirceCodecs.Encoders.aggregatedStickerGetResponseEncoder
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import io.circe.Encoder._
import io.circe.syntax._

import scala.util.Try

class BookmarkStickersRoute(queryHandler: AggregatedStickerIndexBookmarkQueryHandler) {

  val route: Route = get {
    path(Segment / "stickers") { platformAndUserId =>
      parsePlatformAndUserIdPathParam(platformAndUserId)
        .flatMap(bookmarkUserId => {
          queryHandler
            .handle(AggregatedStickerIndexBookmarkQuery(bookmarkUserId))
        })
        .fold(
          throwable => {
            complete(HttpResponse(BadRequest, entity = s"Something went wrong. throwable=${throwable.getMessage}"))
          },
          (stickers: Seq[AggregatedSticker]) => {
            val response: Seq[AggregatedStickerGetResponse] = stickers.map(sticker =>
              AggregatedStickerGetResponse(
                sticker = sticker.sticker,
                links = sticker.links,
                stickerDirectoryRelation = sticker.stickerDirectoryRelation
              )
            )
            complete(response.asJson.toString())
          }
        )
    }
  }

  def parsePlatformAndUserIdPathParam(platformAndUserId: String): Try[BookmarkUserId] = Try {
    val strings: Seq[String] = platformAndUserId.split(':').toSeq
    val rawPlatform = strings.head
    val rawUserId = strings.tail.head
    BookmarkUserId(rawUserId, BookmarkUserPlatforms.withName(rawPlatform.toUpperCase))
  }
}
