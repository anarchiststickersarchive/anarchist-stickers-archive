package acab.devcon0.input.telegrambot

import acab.devcon0.domain.adapters.previewsticker.PreviewStickerCommandHandlerImpl
import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.dtos.{PublicationPlatforms, TelegramUserId}
import acab.devcon0.domain.ports.input.preview._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{ArgumentParser, RoutesAuthorizationUtils, TelegramRouteExecutionUtils}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

class PreviewStickerRoutes(
    val previewStickerCommandHandler: PreviewStickerCommandHandlerImpl,
    val previewStickerSocialMediaPlainCommandHandler: PreviewStickerSocialMediaPlainCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    previewRoute(),
    previewPlainRoute()
  )

  private def previewRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/preview",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = previewAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "previewRoute")
    )
  }

  private def previewPlainRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/previewplain",
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = previewSocialMediaPlainAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "previewRoute")
    )
  }

  private def previewAction: Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    val stickerId: MaybeStickerId = args.headOption.map(_.toLong)
    val command = PreviewStickerCommand(stickerId, TelegramUserId(message.source.toString))
    val commandResult: Try[PreviewStickerEvent[_]] = previewStickerCommandHandler.handle(command)
    val eventFlattenCommandResult = PreviewStickerEventProcessor.apply(commandResult, logger)
    TelegramRouteExecutionUtils.postProcess(eventFlattenCommandResult, logger)
  }

  private def previewSocialMediaPlainAction: Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    val stickerId: MaybeStickerId = args.headOption.map(_.toLong)
    val publicationPlatform: Option[PublicationPlatform] = args.tail.headOption.map(PublicationPlatforms.withName)
    val command = PreviewStickerSocialMediaPlainCommand(
      stickerId = stickerId,
      telegramUserId = TelegramUserId(message.source.toString),
      publicationPlatform = publicationPlatform
    )
    val commandResult: Try[PreviewStickerEvent[_]] = previewStickerSocialMediaPlainCommandHandler.handle(command)
    val eventFlattenCommandResult = PreviewStickerSocialMediaPlainEventProcessor.apply(commandResult, logger)
    TelegramRouteExecutionUtils.postProcess(eventFlattenCommandResult, logger)
  }
}
