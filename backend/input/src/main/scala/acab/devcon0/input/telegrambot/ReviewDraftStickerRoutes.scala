package acab.devcon0.input.telegrambot

import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.ports.input.preview.PreviewDraftStickerCommandImplicits.PreviewDraftStickerCommandEventFlattenOps
import acab.devcon0.domain.ports.input.preview.{PreviewDraftStickerAdminsCommand, PreviewDraftStickerCommandHandler}
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{ArgumentParser, RoutesAuthorizationUtils, TelegramRouteExecutionUtils}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import java.util.UUID
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

class ReviewDraftStickerRoutes(
    val previewDraftStickerCommandHandler: PreviewDraftStickerCommandHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    acceptRoute()
  )

  private def acceptRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/review",
      authorityFilter = RoutesAuthorizationUtils.isAuthorized(adminUsers),
      action = acceptAction,
      unauthorizedAccessAction = RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "reviewRoute")
    )
  }

  def acceptAction: Message => Future[Unit] = { message: Message =>
    val args: Args = ArgumentParser.parse(message)
    postProcessExecutionResult(executionResult = for {
      arguments <- ReviewByIdRouteArguments.parser(args)
      telegramUserId = TelegramUserId(message.source.toString)
      command = PreviewDraftStickerAdminsCommand(
        draftStickerId = arguments.draftStickerId,
        telegramUserId = telegramUserId
      )
      commandResult <- previewDraftStickerCommandHandler.handle(command).flattenEvents
    } yield commandResult)
  }

  private def postProcessExecutionResult(executionResult: Try[_]): Future[Unit] = {
    TelegramRouteExecutionUtils.postProcess(executionResult, logger)
  }
}

object ReviewByIdRouteArguments {
  def parser(args: Args): Try[ReviewByIdRouteArguments] = Try {
    val draftStickerId: DraftStickerId = args.headOption.map(UUID.fromString).get
    ReviewByIdRouteArguments(draftStickerId)

  }
}

case class ReviewByIdRouteArguments(draftStickerId: DraftStickerId)
