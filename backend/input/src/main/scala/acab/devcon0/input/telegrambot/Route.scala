package acab.devcon0.input.telegrambot

import com.bot4s.telegram.api.declarative.{Action, Commands, Filter}
import com.bot4s.telegram.future.{Polling, TelegramBot}
import com.bot4s.telegram.models.Message
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

trait RouteFacade extends TelegramBot with Commands[Future] with Polling
trait Route extends LazyLogging
trait TelegramRoute extends Route with LazyLogging {
  def routes: Seq[DeclarativeRoute]
}

sealed trait DeclarativeRoute

case class PublicCommandDeclarativeRoute(command: String, action: Action[Future, Message]) extends DeclarativeRoute
case class PublicActionDeclarativeRoute(action: Action[Future, Message]) extends DeclarativeRoute

case class AuthorizedCommandRoute(
    command: String,
    action: Action[Future, Message],
    authorityFilter: Filter[Message],
    unauthorizedAccessAction: Action[Future, Message]
) extends DeclarativeRoute

case class AuthorizedMessageRoute(
    action: Action[Future, Message],
    authorityFilter: Filter[Message],
    unauthorizedAccessAction: Action[Future, Message]
) extends DeclarativeRoute
