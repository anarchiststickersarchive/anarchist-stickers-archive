package acab.devcon0.input.telegrambot.utils

import acab.devcon0.commons.{FunctionUtils, TryUtils}
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future
import scala.util.Try

object TelegramRouteExecutionUtils {

  def postProcess(executionResult: Try[_], logger: Logger): Future[Unit] = {
    executionResult
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
      .fold(FunctionUtils.flushResultToFuture, event => Future.successful(event.toString))
  }
}
