package acab.devcon0.input.http

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

object CirceCodecs {

  object Decoders {
    import acab.devcon0.domain.codecs.CirceCodecs.Decoders._
  }

  object Encoders {
    import acab.devcon0.domain.codecs.CirceCodecs.Encoders._
    implicit val stickerLanguageEncoder: Encoder[StickerLanguage] = deriveEncoder
    implicit val stickersCountGetResponseEncoder: Encoder[StickersCountGetResponse] = deriveEncoder
    implicit val aggregatedStickerGetResponseEncoder: Encoder[AggregatedStickerGetResponse] = deriveEncoder
    implicit val aggregatedStickerGetResponse2Encoder: Encoder[AggregatedStickerGetResponse2] = deriveEncoder
  }
}
