package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.adapters.telegramfsm.TelegramAddFSMQueryHandler
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.fsm.addsticker._
import acab.devcon0.domain.ports.input.addsticker.DraftStickerAcceptCommandImplicits.DraftStickerAcceptCommandEventFlattenOps
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.ports.input.ingestion.RefreshIngestionFilesListCommandImplicits.RefreshIngestionFilesListCommandEventFlattenOps
import acab.devcon0.domain.ports.input.ingestion.{RefreshIngestionFilesListCommand, RefreshIngestionFilesListCommandHandler}
import acab.devcon0.domain.ports.input.preview._
import acab.devcon0.domain.ports.input.telegramfsm.TelegramFSMDeleteCommandImplicits.EventFlattenOps
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class AddStickerRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler,
    val telegramAddFSMExistsQueryHandler: TelegramFSMExistsQueryHandler,
    val telegramAddFSMQueryHandler: TelegramAddFSMQueryHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val draftStickerUpsertTextFieldCommandHandler: DraftStickerUpsertTextFieldCommandHandler,
    val newDraftStickerCommandHandler: DraftStickerNewCommandHandler,
    val draftStickerUpsertBinaryCommandHandler: DraftStickerUpsertBinaryCommandHandler,
    val draftStickerAcceptCommandHandler: DraftStickerAcceptCommandHandler,
    val draftStickerAbortCommandHandler: DraftStickerAbortCommandHandler,
    val previewStickerCommandHandler: PreviewStickerCommandHandler,
    val previewDraftStickerCommandHandler: PreviewDraftStickerCommandHandler,
    val telegramService: TelegramService,
    val refreshIngestionFilesListCommandHandler: RefreshIngestionFilesListCommandHandler,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    addStickerMessageRoute()
  )

  private def addStickerMessageRoute(): DeclarativeRoute = {
    AuthorizedMessageRoute(
      authorityFilter = RoutesAuthorizationUtils.isAuthorizedOrPrivateChat(adminUsers),
      action = addStickerMessageAction(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "addStickerMessageRoute")
    )
  }

  private def addStickerMessageAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val existsSomeFsmForThisUserValue: Try[Boolean] = existsSomeFsmForThisUser(userId)
    val existsMatchingFsmForThisUserValue: Try[Boolean] = existsMatchingFSMForThisUser(userId)
    val isAllBasicDataAtOnce: Try[Boolean] = doesMessageHasAllBasicData(message)
    (existsSomeFsmForThisUserValue, existsMatchingFsmForThisUserValue, isAllBasicDataAtOnce) match {
      case (Failure(exception), _, _)                     => handleExceptionCase(exception)
      case (_, Failure(exception), _)                     => handleExceptionCase(exception)
      case (Success(true), Success(false), _)             => handleOtherFSMTakingCareCase()
      case (Success(true), Success(true), Success(false)) => handleAnyMessageWhenFSMRunningCase(message)
      case (_, _, _)                                      => Future.successful()
    }
  }

  private def handleAnyMessageWhenFSMRunningCase(message: Message): Future[Unit] = {
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val fsmInstance: Try[TelegramAddFSMInstance] = telegramAddFSMQueryHandler.handle(TelegramFSMQuery(userId))
    val fsmState: Try[AddStickerFSMState] = fsmInstance.map(_.state)
    val maybeEvent: Option[AddStickerFSMEvent] = parseEvent(message)
    (fsmInstance, fsmState, maybeEvent) match {
      case (Failure(exception), _, _)                                             => handleExceptionCase(exception)
      case (_, Failure(exception), _)                                             => handleExceptionCase(exception)
      case (Success(_), Success(state), Some(Skip)) if state.equals(AwaitingName) => skipIngestion(userId)
      case (Success(instance), Success(BasicDataGathered), Some(Cancel)) =>
        draftStickerAbortCommandHandler
          .handle(DraftStickerAbortCommand(draftStickerId = instance.data.draftStickerId, telegramFSMId = userId))
          .flatMap(_ => reply(userId = userId, text = UIMessages.addStickerAbort))
        Future.successful()
      case (Success(instance), Success(state), Some(Cancel))
          if Set(
            AwaitingName,
            AwaitingOriginalSticker
          ).exists(_.equals(state)) =>
        draftStickerAbortCommandHandler
          .handle(DraftStickerAbortCommand(draftStickerId = instance.data.draftStickerId, telegramFSMId = userId))
          .flatMap(_ => reply(userId = userId, text = UIMessages.addStickerAbort))
        Future.successful()
      case (Success(instance), Success(state), Some(Cancel))
          if Set(
            AwaitingNotes,
            AwaitingSource,
            AwaitingLanguage,
            AwaitingVectorSticker
          ).exists(_.equals(state)) =>
        upsertFSMForBasicDataCase(userId, instance.data.draftStickerId)
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(AwaitingName), Some(TextSubmitted)) =>
        upsertName(message, instance.data.draftStickerId)
          .flatMap(_ => upsertFSMForBasicDataCase(userId, instance.data.draftStickerId))
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(AwaitingNotes), Some(TextSubmitted)) =>
        upsertNotes(message, instance.data.draftStickerId)
          .flatMap(_ => upsertFSMForBasicDataCase(userId, instance.data.draftStickerId))
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(AwaitingSource), Some(TextSubmitted)) =>
        upsertSource(message, instance.data.draftStickerId)
          .flatMap(_ => upsertFSMForBasicDataCase(userId, instance.data.draftStickerId))
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(AwaitingLanguage), Some(TextSubmitted)) =>
        upsertLanguage(message, instance.data.draftStickerId)
          .flatMap(_ => upsertFSMForBasicDataCase(userId, instance.data.draftStickerId))
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(AwaitingVectorSticker), Some(DocumentSubmitted)) =>
        upsertLink(
          draftStickerId = instance.data.draftStickerId,
          stickerLinkType = StickerLinkTypes.VECTOR,
          telegramFileId = message.document.get.fileId,
          extension = message.document.get.fileName.get.split('.').last
        ).flatMap(_ => upsertFSMForBasicDataCase(userId, instance.data.draftStickerId))
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(AwaitingOriginalSticker), Some(DocumentSubmitted)) =>
        upsertLink(
          draftStickerId = instance.data.draftStickerId,
          stickerLinkType = StickerLinkTypes.ORIGINAL,
          telegramFileId = message.document.get.fileId,
          extension = message.document.get.fileName.get.split('.').last
        ).flatMap(_ => upsertFSMForAwaitingName(userId, instance.data.draftStickerId))
          .flatMap(_ => sendOriginalStickerReceivedReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
      case (Success(instance), Success(BasicDataGathered), Some(Save)) => saveOrSuggest(message, userId, instance)
      case (Success(instance), Success(BasicDataGathered), Some(AddNotes)) =>
        putFSMOnState(userId, instance.data.draftStickerId, AwaitingNotes)
          .flatMap(_ => reply(userId = userId, text = UIMessages.onUpsertNotes))
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case (Success(instance), Success(BasicDataGathered), Some(AddSource)) =>
        putFSMOnState(userId, instance.data.draftStickerId, AwaitingSource)
          .flatMap(_ => reply(userId = userId, text = UIMessages.onUpsertSource))
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case (Success(instance), Success(BasicDataGathered), Some(AddLanguage)) =>
        putFSMOnState(userId, instance.data.draftStickerId, AwaitingLanguage)
          .flatMap(_ =>
            reply(
              userId = userId,
              text = UIMessages.onUpsertLanguage
            )
          )
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case (Success(instance), Success(BasicDataGathered), Some(AddVector)) =>
        putFSMOnState(userId, instance.data.draftStickerId, AwaitingVectorSticker)
          .flatMap(_ =>
            reply(
              userId = userId,
              text = UIMessages.onUpsertVector
            )
          )
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case (Success(instance), Success(BasicDataGathered), Some(VectorizeOnDemand)) =>
        reply(
          userId = userId,
          text = UIMessages.onVectorizeOnDemandRequest
        ).flatMap(_ => upsertVectorOnDemandCommand(instance.data.draftStickerId))
          .flatMap(_ =>
            reply(
              userId = userId,
              text = UIMessages.onSuccessfulVectorizeOnDemand
            )
          )
          .flatMap(_ => sendBasicDataGatheredReply(userId))
          .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
    }
  }

  private def skipIngestion(userId: AuthorizedUser): Future[Unit] = {
    refreshIngestionFilesListCommandHandler
      .handle(RefreshIngestionFilesListCommand())
      .flattenEvents
    telegramFSMDeleteCommandHandler
      .handle(TelegramFSMDeleteCommand(userId))
      .flatMap(_ => reply(userId = userId, text = UIMessages.addStickerSkip))
    Future.successful()
  }

  private def saveOrSuggest(
      message: Message,
      userId: TelegramUserId,
      instance: TelegramAddFSMInstance
  ): Future[Unit] = {
    if (adminUsers.contains(userId)) {
      save(message, userId, instance)
    } else {
      submit(userId, instance)
    }
  }

  private def save(message: Message, userId: TelegramUserId, instance: TelegramAddFSMInstance): Future[Unit] = {
    val command: DraftStickerAcceptCommand = DraftStickerAcceptCommand(instance.data.draftStickerId)
    val commandResult: Try[Sticker] = draftStickerAcceptCommandHandler.handle(command).flattenEvents
    commandResult match {
      case Success(sticker) =>
        val fSMDeleteCommand = TelegramFSMDeleteCommand(userId)
        telegramFSMDeleteCommandHandler
          .handle(fSMDeleteCommand)
          .flattenEvents
          .flatMap(_ => reply(userId = userId, text = UIMessages.afterAcceptingDraftSticker))
          .flatMap(_ => previewStickerCommandHandler.handle(PreviewStickerCommand(sticker.id, userId)))
          .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
      case Failure(throwable) => notifyException(message)(throwable)
    }
  }

  private def submit(userId: TelegramUserId, instance: TelegramAddFSMInstance): Future[Unit] = {
    val fSMDeleteCommand = TelegramFSMDeleteCommand(userId)
    telegramFSMDeleteCommandHandler
      .handle(fSMDeleteCommand)
      .flattenEvents
      .flatMap(_ => reply(userId = userId, text = UIMessages.afterSuggestingDraftSticker))
      .flatMap(_ => {
        adminUsers.foreach(adminUser => {
          previewDraftStickerCommandHandler.handle(
            PreviewDraftStickerAdminsCommand(instance.data.draftStickerId, adminUser)
          )
        })
        previewDraftStickerCommandHandler.handle(
          PreviewDraftStickerNonAdminCommand(instance.data.draftStickerId, userId)
        )
      })
      .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
  }

  private def sendBasicDataGatheredReply(
      userId: TelegramUserId
  ): Try[Unit] = {
    reply(
      userId = userId,
      text = UIMessages.afterAllBasicDataAtOnceAction
    )
  }

  private def sendOriginalStickerReceivedReply(
      userId: TelegramUserId
  ): Try[Unit] = {
    reply(
      userId = userId,
      text = UIMessages.onUpsertOriginalSticker
    )
  }

  private def upsertLink(
      draftStickerId: DraftStickerId,
      stickerLinkType: StickerLinkType,
      telegramFileId: String,
      extension: String
  ): Try[DraftSticker] = {
    val draftStickerUpsertBinaryCommand = DraftStickerUpsertBinaryTelegramCommand(
      draftStickerId = draftStickerId,
      stickerLinkType = stickerLinkType,
      eitherTelegramFileIdOrAbsoluteFilename = Left(TelegramFileId(telegramFileId)),
      extension = extension
    )
    draftStickerUpsertBinaryCommandHandler.handle(draftStickerUpsertBinaryCommand) match {
      case Failure(throwable)                                          => Failure(throwable)
      case Success(DraftStickerUpsertBinarySuccessEvent(draftSticker)) => Success(draftSticker)
      case Success(DraftStickerUpsertBinaryErrorEvent(_, throwable))   => Failure(throwable)
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def existsMatchingFSMForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramAddFSMExistsQueryHandler.handle(TelegramFSMExistsQuery(id = userId))
  }

  private def doesMessageHasAllBasicData(message: Message): Try[Boolean] = Try {
    message.document.isDefined && message.caption.isDefined
  }

  private def parseEvent(message: Message): Option[AddStickerFSMEvent] = {
    message.document match {
      case Some(_) => Some(DocumentSubmitted)
      case None =>
        message.caption.orElse(message.text).map(_.toLowerCase()) match {
          case Some(">")                         => Some(Skip)
          case Some(".") | Some(".cancel")       => Some(Cancel)
          case Some("s") | Some("save")          => Some(Save)
          case Some("l") | Some("language")      => Some(AddLanguage)
          case Some("n") | Some("notes")         => Some(AddNotes)
          case Some("so") | Some("source")       => Some(AddSource)
          case Some("v") | Some("vector")        => Some(AddVector)
          case Some("dv") | Some("doVectorize") => Some(VectorizeOnDemand)
          case Some(_)                           => Some(TextSubmitted)
          case None                              => None
        }
    }
  }

  private def upsertFSMForBasicDataCase(
      userId: TelegramUserId,
      draftStickerId: DraftStickerId
  ): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = TelegramAddFSMInstance(
      id = userId,
      state = BasicDataGathered,
      data = AddStickerFSMData(draftStickerId = draftStickerId)
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def upsertFSMForAwaitingName(
      userId: TelegramUserId,
      draftStickerId: DraftStickerId
  ): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = TelegramAddFSMInstance(
      id = userId,
      state = AwaitingName,
      data = AddStickerFSMData(draftStickerId = draftStickerId)
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def putFSMOnState(
      userId: TelegramUserId,
      draftStickerId: DraftStickerId,
      state: AddStickerFSMState
  ): Try[TelegramFSMUpdateEvent[_]] = {
    val instance =
      TelegramAddFSMInstance(
        id = userId,
        state = state,
        data = AddStickerFSMData(draftStickerId = draftStickerId)
      )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def handleExceptionCase(exception: Throwable): Future[Unit] = {
    logger.error("addStickerMessageAction", exception)
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }

  private def upsertName(message: Message, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val cmd = DraftStickerUpsertNameCommand(draftStickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertNotes(message: Message, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val cmd = DraftStickerUpsertNotesCommand(draftStickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertSource(message: Message, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val cmd = DraftStickerUpsertSourceCommand(draftStickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertLanguage(message: Message, draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val cmd = DraftStickerUpsertLanguageCommand(draftStickerId, message.text.get)
    upsertDraftStickerTextField(cmd)
  }

  private def upsertDraftStickerTextField(cmd: DraftStickerUpsertTextFieldCommand): Try[DraftStickerId] = {
    val commandResult: Try[DraftStickerUpsertTextFieldEvent[_]] = draftStickerUpsertTextFieldCommandHandler.handle(cmd)
    DraftStickerUpsertTextFieldEventProcessor.apply(commandResult, logger)
  }

  private def upsertVectorOnDemandCommand(draftStickerId: DraftStickerId): Try[DraftStickerId] = {
    val command: DraftStickerUpsertBinaryCommand = DraftStickerUpsertVectorOnDemandCommand(draftStickerId)
    val commandResult: Try[DraftStickerUpsertBinaryEvent[_]] = draftStickerUpsertBinaryCommandHandler.handle(command)
    DraftStickerUpsertBinaryEventProcessor.apply(commandResult, logger)
  }

  private def notifyException(message: Message): Throwable => Future[Unit] = { throwable =>
    {
      logger.error(s"throwable=$throwable")
      reply(userId = TelegramUserId(message.source.toString), text = UIMessages.publishingFailed)
        .fold(TryUtils.foldToFutureFailed, _ => TryUtils.foldToFuture())
    }
  }
}
