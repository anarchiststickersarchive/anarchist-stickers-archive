package acab.devcon0.input.http

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors

class Routes(
    stickersRoute: StickersRoute,
    bookmarkStickersRoute: BookmarkStickersRoute,
    rssRoute: RssRoute
) {
  val routes: Route = cors() {
    pathPrefix("api") {
      concat(
        pathPrefix("stickers") {
          concat(
            stickersRoute.countRoute,
            stickersRoute.idRoute,
            stickersRoute.indexRoute
          )
        },
        pathPrefix("bookmarks") {
          bookmarkStickersRoute.route
        },
        pathPrefix(".rss") {
          rssRoute.route
        }
      )
    }
  }
}
