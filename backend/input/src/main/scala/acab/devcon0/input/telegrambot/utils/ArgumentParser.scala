package acab.devcon0.input.telegrambot.utils

import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import scala.util.Try

object ArgumentParser {

  def parse(message: Message): Args = {
    Try(unsafeParse(message))
      .fold(_ => List(), list => list)
  }

  private def unsafeParse(message: Message): Args = {
    message.text
      .map(_.trim.split("\\s+").toList)
      .map(_.tail)
      .getOrElse(List())
  }
}
