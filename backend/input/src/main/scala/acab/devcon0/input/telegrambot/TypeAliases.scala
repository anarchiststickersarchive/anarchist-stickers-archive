package acab.devcon0.input.telegrambot

import acab.devcon0.domain.dtos.TelegramUserId

object TypeAliases {
  type AuthorizedUser = TelegramUserId
}
