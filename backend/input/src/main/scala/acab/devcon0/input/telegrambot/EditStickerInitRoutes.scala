package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.fsm.editsticker.{AwaitingForFieldToEdit, EditStickerFSMData, TelegramEditFSMInstance}
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{ArgumentParser, RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.api.declarative.Args
import com.bot4s.telegram.models.Message

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class EditStickerInitRoutes(
    val telegramFSMUpdateCommandHandler: TelegramFSMUpdateCommandHandler,
    val telegramFSMDeleteCommandHandler: TelegramFSMDeleteCommandHandler,
    val telegramFSMExistsAnyQueryHandler: TelegramFSMExistsAnyQueryHandler,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    addStickerCommandRoute()
  )

  private def addStickerCommandRoute(): DeclarativeRoute = {
    PublicCommandDeclarativeRoute(
      command = "/edit",
      action = addStickerCommandAction()
    )
  }

  private def addStickerCommandAction(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    val args: Args = ArgumentParser.parse(message)
    val stickerId: MaybeStickerId = Some(args.head.toLong)

    existsSomeFsmForThisUser(userId) match {
      case Failure(exception) => handleExceptionCase(userId, exception)
      case Success(true)      => handleOtherFSMTakingCareCase()
      case Success(false)     => handleEditStickerCommandSuccessCase(userId, stickerId)
    }
  }

  private def existsSomeFsmForThisUser(userId: TelegramUserId): Try[Boolean] = {
    telegramFSMExistsAnyQueryHandler.handle(TelegramFSMExistsAnyQuery(id = userId))
  }

  private def initFSM(userId: TelegramUserId, stickerId: MaybeStickerId): Try[TelegramFSMUpdateEvent[_]] = {
    val instance = TelegramEditFSMInstance(
      id = userId,
      state = AwaitingForFieldToEdit,
      data = EditStickerFSMData(stickerId = stickerId)
    )
    telegramFSMUpdateCommandHandler.handle(TelegramFSMUpdateCommand(id = userId, instance = instance))
  }

  private def handleEditStickerCommandSuccessCase(userId: TelegramUserId, stickerId: MaybeStickerId): Future[Unit] = {
    if (adminUsers.contains(userId)) {
      initFSM(userId, stickerId)
        .flatMap(_ => reply(userId = userId, text = UIMessages.afterEditCommand))
        .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)
    } else {
      RoutesAuthorizationUtils.notifyUnauthorizedAccess(telegramService, "handleEditStickerCommandSuccessCase", userId)
    }
  }

  private def handleExceptionCase(userId: TelegramUserId, exception: Throwable): Future[Unit] = {
    logger.error("addStickerMessageAction", exception)
    telegramFSMDeleteCommandHandler.handle(TelegramFSMDeleteCommand(userId))
    Future.successful()
  }

  private def handleOtherFSMTakingCareCase(): Future[Unit] = {
    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }
}
