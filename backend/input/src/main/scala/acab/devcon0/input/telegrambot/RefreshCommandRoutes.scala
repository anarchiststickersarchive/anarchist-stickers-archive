package acab.devcon0.input.telegrambot

import acab.devcon0.commons.TryUtils
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.service.TelegramService
import acab.devcon0.input.telegrambot.TypeAliases.AuthorizedUser
import acab.devcon0.input.telegrambot.utils.{RoutesAuthorizationUtils, UIMessages}
import com.bot4s.telegram.models.Message

import java.io.File
import scala.concurrent.Future
import scala.util.Try

class RefreshCommandRoutes(
    val configuration: Configuration,
    val telegramService: TelegramService,
    val adminUsers: Seq[AuthorizedUser]
) extends TelegramRoute {

  override def routes: Seq[DeclarativeRoute] = List(
    refreshCommandRoute()
  )

  private def refreshCommandRoute(): DeclarativeRoute = {
    AuthorizedCommandRoute(
      command = "/refresh",
      authorityFilter = RoutesAuthorizationUtils.isAuthorized(adminUsers),
      action = refreshCommand(),
      unauthorizedAccessAction =
        RoutesAuthorizationUtils.unauthorizedAccessAction(telegramService, "refreshCommandRoutes")
    )
  }

  private def refreshCommand(): Message => Future[Unit] = { message: Message =>
    val userId: TelegramUserId = TelegramUserId(message.source.toString)
    Try(new File(configuration.refreshLockAbsolutePath).delete())
      .flatMap(_ => reply(userId = userId, text = UIMessages.refreshCommandSuccessful))
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
      .fold(TryUtils.foldToFutureFailed, TryUtils.foldToFuture)

    Future.successful()
  }

  private def reply(userId: TelegramUserId, text: String): Try[Unit] = {
    telegramService.sendMessage(to = userId, text = text)
  }
}
