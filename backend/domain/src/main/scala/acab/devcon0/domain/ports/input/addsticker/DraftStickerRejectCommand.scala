package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.util.{Failure, Success, Try}

case class DraftStickerRejectCommand(draftStickerId: DraftStickerId) extends Command[DraftStickerId]

sealed abstract class DraftStickerRejectEvent[T] extends Event[T]

case class DraftStickerRejectSuccessEvent(draftStickerId: DraftStickerId)
    extends DraftStickerRejectEvent[DraftStickerId]

case class DraftStickerRejectErrorEvent(
    draftStickerId: DraftStickerId,
    throwable: Throwable
) extends DraftStickerRejectEvent[DraftStickerId]

trait DraftStickerRejectCommandHandler
    extends CommandHandler[DraftStickerRejectCommand, DraftStickerId, DraftStickerRejectEvent[_]]

object DraftStickerRejectCommandImplicits {
  implicit class DraftStickerRejectCommandEventFlattenOps(result: Try[DraftStickerRejectEvent[_]]) extends LazyLogging {
    def flattenEvents: Try[DraftStickerId] = {
      result match {
        case Failure(exception)                                      => handleErrorCase(exception, logger)
        case Success(DraftStickerRejectErrorEvent(_, exception))     => handleErrorCase(exception, logger)
        case Success(DraftStickerRejectSuccessEvent(draftStickerId)) => Success(draftStickerId)
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
      logger.error("DraftStickerRejectEventProcessor", exception)
      Failure(exception)
    }
  }
}
