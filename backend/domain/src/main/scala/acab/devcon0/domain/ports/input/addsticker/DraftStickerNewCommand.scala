package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.dtos.{DraftSticker, TelegramUserId}
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

case class DraftStickerNewCommand(draftStickerId: DraftStickerId, submittingUser: TelegramUserId)
    extends Command[DraftStickerId]

sealed abstract class DraftStickerNewEvent[T] extends Event[T]

case class DraftStickerSuccessNewEvent(draftSticker: DraftSticker) extends DraftStickerNewEvent[DraftStickerId]

case class DraftStickerErrorNewEvent(
    draftStickerId: DraftStickerId,
    throwable: Throwable
) extends DraftStickerNewEvent[DraftStickerId]

trait DraftStickerNewCommandHandler
    extends CommandHandler[DraftStickerNewCommand, DraftStickerId, DraftStickerNewEvent[_]]

object DraftStickerNewEventProcessor {
  def apply(result: Try[DraftStickerNewEvent[_]], logger: Logger): Try[DraftStickerId] = {
    result match {
      case Failure(exception)                                 => handleErrorCase(exception, logger)
      case Success(DraftStickerErrorNewEvent(_, exception))   => handleErrorCase(exception, logger)
      case Success(DraftStickerSuccessNewEvent(draftSticker)) => Success(draftSticker.id)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
    logger.error("NewDraftStickerEventProcessor", exception)
    Failure(exception)
  }
}
