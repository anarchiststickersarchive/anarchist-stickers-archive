package acab.devcon0.domain.adapters.editsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.editsticker._
import acab.devcon0.domain.ports.output.VectorizerClient
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import java.awt.image.BufferedImage
import java.io.File
import java.nio.file.Files
import java.time.LocalDateTime
import javax.imageio.ImageIO
import scala.util.{Failure, Success, Try}

class StickerUpsertBinaryCommandHandlerImpl(
    configuration: Configuration,
    stickerLinksService: StickerLinksService,
    stickerDirectoryRelationsService: StickerDirectoryRelationsService,
    ipfsService: IpfsService,
    telegramService: TelegramService,
    fileSystemPersistenceHandler: FileSystemPersistenceHandler = new FileSystemPersistenceHandler() {},
    fileSystemPathHandler: FileSystemPathHandler = new FileSystemPathHandler() {},
    vectorizerClient: VectorizerClient,
    aggregatedStickersService: AggregatedStickersService
) extends StickerUpsertBinaryCommandHandler
    with LazyLogging {

  private val targetSize: Int = 2500

  override def handle(cmd: StickerUpsertBinaryCommand): Try[StickerUpsertBinaryEvent[_]] = {
    cmd match {
      case telegramCmd: StickerUpsertBinaryTelegramCommand       => handleTelegram(telegramCmd)
      case vectorOnDemandCmd: StickerUpsertVectorOnDemandCommand => handleVectorOnDemand(vectorOnDemandCmd)
    }
  }

  private def handleVectorOnDemand(cmd: StickerUpsertVectorOnDemandCommand): Try[StickerUpsertBinaryEvent[_]] = {
    handleVectorOnDemandInner(cmd) match {
      case Failure(throwable) =>
        Success(StickerUpsertBinaryErrorEvent(stickerId = cmd.stickerId, throwable = throwable))
      case Success(stickerLink) => Success(StickerUpsertBinarySuccessEvent(stickerLinkId = stickerLink.id))
    }
  }

  private def handleVectorOnDemandInner(cmd: StickerUpsertVectorOnDemandCommand): Try[StickerLink] = {
    val stickerId: MaybeStickerId = cmd.stickerId
    val stickerLinkType: StickerLinkType = StickerLinkTypes.VECTOR
    val extension: String = "svg"

    for {
      aggregatedSticker <- aggregatedStickersService.getByStickerId(cmd.stickerId)
      absolutePathFile = aggregatedSticker.publishingPhoto.toAbsolutePath.toFile
      stickerDimensions <- getStickerDimensionsWhenFilename(absolutePathFile)
      desiredDimensions <- getDesiredDimensions(stickerDimensions)
      relation <- getStickerDirectoryRelation(stickerId)
      vectorBytes <- vectorizerClient.get(
        desiredWidth = desiredDimensions.width,
        desiredHeight = desiredDimensions.height,
        model = "clipart",
        colors = "auto",
        imageInputAbsolutePath = absolutePathFile.toString,
        stickerRasterizeBytes = Files.readAllBytes(aggregatedSticker.publishingPhoto)
      )
      () <- persistInFs(stickerLinkType, extension, relation, vectorBytes)
      ipfsFile <- persistInIPFS(stickerLinkType, relation.directoryName, extension, vectorBytes)
      stickerLink <- persistInDB(stickerId, ipfsFile, stickerLinkType)
    } yield stickerLink
  }

  private def handleTelegram(cmd: StickerUpsertBinaryTelegramCommand): Try[StickerUpsertBinaryEvent[_]] = {
    val stickerId: MaybeStickerId = cmd.stickerId
    val fileId: String = cmd.telegramFileId
    val extension = cmd.extension
    handleTelegramInner(cmd, stickerId, fileId, extension) match {
      case Failure(throwable)   => Success(StickerUpsertBinaryErrorEvent(stickerId = stickerId, throwable = throwable))
      case Success(stickerLink) => Success(StickerUpsertBinarySuccessEvent(stickerLinkId = stickerLink.id))
    }
  }

  private def handleTelegramInner(
      cmd: StickerUpsertBinaryTelegramCommand,
      stickerId: MaybeStickerId,
      fileId: String,
      extension: String
  ): Try[StickerLink] = {
    val stickerLinkType: StickerLinkType = cmd.stickerLinkType
    for {
      relation <- getStickerDirectoryRelation(stickerId)
      fileBytes <- telegramService.getFileBytes(TelegramFileId(fileId))
      () <- persistInFs(cmd.stickerLinkType, extension, relation, fileBytes)
      ipfsFile <- persistInIPFS(cmd.stickerLinkType, relation.directoryName, extension, fileBytes)
      stickerLink <- persistInDB(stickerId, ipfsFile, stickerLinkType)
    } yield stickerLink
  }

  private def getDesiredDimensions(stickerDimensions: StickerDimensions): Try[StickerDimensions] = Try {
    if (stickerDimensions.width < stickerDimensions.height) {
      StickerDimensions(targetSize, targetSize * stickerDimensions.height / stickerDimensions.width)
    } else {
      StickerDimensions(targetSize * stickerDimensions.width / stickerDimensions.height, targetSize)
    }
  }

  private def getStickerDimensionsWhenFilename(absolutePathFile: File): Try[StickerDimensions] = {
    Try {
      val bufferedImage: BufferedImage = ImageIO.read(absolutePathFile)
      StickerDimensions(bufferedImage.getWidth, bufferedImage.getHeight)
    }.transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }

  private def persistInFs(
      stickerLinkType: StickerLinkType,
      extension: String,
      relation: StickerDirectoryRelation,
      fileBytes: Array[Byte]
  ): Try[Unit] = {
    fileSystemPersistenceHandler
      .persistInFS(stickerLinkType, fileBytes, extension, relation.directoryName, configuration)
  }

  private def getStickerDirectoryRelation(stickerId: MaybeStickerId): Try[StickerDirectoryRelation] = {
    stickerDirectoryRelationsService
      .getByStickerId(stickerId)
      .flatMap(TryUtils.forceOptionExistence(stickerId))
  }

  private def persistInDB(
      stickerId: MaybeStickerId,
      ipfsFile: IpfsFile,
      stickerLinkType: StickerLinkType
  ): Try[StickerLink] = {
    val stickerLink: StickerLink = dtos.StickerLink(
      id = None,
      stickerId = stickerId,
      filename = ipfsFile.name,
      ipfsCID = ipfsFile.hash,
      `type` = stickerLinkType,
      creationTimestamp = LocalDateTime.now()
    )
    stickerLinksService.upsert(stickerLink).flatMap(TryUtils.forceOptionExistence())
  }

  private def persistInIPFS(
      stickerLinkType: StickerLinkType,
      directoryName: String,
      extension: String,
      fileBytes: Array[Byte]
  ): Try[IpfsFile] = {
    val remoteAbsolutePath =
      fileSystemPathHandler.getRemoteAbsolutePath(configuration, directoryName, stickerLinkType, extension)
    val filename = fileSystemPathHandler.getFilename(directoryName, stickerLinkType, extension)
    ipfsService
      .addNoCopy(bytes = fileBytes, fileName = filename, remoteAbsolutePath = remoteAbsolutePath)
  }
}
