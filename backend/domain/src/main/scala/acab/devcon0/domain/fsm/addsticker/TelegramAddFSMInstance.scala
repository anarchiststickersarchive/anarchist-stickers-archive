package acab.devcon0.domain.fsm.addsticker

import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import acab.devcon0.fsm.FiniteStateMachineInstance

case class TelegramAddFSMInstance(
    override val id: TelegramFSMId,
    override val state: AddStickerFSMState,
    override val data: AddStickerFSMData
) extends FiniteStateMachineInstance[TelegramFSMId](id, state, data)
