package acab.devcon0.domain.adapters.index

import acab.devcon0.domain.dtos.AggregatedSticker
import acab.devcon0.domain.ports.input.index.{IndexByPageQuery, IndexByPageQueryHandler}
import acab.devcon0.domain.service.AggregatedStickersService

import scala.util.Try

class IndexByPageQueryHandlerImpl(val aggregatedStickersService: AggregatedStickersService)
    extends IndexByPageQueryHandler {
  override def handle(query: IndexByPageQuery): Try[Seq[AggregatedSticker]] =
    aggregatedStickersService.getAll(query.page, query.pageSize, query.searchTerm)
}
