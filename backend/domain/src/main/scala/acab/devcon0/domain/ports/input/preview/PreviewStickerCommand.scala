package acab.devcon0.domain.ports.input.preview

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import com.typesafe.scalalogging.Logger

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

case class PreviewStickerCommand(stickerId: MaybeStickerId, telegramUserId: TelegramUserId)
    extends Command[MaybeStickerId]

case class PreviewStickerSocialMediaPlainCommand(
    stickerId: MaybeStickerId,
    telegramUserId: TelegramUserId,
    publicationPlatform: Option[PublicationPlatform] = None
) extends Command[MaybeStickerId]

sealed abstract class PreviewStickerEvent[T] extends Event[T]
case class PreviewStickerSuccessEvent(stickerId: MaybeStickerId) extends PreviewStickerEvent[Unit]
case class PreviewStickerErrorEvent(stickerId: MaybeStickerId, throwable: Throwable)
    extends PreviewStickerEvent[Throwable]

trait PreviewStickerCommandHandler extends CommandHandler[PreviewStickerCommand, MaybeStickerId, PreviewStickerEvent[_]]
trait PreviewStickerSocialMediaPlainCommandHandler
    extends CommandHandler[PreviewStickerSocialMediaPlainCommand, MaybeStickerId, PreviewStickerEvent[_]]

object PreviewStickerEventProcessor {
  def apply(result: Try[PreviewStickerEvent[_]], logger: Logger): Try[MaybeStickerId] = {
    result match {
      case Failure(exception)                              => handleErrorCase(exception, logger)
      case Success(PreviewStickerErrorEvent(_, exception)) => handleErrorCase(exception, logger)
      case Success(PreviewStickerSuccessEvent(stickerId))  => Success(stickerId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[MaybeStickerId] = {
    logger.error("PreviewStickerEventProcessor", exception)
    Failure(exception)
  }
}

object PreviewStickerSocialMediaPlainEventProcessor {
  def apply(result: Try[PreviewStickerEvent[_]], logger: Logger): Try[MaybeStickerId] = {
    result match {
      case Failure(exception)                              => handleErrorCase(exception, logger)
      case Success(PreviewStickerErrorEvent(_, exception)) => handleErrorCase(exception, logger)
      case Success(PreviewStickerSuccessEvent(stickerId))  => Success(stickerId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[MaybeStickerId] = {
    logger.error("PreviewStickerEventProcessor", exception)
    Failure(exception)
  }
}
