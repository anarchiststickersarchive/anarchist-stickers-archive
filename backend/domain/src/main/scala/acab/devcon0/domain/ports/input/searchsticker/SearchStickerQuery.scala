package acab.devcon0.domain.ports.input.searchsticker

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.Sticker

case class SearchStickerQuery(searchTerm: String) extends Query[Seq[Sticker]]
trait SearchStickerQueryHandler extends QueryHandler[SearchStickerQuery, Seq[Sticker]]
