package acab.devcon0.domain.service

import java.io.File
import scala.util.Try

trait IngestionQueueService {
  def getRasterize(maybeSource: Option[String]): Try[Option[File]]

  def getVector(rasterize: File): Try[Option[File]]

  def remove(rasterize: File, vector: Option[File]): Try[Unit]

  def count(): Try[Long]

  def refresh(): Try[Unit]
}
