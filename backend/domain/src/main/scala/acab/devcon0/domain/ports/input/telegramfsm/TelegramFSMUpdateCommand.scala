package acab.devcon0.domain.ports.input.telegramfsm

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.service.TelegramFiniteStateMachine.{TelegramFSMId, TelegramFSMInstance}
import com.typesafe.scalalogging.Logger

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

case class TelegramFSMUpdateCommand(id: TelegramFSMId, instance: TelegramFSMInstance) extends Command[TelegramFSMId]
sealed abstract class TelegramFSMUpdateEvent[T] extends Event[T]
case class TelegramFSMSuccessUpdateEvent(id: TelegramFSMId) extends TelegramFSMUpdateEvent[Unit]
case class TelegramFSMErrorUpdateEvent(id: TelegramFSMId, throwable: Throwable)
    extends TelegramFSMUpdateEvent[Throwable]

trait TelegramFSMUpdateCommandHandler
    extends CommandHandler[TelegramFSMUpdateCommand, TelegramFSMId, TelegramFSMUpdateEvent[_]]

object TelegramFSMUpdateEventProcessor {
  def apply(result: Try[TelegramFSMUpdateEvent[_]], logger: Logger): Try[TelegramFSMId] = {
    result match {
      case Failure(exception)                                    => handleErrorCase(exception, logger)
      case Success(TelegramFSMErrorUpdateEvent(_, exception))    => handleErrorCase(exception, logger)
      case Success(TelegramFSMSuccessUpdateEvent(telegramFSMId)) => Success(telegramFSMId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[TelegramFSMId] = {
    logger.error("TelegramFSMUpdateEventProcessor", exception)
    Failure(exception)
  }
}
