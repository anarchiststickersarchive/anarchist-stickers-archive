package acab.devcon0.domain.dtos

import io.bartholomews.iso.LanguageCode

import java.util.UUID

object TypeAliases {
  type StickerId = Long
  type MaybeStickerId = Option[StickerId]
  type StickerName = String
  type StickerNotes = Option[String]
  type StickerSource = Option[String]
  type StickerLanguage = Option[LanguageCode]

  type TagId = Option[Long]
  type TagValue = String

  type StickerLinkId = Option[Long]
  type StickerLinkFilename = String
  type StickerLinkIpfsCid = String
  type PublicationId = Option[Long]

  type StickerDirectoryRelationDirectoryName = String

  type DraftStickerId = UUID
}
