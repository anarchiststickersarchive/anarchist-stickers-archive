package acab.devcon0.domain.adapters.addsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.{DraftSticker, DraftStickerLink, DraftStickerLinkFilename, StickerLinkTypes}
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import java.io.File
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class DraftStickerAbortCommandHandlerImpl(
                                           val telegramFSMService: TelegramFSMService,
                                           val ingestionFinalPickService: IngestionQueueService,
                                           val draftStickersService: DraftStickersService
) extends DraftStickerAbortCommandHandler
    with LazyLogging {

  override def handle(cmd: DraftStickerAbortCommand): Try[DraftStickerAbortEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) => handleErrorCase(cmd, throwable)
      case Success(_)         => handleSuccess(cmd)
    }
  }

  private def handleInner(cmd: DraftStickerAbortCommand): Try[Unit] = {
    deleteDraftStickerFiles(cmd)
    telegramFSMService.delete(cmd.telegramFSMId)
  }

  private def deleteDraftStickerFiles(cmd: DraftStickerAbortCommand): Try[Unit] = {
    for {
      draftSticker <- getDraftSticker(cmd)
      _ <- deleteDraftStickerFilesInner(draftSticker)
    } yield ()
  }

  private def getDraftSticker(cmd: DraftStickerAbortCommand): Try[DraftSticker] = {
    draftStickersService
      .getById(cmd.draftStickerId)
      .flatMap(TryUtils.forceOptionExistence())
  }

  private def deleteDraftStickerFilesInner(draftSticker: DraftSticker): Try[Unit] = Try {
    val originalDraftStickerLink: DraftStickerLink = draftSticker.stickerLinks(StickerLinkTypes.ORIGINAL)
    val maybeVectorDraftStickerLink: Option[DraftStickerLink] = draftSticker.stickerLinks.get(StickerLinkTypes.VECTOR)
    val maybeOriginalFile: Option[File] = originalDraftStickerLink match {
      case DraftStickerLinkFilename(absolutePath, _, _) => Some(new File(absolutePath))
      case _                                            => None
    }
    val maybeVectorFile: Option[File] = maybeVectorDraftStickerLink.flatMap {
      case DraftStickerLinkFilename(absolutePath, _, _) => Some(new File(absolutePath))
      case _                                            => None
    }
    maybeOriginalFile.map(originalFile => {
      ingestionFinalPickService.remove(
        originalFile,
        maybeVectorFile
      )
    })
  }

  private def handleSuccess(cmd: DraftStickerAbortCommand): Try[DraftStickerAbortEvent[_]] = {
    Success(
      DraftStickerAbortSuccessEvent(
        draftStickerId = cmd.draftStickerId
      )
    )
  }

  private def handleErrorCase(cmd: DraftStickerAbortCommand, throwable: Throwable): Try[DraftStickerAbortEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    Success(
      DraftStickerAbortErrorEvent(
        draftStickerId = cmd.draftStickerId,
        telegramFSMId = cmd.telegramFSMId,
        throwable = throwable
      )
    )
  }
}
