package acab.devcon0.domain.service

import acab.devcon0.domain.ports.output.BlueskyClient

import java.io.File
import scala.util.Try

class BlueskyServiceImpl(val blueskyClient: BlueskyClient) extends BlueskyService {

  override def addStatus(file: File, text: String): Try[Unit] = {
    for {
      login <- blueskyClient.login()
      mediaResponse <- blueskyClient.addMedia(file)(login)
      _ <- blueskyClient.createPost(text, mediaResponse.blob)(login)
    } yield ()
  }
}
