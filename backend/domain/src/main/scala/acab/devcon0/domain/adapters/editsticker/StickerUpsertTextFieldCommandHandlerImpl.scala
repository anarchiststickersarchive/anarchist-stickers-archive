package acab.devcon0.domain.adapters.editsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.ports.input.editsticker._
import acab.devcon0.domain.service.StickersService
import com.typesafe.scalalogging.LazyLogging
import io.bartholomews.iso.LanguageCode

import scala.util.{Failure, Success, Try}

class StickerUpsertTextFieldCommandHandlerImpl(stickersService: StickersService)
    extends StickerUpsertTextFieldCommandHandler
    with LazyLogging {

  override def handle(cmd: StickerUpsertTextFieldCommand): Try[StickerUpsertTextFieldEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) =>
        Success(StickerUpsertTextFieldErrorEvent(stickerId = cmd.stickerId, throwable = throwable))
      case Success(sticker) => Success(StickerUpsertTextFieldSuccessEvent(sticker = sticker))
    }
  }

  private def handleInner(cmd: StickerUpsertTextFieldCommand): Try[Sticker] = {
    cmd match {
      case StickerUpsertNameCommand(stickerId, value)     => upsertName(stickerId, value)
      case StickerUpsertLanguageCommand(stickerId, value) => upsertLanguage(stickerId, value)
      case StickerUpsertNotesCommand(stickerId, value)    => upsertNotes(stickerId, value)
      case StickerUpsertSourceCommand(stickerId, value)   => upsertSource(stickerId, value)
    }
  }

  private def upsertName(id: MaybeStickerId, value: String): Try[Sticker] = {
    val modifyFunction: Sticker => Sticker = addSticker => addSticker.copy(name = value)
    upsert(id, modifyFunction)
  }

  private def upsertNotes(id: MaybeStickerId, value: String): Try[Sticker] = {
    val modifyFunction: Sticker => Sticker = addSticker => addSticker.copy(notes = Some(value))
    upsert(id, modifyFunction)
  }

  private def upsertSource(id: MaybeStickerId, value: String): Try[Sticker] = {
    val modifyFunction: Sticker => Sticker = addSticker => addSticker.copy(source = Some(value))
    upsert(id, modifyFunction)
  }

  private def upsertLanguage(id: MaybeStickerId, value: String): Try[Sticker] = {
    val modifyFunction: Sticker => Sticker = addSticker =>
      addSticker.copy(language = LanguageCode.withValueOpt(value.toLowerCase))
    upsert(id, modifyFunction)
  }

  private def upsert(
      id: MaybeStickerId,
      modifyAddStickerFunction: Sticker => Sticker
  ): Try[Sticker] = {
    getAddSticker(id)
      .map(modifyAddStickerFunction)
      .flatMap(stickersService.upsert)
      .flatMap(TryUtils.forceOptionExistence[Sticker, MaybeStickerId](Some(id)))
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }

  private def getAddSticker(id: MaybeStickerId): Try[Sticker] = {
    stickersService
      .getById(id)
      .flatMap(TryUtils.forceOptionExistence())
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }
}
