package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.{Bookmark, BookmarkId, BookmarkUserId}

import scala.util.Try

trait BookmarksRepository {

  def getByUser(bookmarkUserId: BookmarkUserId): Try[Seq[Bookmark]]

  def upsert(bookmark: Bookmark): Try[Bookmark]

  def delete(bookmarkId: BookmarkId): Try[Unit]
}
