package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.IpfsFile
import acab.devcon0.domain.ports.output.{IpfsClient, RemotePinRepository}
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

class IpfsService(ipfsClient: IpfsClient) extends LazyLogging {
  def addNoCopy(bytes: Array[Byte], fileName: String, remoteAbsolutePath: String): Try[IpfsFile] = {
    ipfsClient
      .addNoCopy(bytes = bytes, fileName = fileName, remoteAbsolutePath = remoteAbsolutePath)
      .map(ipfsAddResponse => IpfsFile(ipfsAddResponse.name, ipfsAddResponse.hash, ipfsAddResponse.size))
  }
}
