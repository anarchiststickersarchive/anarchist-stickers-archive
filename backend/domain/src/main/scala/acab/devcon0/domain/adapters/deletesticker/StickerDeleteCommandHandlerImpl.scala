package acab.devcon0.domain.adapters.deletesticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.TypeAliases.StickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.deletesticker._
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}

class StickerDeleteCommandHandlerImpl(
    stickersService: StickersService,
    configuration: Configuration,
    stickerLinksService: StickerLinksService,
    stickerDirectoryRelationsService: StickerDirectoryRelationsService,
    publicationsService: PublicationsService,
    fileSystemPersistenceHandler: FileSystemPersistenceHandler = new FileSystemPersistenceHandler() {}
) extends StickerDeleteCommandHandler
    with LazyLogging {

  override def handle(cmd: StickerDeleteCommand): Try[StickerDeleteEvent[_]] = {
    val id: StickerId = cmd.stickerId
    handleInner(id) match {
      case Failure(throwable) =>
        Success(StickerDeleteErrorEvent(stickerId = cmd.stickerId, throwable = throwable))
      case Success(sticker) => Success(StickerDeleteSuccessEvent(sticker = sticker))
    }
  }

  private def handleInner(id: StickerId): Try[Sticker] = {
    getSticker(id)
      .flatMap(sticker => tupleStickerWithDirectoryRelation(sticker))
      .flatMap(tuple2 => deleteBinaries(tuple2))
      .flatMap(sticker => deleteStickerDirectoryRelation(sticker))
      .flatMap(sticker => deleteStickerLinks(sticker))
      .flatMap(sticker => deleteStickerPublications(sticker))
      .flatMap(sticker => deleteSticker(sticker))
  }

  private def deleteSticker(sticker: Sticker): Try[Sticker] = {
    stickersService.delete(sticker.id).map(_ => sticker)
  }

  private def deleteStickerPublications(sticker: Sticker): Try[Sticker] = {
    publicationsService.deleteByStickerId(sticker.id).map(_ => sticker)
  }

  private def deleteStickerLinks(sticker: Sticker): Try[Sticker] = {
    stickerLinksService.deleteByStickerId(sticker.id).map(_ => sticker)
  }

  private def deleteStickerDirectoryRelation(sticker: Sticker): Try[Sticker] = {
    stickerDirectoryRelationsService.deleteByStickerId(sticker.id).map(_ => sticker)
  }

  private def deleteBinaries(tuple2: (Sticker, StickerDirectoryRelation)): Try[Sticker] = {
    fileSystemPersistenceHandler
      .deleteStickerDirectory(tuple2._2.directoryName, configuration)
      .map(_ => tuple2._1)
  }

  private def tupleStickerWithDirectoryRelation(sticker: Sticker): Try[(Sticker, StickerDirectoryRelation)] = {
    stickerDirectoryRelationsService
      .getByStickerId(stickerId = sticker.id)
      .flatMap(TryUtils.forceOptionExistence())
      .map(relation => (sticker, relation))
  }

  private def getSticker(id: StickerId): Try[Sticker] = {
    stickersService
      .getById(Some(id))
      .flatMap(TryUtils.forceOptionExistence())
  }
}
