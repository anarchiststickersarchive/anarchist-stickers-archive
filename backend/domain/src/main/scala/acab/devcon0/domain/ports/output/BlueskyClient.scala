package acab.devcon0.domain.ports.output

import acab.devcon0.domain.codecs.CirceCodecs.Decoders.{
  blueskyAddMediaResponseDecoder,
  blueskyPostStatusResponseDecoder
}
import acab.devcon0.domain.codecs.CirceCodecs.Encoders.{blueskyCreatePostRequestEncoder, blueskyLoginRequestEncoder}
import acab.devcon0.domain.dtos.{BlueskyAddMediaResponse, BlueskyBlob, BlueskyLoginResponse, BlueskyPostStatusResponse}

import java.io.File
import scala.util.Try

trait BlueskyClient {

  def addMedia(file: File)(implicit login: BlueskyLoginResponse): Try[BlueskyAddMediaResponse]

  def createPost(text: String, blob: BlueskyBlob)(implicit login: BlueskyLoginResponse): Try[BlueskyPostStatusResponse]

  def login(): Try[BlueskyLoginResponse]
}
