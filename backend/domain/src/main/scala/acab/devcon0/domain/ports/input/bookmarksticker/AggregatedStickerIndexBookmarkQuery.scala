package acab.devcon0.domain.ports.input.bookmarksticker

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.{AggregatedSticker, BookmarkUserId}

case class AggregatedStickerIndexBookmarkQuery(bookmarkUserId: BookmarkUserId) extends Query[Seq[AggregatedSticker]]
trait AggregatedStickerIndexBookmarkQueryHandler
    extends QueryHandler[AggregatedStickerIndexBookmarkQuery, Seq[AggregatedSticker]]
