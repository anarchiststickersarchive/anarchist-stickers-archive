package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.DraftSticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId

import scala.util.Try

trait DraftStickersRepository {
  def getById(id: DraftStickerId): Try[Option[DraftSticker]]
  def upsert(draftSticker: DraftSticker): Try[DraftStickerId]
  def delete(id: DraftStickerId): Try[Unit]
}
