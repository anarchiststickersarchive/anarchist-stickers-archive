package acab.devcon0.domain.adapters.publish

import acab.devcon0.commons.{FunctionUtils, ProjectError, TryUtils}
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.dtos.{AggregatedSticker, Publication, TelegramUserId}
import acab.devcon0.domain.ports.input.publish.PublishStickerCommandFacade
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import java.time.LocalDateTime
import scala.language.postfixOps
import scala.util.{Failure, Try}

class PublishStickerCommandFacadeImpl(
    val configuration: Configuration,
    val captionService: CaptionService,
    val aggregatedStickersService: AggregatedStickersService,
    val publicationsService: PublicationsService,
    val publishToPlatformCommands: Map[PublicationPlatform, PublishToPlatformCommand],
    val telegramService: TelegramService
) extends LazyLogging
    with PublishStickerCommandFacade {

  override def apply(stickerId: MaybeStickerId, publicationPlatforms: Seq[PublicationPlatform]): Try[Unit] = {
    getAggregatedStickerByStickerId(stickerId)
      .transform(TryUtils.doNothing(), notifyAdminsOnDomainError)
      .flatMap(aggregatedSticker => publishToPlatforms(publicationPlatforms, aggregatedSticker))
      .map(FunctionUtils.flushResult)
  }

  private def notifyAdminsOnDomainError: Throwable => Try[AggregatedSticker] = { case d: ProjectError =>
    telegramService.sendMessage(TelegramUserId(configuration.telegram.adminUserId.id), d.getMessage)
    Failure(d)
  }

  private def getAggregatedStickerByStickerId(stickerId: MaybeStickerId): Try[AggregatedSticker] = {
    aggregatedStickersService
      .getByStickerId(stickerId)
  }

  private def publishToPlatforms(
      publicationPlatforms: Seq[PublicationPlatform],
      aggregatedSticker: AggregatedSticker
  ): Try[Seq[Publication]] = {
    val listOfTriedPublications = publishToPlatformCommands
      .filter(entrySet => publicationPlatforms.contains(entrySet._1))
      .map(entrySet => publishToPlatform(aggregatedSticker, entrySet))
    Try(listOfTriedPublications.map(_.get).toList)
  }

  private def publishToPlatform(
      aggregatedSticker: AggregatedSticker,
      entrySet: (PublicationPlatform, PublishToPlatformCommand)
  ): Try[Publication] = {
    val publicationPlatform: PublicationPlatform = entrySet._1
    val command: PublishToPlatformCommand = entrySet._2
    logger.debug(s"Publishing to $publicationPlatform, aggregatedSticker=$aggregatedSticker")
    command
      .publish(aggregatedSticker)
      .flatMap(_ => publicationsService.upsert(buildPublication(aggregatedSticker, publicationPlatform)))
      .flatMap(TryUtils.forceOptionExistence())
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }

  private def buildPublication(
      aggregatedSticker: AggregatedSticker,
      publicationPlatform: PublicationPlatform
  ): Publication = {
    Publication(
      id = None,
      stickerId = aggregatedSticker.sticker.id,
      platform = publicationPlatform,
      creationTimestamp = LocalDateTime.now()
    )
  }
}
