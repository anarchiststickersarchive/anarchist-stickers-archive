package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.Publication
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, PublicationId}
import acab.devcon0.domain.ports.output.PublicationsRepository
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

class PublicationsService(repository: PublicationsRepository) extends LazyLogging {

  def getAll: Try[Seq[Publication]] = {
    repository.getAll
  }

  def getById(publicationId: PublicationId): Try[Option[Publication]] = {
    repository.getById(publicationId)
  }

  def upsert(publication: Publication): Try[Option[Publication]] = {
    repository
      .upsert(publication)
      .flatMap(publicationId => getById(publicationId))
  }

  def delete(publicationId: PublicationId): Try[Unit] = {
    repository.delete(publicationId)
  }

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit] = {
    repository.deleteByStickerId(stickerId)
  }
}
