package acab.devcon0.domain.adapters.previewsticker

import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.{AggregatedSticker, PublicationPlatforms}
import acab.devcon0.domain.ports.input.preview._
import acab.devcon0.domain.service._
import com.bot4s.telegram.models.InputFile
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class PreviewStickerSocialMediaPlainCommandHandlerImpl(
    val captionService: CaptionService,
    val aggregatedStickersService: AggregatedStickersService,
    val telegramService: TelegramService,
    val squarePhotoHandler: SquarePhotoHandler
) extends PreviewStickerSocialMediaPlainCommandHandler
    with LazyLogging {

  override def handle(cmd: PreviewStickerSocialMediaPlainCommand): Try[PreviewStickerEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) => handleErrorCase(cmd, throwable)
      case Success(_)         => handleSuccess(cmd)
    }
  }

  private def handleInner(cmd: PreviewStickerSocialMediaPlainCommand): Try[Unit] = {
    for {
      aggregatedSticker <- aggregatedStickersService.getByStickerId(cmd.stickerId)
      caption <- captionService.get(aggregatedSticker.sticker, aggregatedSticker.links)
      result <- telegramService.sendPhoto(
        to = cmd.telegramUserId,
        text = caption,
        inputFile = getPhoto(aggregatedSticker, cmd.publicationPlatform)
      )
    } yield result
  }

  private def getPhoto(
      aggregatedSticker: AggregatedSticker,
      publicationPlatform: Option[PublicationPlatform]
  ): InputFile = {
    publicationPlatform match {
      case Some(PublicationPlatforms.INSTAGRAM) => getSquaredPhoto(aggregatedSticker)
      case _                                    => getOriginalPhoto(aggregatedSticker)
    }
  }

  private def getOriginalPhoto(aggregatedSticker: AggregatedSticker): InputFile = {
    InputFile(aggregatedSticker.publishingPhoto)
  }

  private def getSquaredPhoto(aggregatedSticker: AggregatedSticker): InputFile = {
    InputFile.Contents(
      filename = "filename",
      contents = squarePhotoHandler.get(aggregatedSticker.publishingPhoto)
    )
  }

  private def handleSuccess(cmd: PreviewStickerSocialMediaPlainCommand): Try[PreviewStickerEvent[_]] = {
    Success(PreviewStickerSuccessEvent(stickerId = cmd.stickerId))
  }

  private def handleErrorCase(
      cmd: PreviewStickerSocialMediaPlainCommand,
      throwable: Throwable
  ): Try[PreviewStickerEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    val message = "Preview failed. If possible, check the logs, otherwise, contact the admins"
    telegramService.sendMessage(to = cmd.telegramUserId, text = message)
    Success(PreviewStickerErrorEvent(stickerId = cmd.stickerId, throwable = throwable))
  }
}
