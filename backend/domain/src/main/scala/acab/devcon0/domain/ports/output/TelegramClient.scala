package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.TelegramUserId
import com.bot4s.telegram.methods.ParseMode.ParseMode
import com.bot4s.telegram.models.{File, InputFile}

import scala.util.Try

trait TelegramClient {
  def sendMessage(to: TelegramUserId, text: String): Try[Unit]
  def sendPhoto(to: TelegramUserId, text: String, inputFile: InputFile, parseMode: Option[ParseMode]): Try[Unit]
  def getFile(fileId: String): Try[File]
  def getFileBytes(path: String): Try[Array[Byte]]
}
