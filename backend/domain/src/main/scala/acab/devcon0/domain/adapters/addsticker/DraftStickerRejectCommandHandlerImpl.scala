package acab.devcon0.domain.adapters.addsticker

import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}

class DraftStickerRejectCommandHandlerImpl(
    draftStickersService: DraftStickersService
) extends DraftStickerRejectCommandHandler
    with LazyLogging {

  override def handle(cmd: DraftStickerRejectCommand): Try[DraftStickerRejectEvent[_]] = {
    handleInner(cmd.draftStickerId) match {
      case Failure(throwable) =>
        Success(DraftStickerRejectErrorEvent(draftStickerId = cmd.draftStickerId, throwable = throwable))
      case Success(draftStickerId) => Success(DraftStickerRejectSuccessEvent(draftStickerId = draftStickerId))
    }
  }

  private def handleInner(id: DraftStickerId): Try[DraftStickerId] = {
    draftStickersService
      .delete(id)
      .map(_ => id)
  }
}
