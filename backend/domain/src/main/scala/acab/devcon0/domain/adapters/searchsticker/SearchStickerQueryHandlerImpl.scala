package acab.devcon0.domain.adapters.searchsticker

import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.ports.input.searchsticker.{SearchStickerQuery, SearchStickerQueryHandler}
import acab.devcon0.domain.service._

import scala.language.postfixOps
import scala.util.Try

class SearchStickerQueryHandlerImpl(val stickersService: StickersService) extends SearchStickerQueryHandler {
  override def handle(query: SearchStickerQuery): Try[Seq[Sticker]] = stickersService.search(query.searchTerm)
}
