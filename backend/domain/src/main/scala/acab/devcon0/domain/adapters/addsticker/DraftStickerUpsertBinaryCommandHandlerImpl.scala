package acab.devcon0.domain.adapters.addsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.ports.output.VectorizerClient
import acab.devcon0.domain.service.{DraftStickersService, TelegramService}
import com.typesafe.scalalogging.LazyLogging

import java.awt.image.BufferedImage
import java.io.{File, FileOutputStream}
import java.nio.file.Files
import java.util.UUID
import javax.imageio.ImageIO
import scala.util.{Failure, Success, Try}

class DraftStickerUpsertBinaryCommandHandlerImpl(
    draftStickersService: DraftStickersService,
    vectorizerClient: VectorizerClient,
    telegramService: TelegramService
) extends DraftStickerUpsertBinaryCommandHandler
    with LazyLogging {

  private val forbiddenExtensions: List[String] = List("pdf", "ai")
  private val targetSize: Int = 2500

  override def handle(cmd: DraftStickerUpsertBinaryCommand): Try[DraftStickerUpsertBinaryEvent[_]] = {
    cmd match {
      case telegramCmd: DraftStickerUpsertBinaryTelegramCommand       => handleTelegram(telegramCmd)
      case vectorOnDemandCmd: DraftStickerUpsertVectorOnDemandCommand => handleVectorOnDemand(vectorOnDemandCmd)
    }
  }

  private def handleVectorOnDemand(
      cmd: DraftStickerUpsertVectorOnDemandCommand
  ): Try[DraftStickerUpsertBinaryEvent[_]] = {
    handleVectorOnDemandInner(cmd) match {
      case Success(draftSticker) => handleSuccessCase(draftSticker)
      case Failure(throwable)    => handleErrorCase(cmd, throwable)
    }
  }

  private def handleVectorOnDemandInner(cmd: DraftStickerUpsertVectorOnDemandCommand): Try[DraftSticker] = {
    for {
      draftSticker <- getDraftSticker(cmd.draftStickerId)
      absolutePathFile <- getStickerRasterizeAsFile(draftSticker)
      stickerDimensions <- getStickerDimensionsWhenFilename(absolutePathFile)
      desiredDimensions <- getDesiredDimensions(stickerDimensions)
      vectorBytes <- vectorizerClient.get(
        desiredWidth = desiredDimensions.width,
        desiredHeight = desiredDimensions.height,
        model = "clipart",
        colors = "auto",
        imageInputAbsolutePath = absolutePathFile.toString,
        stickerRasterizeBytes = Files.readAllBytes(absolutePathFile.toPath)
      )
      updatedDraftSticker <- upsertVectorToDraftSticker(draftSticker, vectorBytes)
    } yield updatedDraftSticker
  }

  private def handleTelegram(cmd: DraftStickerUpsertBinaryTelegramCommand): Try[DraftStickerUpsertBinaryEvent[_]] = {
    upsertDraftStickerWithFileBytes(
      draftStickerId = cmd.draftStickerId,
      stickerLinkType = cmd.stickerLinkType,
      eitherTelegramFileIdOrAbsoluteFilename = cmd.eitherTelegramFileIdOrAbsoluteFilename,
      extension = cmd.extension
    ) match {
      case Success(draftSticker) => handleSuccessCase(draftSticker)
      case Failure(throwable)    => handleErrorCase(cmd, throwable)
    }
  }

  private def upsertDraftStickerWithFileBytes(
      draftStickerId: DraftStickerId,
      extension: String,
      stickerLinkType: StickerLinkType,
      eitherTelegramFileIdOrAbsoluteFilename: Either[TelegramFileId, String]
  ): Try[DraftSticker] = {
    filterOutNonValidExtensions(extension)
      .flatMap(_ => getDraftSticker(draftStickerId))
      .flatMap(buildUpdatedDraftSticker(extension, stickerLinkType, eitherTelegramFileIdOrAbsoluteFilename, _))
      .flatMap(draftStickersService.upsert)
      .flatMap(TryUtils.forceOptionExistence[DraftSticker, DraftStickerId](Some(draftStickerId)))
  }

  private def buildUpdatedDraftSticker(
      extension: String,
      stickerLinkType: StickerLinkType,
      eitherTelegramFileIdOrAbsoluteFilename: Either[TelegramFileId, String],
      draftSticker: DraftSticker
  ): Try[DraftSticker] = Try {
    val maybeStickerDimensions: Option[StickerDimensions] = Some(stickerLinkType)
      .filter(linkType => linkType.equals(StickerLinkTypes.ORIGINAL))
      .flatMap(_ => getStickerDimensions(eitherTelegramFileIdOrAbsoluteFilename))
    val triedLink: Try[DraftStickerLink] = eitherTelegramFileIdOrAbsoluteFilename
      .fold(
        telegramFileId => {
          telegramService
            .getFileBytes(telegramFileId)
            .map(fileBytes => DraftStickerLinkBinary(fileBytes, extension, maybeStickerDimensions))
        },
        absoluteFilename => {
          Success(DraftStickerLinkFilename(absoluteFilename, extension, maybeStickerDimensions))
        }
      )
    triedLink
      .map(draftStickerLink => {
        val stickerLinksMap = draftSticker.stickerLinks.updated(stickerLinkType, draftStickerLink)
        draftSticker.copy(stickerLinks = stickerLinksMap)
      })
  }.flatten

  private def filterOutNonValidExtensions(extension: String): Try[Unit] = {
    Try(extension.toLowerCase)
      .filter(!forbiddenExtensions.contains(_))
      .map(_ => ())
  }

  private def upsertVectorToDraftSticker(
      draftSticker: DraftSticker,
      vectorArrayBytes: Array[Byte]
  ): Try[DraftSticker] = {
    Try(DraftStickerLinkBinary(vectorArrayBytes, "svg", None))
      .map(draftStickerLink => {
        val stickerLinksMap = draftSticker.stickerLinks.updated(StickerLinkTypes.VECTOR, draftStickerLink)
        draftSticker.copy(stickerLinks = stickerLinksMap)
      })
      .flatMap(draftStickersService.upsert)
      .flatMap(TryUtils.forceOptionExistence[DraftSticker, DraftStickerId](Some(draftSticker.id)))
  }

  private def getStickerDimensions(
      eitherTelegramFileIdOrAbsoluteFilename: Either[TelegramFileId, String]
  ): Option[StickerDimensions] = {
    eitherTelegramFileIdOrAbsoluteFilename
      .fold(
        telegramFileId => telegramService.getFileBytes(telegramFileId).toOption.flatMap(getStickerDimensionsWhenBinary),
        absoluteFilename => getStickerDimensionsWhenFilename(absoluteFilename)
      )
  }

  private def getStickerRasterizeAsFile(draftSticker: DraftSticker): Try[File] = {
    (draftSticker.stickerLinks.get(StickerLinkTypes.ORIGINAL) match {
      case Some(value) => Success(value.bytes)
      case None        => Failure(new NoSuchElementException("Original not present"))
    }).map(arrayBytes => {
      val tempFile: File = File.createTempFile("prefix", UUID.randomUUID().toString, null)
      val fos = new FileOutputStream(tempFile)
      fos.write(arrayBytes)
      tempFile
    })

  }

  private def getStickerDimensionsWhenBinary(fileBytes: Array[Byte]): Option[StickerDimensions] = {
    Try {
      val tempFile = File.createTempFile("prefix", UUID.randomUUID().toString, null)
      val fos = new FileOutputStream(tempFile)
      fos.write(fileBytes)
      val bufferedImage: BufferedImage = ImageIO.read(tempFile)
      StickerDimensions(bufferedImage.getWidth, bufferedImage.getHeight)
    }.transform(TryUtils.doNothing(), TryUtils.peekException(logger)).toOption
  }

  private def getStickerDimensionsWhenFilename(absolutePathFile: File): Try[StickerDimensions] = {
    Try {
      val bufferedImage: BufferedImage = ImageIO.read(absolutePathFile)
      StickerDimensions(bufferedImage.getWidth, bufferedImage.getHeight)
    }.transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }

  private def getStickerDimensionsWhenFilename(absoluteFilename: String): Option[StickerDimensions] = {
    Try(new File(absoluteFilename))
      .flatMap(getStickerDimensionsWhenFilename)
      .toOption
  }

  private def getDesiredDimensions(stickerDimensions: StickerDimensions): Try[StickerDimensions] = Try {
    if (stickerDimensions.width < stickerDimensions.height) {
      StickerDimensions(targetSize, targetSize * stickerDimensions.height / stickerDimensions.width)
    } else {
      StickerDimensions(targetSize * stickerDimensions.width / stickerDimensions.height, targetSize)
    }
  }

  private def getDraftSticker(draftStickerId: DraftStickerId): Try[DraftSticker] = {
    draftStickersService
      .getById(draftStickerId)
      .flatMap(TryUtils.forceOptionExistence())
  }

  private def handleSuccessCase(draftSticker: DraftSticker): Try[DraftStickerUpsertBinarySuccessEvent] = {
    Success(DraftStickerUpsertBinarySuccessEvent(draftSticker = draftSticker))
  }

  private def handleErrorCase(
      cmd: DraftStickerUpsertBinaryCommand,
      throwable: Throwable
  ): Try[DraftStickerUpsertBinaryErrorEvent] = {
    logger.error(this.getClass.getName, throwable)
    Success(DraftStickerUpsertBinaryErrorEvent(draftStickerId = cmd.draftStickerId, throwable = throwable))
  }
}
