package acab.devcon0.domain.adapters.bookmarksticker

import acab.devcon0.domain.dtos.{AggregatedSticker, Bookmark}
import acab.devcon0.domain.ports.input.bookmarksticker.{
  AggregatedStickerIndexBookmarkQuery,
  AggregatedStickerIndexBookmarkQueryHandler
}
import acab.devcon0.domain.service.{AggregatedStickersService, BookmarksService}
import cats.implicits._

import scala.util.Try

class AggregatedStickerIndexBookmarkQueryHandlerImpl(
    val aggregatedStickersService: AggregatedStickersService,
    val bookmarksService: BookmarksService
) extends AggregatedStickerIndexBookmarkQueryHandler {
  override def handle(query: AggregatedStickerIndexBookmarkQuery): Try[Seq[AggregatedSticker]] = {
    for {
      bookmarks <- bookmarksService.getByUser(query.bookmarkUserId)
      stickers: Seq[AggregatedSticker] <- bookmarkToSticker(bookmarks)
    } yield stickers
  }

  private def bookmarkToSticker(bookmarks: Seq[Bookmark]): Try[Seq[AggregatedSticker]] = {
    bookmarks.toList.traverse(bookmark => aggregatedStickersService.getByStickerId(Some(bookmark.id.stickerId)))
  }
}
