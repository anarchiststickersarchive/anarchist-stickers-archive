package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.Publication
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, PublicationId}

import scala.util.Try

trait PublicationsRepository {

  def getAll: Try[Seq[Publication]]

  def getById(id: PublicationId): Try[Option[Publication]]

  def upsert(publication: Publication): Try[PublicationId]

  def delete(publicationId: PublicationId): Try[Unit]

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit]
}
