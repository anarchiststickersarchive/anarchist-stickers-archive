package acab.devcon0.domain.fsm.addsticker

import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.fsm.FiniteStateMachineData

case class AddStickerFSMData(draftStickerId: DraftStickerId) extends FiniteStateMachineData
