package acab.devcon0.domain.service

import acab.devcon0.domain.ports.output.PixelfedClient

import java.io.File
import scala.util.Try

class PixelfedServiceImpl(val pixelfedClient: PixelfedClient) extends PixelfedService {

  override def addStatus(file: File, text: String): Try[Unit] = {
    for {
      addMediaResponse <- pixelfedClient.addMedia(file)
      _ <- pixelfedClient.createPost(text, addMediaResponse.id)
    } yield {}
  }
}
