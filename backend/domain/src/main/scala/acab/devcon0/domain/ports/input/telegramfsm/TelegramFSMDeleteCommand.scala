package acab.devcon0.domain.ports.input.telegramfsm

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

case class TelegramFSMDeleteCommand(id: TelegramFSMId) extends Command[TelegramFSMId]
sealed abstract class TelegramFSMDeleteEvent[T] extends Event[T]
case class TelegramFSMSuccessDeleteEvent(id: TelegramFSMId) extends TelegramFSMDeleteEvent[Unit]
case class TelegramFSMErrorDeleteEvent(id: TelegramFSMId, throwable: Throwable)
    extends TelegramFSMDeleteEvent[Throwable]

trait TelegramFSMDeleteCommandHandler
    extends CommandHandler[TelegramFSMDeleteCommand, TelegramFSMId, TelegramFSMDeleteEvent[_]]

object TelegramFSMDeleteCommandImplicits {
  implicit class EventFlattenOps(result: Try[TelegramFSMDeleteEvent[_]]) extends LazyLogging {
    def flattenEvents: Try[TelegramFSMId] = {
      result match {
        case Failure(exception)                                 => handleErrorCase(exception)
        case Success(TelegramFSMErrorDeleteEvent(_, exception)) => handleErrorCase(exception)
        case Success(TelegramFSMSuccessDeleteEvent(sticker))    => Success(sticker)
      }
    }

    private def handleErrorCase(exception: Throwable): Failure[TelegramFSMId] = {
      logger.error("TelegramFSMDeleteEventProcessor", exception)
      Failure(exception)
    }
  }
}
