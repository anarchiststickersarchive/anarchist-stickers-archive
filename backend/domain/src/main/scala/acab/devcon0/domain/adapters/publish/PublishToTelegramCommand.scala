package acab.devcon0.domain.adapters.publish

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.{AggregatedSticker, TelegramUserId}
import acab.devcon0.domain.service.{CaptionService, TelegramService}
import com.bot4s.telegram.models.InputFile
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.Try

class PublishToTelegramCommand(
    val configuration: Configuration,
    val telegramService: TelegramService,
    val captionService: CaptionService
) extends PublishToPlatformCommand
    with LazyLogging {

  private val channelChatId: TelegramUserId = TelegramUserId(configuration.telegram.channelChatId)

  override def publish(aggregatedSticker: AggregatedSticker): Try[Unit] = {
    for {
      caption <- captionService.getMarkdown(aggregatedSticker.sticker, aggregatedSticker.links)
      _ <- sendPhotoWithMarkdown(aggregatedSticker, caption)
    } yield ()
  }

  private def sendPhotoWithMarkdown(aggregatedSticker: AggregatedSticker, caption: String): Try[Unit] = {
    telegramService
      .sendPhotoWithMarkdownCaption(
        to = channelChatId,
        text = caption,
        inputFile = InputFile(aggregatedSticker.publishingPhoto)
      )
  }
}
