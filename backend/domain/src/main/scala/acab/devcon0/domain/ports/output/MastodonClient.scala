package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.{MastodonAddMediaResponse, MastodonPostStatusResponse}

import java.io.File
import scala.util.Try

trait MastodonClient {

  def addMedia(file: File): Try[MastodonAddMediaResponse]

  def createPost(status: String, media_id: String): Try[MastodonPostStatusResponse]
}
