package acab.devcon0.domain.adapters.bookmarksticker

import acab.devcon0.domain.dtos.{Bookmark, Sticker}
import acab.devcon0.domain.ports.input.bookmarksticker.{StickerIndexBookmarkQuery, StickerIndexBookmarkQueryHandler}
import acab.devcon0.domain.service.{BookmarksService, StickersService}
import cats.implicits._

import scala.util.Try

class StickerIndexBookmarkQueryHandlerImpl(val stickersService: StickersService, val bookmarksService: BookmarksService)
    extends StickerIndexBookmarkQueryHandler {
  override def handle(query: StickerIndexBookmarkQuery): Try[Seq[Sticker]] = {
    for {
      bookmarks <- bookmarksService.getByUser(query.bookmarkUserId)
      maybeStickers: Seq[Option[Sticker]] <- bookmarkToSticker(bookmarks)
      stickers = maybeStickers.flatten
    } yield stickers
  }

  private def bookmarkToSticker(bookmarks: Seq[Bookmark]): Try[List[Option[Sticker]]] = {
    bookmarks.toList.traverse(bookmark => stickersService.getById(Some(bookmark.id.stickerId)))
  }
}
