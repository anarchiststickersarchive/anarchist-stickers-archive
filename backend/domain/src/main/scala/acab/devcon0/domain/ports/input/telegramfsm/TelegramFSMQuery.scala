package acab.devcon0.domain.ports.input.telegramfsm

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.service.TelegramFiniteStateMachine.{TelegramFSMId, TelegramFSMInstance}

case class TelegramFSMQuery(id: TelegramFSMId) extends Query[TelegramFSMInstance]
trait TelegramFSMQueryHandler extends QueryHandler[TelegramFSMQuery, TelegramFSMInstance]

case class TelegramFSMExistsAnyQuery(id: TelegramFSMId) extends Query[Boolean]
trait TelegramFSMExistsAnyQueryHandler extends QueryHandler[TelegramFSMExistsAnyQuery, Boolean]

case class TelegramFSMExistsQuery(id: TelegramFSMId) extends Query[Boolean]
trait TelegramFSMExistsQueryHandler extends QueryHandler[TelegramFSMExistsQuery, Boolean]
