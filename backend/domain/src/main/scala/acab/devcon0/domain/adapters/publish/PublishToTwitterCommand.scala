package acab.devcon0.domain.adapters.publish

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.AggregatedSticker
import acab.devcon0.domain.service.{CaptionService, TwitterService}
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.Try

class PublishToTwitterCommand(val captionService: CaptionService, val twitterService: TwitterService)
    extends PublishToPlatformCommand
    with LazyLogging {

  override def publish(aggregatedSticker: AggregatedSticker): Try[Unit] = {
    {
      for {
        caption <- captionService.get(aggregatedSticker.sticker, aggregatedSticker.links)
        upload <- twitterService.uploadMediaFromFile(aggregatedSticker.publishingPhoto.toFile)
        _ <- twitterService.createTweet(status = caption, mediaIds = Seq(upload.media_id))
      } yield ()
    }.transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }
}
