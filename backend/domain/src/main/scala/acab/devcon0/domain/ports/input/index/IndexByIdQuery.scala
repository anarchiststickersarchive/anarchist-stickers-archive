package acab.devcon0.domain.ports.input.index

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.AggregatedSticker
import acab.devcon0.domain.dtos.TypeAliases.StickerId

case class IndexByIdQuery(stickerId: StickerId) extends Query[AggregatedSticker]
trait IndexByIdQueryHandler extends QueryHandler[IndexByIdQuery, AggregatedSticker]
