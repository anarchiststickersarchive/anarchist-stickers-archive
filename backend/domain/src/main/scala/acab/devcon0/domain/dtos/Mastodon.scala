package acab.devcon0.domain.dtos

case class MastodonAddMediaResponse(id: String)
case class MastodonPostStatusResponse(id: String)
case class MastodonPostStatusRequest(status: String, mediaIds: Seq[String])
