package acab.devcon0.domain.ports.output

import scala.util.Try

trait RemotePinRepository {
  def get: Try[Option[String]]
  def upsert(cidHash: String): Try[Unit]
}
