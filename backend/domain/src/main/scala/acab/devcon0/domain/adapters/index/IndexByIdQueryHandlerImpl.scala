package acab.devcon0.domain.adapters.index

import acab.devcon0.domain.dtos.AggregatedSticker
import acab.devcon0.domain.ports.input.index.{IndexByIdQuery, IndexByIdQueryHandler}
import acab.devcon0.domain.service.AggregatedStickersService

import scala.util.Try

class IndexByIdQueryHandlerImpl(val aggregatedStickersService: AggregatedStickersService)
    extends IndexByIdQueryHandler {
  override def handle(query: IndexByIdQuery): Try[AggregatedSticker] =
    aggregatedStickersService.getByStickerId(Some(query.stickerId))
}
