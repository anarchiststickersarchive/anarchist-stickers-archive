package acab.devcon0.domain.ports.input.deletesticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.StickerId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.util.{Failure, Success, Try}

case class StickerDeleteCommand(stickerId: StickerId) extends Command[StickerId]

sealed abstract class StickerDeleteEvent[T] extends Event[T]

case class StickerDeleteSuccessEvent(sticker: Sticker) extends StickerDeleteEvent[Sticker]

case class StickerDeleteErrorEvent(
    stickerId: StickerId,
    throwable: Throwable
) extends StickerDeleteEvent[StickerId]

trait StickerDeleteCommandHandler extends CommandHandler[StickerDeleteCommand, StickerId, StickerDeleteEvent[_]]

object StickerDeleteEventProcessor {
  def apply(result: Try[StickerDeleteEvent[_]], logger: Logger): Try[StickerId] = {
    result match {
      case Failure(exception)                             => handleErrorCase(exception, logger)
      case Success(StickerDeleteErrorEvent(_, exception)) => handleErrorCase(exception, logger)
      case Success(StickerDeleteSuccessEvent(sticker))    => Success(sticker.id.get)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[StickerId] = {
    logger.error("StickerDeleteEventProcessor", exception)
    Failure(exception)
  }
}

object StickerDeleteCommandImplicits {
  implicit class StickerDeleteCommandEventFlattenOps(result: Try[StickerDeleteEvent[_]]) extends LazyLogging {
    def flattenEvents: Try[Sticker] = {
      result match {
        case Failure(exception)                             => handleErrorCase(exception, logger)
        case Success(StickerDeleteErrorEvent(_, exception)) => handleErrorCase(exception, logger)
        case Success(StickerDeleteSuccessEvent(sticker))    => Success(sticker)
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[Sticker] = {
      logger.error("StickerDeleteEventProcessor", exception)
      Failure(exception)
    }
  }
}
