package acab.devcon0.domain.fsm.bookmarksticker

import acab.devcon0.fsm.FiniteStateMachineEvent

sealed trait BookmarkStickerFSMEvent extends FiniteStateMachineEvent
case object Init extends BookmarkStickerFSMEvent
case object Exit extends BookmarkStickerFSMEvent
case object StickerIdsSubmitted extends BookmarkStickerFSMEvent
case object Add extends BookmarkStickerFSMEvent
case object Delete extends BookmarkStickerFSMEvent
