package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.SourceDto

import scala.util.Try

trait SourceService {
  def parse(rawSource: String): Try[SourceDto]
}
