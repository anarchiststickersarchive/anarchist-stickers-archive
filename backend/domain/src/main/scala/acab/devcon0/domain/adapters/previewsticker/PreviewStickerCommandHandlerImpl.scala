package acab.devcon0.domain.adapters.previewsticker

import acab.devcon0.domain.ports.input.preview._
import acab.devcon0.domain.service._
import com.bot4s.telegram.models.InputFile
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class PreviewStickerCommandHandlerImpl(
    val captionService: CaptionService,
    val aggregatedStickersService: AggregatedStickersService,
    val telegramService: TelegramService
) extends PreviewStickerCommandHandler
    with LazyLogging {

  override def handle(cmd: PreviewStickerCommand): Try[PreviewStickerEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) => handleErrorCase(cmd, throwable)
      case Success(_)         => handleSuccess(cmd)
    }
  }

  private def handleInner(cmd: PreviewStickerCommand): Try[Unit] = {
    for {
      aggregatedSticker <- aggregatedStickersService.getByStickerId(cmd.stickerId)
      caption <- captionService.getMarkdown(aggregatedSticker.sticker, aggregatedSticker.links)
      result <- telegramService.sendPhotoWithMarkdownCaption(
        to = cmd.telegramUserId,
        text = caption,
        inputFile = InputFile(aggregatedSticker.publishingPhoto)
      )
    } yield result
  }

  private def handleSuccess(cmd: PreviewStickerCommand): Try[PreviewStickerEvent[_]] = {
    Success(PreviewStickerSuccessEvent(stickerId = cmd.stickerId))
  }

  private def handleErrorCase(cmd: PreviewStickerCommand, throwable: Throwable): Try[PreviewStickerEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    val message = "Preview failed. If possible, check the logs, otherwise, contact the admins"
    telegramService.sendMessage(to = cmd.telegramUserId, text = message)
    Success(PreviewStickerErrorEvent(stickerId = cmd.stickerId, throwable = throwable))
  }
}
