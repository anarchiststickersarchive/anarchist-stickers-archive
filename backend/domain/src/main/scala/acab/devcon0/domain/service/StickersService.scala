package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.ports.output.StickersRepository
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

class StickersService(repository: StickersRepository) extends LazyLogging {

  def getAll: Try[Seq[Sticker]] = {
    repository.getAll
  }

  def getAll(page: Int, pageSize: Int, searchTerm: Option[String]): Try[Seq[Sticker]] = {
    repository.getAll(page, pageSize, searchTerm)
  }

  def getById(stickerId: MaybeStickerId): Try[Option[Sticker]] = {
    repository.getById(stickerId)
  }

  def upsert(sticker: Sticker): Try[Option[Sticker]] = {
    repository
      .upsert(sticker)
      .flatMap(stickerId => getById(stickerId))
  }

  def delete(stickerId: MaybeStickerId): Try[Unit] = {
    repository.delete(stickerId)
  }

  def search(searchTerm: String): Try[Seq[Sticker]] = {
    repository.search(searchTerm)
  }

  object Sugar {

    def countAll: Try[Option[Long]] = repository.countAll

    def countUnpublished: Try[Option[Long]] = repository.countUnpublished

    def getRandomNotPublishedAnywhere: Try[Option[Sticker]] = repository.getRandomNotPublishedAnywhere

    // Stickers that are not published at no platform
    def getUnpublished: Try[Seq[Sticker]] = repository.getUnpublished
  }
}
