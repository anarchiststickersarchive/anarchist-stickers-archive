package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases._

import java.time.LocalDateTime

case class StickerLink(
    id: StickerLinkId = None,
    stickerId: MaybeStickerId = None,
    filename: StickerLinkFilename,
    ipfsCID: StickerLinkIpfsCid,
    `type`: StickerLinkType,
    creationTimestamp: LocalDateTime
)

object StickerLinkTypes extends Enumeration {
  type StickerLinkType = Value

  val VECTOR, ORIGINAL = Value
}
