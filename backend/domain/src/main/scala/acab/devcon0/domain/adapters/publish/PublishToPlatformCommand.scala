package acab.devcon0.domain.adapters.publish

import acab.devcon0.domain.dtos.AggregatedSticker

import scala.util.Try

trait PublishToPlatformCommand {
  def publish(aggregatedSticker: AggregatedSticker): Try[Unit]
}
