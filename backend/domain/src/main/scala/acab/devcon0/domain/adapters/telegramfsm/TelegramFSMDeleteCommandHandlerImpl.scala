package acab.devcon0.domain.adapters.telegramfsm

import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class TelegramFSMDeleteCommandHandlerImpl(
    val telegramFSMService: TelegramFSMService
) extends TelegramFSMDeleteCommandHandler
    with LazyLogging {

  override def handle(cmd: TelegramFSMDeleteCommand): Try[TelegramFSMDeleteEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) => handleErrorCase(cmd, throwable)
      case Success(_)         => handleSuccess(cmd)
    }
  }

  private def handleInner(cmd: TelegramFSMDeleteCommand): Try[Unit] = {
    telegramFSMService
      .delete(cmd.id)
  }

  private def handleSuccess(cmd: TelegramFSMDeleteCommand): Try[TelegramFSMDeleteEvent[_]] = {
    Success(TelegramFSMSuccessDeleteEvent(id = cmd.id))
  }

  private def handleErrorCase(cmd: TelegramFSMDeleteCommand, throwable: Throwable): Try[TelegramFSMDeleteEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    Success(TelegramFSMErrorDeleteEvent(id = cmd.id, throwable = throwable))
  }
}
