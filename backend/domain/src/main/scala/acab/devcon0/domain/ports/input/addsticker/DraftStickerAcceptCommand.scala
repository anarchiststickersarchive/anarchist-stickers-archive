package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.util.{Failure, Success, Try}

case class DraftStickerAcceptCommand(draftStickerId: DraftStickerId) extends Command[DraftStickerId]

sealed abstract class DraftStickerAcceptEvent[T] extends Event[T]

case class DraftStickerAcceptSuccessEvent(sticker: Sticker) extends DraftStickerAcceptEvent[Sticker]

case class DraftStickerAcceptErrorEvent(
    draftStickerId: DraftStickerId,
    throwable: Throwable
) extends DraftStickerAcceptEvent[DraftStickerId]

trait DraftStickerAcceptCommandHandler
    extends CommandHandler[DraftStickerAcceptCommand, DraftStickerId, DraftStickerAcceptEvent[_]]

object DraftStickerAcceptCommandImplicits {
  implicit class DraftStickerAcceptCommandEventFlattenOps(result: Try[DraftStickerAcceptEvent[_]]) extends LazyLogging {
    def flattenEvents: Try[Sticker] = {
      result match {
        case Failure(exception)                                  => handleErrorCase(exception, logger)
        case Success(DraftStickerAcceptErrorEvent(_, exception)) => handleErrorCase(exception, logger)
        case Success(DraftStickerAcceptSuccessEvent(sticker))    => Success(sticker)
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[Sticker] = {
      logger.error("DraftStickerAcceptEventProcessor", exception)
      Failure(exception)
    }
  }
}
