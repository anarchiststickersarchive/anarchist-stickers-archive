package acab.devcon0.domain.ports.input.editsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLinkId}
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

sealed abstract class StickerUpsertBinaryCommand(val stickerId: MaybeStickerId) extends Command[MaybeStickerId]

case class StickerUpsertBinaryTelegramCommand(
    override val stickerId: MaybeStickerId,
    stickerLinkType: StickerLinkType,
    telegramFileId: String,
    extension: String
) extends StickerUpsertBinaryCommand(stickerId)

case class StickerUpsertVectorOnDemandCommand(
    override val stickerId: MaybeStickerId
) extends StickerUpsertBinaryCommand(stickerId)

sealed abstract class StickerUpsertBinaryEvent[T] extends Event[T]

case class StickerUpsertBinarySuccessEvent(
    stickerLinkId: StickerLinkId
) extends StickerUpsertBinaryEvent[Sticker]

case class StickerUpsertBinaryErrorEvent(
    stickerId: MaybeStickerId,
    throwable: Throwable
) extends StickerUpsertBinaryEvent[MaybeStickerId]

trait StickerUpsertBinaryCommandHandler
    extends CommandHandler[StickerUpsertBinaryCommand, StickerLinkId, StickerUpsertBinaryEvent[_]]

object StickerUpsertBinaryEventProcessor {
  def apply(result: Try[StickerUpsertBinaryEvent[_]], logger: Logger): Try[StickerLinkId] = {
    result match {
      case Failure(exception)                                      => handleErrorCase(exception, logger)
      case Success(StickerUpsertBinaryErrorEvent(_, exception))    => handleErrorCase(exception, logger)
      case Success(StickerUpsertBinarySuccessEvent(stickerLinkId)) => Success(stickerLinkId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[StickerLinkId] = {
    logger.error("DraftStickerAcceptEventProcessor", exception)
    Failure(exception)
  }
}
