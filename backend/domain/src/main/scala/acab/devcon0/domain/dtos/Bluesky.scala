package acab.devcon0.domain.dtos

case class blueskyBlobRef(`$link`: String)
case class BlueskyBlob(`$type`: String, ref: blueskyBlobRef, mimeType: String, size: Int)
case class BlueskyAddMediaResponse(blob: BlueskyBlob)

case class BlueskyEmbedImage(image: BlueskyBlob, alt: String)
case class BlueskyEmbed(`$type`: String, images: List[BlueskyEmbedImage])
case class BlueskyRecord(text: String, createdAt: String, embed: BlueskyEmbed)

case class BlueskyCreatePostRequest(repo: String, collection: String, record: BlueskyRecord)
case class BlueskyLoginRequest(identifier: String, password: String)
case class BlueskyLoginResponse(accessJwt: String, refreshJwt: String, did: String)
case class BlueskyPostStatusResponse(validationStatus: String, cid: String)
