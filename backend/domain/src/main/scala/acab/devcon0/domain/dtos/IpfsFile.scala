package acab.devcon0.domain.dtos

case class IpfsFile(name: String, hash: String, size: String)
