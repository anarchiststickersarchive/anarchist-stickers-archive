package acab.devcon0.domain.fsm.editsticker

import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import acab.devcon0.fsm.FiniteStateMachineInstance

case class TelegramEditFSMInstance(
    override val id: TelegramFSMId,
    override val state: EditStickerFSMState,
    override val data: EditStickerFSMData
) extends FiniteStateMachineInstance[TelegramFSMId](id, state, data)
