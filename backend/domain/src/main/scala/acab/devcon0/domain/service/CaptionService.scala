package acab.devcon0.domain.service
import acab.devcon0.domain.dtos.{DraftSticker, Sticker, StickerLink}

import scala.util.Try

trait CaptionService {

  def get(sticker: Sticker, links: Seq[StickerLink]): Try[String]

  def getMarkdown(sticker: Sticker, links: Seq[StickerLink]): Try[String]

  def get(draftSticker: DraftSticker): Try[String]
}
