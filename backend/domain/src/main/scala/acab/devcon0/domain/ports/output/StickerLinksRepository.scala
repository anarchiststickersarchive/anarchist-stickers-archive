package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.StickerLink
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLinkId, StickerLinkIpfsCid}

import scala.util.Try

trait StickerLinksRepository {

  def getAll: Try[Seq[StickerLink]]

  def getById(id: StickerLinkId): Try[Option[StickerLink]]

  def getByStickerId(stickerId: MaybeStickerId): Try[Seq[StickerLink]]

  def getByIpfsCID(ipfsCid: StickerLinkIpfsCid): Try[Option[StickerLink]]

  def upsert(stickerLink: StickerLink): Try[StickerLinkId]

  def delete(stickerLinkId: StickerLinkId): Try[Unit]

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit]
}
