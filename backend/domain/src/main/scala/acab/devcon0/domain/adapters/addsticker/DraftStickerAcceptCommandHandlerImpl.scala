package acab.devcon0.domain.adapters.addsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.{DraftStickerId, StickerDirectoryRelationDirectoryName}
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.service._
import cats.syntax.traverse.toTraverseOps
import com.typesafe.scalalogging.LazyLogging

import java.io.File
import java.time.LocalDateTime
import scala.util.{Failure, Success, Try}

class DraftStickerAcceptCommandHandlerImpl(
    draftStickersService: DraftStickersService,
    configuration: Configuration,
    stickersService: StickersService,
    stickerLinksService: StickerLinksService,
    stickerDirectoryRelationsService: StickerDirectoryRelationsService,
    ipfsService: IpfsService,
    fileSystemPersistenceHandler: FileSystemPersistenceHandler = new FileSystemPersistenceHandler() {},
    fileSystemPathHandler: FileSystemPathHandler = new FileSystemPathHandler() {}
) extends DraftStickerAcceptCommandHandler
    with LazyLogging {

  override def handle(cmd: DraftStickerAcceptCommand): Try[DraftStickerAcceptEvent[_]] = {
    val id: DraftStickerId = cmd.draftStickerId
    handleInner(id) match {
      case Failure(throwable) =>
        Success(DraftStickerAcceptErrorEvent(draftStickerId = cmd.draftStickerId, throwable = throwable))
      case Success(sticker) => Success(DraftStickerAcceptSuccessEvent(sticker = sticker))
    }
  }

  private def handleInner(id: DraftStickerId): Try[Sticker] = {
    getDraftSticker(id)
      .flatMap(draftSticker => forceNameDirectoryUniqueness(draftSticker))
      .flatMap(addSticker => persistInFS(addSticker))
      .flatMap(addSticker => persistInIPFS(addSticker))
      .flatMap(ipfsPersistenceResult => persistInDB(ipfsPersistenceResult))
      .map(stickerTuple => deleteFromDrafts(stickerTuple)._1)
  }

  private def forceNameDirectoryUniqueness(draftSticker: DraftSticker): Try[DraftSticker] = {
    stickerDirectoryRelationsService.getByDirectoryName(draftSticker.directoryName.get) match {
      case Failure(exception) => Failure(exception)
      case Success(Some(_))   => Failure(new IllegalArgumentException("Duplicate name directory"))
      case Success(None)      => Success(draftSticker)
    }
  }

  private def getDraftSticker(id: DraftStickerId): Try[DraftSticker] = {
    draftStickersService
      .getById(id)
      .flatMap(TryUtils.forceOptionExistence())
  }

  private def deleteFromDrafts(stickerTuple: (Sticker, DraftSticker)): (Sticker, DraftSticker) = {
    logger.info(stickerTuple.toString())
    val draftSticker: DraftSticker = stickerTuple._2
    draftStickersService.delete(draftSticker.id)
    draftSticker.stickerLinks.values.toList
      .foreach(draftStickerLink =>
        Try {
          draftStickerLink match {
            case DraftStickerLinkBinary(_, _, _)              => ()
            case DraftStickerLinkFilename(absolutePath, _, _) => new File(absolutePath).delete()
          }
        }
      )
    stickerTuple
  }

  private def persistInDB(ipfsPersistenceResult: IpfsPersistenceResult): Try[(Sticker, DraftSticker)] = {
    logger.info(ipfsPersistenceResult.toString)
    val draftSticker: DraftSticker = ipfsPersistenceResult.addSticker
    for {
      sticker <- upsertSticker(draftSticker)
      stickerLinks <- upsertStickerLinksWithRollback(ipfsPersistenceResult, sticker)
      _ <- upsertStickerDirectoryRelationWithRollback(ipfsPersistenceResult, draftSticker, sticker, stickerLinks)
    } yield (sticker, draftSticker)
  }

  private def upsertStickerLinksWithRollback(
      ipfsPersistenceResult: IpfsPersistenceResult,
      sticker: Sticker
  ): Try[List[StickerLink]] = {
    val stickerLinks: List[StickerLink] = buildStickerLinks(ipfsPersistenceResult, sticker).toList
    stickerLinks
      .traverse(stickerLink => {
        stickerLinksService
          .upsert(stickerLink)
          .flatMap(TryUtils.forceOptionExistence())
      })
      .recoverWith({ case throwable: Throwable =>
        stickerLinks.foreach(stickerLink => stickerLinksService.delete(stickerLink.id))
        stickersService.delete(sticker.id)
        Failure[List[StickerLink]](throwable)
      })
  }

  private def upsertSticker(draftSticker: DraftSticker): Try[Sticker] = {
    val upsertingSticker: Sticker = buildSticker(draftSticker)
    stickersService
      .upsert(upsertingSticker)
      .flatMap(TryUtils.forceOptionExistence())
  }

  private def upsertStickerDirectoryRelationWithRollback(
      ipfsPersistenceResult: IpfsPersistenceResult,
      draftSticker: DraftSticker,
      sticker: Sticker,
      stickerLinks: List[StickerLink]
  ): Try[(Sticker, DraftSticker)] = {
    val directoryName: StickerDirectoryRelationDirectoryName = ipfsPersistenceResult.addSticker.directoryName.get
    val stickerDirectoryRelation: StickerDirectoryRelation = StickerDirectoryRelation(sticker.id, directoryName)
    stickerDirectoryRelationsService
      .upsert(stickerDirectoryRelation)
      .flatMap(TryUtils.forceOptionExistence())
      .map(_ => (sticker, draftSticker))
      .recoverWith({ case throwable: Throwable =>
        stickerLinks.foreach(stickerLink => stickerLinksService.delete(stickerLink.id))
        stickersService.delete(sticker.id)
        Failure[(Sticker, DraftSticker)](throwable)
      })
  }

  private def buildStickerLinks(ipfsPersistenceResult: IpfsPersistenceResult, sticker: Sticker): Seq[StickerLink] = {
    ipfsPersistenceResult.ipfsCids
      .map(entry => buildStickerLink(sticker, entry._1, entry._2))
      .toList
  }

  private def buildStickerLink(sticker: Sticker, stickerLinkType: StickerLinkType, ipfsFile: IpfsFile): StickerLink = {
    StickerLink(
      id = None,
      stickerId = sticker.id,
      filename = ipfsFile.name,
      ipfsCID = ipfsFile.hash,
      `type` = stickerLinkType,
      creationTimestamp = LocalDateTime.now()
    )
  }

  private def buildSticker(addSticker: DraftSticker) = {
    Sticker(
      id = None,
      name = addSticker.name.get,
      notes = addSticker.notes,
      source = addSticker.source,
      language = addSticker.language,
      creationTimestamp = LocalDateTime.now()
    )
  }

  private def persistInIPFS(
      addSticker: DraftSticker
  ): Try[IpfsPersistenceResult] = {
    addSticker.stickerLinks.toList
      .traverse(entry => persistInIPFS(directoryName = addSticker.directoryName.get, entry = entry))
      .map(list => list.toMap)
      .map(persistenceResultAsMap => IpfsPersistenceResult(addSticker, persistenceResultAsMap))
  }

  private def persistInIPFS(
      directoryName: String,
      entry: (StickerLinkType, DraftStickerLink)
  ): Try[(StickerLinkType, IpfsFile)] = {
    val stickerLinkType: StickerLinkType = entry._1
    val extension: String = entry._2.extension
    val remoteAbsolutePath =
      fileSystemPathHandler.getRemoteAbsolutePath(configuration, directoryName, stickerLinkType, extension)
    val bytes: Array[Byte] = entry._2.bytes
    val filename = fileSystemPathHandler.getFilename(directoryName, stickerLinkType, extension)
    ipfsService
      .addNoCopy(bytes = bytes, fileName = filename, remoteAbsolutePath = remoteAbsolutePath)
      .map(ipfsFile => (stickerLinkType, ipfsFile))
  }

  private def persistInFS(addSticker: DraftSticker): Try[DraftSticker] = Try {
    val stickerDirectory = addSticker.directoryName.get
    fileSystemPersistenceHandler.mkdirUpcomingStickerDirectory(stickerDirectory, configuration)
    addSticker.stickerLinks.toList.foreach(tuple =>
      fileSystemPersistenceHandler.persistInFS(
        stickerLinkType = tuple._1,
        fileBytes = tuple._2.bytes,
        extension = tuple._2.extension,
        folderName = stickerDirectory,
        configuration = configuration
      )
    )
    addSticker
  }
}

private case class IpfsPersistenceResult(
    addSticker: DraftSticker,
    ipfsCids: Map[StickerLinkType, IpfsFile]
)
