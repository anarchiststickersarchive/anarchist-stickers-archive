package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLanguage, StickerName, StickerNotes, StickerSource}

import java.time.LocalDateTime

case class Sticker(
    id: MaybeStickerId = None,
    name: StickerName,
    notes: StickerNotes = None,
    source: StickerSource = None,
    language: StickerLanguage = None,
    creationTimestamp: LocalDateTime
)
