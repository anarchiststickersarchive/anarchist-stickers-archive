package acab.devcon0.domain.service

import acab.devcon0.configuration.Configuration
import com.typesafe.scalalogging.LazyLogging

import java.io.File
import scala.util.{Random, Try}

class IngestionQueueServiceImpl(
    val configuration: Configuration
) extends IngestionQueueService
    with LazyLogging {

  private val extensions = List("jpg", "jpeg", "png", "gif")

  private var allCandidateFiles: Seq[File] = getFreshListOfCandidatesFiles

  override def getRasterize(maybeSource: Option[String]): Try[Option[File]] = {
    Try(getFirstThatMatches(allCandidateFiles, maybeSource))
  }

  override def refresh(): Try[Unit] = Try {
    allCandidateFiles = getFreshListOfCandidatesFiles
  }

  override def count(): Try[Long] = Try {
    listFiles(new File(configuration.ingestionFinalDirectory))
      .count(f => extensions.exists(f.getName.toLowerCase.endsWith))
  }

  override def getVector(rasterize: File): Try[Option[File]] = Try {
    Some(rasterize.getAbsolutePath)
      .map(path => getAsVectorPath(path))
      .map(virtualVectorPath => new File(virtualVectorPath))
      .filter(file => file.exists())
  }

  override def remove(rasterize: File, vector: Option[File]): Try[Unit] = Try {
    rasterize.delete()
    vector.foreach(_.delete())
  }

  private def getFreshListOfCandidatesFiles: Seq[File] = {
    val nonRandomizeListOfFiles: Seq[File] = listFiles(new File(configuration.ingestionFinalDirectory))
      .filter(_.exists())
      .filter(f => extensions.exists(f.getName.toLowerCase.endsWith))
    Random.shuffle(nonRandomizeListOfFiles)
  }

  private def getFirstThatMatches(files: Seq[File], maybeSource: Option[String]): Option[File] = {
    logger.info(s"IngestionFinalPickService.getFirstThatMatches files.size=${files.size} maybeSource=$maybeSource")
    if (files.isEmpty)
      None
    else {
      val maybeFile: Option[File] = files.headOption
        .filter(_.exists())
        .filter(f => maybeSource.isEmpty || maybeSource.exists(source => f.getAbsolutePath.contains(source)))
        .orElse(getFirstThatMatches(files.tail, maybeSource))
      logger.info(s"IngestionFinalPickService.getFirstThatMatches candidate=$maybeFile")
      maybeFile
    }
  }

  private def getAsVectorPath(filename: String): String = {
    val withoutExtension = filename.lastIndexOf('.') match {
      case -1 => filename
      case i  => filename.substring(0, i)
    }
    s"$withoutExtension.svg"
  }

  private def listFiles(file: File): Seq[File] = {
    val children = Option(file.listFiles).toSeq.flatten
    children ++ children.filter(_.isDirectory).flatMap(listFiles)
  }
}
