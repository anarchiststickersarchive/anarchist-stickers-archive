package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.StickerDirectoryRelation
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerDirectoryRelationDirectoryName}
import acab.devcon0.domain.ports.output.StickerDirectoryRelationsRepository
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

class StickerDirectoryRelationsService(repository: StickerDirectoryRelationsRepository) extends LazyLogging {

  def getAll: Try[Seq[StickerDirectoryRelation]] = {
    repository.getAll
  }

  def getByStickerId(stickerId: MaybeStickerId): Try[Option[StickerDirectoryRelation]] = {
    repository.getByStickerId(stickerId)
  }

  def getByDirectoryName(
      directoryName: StickerDirectoryRelationDirectoryName
  ): Try[Option[StickerDirectoryRelation]] = {
    repository.getByDirectoryName(directoryName)
  }

  def upsert(stickerDirectoryRelation: StickerDirectoryRelation): Try[Option[StickerDirectoryRelation]] = {
    repository
      .upsert(stickerDirectoryRelation)
      .flatMap(_ => getByStickerId(stickerDirectoryRelation.stickerId))
  }

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit] = {
    repository.deleteByStickerId(stickerId)
  }

}
