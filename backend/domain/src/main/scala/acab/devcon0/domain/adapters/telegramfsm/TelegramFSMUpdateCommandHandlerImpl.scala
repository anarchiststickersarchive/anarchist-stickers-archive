package acab.devcon0.domain.adapters.telegramfsm

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramFiniteStateMachine.{TelegramFSMId, TelegramFSMInstance}
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class TelegramFSMUpdateCommandHandlerImpl(
    val telegramFSMService: TelegramFSMService
) extends TelegramFSMUpdateCommandHandler
    with LazyLogging {

  override def handle(cmd: TelegramFSMUpdateCommand): Try[TelegramFSMUpdateEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) => handleErrorCase(cmd, throwable)
      case Success(_)         => handleSuccess(cmd)
    }
  }

  private def handleInner(cmd: TelegramFSMUpdateCommand): Try[TelegramFSMInstance] = {
    telegramFSMService
      .upsert(cmd.instance)
      .flatMap(TryUtils.forceOptionExistence[TelegramFSMInstance, TelegramFSMId](Some(cmd.id)))
  }

  private def handleSuccess(cmd: TelegramFSMUpdateCommand): Try[TelegramFSMUpdateEvent[_]] = {
    Success(TelegramFSMSuccessUpdateEvent(id = cmd.id))
  }

  private def handleErrorCase(cmd: TelegramFSMUpdateCommand, throwable: Throwable): Try[TelegramFSMUpdateEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    Success(TelegramFSMErrorUpdateEvent(id = cmd.id, throwable = throwable))
  }
}
