package acab.devcon0.domain.codecs

import acab.devcon0.configuration._
import acab.devcon0.domain.dtos.BookmarkUserPlatforms.BookmarkUserPlatform
import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.fsm.addsticker.{AddStickerFSMData, AddStickerFSMState, TelegramAddFSMInstance}
import acab.devcon0.domain.fsm.bookmarksticker.{
  BookmarkStickerFSMData,
  BookmarkStickerFSMState,
  TelegramBookmarkFSMInstance
}
import acab.devcon0.domain.fsm.editsticker.{EditStickerFSMData, EditStickerFSMState, TelegramEditFSMInstance}
import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMInstance
import cats.syntax.functor._
import io.bartholomews.iso.LanguageCode
import io.circe._
import io.circe.generic.auto._
import io.circe.generic.extras.semiauto.{deriveEnumerationDecoder, deriveEnumerationEncoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._

import scala.util.Try

object CirceCodecs {

  object Decoders {
    implicit val publicationPlatformDecoder: Decoder[PublicationPlatform] =
      Decoder.decodeEnumeration(PublicationPlatforms)
    implicit val bookmarkUserPlatformDecoder: Decoder[BookmarkUserPlatform] =
      Decoder.decodeEnumeration(BookmarkUserPlatforms)
    implicit val stickerLinkTypeDecoder: Decoder[StickerLinkType] =
      Decoder.decodeEnumeration(StickerLinkTypes)
    implicit val languageCodeDecoder: Decoder[LanguageCode] = Decoder.decodeString.emap(str =>
      LanguageCode.values
        .find(_.value == str)
        .toRight(s"Invalid ISO_639 code: [$str]")
    )

    implicit val bookmarkDecoder: Decoder[Bookmark] = deriveDecoder
    implicit val bookmarkDecoderId: Decoder[BookmarkId] = deriveDecoder
    implicit val bookmarkUserIdDecoderId: Decoder[BookmarkUserId] = deriveDecoder
    implicit val stickerDecoder: Decoder[Sticker] = deriveDecoder
    implicit val stickerLinkDecoder: Decoder[StickerLink] = deriveDecoder
    implicit val stickerDirectoryRelationDecoder: Decoder[StickerDirectoryRelation] = deriveDecoder
    implicit val ipfsAddResponseDecoder: Decoder[IpfsAddResponse] = (c: HCursor) =>
      for {
        name <- c.downField("Name").as[String]
        hash <- c.downField("Hash").as[String]
        size <- c.downField("Size").as[String]
      } yield {
        IpfsAddResponse(name, hash, size)
      }

    implicit val mastodonAddMediaResponseDecoder: Decoder[MastodonAddMediaResponse] = deriveDecoder
    implicit val mastodonPostStatusResponseDecoder: Decoder[MastodonPostStatusResponse] = deriveDecoder

    implicit val blueskyBlobRefDecoder: Decoder[blueskyBlobRef] = deriveDecoder
    implicit val blueskyBlobDecoder: Decoder[BlueskyBlob] = deriveDecoder
    implicit val blueskyAddMediaResponseDecoder: Decoder[BlueskyAddMediaResponse] = deriveDecoder
    implicit val blueskyPostStatusResponseDecoder: Decoder[BlueskyPostStatusResponse] = deriveDecoder
    implicit val blueskyLoginResponseDecoder: Decoder[BlueskyLoginResponse] = deriveDecoder

    implicit val pixelfedAddMediaResponseDecoder: Decoder[PixelfedAddMediaResponse] = deriveDecoder
    implicit val pixelfedPostStatusResponseDecoder: Decoder[PixelfedPostStatusResponse] = deriveDecoder

    implicit val telegramAddFSMStateDecoder: Decoder[AddStickerFSMState] =
      deriveEnumerationDecoder[AddStickerFSMState]
    implicit val telegramEditFSMStateDecoder: Decoder[EditStickerFSMState] =
      deriveEnumerationDecoder[EditStickerFSMState]
    implicit val telegramBookmarkFSMStateDecoder: Decoder[BookmarkStickerFSMState] =
      deriveEnumerationDecoder[BookmarkStickerFSMState]

    implicit val telegramAddFSMDataDecoder: Decoder[AddStickerFSMData] =
      deriveDecoder[AddStickerFSMData]
    implicit val telegramEditFSMDataDecoder: Decoder[EditStickerFSMData] =
      deriveDecoder[EditStickerFSMData]
    implicit val telegramBookmarkFSMDataDecoder: Decoder[BookmarkStickerFSMData] =
      deriveDecoder[BookmarkStickerFSMData]

    implicit val telegramAddFSMInstanceDecoder: Decoder[TelegramAddFSMInstance] =
      deriveDecoder[TelegramAddFSMInstance]
    implicit val telegramEditFSMInstanceDecoder: Decoder[TelegramEditFSMInstance] =
      deriveDecoder[TelegramEditFSMInstance]
    implicit val telegramBookmarkFSMInstanceDecoder: Decoder[TelegramBookmarkFSMInstance] =
      deriveDecoder[TelegramBookmarkFSMInstance]
    implicit val telegramFSMInstanceDecoder: Decoder[TelegramFSMInstance] =
      List[Decoder[TelegramFSMInstance]](
        Decoder[TelegramAddFSMInstance].widen,
        Decoder[TelegramEditFSMInstance].widen,
        Decoder[TelegramBookmarkFSMInstance].widen
      ).reduceLeft(_ or _)

    implicit val draftStickerLinkBinaryDecoder: Decoder[DraftStickerLinkBinary] = deriveDecoder[DraftStickerLinkBinary]
    implicit val draftStickerLinkFilenameDecoder: Decoder[DraftStickerLinkFilename] =
      deriveDecoder[DraftStickerLinkFilename]
    implicit val draftStickerLinkDecoder: Decoder[DraftStickerLink] = List[Decoder[DraftStickerLink]](
      Decoder[DraftStickerLinkBinary].widen,
      Decoder[DraftStickerLinkFilename].widen
    ).reduceLeft(_ or _)
    implicit val stickerLinkTypeKeyDecoder: KeyDecoder[StickerLinkType] =
      KeyDecoder.instance(value => Try(StickerLinkTypes.withName(value)).toOption)
    implicit val draftStickerLinksDecoder: Decoder[Map[StickerLinkType, DraftStickerLink]] =
      Decoder.decodeMap[StickerLinkType, DraftStickerLink](stickerLinkTypeKeyDecoder, draftStickerLinkDecoder)
    implicit val draftStickerDecoder: Decoder[DraftSticker] = deriveDecoder[DraftSticker]
  }

  object Encoders {
    implicit val publicationPlatformEncoder: Encoder[PublicationPlatform] =
      Encoder.encodeEnumeration(PublicationPlatforms)
    implicit val bookmarkUserPlatformEncoder: Encoder[BookmarkUserPlatform] =
      Encoder.encodeEnumeration(BookmarkUserPlatforms)
    implicit val stickerLinkTypeEncoder: Encoder[StickerLinkType] =
      Encoder.encodeEnumeration(StickerLinkTypes)

    implicit val bookmarkEncoder: Encoder[Bookmark] = deriveEncoder[Bookmark]
    implicit val bookmarkIdEncoder: Encoder[BookmarkId] = deriveEncoder[BookmarkId]
    implicit val bookmarkUserIdEncoder: Encoder[BookmarkUserId] = deriveEncoder[BookmarkUserId]
    implicit val stickerEncoder: Encoder[Sticker] = deriveEncoder[Sticker]
    implicit val stickerLinkEncoder: Encoder[StickerLink] = deriveEncoder[StickerLink]
    implicit val stickerDirectoryRelationEncoder: Encoder[StickerDirectoryRelation] =
      deriveEncoder[StickerDirectoryRelation]
    implicit val flywayDataSourceConfigEncoder: Encoder[FlywayDataSource] = deriveEncoder[FlywayDataSource]
    implicit val instagramCredentialsEncoder: Encoder[InstagramConfiguration] = deriveEncoder[InstagramConfiguration]
    implicit val telegramConfigurationTelegramUserIdEncoder: Encoder[acab.devcon0.domain.dtos.TelegramUserId] =
      deriveEncoder[acab.devcon0.domain.dtos.TelegramUserId]
    implicit val telegramConfigurationTelegramUserIdConfigurationEncoder
        : Encoder[acab.devcon0.configuration.TelegramUserId] = deriveEncoder[acab.devcon0.configuration.TelegramUserId]
    implicit val telegramConfigurationEncoder: Encoder[TelegramConfiguration] = deriveEncoder[TelegramConfiguration]
    implicit val ipfsConfigurationEncoder: Encoder[IpfsConfiguration] = deriveEncoder[IpfsConfiguration]
    implicit val httpConfigurationEncoder: Encoder[HttpConfiguration] = deriveEncoder[HttpConfiguration]
    implicit val botConfigEncoder: Encoder[Configuration] = deriveEncoder[Configuration]
    implicit val redisConfigurationEncoder: Encoder[RedisConfiguration] = deriveEncoder
    implicit val telegramAddFSMStateEncoder: Encoder[AddStickerFSMState] =
      deriveEnumerationEncoder[AddStickerFSMState]
    implicit val telegramEditFSMStateEncoder: Encoder[EditStickerFSMState] =
      deriveEnumerationEncoder[EditStickerFSMState]
    implicit val telegramBookmarkFSMStateEncoder: Encoder[BookmarkStickerFSMState] =
      deriveEnumerationEncoder[BookmarkStickerFSMState]

    implicit val telegramFSMInstanceEncoder: Encoder[TelegramFSMInstance] = Encoder.instance {
      case add @ TelegramAddFSMInstance(_, _, _)           => add.asJson
      case edit @ TelegramEditFSMInstance(_, _, _)         => edit.asJson
      case bookmark @ TelegramBookmarkFSMInstance(_, _, _) => bookmark.asJson
    }

    implicit val mastodonPostStatusRequestEncoder: Encoder[MastodonPostStatusRequest] =
      (obj: MastodonPostStatusRequest) =>
        Json.obj(
          "status" -> obj.status.asJson,
          "media_ids" -> obj.mediaIds.asJson
        )

    implicit val blueskyCreatePostRequestEncoder: Encoder[BlueskyCreatePostRequest] = deriveEncoder
    implicit val blueskyLoginRequestEncoder: Encoder[BlueskyLoginRequest] = deriveEncoder
    implicit val blueskyBlobRefEncoder: Encoder[blueskyBlobRef] = deriveEncoder
    implicit val blueskyBlobEncoder: Encoder[BlueskyBlob] = deriveEncoder

    implicit val pixelfedPostStatusRequestEncoder: Encoder[PixelfedPostStatusRequest] =
      (obj: PixelfedPostStatusRequest) =>
        Json.obj(
          "status" -> obj.status.asJson,
          "media_ids" -> obj.mediaIds.asJson
        )

    implicit val ipfsAddResponseEncoder: Encoder[IpfsAddResponse] = deriveEncoder

    implicit val languageDecoder: Encoder[LanguageCode] = (a: LanguageCode) => Json.fromString(a.value)

    implicit val draftStickerLinkBinaryEncoder: Encoder[DraftStickerLinkBinary] = deriveEncoder[DraftStickerLinkBinary]
    implicit val draftStickerLinkFilenameEncoder: Encoder[DraftStickerLinkFilename] =
      deriveEncoder[DraftStickerLinkFilename]
    implicit val draftStickerLinkEncoder: Encoder[DraftStickerLink] = Encoder.instance {
      case binary @ DraftStickerLinkBinary(_, _, _)     => binary.asJson
      case filename @ DraftStickerLinkFilename(_, _, _) => filename.asJson
    }
    implicit val stickerLinkTypeKeyEncoder: KeyEncoder[StickerLinkType] =
      KeyEncoder.instance[StickerLinkType](value => value.toString)
    implicit val draftStickerLinksEncoder: Encoder[Map[StickerLinkType, DraftStickerLink]] =
      Encoder.encodeMap[StickerLinkType, DraftStickerLink](stickerLinkTypeKeyEncoder, draftStickerLinkEncoder)
    implicit val draftStickerEncoder: Encoder[DraftSticker] = deriveEncoder[DraftSticker]
  }
}
