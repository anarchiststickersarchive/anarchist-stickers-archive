package acab.devcon0.domain.adapters.ingestion

import acab.devcon0.domain.ports.input.ingestion._
import acab.devcon0.domain.service._
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class RefreshIngestionFilesListCommandHandlerImpl(
    val ingestionFinalPickService: IngestionQueueService
) extends RefreshIngestionFilesListCommandHandler
    with LazyLogging {

  override def handle(cmd: RefreshIngestionFilesListCommand): Try[RefreshIngestionFilesListEvent[_]] = {
    handleInner() match {
      case Failure(throwable) => handleErrorCase(throwable)
      case Success(_)         => handleSuccess()
    }
  }

  private def handleInner(): Try[Unit] = {
    ingestionFinalPickService.refresh()
  }

  private def handleSuccess(): Try[RefreshIngestionFilesListEvent[_]] = {
    Success(RefreshIngestionFilesListSuccessEvent())
  }

  private def handleErrorCase(throwable: Throwable): Try[RefreshIngestionFilesListEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    Success(
      RefreshIngestionFilesListErrorEvent(
        throwable = throwable
      )
    )
  }
}
