package acab.devcon0.domain.dtos

import java.nio.file.Path

case class AggregatedSticker(
    sticker: Sticker,
    links: Seq[StickerLink],
    stickerDirectoryRelation: StickerDirectoryRelation,
    publishingPhoto: Path
)
