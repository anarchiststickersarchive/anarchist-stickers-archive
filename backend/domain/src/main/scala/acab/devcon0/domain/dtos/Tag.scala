package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, TagId, TagValue}

import java.time.LocalDateTime

case class Tag(
    id: TagId,
    stickerId: MaybeStickerId,
    value: TagValue,
    creationTimestamp: LocalDateTime
)
