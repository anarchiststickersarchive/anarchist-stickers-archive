package acab.devcon0.domain.adapters.previewsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.StickerLinkTypes
import acab.devcon0.domain.ports.input.preview._
import acab.devcon0.domain.service._
import com.bot4s.telegram.models.InputFile.Contents
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class PreviewDraftStickerCommandHandlerImpl(
    val captionService: CaptionService,
    val draftStickersService: DraftStickersService,
    val telegramService: TelegramService
) extends PreviewDraftStickerCommandHandler
    with LazyLogging {

  override def handle(cmd: PreviewDraftStickerCommand): Try[PreviewDraftStickerEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) => handleErrorCase(cmd, throwable)
      case Success(_)         => handleSuccess(cmd)
    }
  }

  private def handleInner(cmd: PreviewDraftStickerCommand): Try[Unit] = {
    for {
      draftSticker <- draftStickersService.getById(cmd.draftStickerId).flatMap(TryUtils.forceOptionExistence())
      decoratedCaption <- captionService.get(draftSticker)
      result <- telegramService.sendPhoto(
        cmd.telegramUserId,
        decoratedCaption,
        Contents("filename", draftSticker.stickerLinks(StickerLinkTypes.ORIGINAL).bytes)
      )
    } yield result
  }

  private def handleSuccess(cmd: PreviewDraftStickerCommand): Try[PreviewDraftStickerEvent[_]] = {
    Success(PreviewDraftStickerSuccessEvent(draftStickerId = cmd.draftStickerId))
  }

  private def handleErrorCase(
      cmd: PreviewDraftStickerCommand,
      throwable: Throwable
  ): Try[PreviewDraftStickerEvent[_]] = {
    logger.error(this.getClass.getName, throwable)
    val message = "Preview failed. If possible, check the logs, otherwise, contact the admins"
    telegramService.sendMessage(to = cmd.telegramUserId, text = message)
    Success(PreviewDraftStickerErrorEvent(draftStickerId = cmd.draftStickerId, throwable = throwable))
  }
}
