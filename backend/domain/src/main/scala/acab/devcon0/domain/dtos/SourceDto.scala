package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos
import acab.devcon0.domain.dtos.Sources.Source

import java.net.URI

object Sources extends Enumeration {
  type Source = Value

  val INSTAGRAM, TELEGRAM, TWITTER, WEBSITE, EMAIL, REDDIT = Value
}

case class SourceDto(value: String, source: Source) {

  def httpLink: URI = {
    this.source match {
      case dtos.Sources.WEBSITE   => new URI(this.value)
      case dtos.Sources.EMAIL     => new URI(s"mailto:$value")
      case dtos.Sources.REDDIT    => new URI(s"https://reddit.com/$value")
      case dtos.Sources.INSTAGRAM => new URI(s"https://instagram.com/$value")
      case dtos.Sources.TELEGRAM  => new URI(s"https://t.me/$value")
      case dtos.Sources.TWITTER   => new URI(s"https://twitter.com/$value")
    }
  }

  def plainLink: String = {
    this.source match {
      case dtos.Sources.EMAIL     => s"mail:$value"
      case dtos.Sources.WEBSITE   => s"website:$value"
      case dtos.Sources.REDDIT    => s"reddit:${value.stripPrefix("@")}"
      case dtos.Sources.INSTAGRAM => s"instagram:@${value.stripPrefix("@")}"
      case dtos.Sources.TELEGRAM  => s"telegram:@${value.stripPrefix("@")}"
      case dtos.Sources.TWITTER   => s"twitter:@${value.stripPrefix("@")}"
    }
  }
}
