package acab.devcon0.domain.fsm.bookmarksticker

import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import acab.devcon0.fsm.FiniteStateMachineInstance

case class TelegramBookmarkFSMInstance(
    override val id: TelegramFSMId,
    override val state: BookmarkStickerFSMState,
    override val data: BookmarkStickerFSMData
) extends FiniteStateMachineInstance[TelegramFSMId](id, state, data)
