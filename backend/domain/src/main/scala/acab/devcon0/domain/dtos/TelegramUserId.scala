package acab.devcon0.domain.dtos

// Channels are strings
// Users private chats are long
// Groups are also long id-ed
case class TelegramUserId(id: String)
