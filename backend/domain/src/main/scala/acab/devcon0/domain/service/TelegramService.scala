package acab.devcon0.domain.service
import acab.devcon0.domain.dtos.{TelegramFileId, TelegramUserId}
import com.bot4s.telegram.models.InputFile

import scala.util.Try

trait TelegramService {

  def sendMessage(to: TelegramUserId, text: String): Try[Unit]

  def sendPhoto(to: TelegramUserId, text: String, inputFile: InputFile): Try[Unit]

  def sendPhotoWithMarkdownCaption(to: TelegramUserId, text: String, inputFile: InputFile): Try[Unit]

  def getFileBytes(telegramFileId: TelegramFileId): Try[Array[Byte]]
}
