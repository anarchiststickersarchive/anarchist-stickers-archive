package acab.devcon0.domain.service

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType

import java.io.{File, FileOutputStream}
import scala.reflect.io.Directory
import scala.util.Try

trait FileSystemPersistenceHandler extends FileSystemPathHandler {
  def persistInFS(
      stickerLinkType: StickerLinkType,
      fileBytes: Array[Byte],
      extension: String,
      folderName: String,
      configuration: Configuration
  ): Try[Unit] = Try {
    mkdirUpcomingStickerDirectory(folderName, configuration)
    val absolutePath: String = getLocalAbsolutePath(configuration, folderName, stickerLinkType, extension)
    val fileOutputStream: FileOutputStream = new FileOutputStream(absolutePath)
    fileOutputStream.write(fileBytes)
  }

  def mkdirUpcomingStickerDirectory(folderName: String, configuration: Configuration): Unit = {
    val stickerDirectory: String = configuration.archiveDirectory + "/" + folderName
    if (!new File(stickerDirectory).exists()) {
      val folderCreationResult = new File(stickerDirectory).mkdirs()
      require(folderCreationResult)
    }
  }

  def deleteStickerDirectory(folderName: String, configuration: Configuration): Try[Unit] = Try {
    val stickerDirectory: String = configuration.archiveDirectory + "/" + folderName
    val directory = new Directory(new File(stickerDirectory))
    directory.deleteRecursively()
  }
}
