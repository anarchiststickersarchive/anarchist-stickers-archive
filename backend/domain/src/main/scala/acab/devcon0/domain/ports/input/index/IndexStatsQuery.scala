package acab.devcon0.domain.ports.input.index

import acab.devcon0.cqrs.{Query, QueryHandler}

case class IndexStatsQuery() extends Query[String]
trait IndexStatsQueryHandler extends QueryHandler[IndexStatsQuery, String]
