package acab.devcon0.domain.adapters.bookmarksticker

import acab.devcon0.domain.dtos.TypeAliases.StickerId
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input.bookmarksticker._
import acab.devcon0.domain.service._
import cats.syntax.traverse.toTraverseOps
import com.typesafe.scalalogging.LazyLogging

import java.time.LocalDateTime
import scala.util.{Failure, Success, Try}

class StickerAddBookmarkCommandHandlerImpl(
    bookmarksService: BookmarksService
) extends StickerAddBookmarkCommandHandler
    with LazyLogging {

  override def handle(cmd: StickerAddBookmarkCommand): Try[StickerAddBookmarkEvent[_]] = {
    val stickerIds: Seq[StickerId] = cmd.stickerIds
    val bookmarkUserId: BookmarkUserId = cmd.bookmarkUserId

    handleInner(stickerIds, bookmarkUserId) match {
      case Failure(throwable) =>
        Success(
          StickerAddBookmarkErrorEvent(
            stickerIds = stickerIds,
            bookmarkUserId = bookmarkUserId,
            throwable = throwable
          )
        )
      case Success(_) => Success(StickerAddBookmarkSuccessEvent(stickerIds = stickerIds))
    }
  }

  private def handleInner(
      stickerIds: Seq[StickerId],
      bookmarkUserId: BookmarkUserId
  ): Try[List[Bookmark]] = {
    stickerIds
      .map(stickerId => Bookmark(BookmarkId(stickerId, bookmarkUserId), LocalDateTime.now()))
      .toList
      .traverse(bookmark => bookmarksService.upsert(bookmark))
  }
}
