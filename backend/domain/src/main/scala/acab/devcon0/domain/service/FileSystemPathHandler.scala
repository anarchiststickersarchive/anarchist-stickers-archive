package acab.devcon0.domain.service

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType

trait FileSystemPathHandler {
  def getRemoteAbsolutePath(
      configuration: Configuration,
      baseName: String,
      stickerLinkType: StickerLinkType,
      extension: String
  ): String = {
    s"" +
      s"${configuration.ipfs.noteDataPath}/" +
      s"$baseName/" +
      s"${getFilename(baseName, stickerLinkType, extension)}"
  }

  def getLocalAbsolutePath(
      configuration: Configuration,
      baseName: String,
      stickerLinkType: StickerLinkType,
      extension: String
  ): String = {
    s"" +
      s"${configuration.archiveDirectory}/" +
      s"$baseName/" +
      s"${getFilename(baseName, stickerLinkType, extension)}"
  }

  def getFilename(baseName: String, stickerLinkType: StickerLinkType, extension: String): String =
    s"${baseName}_" + stickerLinkType.toString.toLowerCase + "." + s"$extension"
}
