package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases._

import java.io.File
import java.nio.file.{Files, Path}
import java.time.LocalDateTime
import java.util.UUID

case class DraftSticker(
    id: DraftStickerId,
    submittingUser: TelegramUserId,
    name: Option[StickerName] = None,
    notes: StickerNotes = None,
    source: StickerSource = None,
    language: StickerLanguage = None,
    stickerLinks: Map[StickerLinkType, DraftStickerLink] = Map.empty,
    creationTimestamp: LocalDateTime = LocalDateTime.now()
) {

  final val directoryName: Option[StickerDirectoryRelationDirectoryName] = {
    val lastCharIndex = Math.min(this.cleanName.map(_.length).getOrElse(0), 63)
    this.cleanName
      .map(_.substring(0, lastCharIndex))
      .map(_ ++ "-" ++ UUID.randomUUID().toString.substring(0, 7))
  }

  private def cleanName: Option[String] = this.name.map(
    _.replace(" ", "-")
      .replaceAll("[^A-Za-z0-9-\\-]", "")
      .toLowerCase
  )
}

sealed trait DraftStickerLink {
  def bytes: Array[Byte]
  def extension: String
  def dimensions: Option[StickerDimensions]
}
case class DraftStickerLinkBinary(bytes: Array[Byte], extension: String, dimensions: Option[StickerDimensions])
    extends DraftStickerLink
case class DraftStickerLinkFilename(absolutePath: String, extension: String, dimensions: Option[StickerDimensions])
    extends DraftStickerLink {
  override def bytes: Array[Byte] = {
    val file: File = new File(this.absolutePath)
    val path: Path = file.toPath
    Files.readAllBytes(path)
  }
}
case class StickerDimensions(width: Int, height: Int)
