package acab.devcon0.domain.ports.input.preview

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

sealed abstract class PreviewDraftStickerCommand(val draftStickerId: DraftStickerId, val telegramUserId: TelegramUserId)
    extends Command[DraftStickerId]
case class PreviewDraftStickerNonAdminCommand(
    override val draftStickerId: DraftStickerId,
    override val telegramUserId: TelegramUserId
) extends PreviewDraftStickerCommand(draftStickerId, telegramUserId)
case class PreviewDraftStickerAdminsCommand(
    override val draftStickerId: DraftStickerId,
    override val telegramUserId: TelegramUserId
) extends PreviewDraftStickerCommand(draftStickerId, telegramUserId)

sealed abstract class PreviewDraftStickerEvent[T] extends Event[T]
case class PreviewDraftStickerSuccessEvent(draftStickerId: DraftStickerId) extends PreviewDraftStickerEvent[Unit]
case class PreviewDraftStickerErrorEvent(draftStickerId: DraftStickerId, throwable: Throwable)
    extends PreviewDraftStickerEvent[Throwable]

trait PreviewDraftStickerCommandHandler
    extends CommandHandler[PreviewDraftStickerCommand, DraftStickerId, PreviewDraftStickerEvent[_]]

object PreviewDraftStickerCommandEventProcessor {
  def apply(result: Try[PreviewDraftStickerEvent[_]], logger: Logger): Try[DraftStickerId] = {
    result match {
      case Failure(exception)                                       => handleErrorCase(exception, logger)
      case Success(PreviewDraftStickerErrorEvent(_, exception))     => handleErrorCase(exception, logger)
      case Success(PreviewDraftStickerSuccessEvent(draftStickerId)) => Success(draftStickerId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
    logger.error("PreviewStickerEventProcessor", exception)
    Failure(exception)
  }
}

object PreviewDraftStickerCommandImplicits {
  implicit class PreviewDraftStickerCommandEventFlattenOps(result: Try[PreviewDraftStickerEvent[_]])
      extends LazyLogging {
    def flattenEvents: Try[DraftStickerId] = {
      result match {
        case Failure(exception)                                       => handleErrorCase(exception, logger)
        case Success(PreviewDraftStickerErrorEvent(_, exception))     => handleErrorCase(exception, logger)
        case Success(PreviewDraftStickerSuccessEvent(draftStickerId)) => Success(draftStickerId)
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
      logger.error("DraftStickerAcceptEventProcessor", exception)
      Failure(exception)
    }
  }
}
