package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerDirectoryRelationDirectoryName}

case class StickerDirectoryRelation(
    stickerId: MaybeStickerId = None,
    directoryName: StickerDirectoryRelationDirectoryName
)
