package acab.devcon0.domain.service

import java.io.File
import scala.util.Try

trait MastodonService {

  def addStatus(file: File, text: String): Try[Unit]
}
