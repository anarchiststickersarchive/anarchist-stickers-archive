package acab.devcon0.domain.ports.output

import com.danielasfregola.twitter4s.entities.{MediaDetails, Tweet}

import java.io.File
import scala.util.Try

trait TwitterClient {

  def uploadMediaFromFile(file: File): Try[MediaDetails]

  def createTweet(status: String, media_ids: Seq[Long] = Seq.empty): Try[Tweet]
}
