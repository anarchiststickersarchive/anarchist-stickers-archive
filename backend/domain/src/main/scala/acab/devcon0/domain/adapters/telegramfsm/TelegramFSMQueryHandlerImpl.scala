package acab.devcon0.domain.adapters.telegramfsm

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.fsm.addsticker.TelegramAddFSMInstance
import acab.devcon0.domain.fsm.bookmarksticker.TelegramBookmarkFSMInstance
import acab.devcon0.domain.fsm.editsticker.TelegramEditFSMInstance
import acab.devcon0.domain.ports.input.telegramfsm._
import acab.devcon0.domain.service.TelegramFiniteStateMachine.{TelegramFSMId, TelegramFSMInstance}
import acab.devcon0.domain.service._

import scala.language.postfixOps
import scala.util.Try

class TelegramFSMExistsAnyQueryHandlerImpl(val telegramFSMService: TelegramFSMService)
    extends TelegramFSMExistsAnyQueryHandler {
  override def handle(query: TelegramFSMExistsAnyQuery): Try[Boolean] = {
    telegramFSMService
      .exists(query.id)
  }
}

class TelegramAddFSMExistsQueryHandler(val telegramFSMService: TelegramFSMService)
    extends TelegramFSMExistsQueryHandler {
  override def handle(query: TelegramFSMExistsQuery): Try[Boolean] = {
    telegramFSMService
      .get(query.id)
      .map(maybe => maybe.isDefined && maybe.get.isInstanceOf[TelegramAddFSMInstance])
  }
}

class TelegramEditFSMExistsQueryHandler(val telegramFSMService: TelegramFSMService)
    extends TelegramFSMExistsQueryHandler {
  override def handle(query: TelegramFSMExistsQuery): Try[Boolean] = {
    telegramFSMService
      .get(query.id)
      .map(maybe => maybe.isDefined && maybe.get.isInstanceOf[TelegramEditFSMInstance])
  }
}

class TelegramBookmarkFSMExistsQueryHandler(val telegramFSMService: TelegramFSMService)
    extends TelegramFSMExistsQueryHandler {
  override def handle(query: TelegramFSMExistsQuery): Try[Boolean] = {
    telegramFSMService
      .get(query.id)
      .map(maybe => maybe.isDefined && maybe.get.isInstanceOf[TelegramBookmarkFSMInstance])
  }
}

class TelegramAddFSMQueryHandler(val telegramFSMService: TelegramFSMService) extends TelegramFSMQueryHandler {
  override def handle(query: TelegramFSMQuery): Try[TelegramAddFSMInstance] = {
    telegramFSMService
      .get(query.id)
      .flatMap(TryUtils.forceOptionExistence[TelegramFSMInstance, TelegramFSMId](Some(query.id)))
      .map(_.asInstanceOf[TelegramAddFSMInstance])
  }
}

class TelegramEditFSMQueryHandler(val telegramFSMService: TelegramFSMService) extends TelegramFSMQueryHandler {
  override def handle(query: TelegramFSMQuery): Try[TelegramEditFSMInstance] = {
    telegramFSMService
      .get(query.id)
      .flatMap(TryUtils.forceOptionExistence[TelegramFSMInstance, TelegramFSMId](Some(query.id)))
      .map(_.asInstanceOf[TelegramEditFSMInstance])
  }
}

class TelegramBookmarkFSMQueryHandler(val telegramFSMService: TelegramFSMService) extends TelegramFSMQueryHandler {
  override def handle(query: TelegramFSMQuery): Try[TelegramBookmarkFSMInstance] = {
    telegramFSMService
      .get(query.id)
      .flatMap(TryUtils.forceOptionExistence[TelegramFSMInstance, TelegramFSMId](Some(query.id)))
      .map(_.asInstanceOf[TelegramBookmarkFSMInstance])
  }
}
