package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.TypeAliases._

import java.time.LocalDateTime

case class Publication(
    id: PublicationId,
    stickerId: MaybeStickerId,
    platform: PublicationPlatform,
    creationTimestamp: LocalDateTime
)

object PublicationPlatforms extends Enumeration {
  type PublicationPlatform = Value

  val TELEGRAM, INSTAGRAM, TWITTER, MASTODON, PIXELFED, BLUESKY = Value
}
