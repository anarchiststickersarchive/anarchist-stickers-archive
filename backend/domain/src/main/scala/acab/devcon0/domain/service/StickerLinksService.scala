package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.StickerLink
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLinkId, StickerLinkIpfsCid}
import acab.devcon0.domain.ports.output.StickerLinksRepository
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}

class StickerLinksService(repository: StickerLinksRepository) extends LazyLogging {

  def getAll: Try[Seq[StickerLink]] = {
    repository.getAll
  }

  def getById(stickerLinkId: StickerLinkId): Try[Option[StickerLink]] = {
    repository.getById(stickerLinkId)
  }

  def getByStickerId(stickerId: MaybeStickerId): Try[Seq[StickerLink]] = {
    repository.getByStickerId(stickerId)
  }

  def getByIpfsCID(ipfsCid: StickerLinkIpfsCid): Try[Option[StickerLink]] = {
    repository.getByIpfsCID(ipfsCid)
  }

  def upsert(stickerLink: StickerLink): Try[Option[StickerLink]] = {
    this.getByIpfsCID(stickerLink.ipfsCID) match {
      case Failure(exception) => Failure(exception)
      case Success(Some(_))   => Failure(new IllegalArgumentException("Duplicate IPFS CID"))
      case Success(None) =>
        repository
          .upsert(stickerLink)
          .flatMap(stickerLinkId => getById(stickerLinkId))
    }
  }

  def delete(stickerLinkId: StickerLinkId): Try[Unit] = {
    repository.delete(stickerLinkId)
  }

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit] = {
    repository.deleteByStickerId(stickerId)
  }
}
