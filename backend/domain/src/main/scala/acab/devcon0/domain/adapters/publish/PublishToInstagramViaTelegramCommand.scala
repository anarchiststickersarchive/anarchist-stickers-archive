package acab.devcon0.domain.adapters.publish

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.{AggregatedSticker, Sticker, StickerLink, TelegramUserId}
import acab.devcon0.domain.service.{CaptionService, SquarePhotoHandler, TelegramService}
import com.bot4s.telegram.models.InputFile
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class PublishToInstagramViaTelegramCommand(
    val captionService: CaptionService,
    val configuration: Configuration,
    val telegramService: TelegramService,
    val squarePhotoHandler: SquarePhotoHandler = new SquarePhotoHandler() {}
) extends PublishToPlatformCommand
    with LazyLogging {

  override def publish(aggregatedSticker: AggregatedSticker): Try[Unit] = {
    publishInner(aggregatedSticker) match {
      case Failure(exception) => handleFailedPublishing(exception)
      case Success(value)     => Success(value)
    }
  }

  private def publishInner(aggregatedSticker: AggregatedSticker): Try[Unit] = {
    getCaption(aggregatedSticker.sticker, aggregatedSticker.links)
      .flatMap(caption => {
        telegramService.sendPhoto(
          to = TelegramUserId(configuration.telegram.adminUserId.id),
          text = caption,
          inputFile = getPhoto(aggregatedSticker)
        )
      })
  }

  private def getPhoto(aggregatedSticker: AggregatedSticker): InputFile = {
    val squarePhoto: Array[Byte] = squarePhotoHandler.get(aggregatedSticker.publishingPhoto)
    InputFile.Contents("filename", squarePhoto)
  }

  private def getCaption(sticker: Sticker, links: Seq[StickerLink]): Try[String] = {
    captionService.get(sticker, links).map(caption => caption + "\n\n" + configuration.instagramConfiguration.hashtags)
  }

  private def handleFailedPublishing(exception: Throwable): Try[Unit] = {
    logger.error("PublishToInstagramViaTelegramCommand", exception)
    Failure(exception)
  }
}
