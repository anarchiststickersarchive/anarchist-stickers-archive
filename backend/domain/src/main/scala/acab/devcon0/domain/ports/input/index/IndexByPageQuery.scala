package acab.devcon0.domain.ports.input.index

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.AggregatedSticker

case class IndexByPageQuery(page: Int = 0, pageSize: Int = 100, searchTerm: Option[String] = None)
    extends Query[Seq[AggregatedSticker]]
trait IndexByPageQueryHandler extends QueryHandler[IndexByPageQuery, Seq[AggregatedSticker]]
