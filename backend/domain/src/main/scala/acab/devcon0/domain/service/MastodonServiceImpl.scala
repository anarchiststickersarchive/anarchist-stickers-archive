package acab.devcon0.domain.service

import acab.devcon0.domain.ports.output.MastodonClient

import java.io.File
import scala.util.Try

class MastodonServiceImpl(val mastodonClient: MastodonClient) extends MastodonService {

  override def addStatus(file: File, text: String): Try[Unit] = {
    for {
      addMediaResponse <- mastodonClient.addMedia(file)
      _ <- mastodonClient.createPost(text, addMediaResponse.id)
    } yield {}
  }
}
