package acab.devcon0.domain.ports.input.index

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.Sticker

case class IndexUnpublishedQuery() extends Query[Seq[Sticker]]
trait IndexUnpublishedQueryHandler extends QueryHandler[IndexUnpublishedQuery, Seq[Sticker]]
