package acab.devcon0.domain.service

import acab.devcon0.commons.ProjectError.NotFound
import acab.devcon0.domain.dtos.{TelegramFileId, TelegramUserId}
import acab.devcon0.domain.ports.output.TelegramClient
import com.bot4s.telegram.methods.ParseMode
import com.bot4s.telegram.models.{File, InputFile}

import java.nio.file.Path
import scala.util.{Failure, Success, Try}

class TelegramServiceImpl(val telegramClient: TelegramClient) extends TelegramService {

  override def sendMessage(to: TelegramUserId, text: String): Try[Unit] = {
    telegramClient.sendMessage(to = to, text = text)
  }

  override def sendPhoto(to: TelegramUserId, text: String, inputFile: InputFile): Try[Unit] = {
    telegramClient.sendPhoto(to = to, text = text, inputFile = inputFile, parseMode = None)
  }

  override def sendPhotoWithMarkdownCaption(to: TelegramUserId, text: String, inputFile: InputFile): Try[Unit] = {
    telegramClient.sendPhoto(to = to, text = text, inputFile = inputFile, parseMode = Some(ParseMode.MarkdownV2))
  }

  override def getFileBytes(telegramFileId: TelegramFileId): Try[Array[Byte]] = {
    getFile(telegramFileId.id)
      .flatMap(file => {
        file.filePath match {
          case Some(value) => Success(value)
          case None        => Failure(new NotFound(s"File not found fileId=$telegramFileId"))
        }
      })
      .flatMap(getFileBytes)
  }

  private def getFile(fileId: String): Try[File] = {
    telegramClient.getFile(fileId)
  }

  private def getFileBytes(path: String): Try[Array[Byte]] = {
    telegramClient.getFileBytes(path = path)
  }
}
