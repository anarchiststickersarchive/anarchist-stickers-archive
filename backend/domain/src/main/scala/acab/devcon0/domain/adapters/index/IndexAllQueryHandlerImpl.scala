package acab.devcon0.domain.adapters.index

import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.ports.input.index.{IndexAllQuery, IndexAllQueryHandler}
import acab.devcon0.domain.service.StickersService

import scala.util.Try

class IndexAllQueryHandlerImpl(val stickersService: StickersService) extends IndexAllQueryHandler {
  override def handle(query: IndexAllQuery): Try[Seq[Sticker]] = stickersService.getAll
}
