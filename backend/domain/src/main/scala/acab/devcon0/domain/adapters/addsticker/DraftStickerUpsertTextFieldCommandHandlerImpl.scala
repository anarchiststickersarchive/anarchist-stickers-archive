package acab.devcon0.domain.adapters.addsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.DraftSticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.service.DraftStickersService
import com.typesafe.scalalogging.LazyLogging
import io.bartholomews.iso.LanguageCode

import scala.util.{Failure, Success, Try}

class DraftStickerUpsertTextFieldCommandHandlerImpl(draftStickersService: DraftStickersService)
    extends DraftStickerUpsertTextFieldCommandHandler
    with LazyLogging {

  override def handle(cmd: DraftStickerUpsertTextFieldCommand): Try[DraftStickerUpsertTextFieldEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) =>
        Success(DraftStickerUpsertTextFieldErrorEvent(draftStickerId = cmd.draftStickerId, throwable = throwable))
      case Success(draftSticker) => Success(DraftStickerUpsertTextFieldSuccessEvent(draftSticker = draftSticker))
    }
  }

  private def handleInner(cmd: DraftStickerUpsertTextFieldCommand): Try[DraftSticker] = {
    cmd match {
      case DraftStickerUpsertNameCommand(draftStickerId, value)     => upsertName(draftStickerId, value)
      case DraftStickerUpsertLanguageCommand(draftStickerId, value) => upsertLanguage(draftStickerId, value)
      case DraftStickerUpsertNotesCommand(draftStickerId, value)    => upsertNotes(draftStickerId, value)
      case DraftStickerUpsertSourceCommand(draftStickerId, value)   => upsertSource(draftStickerId, value)
    }
  }

  private def upsertName(id: DraftStickerId, value: String): Try[DraftSticker] = {
    val modifyFunction: DraftSticker => DraftSticker = addSticker => addSticker.copy(name = Some(value))
    upsert(id, modifyFunction)
  }

  private def upsertNotes(id: DraftStickerId, value: String): Try[DraftSticker] = {
    val modifyFunction: DraftSticker => DraftSticker = addSticker => addSticker.copy(notes = Some(value))
    upsert(id, modifyFunction)
  }

  private def upsertSource(id: DraftStickerId, value: String): Try[DraftSticker] = {
    val modifyFunction: DraftSticker => DraftSticker = addSticker => addSticker.copy(source = Some(value))
    upsert(id, modifyFunction)
  }

  private def upsertLanguage(id: DraftStickerId, value: String): Try[DraftSticker] = {
    val modifyFunction: DraftSticker => DraftSticker = addSticker =>
      addSticker.copy(language = LanguageCode.withValueOpt(value.toLowerCase))
    upsert(id, modifyFunction)
  }

  private def upsert(
      id: DraftStickerId,
      modifyAddStickerFunction: DraftSticker => DraftSticker
  ): Try[DraftSticker] = {
    getAddSticker(id)
      .map(modifyAddStickerFunction)
      .flatMap(draftStickersService.upsert)
      .flatMap(TryUtils.forceOptionExistence[DraftSticker, DraftStickerId](Some(id)))
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }

  private def getAddSticker(id: DraftStickerId): Try[DraftSticker] = {
    draftStickersService
      .getById(id)
      .flatMap(TryUtils.forceOptionExistence())
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }
}
