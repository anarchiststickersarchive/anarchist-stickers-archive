package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.util.{Failure, Success, Try}

case class DraftStickerSubmitCommand(draftStickerId: DraftStickerId) extends Command[DraftStickerId]

sealed abstract class DraftStickerSubmitEvent[T] extends Event[T]

case class DraftStickerSubmitSuccessEvent(sticker: Sticker) extends DraftStickerSubmitEvent[Sticker]

case class DraftStickerSubmitErrorEvent(
    draftStickerId: DraftStickerId,
    throwable: Throwable
) extends DraftStickerSubmitEvent[DraftStickerId]

trait DraftStickerSubmitCommandHandler
    extends CommandHandler[DraftStickerSubmitCommand, DraftStickerId, DraftStickerSubmitEvent[_]]

object DraftStickerSubmitCommandImplicits {
  implicit class DraftStickerSubmitCommandEventFlattenOps(result: Try[DraftStickerSubmitEvent[_]]) extends LazyLogging {
    def flattenEvents: Try[Sticker] = {
      result match {
        case Failure(exception)                                  => handleErrorCase(exception, logger)
        case Success(DraftStickerSubmitErrorEvent(_, exception)) => handleErrorCase(exception, logger)
        case Success(DraftStickerSubmitSuccessEvent(sticker))    => Success(sticker)
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[Sticker] = {
      logger.error("DraftStickerSubmitEventProcessor", exception)
      Failure(exception)
    }
  }
}
