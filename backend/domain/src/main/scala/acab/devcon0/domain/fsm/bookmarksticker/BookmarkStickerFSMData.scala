package acab.devcon0.domain.fsm.bookmarksticker

import acab.devcon0.fsm.FiniteStateMachineData

case class BookmarkStickerFSMData() extends FiniteStateMachineData
