package acab.devcon0.domain.fsm.addsticker

import acab.devcon0.fsm._

sealed trait AddStickerFSMState extends FiniteStateMachineState
case object AwaitingName extends AddStickerFSMState
case object AwaitingOriginalSticker extends AddStickerFSMState
case object AwaitingLanguage extends AddStickerFSMState
case object AwaitingNotes extends AddStickerFSMState
case object AwaitingSource extends AddStickerFSMState
case object AwaitingVectorSticker extends AddStickerFSMState
case object BasicDataGathered extends AddStickerFSMState
case object End extends AddStickerFSMState
