package acab.devcon0.domain.ports.input.bookmarksticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.TypeAliases.StickerId
import acab.devcon0.domain.dtos.{BookmarkUserId, Sticker}
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

case class StickerAddBookmarkCommand(
    stickerIds: Seq[StickerId],
    bookmarkUserId: BookmarkUserId
) extends Command[Seq[StickerId]]

sealed abstract class StickerAddBookmarkEvent[T] extends Event[T]

case class StickerAddBookmarkSuccessEvent(
    stickerIds: Seq[StickerId]
) extends StickerAddBookmarkEvent[Sticker]

case class StickerAddBookmarkErrorEvent(
    stickerIds: Seq[StickerId],
    bookmarkUserId: BookmarkUserId,
    throwable: Throwable
) extends StickerAddBookmarkEvent[Seq[StickerId]]

trait StickerAddBookmarkCommandHandler
    extends CommandHandler[StickerAddBookmarkCommand, Seq[StickerId], StickerAddBookmarkEvent[_]]

object StickerAddBookmarkEventProcessor {
  def apply(result: Try[StickerAddBookmarkEvent[_]], logger: Logger): Try[Seq[StickerId]] = {
    result match {
      case Failure(exception)                                     => handleErrorCase(exception, logger)
      case Success(StickerAddBookmarkErrorEvent(_, _, exception)) => handleErrorCase(exception, logger)
      case Success(StickerAddBookmarkSuccessEvent(stickerLinkId)) => Success(stickerLinkId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[Seq[StickerId]] = {
    logger.error("StickerAddBookmarkEventProcessor", exception)
    Failure(exception)
  }
}
