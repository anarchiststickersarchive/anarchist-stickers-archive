package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType

private case class IpfsPersistenceResult(
    draftSticker: DraftSticker,
    ipfsCIDs: Map[StickerLinkType, IpfsFile]
)
