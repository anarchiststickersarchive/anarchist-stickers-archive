package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.{PixelfedAddMediaResponse, PixelfedPostStatusResponse}

import java.io.File
import scala.util.Try

trait PixelfedClient {

  def addMedia(file: File): Try[PixelfedAddMediaResponse]

  def createPost(status: String, media_id: String): Try[PixelfedPostStatusResponse]
}
