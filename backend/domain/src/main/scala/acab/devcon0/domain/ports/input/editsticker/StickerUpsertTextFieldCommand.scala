package acab.devcon0.domain.ports.input.editsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

sealed abstract class StickerUpsertTextFieldCommand(val stickerId: MaybeStickerId) extends Command[MaybeStickerId]
case class StickerUpsertNameCommand(
    override val stickerId: MaybeStickerId,
    value: String
) extends StickerUpsertTextFieldCommand(stickerId)
case class StickerUpsertLanguageCommand(
    override val stickerId: MaybeStickerId,
    value: String
) extends StickerUpsertTextFieldCommand(stickerId)
case class StickerUpsertNotesCommand(
    override val stickerId: MaybeStickerId,
    value: String
) extends StickerUpsertTextFieldCommand(stickerId)
case class StickerUpsertSourceCommand(
    override val stickerId: MaybeStickerId,
    value: String
) extends StickerUpsertTextFieldCommand(stickerId)

sealed abstract class StickerUpsertTextFieldEvent[T] extends Event[T]

case class StickerUpsertTextFieldSuccessEvent(sticker: Sticker) extends StickerUpsertTextFieldEvent[Sticker]

case class StickerUpsertTextFieldErrorEvent(
    stickerId: MaybeStickerId,
    throwable: Throwable
) extends StickerUpsertTextFieldEvent[MaybeStickerId]

trait StickerUpsertTextFieldCommandHandler
    extends CommandHandler[StickerUpsertTextFieldCommand, MaybeStickerId, StickerUpsertTextFieldEvent[_]]

object StickerUpsertTextFieldEventProcessor {
  def apply(result: Try[StickerUpsertTextFieldEvent[_]], logger: Logger): Try[MaybeStickerId] = {
    result match {
      case Failure(exception)                                      => handleErrorCase(exception, logger)
      case Success(StickerUpsertTextFieldErrorEvent(_, exception)) => handleErrorCase(exception, logger)
      case Success(StickerUpsertTextFieldSuccessEvent(sticker))    => Success(sticker.id)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[MaybeStickerId] = {
    logger.error("StickerUpsertTextFieldEventProcessor", exception)
    Failure(exception)
  }
}
