package acab.devcon0.domain.dtos

case class PixelfedAddMediaResponse(id: String)
case class PixelfedPostStatusResponse(id: String)
case class PixelfedPostStatusRequest(status: String, mediaIds: Seq[String])
