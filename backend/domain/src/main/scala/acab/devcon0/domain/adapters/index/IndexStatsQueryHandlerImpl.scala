package acab.devcon0.domain.adapters.index

import acab.devcon0.domain.ports.input.index.{IndexStatsQuery, IndexStatsQueryHandler}
import acab.devcon0.domain.service.{IngestionQueueService, StickersService}

import scala.util.Try

class IndexStatsQueryHandlerImpl(
    val stickersService: StickersService,
    ingestionFinalPickService: IngestionQueueService
) extends IndexStatsQueryHandler {
  override def handle(query: IndexStatsQuery): Try[String] = {
    for {
      all <- countAll
      unpublished <- countUnpublished
      ingested <- ingestionFinalPickService.count()
      queryResultMessage <- buildQueryResultMessage(all, unpublished, ingested)
    } yield queryResultMessage
  }

  private def buildQueryResultMessage(all: Long, unpublished: Long, ingested: Long): Try[String] = Try {
    s"There are $all stickers in the index.\n" +
      s"$unpublished stickers in the index that have not been published yet.\n" +
      s"$ingested are awaiting for final polishing.\n"
  }

  private def countUnpublished: Try[Long] = {
    stickersService.Sugar.countUnpublished
      .map(maybeValue => maybeValue.getOrElse(-1))
  }

  private def countAll: Try[Long] = {
    stickersService.Sugar.countAll
      .map(maybeValue => maybeValue.getOrElse(-1))
  }
}
