package acab.devcon0.domain.ports.input.ingestion

import acab.devcon0.cqrs.{Query, QueryHandler}

import java.io.File

case class IngestionQueueRasterizeQuery(maybeSource: Option[String]) extends Query[Option[File]]
case class IngestionQueueVectorQuery(rasterized: File) extends Query[Option[File]]
trait IngestionQueueRasterizeQueryHandler extends QueryHandler[IngestionQueueRasterizeQuery, Option[File]]
trait IngestionQueueVectorQueryHandler extends QueryHandler[IngestionQueueVectorQuery, Option[File]]
