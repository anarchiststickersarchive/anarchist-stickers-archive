package acab.devcon0.domain.adapters.index

import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.ports.input.index.{IndexUnpublishedQuery, IndexUnpublishedQueryHandler}
import acab.devcon0.domain.service.StickersService

import scala.util.Try

class IndexUnpublishedQueryHandlerImpl(val stickersService: StickersService) extends IndexUnpublishedQueryHandler {
  override def handle(query: IndexUnpublishedQuery): Try[Seq[Sticker]] = stickersService.Sugar.getUnpublished
}
