package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.DraftSticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.ports.output.DraftStickersRepository
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

class DraftStickersService(repository: DraftStickersRepository) extends LazyLogging {

  def getById(id: DraftStickerId): Try[Option[DraftSticker]] = {
    repository.getById(id)
  }

  def upsert(draftSticker: DraftSticker): Try[Option[DraftSticker]] = {
    repository
      .upsert(draftSticker)
      .flatMap(id => getById(id))
  }

  def delete(id: DraftStickerId): Try[Unit] = {
    repository.delete(id)
  }
}
