package acab.devcon0.domain.fsm.editsticker

import acab.devcon0.fsm.FiniteStateMachineEvent

sealed trait EditStickerFSMEvent extends FiniteStateMachineEvent
case object Init extends EditStickerFSMEvent
case object Cancel extends EditStickerFSMEvent
case object TextSubmitted extends EditStickerFSMEvent
case object DocumentSubmitted extends EditStickerFSMEvent
case object EditName extends EditStickerFSMEvent
case object EditNotes extends EditStickerFSMEvent
case object EditSource extends EditStickerFSMEvent
case object EditLanguage extends EditStickerFSMEvent
case object EditVector extends EditStickerFSMEvent
case object EditOriginal extends EditStickerFSMEvent
case object VectorizeOnDemand extends EditStickerFSMEvent
