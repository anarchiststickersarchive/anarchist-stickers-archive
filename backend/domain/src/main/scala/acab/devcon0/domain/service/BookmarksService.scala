package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.{Bookmark, BookmarkId, BookmarkUserId}
import acab.devcon0.domain.ports.output.BookmarksRepository
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

trait BookmarksService {

  def getByUser(bookmarkUserId: BookmarkUserId): Try[Seq[Bookmark]]

  def upsert(bookmark: Bookmark): Try[Bookmark]

  def delete(bookmarkId: BookmarkId): Try[Unit]
}

class BookmarksServiceImpl(repository: BookmarksRepository) extends BookmarksService with LazyLogging {

  override def getByUser(bookmarkUserId: BookmarkUserId): Try[Seq[Bookmark]] = {
    repository.getByUser(bookmarkUserId)
  }

  override def upsert(bookmark: Bookmark): Try[Bookmark] = {
    repository.upsert(bookmark)
  }

  override def delete(bookmarkId: BookmarkId): Try[Unit] = {
    repository.delete(bookmarkId)
  }
}
