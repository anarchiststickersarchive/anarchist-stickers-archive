package acab.devcon0.domain.ports.input.bookmarksticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.TypeAliases.StickerId
import acab.devcon0.domain.dtos.{BookmarkUserId, Sticker}
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

case class StickerDeleteBookmarkCommand(
    stickerIds: Seq[StickerId],
    bookmarkUserId: BookmarkUserId
) extends Command[Seq[StickerId]]

sealed abstract class StickerDeleteBookmarkEvent[T] extends Event[T]

case class StickerDeleteBookmarkSuccessEvent(
    stickerIds: Seq[StickerId]
) extends StickerDeleteBookmarkEvent[Sticker]

case class StickerDeleteBookmarkErrorEvent(
    stickerIds: Seq[StickerId],
    bookmarkUserId: BookmarkUserId,
    throwable: Throwable
) extends StickerDeleteBookmarkEvent[Seq[StickerId]]

trait StickerDeleteBookmarkCommandHandler
    extends CommandHandler[StickerDeleteBookmarkCommand, Seq[StickerId], StickerDeleteBookmarkEvent[_]]

object StickerDeleteBookmarkEventProcessor {
  def apply(result: Try[StickerDeleteBookmarkEvent[_]], logger: Logger): Try[Seq[StickerId]] = {
    result match {
      case Failure(exception)                                        => handleErrorCase(exception, logger)
      case Success(StickerDeleteBookmarkErrorEvent(_, _, exception)) => handleErrorCase(exception, logger)
      case Success(StickerDeleteBookmarkSuccessEvent(stickerLinkId)) => Success(stickerLinkId)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[Seq[StickerId]] = {
    logger.error("StickerDeleteBookmarkEventProcessor", exception)
    Failure(exception)
  }
}
