package acab.devcon0.domain.dtos

case class IpfsAddResponse(name: String, hash: String, size: String)
case class IpfsAddDirectoryResponse(name: String, hash: String)
