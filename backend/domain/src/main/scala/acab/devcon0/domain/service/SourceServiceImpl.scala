package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.{SourceDto, Sources}

import scala.util.Try

class SourceServiceImpl extends SourceService {

  override def parse(rawSource: String): Try[SourceDto] = Try {
    val tokens = rawSource.split(":", 2)
    tokens.headOption match {
      case None => throw new IllegalArgumentException(s"No source at the left of the @? Original value=$rawSource")
      case Some(value) =>
        Try(Sources.withName(value.toUpperCase))
          .fold(
            throwable =>
              throw new IllegalArgumentException(s"Could not find given source. value=$rawSource", throwable),
            source => {
              val url = tokens.tail.mkString.stripPrefix("@")
              SourceDto(url, source)
            }
          )
    }
  }
}
