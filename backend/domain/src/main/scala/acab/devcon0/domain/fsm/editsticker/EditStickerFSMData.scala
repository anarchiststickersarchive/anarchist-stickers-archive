package acab.devcon0.domain.fsm.editsticker

import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.fsm.FiniteStateMachineData

case class EditStickerFSMData(stickerId: MaybeStickerId) extends FiniteStateMachineData
