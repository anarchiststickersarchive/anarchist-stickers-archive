package acab.devcon0.domain.adapters.addsticker

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.DraftSticker
import acab.devcon0.domain.ports.input.addsticker._
import acab.devcon0.domain.service.DraftStickersService
import com.typesafe.scalalogging.LazyLogging

import java.util.UUID
import scala.util.{Failure, Success, Try}

class DraftStickerNewCommandHandlerImpl(draftStickersService: DraftStickersService)
    extends DraftStickerNewCommandHandler
    with LazyLogging {
  override def handle(cmd: DraftStickerNewCommand): Try[DraftStickerNewEvent[_]] = {
    handleInner(cmd) match {
      case Failure(throwable) =>
        Success(DraftStickerErrorNewEvent(draftStickerId = cmd.draftStickerId, throwable = throwable))
      case Success(draftSticker) => Success(DraftStickerSuccessNewEvent(draftSticker = draftSticker))
    }
  }

  private def handleInner(cmd: DraftStickerNewCommand): Try[DraftSticker] = {
    val draftSticker: DraftSticker = DraftSticker(id = UUID.randomUUID(), submittingUser = cmd.submittingUser)
    draftStickersService
      .upsert(draftSticker)
      .flatMap(TryUtils.forceOptionExistence())
  }
}
