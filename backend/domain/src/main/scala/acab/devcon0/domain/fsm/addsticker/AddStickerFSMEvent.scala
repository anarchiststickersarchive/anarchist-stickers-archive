package acab.devcon0.domain.fsm.addsticker

import acab.devcon0.fsm.FiniteStateMachineEvent

sealed trait AddStickerFSMEvent extends FiniteStateMachineEvent
case object Init extends AddStickerFSMEvent
case object InitWithBasicData extends AddStickerFSMEvent
case object Skip extends AddStickerFSMEvent
case object Cancel extends AddStickerFSMEvent
case object Save extends AddStickerFSMEvent
case object TextSubmitted extends AddStickerFSMEvent
case object DocumentSubmitted extends AddStickerFSMEvent
case object AddNotes extends AddStickerFSMEvent
case object AddLanguage extends AddStickerFSMEvent
case object AddVector extends AddStickerFSMEvent
case object AddSource extends AddStickerFSMEvent
case object VectorizeOnDemand extends AddStickerFSMEvent
