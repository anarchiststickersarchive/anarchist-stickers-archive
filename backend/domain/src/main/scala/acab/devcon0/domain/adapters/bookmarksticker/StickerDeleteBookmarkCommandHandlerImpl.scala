package acab.devcon0.domain.adapters.bookmarksticker

import acab.devcon0.domain.dtos.TypeAliases.StickerId
import acab.devcon0.domain.dtos.{BookmarkId, BookmarkUserId}
import acab.devcon0.domain.ports.input.bookmarksticker._
import acab.devcon0.domain.service._
import cats.syntax.traverse.toTraverseOps
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}

class StickerDeleteBookmarkCommandHandlerImpl(
    bookmarksService: BookmarksService
) extends StickerDeleteBookmarkCommandHandler
    with LazyLogging {

  override def handle(cmd: StickerDeleteBookmarkCommand): Try[StickerDeleteBookmarkEvent[_]] = {
    val stickerIds: Seq[StickerId] = cmd.stickerIds
    val bookmarkUserId: BookmarkUserId = cmd.bookmarkUserId

    handleInner(stickerIds, bookmarkUserId) match {
      case Failure(throwable) =>
        Success(
          StickerDeleteBookmarkErrorEvent(
            stickerIds = stickerIds,
            bookmarkUserId = bookmarkUserId,
            throwable = throwable
          )
        )
      case Success(_) => Success(StickerDeleteBookmarkSuccessEvent(stickerIds = stickerIds))
    }
  }

  private def handleInner(
      stickerIds: Seq[StickerId],
      bookmarkUserId: BookmarkUserId
  ): Try[List[Unit]] = {
    stickerIds.toList
      .traverse(stickerId => {
        val bookmarkId: BookmarkId = BookmarkId(stickerId, bookmarkUserId)
        bookmarksService.delete(bookmarkId)
      })
  }
}
