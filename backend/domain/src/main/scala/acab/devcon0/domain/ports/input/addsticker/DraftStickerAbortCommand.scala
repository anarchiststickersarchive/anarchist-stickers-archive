package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.service.TelegramFiniteStateMachine.TelegramFSMId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.util.{Failure, Success, Try}

case class DraftStickerAbortCommand(draftStickerId: DraftStickerId, telegramFSMId: TelegramFSMId)
    extends Command[DraftStickerId]

sealed abstract class DraftStickerAbortEvent[T] extends Event[T]

case class DraftStickerAbortSuccessEvent(draftStickerId: DraftStickerId) extends DraftStickerAbortEvent[Sticker]

case class DraftStickerAbortErrorEvent(
    draftStickerId: DraftStickerId,
    telegramFSMId: TelegramFSMId,
    throwable: Throwable
) extends DraftStickerAbortEvent[DraftStickerId]

trait DraftStickerAbortCommandHandler
    extends CommandHandler[DraftStickerAbortCommand, DraftStickerId, DraftStickerAbortEvent[_]]

object DraftStickerAbortCommandImplicits {
  implicit class DraftStickerAbortCommandEventFlattenOps(result: Try[DraftStickerAbortEvent[_]]) extends LazyLogging {
    def flattenEvents: Try[DraftStickerId] = {
      result match {
        case Failure(exception)                                     => handleErrorCase(exception, logger)
        case Success(DraftStickerAbortErrorEvent(_, _, exception))  => handleErrorCase(exception, logger)
        case Success(DraftStickerAbortSuccessEvent(draftStickerId)) => Success(draftStickerId)
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
      logger.error("DraftStickerAbortEventProcessor", exception)
      Failure(exception)
    }
  }
}
