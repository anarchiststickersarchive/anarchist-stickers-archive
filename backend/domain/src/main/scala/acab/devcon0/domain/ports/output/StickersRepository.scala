package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId

import scala.util.Try

trait StickersRepository {

  def getAll: Try[Seq[Sticker]]

  def getAll(page: Int, pageSize: Int, searchTerm: Option[String]): Try[Seq[Sticker]]

  def getById(id: MaybeStickerId): Try[Option[Sticker]]

  def upsert(sticker: Sticker): Try[MaybeStickerId]

  def delete(stickerId: MaybeStickerId): Try[Unit]

  def search(searchTerm: String): Try[Seq[Sticker]]

  def countAll: Try[Option[Long]]

  def countUnpublished: Try[Option[Long]]

  // Stickers that are not published at no platform
  def getUnpublished: Try[Seq[Sticker]]

  def getRandomNotPublishedAnywhere: Try[Option[Sticker]]
}
