package acab.devcon0.domain.service

import acab.devcon0.commons.TryUtils
import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLinkFilename}
import acab.devcon0.domain.dtos._
import cats.implicits.toTraverseOps
import com.typesafe.scalalogging.LazyLogging

import java.io.File
import java.nio.file.Path
import scala.util.Try

class AggregatedStickersService(
    val configuration: Configuration,
    val stickersService: StickersService,
    val stickerLinksService: StickerLinksService,
    val stickerDirectoryRelationsService: StickerDirectoryRelationsService
) extends LazyLogging {

  def getByStickerId(stickerId: MaybeStickerId): Try[AggregatedSticker] = {
    for {
      sticker <- getStickerByIdInner(stickerId)
      links <- stickerLinksService.getByStickerId(sticker.id)
      stickerDirectoryRelation <- getStickerDirectoryRelationByStickerId(sticker.id)
      bestPhotoAsPath <- getBestAvailablePhotoAsPath(links, stickerDirectoryRelation)
    } yield AggregatedSticker(sticker, links, stickerDirectoryRelation, bestPhotoAsPath)
  }

  def getAll(page: Int, pageSize: Int, searchTerm: Option[String]): Try[Seq[AggregatedSticker]] = {
    stickersService
      .getAll(page, pageSize, searchTerm)
      .flatMap(stickers => stickers.toList.traverse(sticker => aggregateByStickerId(sticker)))
  }

  private def aggregateByStickerId(sticker: Sticker): Try[AggregatedSticker] = {
    for {
      links <- stickerLinksService.getByStickerId(sticker.id)
      stickerDirectoryRelation <- getStickerDirectoryRelationByStickerId(sticker.id)
      bestPhotoAsPath <- getBestAvailablePhotoAsPath(links, stickerDirectoryRelation)
    } yield AggregatedSticker(sticker, links, stickerDirectoryRelation, bestPhotoAsPath)
  }

  private def getStickerByIdInner(stickerId: MaybeStickerId): Try[Sticker] = {
    stickersService
      .getById(stickerId)
      .flatMap(TryUtils.forceOptionExistence(Some(stickerId)))
  }

  private def getStickerDirectoryRelationByStickerId(stickerId: MaybeStickerId): Try[StickerDirectoryRelation] = {
    stickerDirectoryRelationsService
      .getByStickerId(stickerId)
      .flatMap(TryUtils.forceOptionExistence(Some(stickerId)))
  }

  private def getBestAvailablePhotoAsPath(
      links: Seq[StickerLink],
      stickerDirectoryRelation: StickerDirectoryRelation
  ): Try[Path] = Try {

    val bestPhotoByQuality: StickerLinkFilename = links
      .find(stickerLink => stickerLink.`type`.equals(StickerLinkTypes.ORIGINAL))
      .map(link => link.filename)
      .get

    new File(
      configuration.archiveDirectory + "/" + stickerDirectoryRelation.directoryName + "/" + bestPhotoByQuality
    ).toPath
  }
}
