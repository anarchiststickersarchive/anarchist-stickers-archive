package acab.devcon0.domain.ports.output

import acab.devcon0.fsm.FiniteStateMachineInstance

import scala.util.Try

trait FiniteStateMachineRepository[ID] {
  def get(id: ID): Try[Option[FiniteStateMachineInstance[ID]]]
  def upsert(INSTANCE: FiniteStateMachineInstance[ID]): Try[ID]
  def delete(id: ID): Try[Unit]
}
