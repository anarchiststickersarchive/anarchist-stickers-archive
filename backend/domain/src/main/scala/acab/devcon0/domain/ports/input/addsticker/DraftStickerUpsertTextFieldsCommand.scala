package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.DraftSticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

sealed abstract class DraftStickerUpsertTextFieldCommand(val draftStickerId: DraftStickerId)
    extends Command[DraftStickerId]
case class DraftStickerUpsertNameCommand(
    override val draftStickerId: DraftStickerId,
    value: String
) extends DraftStickerUpsertTextFieldCommand(draftStickerId)
case class DraftStickerUpsertLanguageCommand(
    override val draftStickerId: DraftStickerId,
    value: String
) extends DraftStickerUpsertTextFieldCommand(draftStickerId)
case class DraftStickerUpsertNotesCommand(
    override val draftStickerId: DraftStickerId,
    value: String
) extends DraftStickerUpsertTextFieldCommand(draftStickerId)
case class DraftStickerUpsertSourceCommand(
    override val draftStickerId: DraftStickerId,
    value: String
) extends DraftStickerUpsertTextFieldCommand(draftStickerId)

sealed abstract class DraftStickerUpsertTextFieldEvent[T] extends Event[T]

case class DraftStickerUpsertTextFieldSuccessEvent(draftSticker: DraftSticker)
    extends DraftStickerUpsertTextFieldEvent[DraftSticker]

case class DraftStickerUpsertTextFieldErrorEvent(
    draftStickerId: DraftStickerId,
    throwable: Throwable
) extends DraftStickerUpsertTextFieldEvent[DraftStickerId]

trait DraftStickerUpsertTextFieldCommandHandler
    extends CommandHandler[DraftStickerUpsertTextFieldCommand, DraftStickerId, DraftStickerUpsertTextFieldEvent[_]]

object DraftStickerUpsertTextFieldEventProcessor {
  def apply(result: Try[DraftStickerUpsertTextFieldEvent[_]], logger: Logger): Try[DraftStickerId] = {
    result match {
      case Failure(exception)                                             => handleErrorCase(exception, logger)
      case Success(DraftStickerUpsertTextFieldErrorEvent(_, exception))   => handleErrorCase(exception, logger)
      case Success(DraftStickerUpsertTextFieldSuccessEvent(draftSticker)) => Success(draftSticker.id)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
    logger.error("DraftStickerUpsertTextFieldEventProcessor", exception)
    Failure(exception)
  }
}
