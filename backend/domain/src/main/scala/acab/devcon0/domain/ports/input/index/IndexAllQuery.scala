package acab.devcon0.domain.ports.input.index

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.Sticker

case class IndexAllQuery() extends Query[Seq[Sticker]]
trait IndexAllQueryHandler extends QueryHandler[IndexAllQuery, Seq[Sticker]]
