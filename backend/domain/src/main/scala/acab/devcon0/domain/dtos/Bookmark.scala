package acab.devcon0.domain.dtos

import acab.devcon0.domain.dtos.BookmarkUserPlatforms.BookmarkUserPlatform
import acab.devcon0.domain.dtos.TypeAliases._

import java.time.LocalDateTime

case class Bookmark(id: BookmarkId, creationTimestamp: LocalDateTime)

case class BookmarkId(
    stickerId: StickerId,
    userId: BookmarkUserId
)

case class BookmarkUserId(
    id: String,
    platform: BookmarkUserPlatform
)

object BookmarkUserPlatforms extends Enumeration {
  type BookmarkUserPlatform = Value

  val TELEGRAM, WEBSITE = Value
}
