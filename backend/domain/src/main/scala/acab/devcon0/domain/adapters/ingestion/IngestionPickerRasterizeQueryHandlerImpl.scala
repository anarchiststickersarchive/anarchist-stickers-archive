package acab.devcon0.domain.adapters.ingestion

import acab.devcon0.domain.ports.input.ingestion.{
  IngestionQueueRasterizeQuery,
  IngestionQueueRasterizeQueryHandler,
  IngestionQueueVectorQuery,
  IngestionQueueVectorQueryHandler
}
import acab.devcon0.domain.service.IngestionQueueService

import java.io.File
import scala.util.Try

class IngestionQueueRasterizeQueryHandlerImpl(
    ingestionQueueService: IngestionQueueService
) extends IngestionQueueRasterizeQueryHandler {
  override def handle(query: IngestionQueueRasterizeQuery): Try[Option[File]] = {
    ingestionQueueService.getRasterize(query.maybeSource)
  }
}

class IngestionQueueVectorQueryHandlerImpl(
    ingestionFinalPickService: IngestionQueueService
) extends IngestionQueueVectorQueryHandler {
  override def handle(query: IngestionQueueVectorQuery): Try[Option[File]] = {
    ingestionFinalPickService.getVector(query.rasterized)
  }
}
