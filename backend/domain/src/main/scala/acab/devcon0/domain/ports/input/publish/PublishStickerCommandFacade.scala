package acab.devcon0.domain.ports.input.publish

import acab.devcon0.domain.dtos.PublicationPlatforms.PublicationPlatform
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId

import scala.util.Try

trait PublishStickerCommandFacade {

  def apply(stickerId: MaybeStickerId, publicationPlatforms: Seq[PublicationPlatform]): Try[Unit]
}
