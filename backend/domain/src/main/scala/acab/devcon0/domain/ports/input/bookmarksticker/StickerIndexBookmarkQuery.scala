package acab.devcon0.domain.ports.input.bookmarksticker

import acab.devcon0.cqrs.{Query, QueryHandler}
import acab.devcon0.domain.dtos.{BookmarkUserId, Sticker}

case class StickerIndexBookmarkQuery(bookmarkUserId: BookmarkUserId) extends Query[Seq[Sticker]]
trait StickerIndexBookmarkQueryHandler extends QueryHandler[StickerIndexBookmarkQuery, Seq[Sticker]]
