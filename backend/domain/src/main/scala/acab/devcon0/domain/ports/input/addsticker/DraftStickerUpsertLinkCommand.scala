package acab.devcon0.domain.ports.input.addsticker

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.dtos.{DraftSticker, TelegramFileId}
import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

sealed abstract class DraftStickerUpsertBinaryCommand(val draftStickerId: DraftStickerId)
    extends Command[DraftStickerId]

case class DraftStickerUpsertBinaryTelegramCommand(
    override val draftStickerId: DraftStickerId,
    stickerLinkType: StickerLinkType,
    eitherTelegramFileIdOrAbsoluteFilename: Either[TelegramFileId, String],
    extension: String
) extends DraftStickerUpsertBinaryCommand(draftStickerId)

case class DraftStickerUpsertVectorOnDemandCommand(
    override val draftStickerId: DraftStickerId
) extends DraftStickerUpsertBinaryCommand(draftStickerId)

sealed abstract class DraftStickerUpsertBinaryEvent[T] extends Event[T]

case class DraftStickerUpsertBinarySuccessEvent(draftSticker: DraftSticker)
    extends DraftStickerUpsertBinaryEvent[DraftSticker]

case class DraftStickerUpsertBinaryErrorEvent(
    draftStickerId: DraftStickerId,
    throwable: Throwable
) extends DraftStickerUpsertBinaryEvent[DraftStickerId]

trait DraftStickerUpsertBinaryCommandHandler
    extends CommandHandler[DraftStickerUpsertBinaryCommand, DraftStickerId, DraftStickerUpsertBinaryEvent[_]]

object DraftStickerUpsertBinaryEventProcessor {
  def apply(result: Try[DraftStickerUpsertBinaryEvent[_]], logger: Logger): Try[DraftStickerId] = {
    result match {
      case Failure(exception)                                          => handleErrorCase(exception, logger)
      case Success(DraftStickerUpsertBinaryErrorEvent(_, exception))   => handleErrorCase(exception, logger)
      case Success(DraftStickerUpsertBinarySuccessEvent(draftSticker)) => Success(draftSticker.id)
    }
  }

  private def handleErrorCase(exception: Throwable, logger: Logger): Failure[DraftStickerId] = {
    logger.error("DraftStickerUpsertBinaryEventProcessor", exception)
    Failure(exception)
  }
}
