package acab.devcon0.domain.adapters.publish

import acab.devcon0.domain.dtos.AggregatedSticker
import acab.devcon0.domain.service.{CaptionService, PixelfedService}
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.Try

class PublishToPixelfedCommand(
    val pixelfedService: PixelfedService,
    val captionService: CaptionService
) extends PublishToPlatformCommand
    with LazyLogging {

  override def publish(aggregatedSticker: AggregatedSticker): Try[Unit] = {
    for {
      caption <- captionService.get(aggregatedSticker.sticker, aggregatedSticker.links)
      _ <- sendPhotoWithMarkdown(aggregatedSticker, caption)
    } yield ()
  }

  private def sendPhotoWithMarkdown(aggregatedSticker: AggregatedSticker, caption: String): Try[Unit] = {
    pixelfedService.addStatus(aggregatedSticker.publishingPhoto.toFile, caption)
  }
}
