package acab.devcon0.domain.service

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.dtos.StickerLinkTypes.StickerLinkType
import acab.devcon0.domain.dtos.TypeAliases.{
  StickerLanguage,
  StickerLinkIpfsCid,
  StickerName,
  StickerNotes,
  StickerSource
}
import acab.devcon0.domain.dtos._
import com.typesafe.scalalogging.LazyLogging
import io.bartholomews.iso.LanguageCode

import scala.collection.immutable.List
import scala.util.{Failure, Try}

class CaptionServiceImpl(
    val configuration: Configuration,
    val sourceService: SourceService
) extends LazyLogging
    with CaptionService {

  private val NON_USED_MARKDOWN_ESCAPING_CHARS: List[Char] = List('~', '>', '+', '-', '=', '|', '{', '}', '.', '!')
  private val USED_MARKDOWN_ESCAPING_CHARS: List[Char] = List('*', '_', '(', ')', '[', ']')

  override def get(sticker: Sticker, links: Seq[StickerLink]): Try[String] = Try {
    val caption: StringBuilder = new StringBuilder()
    addName(caption, sticker.name)
    addLanguage(caption, sticker.language)
    addNotes(caption, sticker.notes)
    addSource(caption, sticker.source)
    addLinks(sticker, links, caption)
    caption.toString()
  }

  override def getMarkdown(sticker: Sticker, links: Seq[StickerLink]): Try[String] = Try {
    val caption: StringBuilder = new StringBuilder()
    addNameMarkdown(caption, sticker.name)
    addLanguageMarkdown(caption, sticker.language)
    addNotesMarkdown(caption, sticker.notes)
    addLinksMarkdown(caption, sticker, links)
    escapeNonUsedMarkdownChars(caption)
  }

  override def get(draftSticker: DraftSticker): Try[String] = Try {
    val caption = new StringBuilder(s"Id: ${draftSticker.id}")
    addName(caption, draftSticker.name)
    addLanguage(caption, draftSticker.language)
    addDimensions(caption, draftSticker)
    addNotes(caption, draftSticker.notes)
    addSource(caption, draftSticker.source)
    caption.toString()
  }

  private def addDimensions(caption: StringBuilder, draftSticker: DraftSticker): Unit = {
    draftSticker.stickerLinks.toSeq
      .filter(tuple => tuple._2.dimensions.isDefined)
      .foreach(tuple => {
        val (linkType: StickerLinkType, stickerLink: DraftStickerLink) = tuple
        val dimensions: StickerDimensions = stickerLink.dimensions.get
        caption.append(s"\n\n $linkType dimensions: ${dimensions.width} x ${dimensions.height}")
      })
  }

  private def addNameMarkdown(caption: StringBuilder, name: String): StringBuilder = {
    val escapedName: String = escapeUsedMarkdownChars(name)
    caption.append("\"*" + escapedName + "*\"")
  }

  private def addLanguageMarkdown(caption: StringBuilder, language: StickerLanguage): Unit = {
    language.map(languageCode => {
      caption.append(s" _\\(${languageCode.value.toUpperCase}: ${languageCode.native.mkString(", ")}\\)_")
    })
  }

  private def addNotesMarkdown(caption: StringBuilder, notes: StickerNotes): Unit = {
    notes.foreach(note => {
      val escapedNote = escapeUsedMarkdownChars(note)
      caption.append("\n\n\"" + escapedNote + "\"")
      addLanguageMarkdown(caption, Some(LanguageCode.ENGLISH))
    })
  }

  private def addLinksMarkdown(caption: StringBuilder, sticker: Sticker, links: Seq[StickerLink]): StringBuilder = {
    val originalIpfsCID: StickerLinkIpfsCid =
      links.find(stickerLink => stickerLink.`type`.equals(StickerLinkTypes.ORIGINAL)).map(_.ipfsCID).get
    val source: String = getSource(sticker.source)
    val permalink: String = s"[Permalink](${configuration.websiteDomainName}/stickers/${sticker.id.get})"
    val originalLink: String = s"[High resolution](${configuration.ipfsGateway + originalIpfsCID})"
    val vectorLinkOrEmpty: String = links
      .find(stickerLink => stickerLink.`type`.equals(StickerLinkTypes.VECTOR))
      .map(vector => s"[Vector](${configuration.ipfsGateway + vector.ipfsCID}) ")
      .getOrElse("")
    val allLinks: String = List(source, permalink, originalLink, vectorLinkOrEmpty)
      .filter(link => !link.isBlank)
      .mkString(" | ")
    caption.append(s"\n\n$allLinks\n\n")
  }

  private def escapeNonUsedMarkdownChars(caption: StringBuilder): StickerLinkIpfsCid = {
    escapeMarkdownChars(caption.toString(), NON_USED_MARKDOWN_ESCAPING_CHARS)
  }

  private def escapeUsedMarkdownChars(caption: String): StickerLinkIpfsCid = {
    escapeMarkdownChars(caption, USED_MARKDOWN_ESCAPING_CHARS)
  }

  private def escapeMarkdownChars(caption: String, markdownChars: List[Char]): StickerLinkIpfsCid = {
    markdownChars.foldLeft(caption) { case (currentString, char) =>
      currentString.replace(char.toString, f"\\$char")
    }
  }

  private def addName(caption: StringBuilder, name: String): StringBuilder = {
    caption.append("\"" + name + "\"")
  }

  private def addName(caption: StringBuilder, draftStickerName: Option[StickerName]): Unit = {
    draftStickerName.foreach(addName(caption, _))
  }

  private def addLanguage(caption: StringBuilder, language: StickerLanguage): Unit = {
    language.map(languageCode => {
      caption.append(s" (${languageCode.value.toUpperCase}: ${languageCode.native.mkString(", ")})")
    })
  }

  private def addSource(caption: StringBuilder, maybeSource: StickerSource): Unit = {
    val triedSource = maybeSource match {
      case Some(value) => Try(value)
      case None        => Failure(new NoSuchElementException())
    }
    triedSource
      .flatMap(source => sourceService.parse(source))
      .foreach(sourceDto => {
        val sourceAsString = sourceDto.plainLink
        caption.append(s"\n\nSource: $sourceAsString")
      })
  }

  private def addNotes(caption: StringBuilder, notes: StickerNotes): Unit = {
    notes.foreach(notes => {
      caption.append("\n\n\"" + notes + "\"")
      addLanguage(caption, Some(LanguageCode.ENGLISH))
    })
  }

  private def addLinks(sticker: Sticker, links: Seq[StickerLink], caption: StringBuilder): StringBuilder = {
    val originalIpfsCID: StickerLinkIpfsCid =
      links.find(stickerLink => stickerLink.`type`.equals(StickerLinkTypes.ORIGINAL)).map(_.ipfsCID).get
    caption.append(s"\n\nOriginal: ${configuration.ipfsGateway + originalIpfsCID}")

    links
      .find(stickerLink => stickerLink.`type`.equals(StickerLinkTypes.VECTOR))
      .foreach(vector => {
        caption.append(s"\n\nVector: ${configuration.ipfsGateway + vector.ipfsCID}")
      })

    caption.append(s"\n\nPermalink: ${configuration.websiteDomainName}/stickers/${sticker.id.get}")
  }

  private def getSource(maybeSource: Option[String]): String = {
    maybeSource match {
      case None => s""
      case Some(rawSource) =>
        sourceService
          .parse(rawSource)
          .fold(
            _ => s"$rawSource",
            sourceDto => s"[Source](${sourceDto.httpLink})"
          )
    }
  }
}
