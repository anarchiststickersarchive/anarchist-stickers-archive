package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.StickerDirectoryRelation
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerDirectoryRelationDirectoryName}

import scala.util.Try

trait StickerDirectoryRelationsRepository {

  def getAll: Try[Seq[StickerDirectoryRelation]]

  def getByStickerId(stickerId: MaybeStickerId): Try[Option[StickerDirectoryRelation]]

  def getByDirectoryName(
      directoryName: StickerDirectoryRelationDirectoryName
  ): Try[Option[StickerDirectoryRelation]]

  def upsert(stickerDirectoryRelation: StickerDirectoryRelation): Try[Unit]

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit]
}
