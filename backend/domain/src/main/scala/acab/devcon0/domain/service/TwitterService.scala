package acab.devcon0.domain.service

import acab.devcon0.domain.ports.output.TwitterClient
import com.danielasfregola.twitter4s.entities.{MediaDetails, Tweet}

import java.io.File
import scala.util.Try

class TwitterService(twitterClient: TwitterClient) {

  def uploadMediaFromFile(file: File): Try[MediaDetails] = {
    twitterClient.uploadMediaFromFile(file)
  }

  def createTweet(status: String, mediaIds: Seq[Long] = Seq.empty): Try[Tweet] = {
    twitterClient.createTweet(status, mediaIds)
  }
}
