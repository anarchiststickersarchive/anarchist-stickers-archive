package acab.devcon0.domain.ports.input.ingestion

import acab.devcon0.cqrs.{Command, CommandHandler, Event}
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import com.typesafe.scalalogging.{LazyLogging, Logger}

import scala.util.{Failure, Success, Try}

case class RefreshIngestionFilesListCommand() extends Command[DraftStickerId]

sealed abstract class RefreshIngestionFilesListEvent[T] extends Event[T]

case class RefreshIngestionFilesListSuccessEvent() extends RefreshIngestionFilesListEvent[Sticker]

case class RefreshIngestionFilesListErrorEvent(
    throwable: Throwable
) extends RefreshIngestionFilesListEvent[DraftStickerId]

trait RefreshIngestionFilesListCommandHandler
    extends CommandHandler[RefreshIngestionFilesListCommand, DraftStickerId, RefreshIngestionFilesListEvent[_]]

object RefreshIngestionFilesListCommandImplicits {
  implicit class RefreshIngestionFilesListCommandEventFlattenOps(result: Try[RefreshIngestionFilesListEvent[_]])
      extends LazyLogging {
    def flattenEvents: Try[Unit] = {
      result match {
        case Failure(exception)                                      => handleErrorCase(exception, logger)
        case Success(RefreshIngestionFilesListErrorEvent(exception)) => handleErrorCase(exception, logger)
        case Success(RefreshIngestionFilesListSuccessEvent())        => Success()
      }
    }

    private def handleErrorCase(exception: Throwable, logger: Logger): Failure[Unit] = {
      logger.error("DraftStickerAbortEventProcessor", exception)
      Failure(exception)
    }
  }
}
