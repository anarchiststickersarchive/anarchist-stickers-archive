package acab.devcon0.domain.ports.output

import acab.devcon0.domain.dtos.{IpfsAddDirectoryResponse, IpfsAddResponse}

import scala.util.Try

trait IpfsClient {
  def addNoCopy(bytes: Array[Byte], fileName: String, remoteAbsolutePath: String): Try[IpfsAddResponse]
}
