package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.ports.output.FiniteStateMachineRepository
import acab.devcon0.domain.service.TelegramFiniteStateMachine.{TelegramFSMId, TelegramFSMInstance}
import acab.devcon0.fsm.FiniteStateMachineInstance
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

object TelegramFiniteStateMachine {
  type TelegramFSMId = TelegramUserId
  type TelegramFSMInstance = FiniteStateMachineInstance[TelegramFSMId]
}

class TelegramFSMService(repository: FiniteStateMachineRepository[TelegramFSMId]) extends LazyLogging {

  def get(
      id: TelegramFSMId
  ): Try[Option[TelegramFSMInstance]] = {
    repository.get(id)
  }

  def exists(id: TelegramFSMId): Try[Boolean] = {
    get(id)
      .map {
        case Some(_) => true
        case None    => false
      }
  }

  def upsert(instance: TelegramFSMInstance): Try[Option[TelegramFSMInstance]] = {
    repository
      .upsert(instance)
      .flatMap(id => get(id))
  }

  def delete(id: TelegramFSMId): Try[Unit] = {
    repository.delete(id)
  }
}
