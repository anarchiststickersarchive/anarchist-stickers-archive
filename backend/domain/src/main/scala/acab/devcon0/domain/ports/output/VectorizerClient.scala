package acab.devcon0.domain.ports.output

import scala.util.Try

trait VectorizerClient {
  def get(
      desiredWidth: Int,
      desiredHeight: Int,
      model: String,
      colors: String,
      imageInputAbsolutePath: String,
      stickerRasterizeBytes: Array[Byte]
  ): Try[Array[Byte]]
}
