package acab.devcon0.domain.service

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.nio.file.Path
import javax.imageio.ImageIO

trait SquarePhotoHandler {

  def get(photo: Path): Array[Byte] = {
    val image: BufferedImage = ImageIO.read(photo.toFile)
    val paddedImg: BufferedImage = pad(image)
    val byteArrayOutputStream: ByteArrayOutputStream = new ByteArrayOutputStream()
    val extension = photo.getFileName.toString.split("\\.").last
    ImageIO.write(paddedImg, extension, byteArrayOutputStream)
    byteArrayOutputStream.flush()
    byteArrayOutputStream.toByteArray
  }

  private def pad(image: BufferedImage): BufferedImage = {
    val big: Int = Math.max(image.getWidth, image.getHeight)
    val small: Int = Math.min(image.getWidth, image.getHeight)
    val paddingWidth: Int = {
      val isVerticalAlike = image.getHeight > image.getWidth
      if (isVerticalAlike) { (big - small) / 2 }
      else { 0 }
    }
    val paddingHeight: Int = {
      val isHorizontalAlike = image.getWidth > image.getHeight
      if (isHorizontalAlike) { (big - small) / 2 }
      else { 0 }
    }
    val newImage = {
      val finalWidth = image.getWidth + 2 * paddingWidth
      val finalHeight = image.getHeight + 2 * paddingHeight
      new BufferedImage(finalWidth, finalHeight, image.getType)
    }

    val g = newImage.getGraphics

    g.setColor(Color.white)
    g.fillRect(0, 0, image.getWidth + 2 * paddingWidth, image.getHeight + 2 * paddingHeight)
    g.drawImage(image, paddingWidth, paddingHeight, null)
    newImage
  }

}
