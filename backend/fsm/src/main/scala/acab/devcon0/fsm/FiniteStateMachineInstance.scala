package acab.devcon0.fsm

abstract class FiniteStateMachineInstance[ID](
    val id: ID,
    val state: FiniteStateMachineState,
    val data: FiniteStateMachineData
)
