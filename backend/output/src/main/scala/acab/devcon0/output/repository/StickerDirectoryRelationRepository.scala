package acab.devcon0.output.repository

import acab.devcon0.domain.dtos
import acab.devcon0.domain.dtos.StickerDirectoryRelation
import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerDirectoryRelationDirectoryName}
import acab.devcon0.domain.ports.output.StickerDirectoryRelationsRepository
import com.typesafe.scalalogging.LazyLogging
import scalikejdbc.{DB, _}

import scala.util.Try

class StickerDirectoryRelationsRepositoryImpl() extends StickerDirectoryRelationsRepository with LazyLogging {

  private val mapper: StickerDirectoryRelationMapper = new StickerDirectoryRelationMapper()

  override def getAll: Try[Seq[StickerDirectoryRelation]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickerDirectoryRelationsTable.tableName}")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def getByStickerId(stickerId: MaybeStickerId): Try[Option[StickerDirectoryRelation]] = Try {
    DB readOnly { implicit session =>
      SQL(
        s"SELECT * FROM ${StickerDirectoryRelationsTable.tableName} " +
          s"WHERE ${StickerDirectoryRelationsTable.stickerIdColumn}=${stickerId.get}"
      )
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  override def getByDirectoryName(
      directoryName: StickerDirectoryRelationDirectoryName
  ): Try[Option[StickerDirectoryRelation]] = Try {
    DB readOnly { implicit session =>
      SQL(
        s"SELECT * FROM ${StickerDirectoryRelationsTable.tableName} " +
          s"WHERE ${StickerDirectoryRelationsTable.directoryNameColumn}='$directoryName'"
      )
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  override def upsert(stickerDirectoryRelation: StickerDirectoryRelation): Try[Unit] = {
    Try {
      DB localTx { implicit session =>
        val query = upsertQuery(stickerDirectoryRelation)
        SQL(query)
          .update()
          .apply()
      }
    }.map(_ => Some(()))
  }

  override def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit] = Try {
    DB localTx { implicit session =>
      SQL(
        s"DELETE FROM ${StickerDirectoryRelationsTable.tableName} WHERE ${StickerDirectoryRelationsTable.stickerIdColumn}=${stickerId.get}"
      )
        .update()
        .apply()
    }
  }

  private def upsertQuery(stickerDirectoryRelation: StickerDirectoryRelation): String = {
    s"INSERT INTO ${StickerDirectoryRelationsTable.tableName} " +
      s"(" +
      s"${StickerDirectoryRelationsTable.stickerIdColumn}, " +
      s"${StickerDirectoryRelationsTable.directoryNameColumn}) " +
      s"VALUES (" +
      s"${stickerDirectoryRelation.stickerId.get}, " +
      s"'${stickerDirectoryRelation.directoryName}'" +
      s")"
  }

}

object StickerDirectoryRelationsTable {
  val tableName: String = "STICKER_DIRECTORY_RELATIONS"
  val stickerIdColumn: String = "STICKER_ID"
  val directoryNameColumn: String = "DIRECTORY_NAME"
}

class StickerDirectoryRelationMapper() {
  def apply(rs: WrappedResultSet): Try[StickerDirectoryRelation] = Try {
    dtos.StickerDirectoryRelation(
      stickerId = rs.longOpt(StickerDirectoryRelationsTable.stickerIdColumn),
      directoryName = rs.string(StickerDirectoryRelationsTable.directoryNameColumn)
    )
  }
}
