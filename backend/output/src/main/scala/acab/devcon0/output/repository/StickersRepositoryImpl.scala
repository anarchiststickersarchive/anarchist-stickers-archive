package acab.devcon0.output.repository

import acab.devcon0.domain.dtos
import acab.devcon0.domain.dtos.Sticker
import acab.devcon0.domain.dtos.TypeAliases.MaybeStickerId
import acab.devcon0.domain.ports.output.StickersRepository
import com.typesafe.scalalogging.LazyLogging
import io.bartholomews.iso.LanguageCode
import scalikejdbc.{DB, _}

import scala.util.Try

class StickersRepositoryImpl() extends LazyLogging with StickersRepository {

  import StickersTable._
  private val mapper: StickerMapper = new StickerMapper()

  override def getAll: Try[Seq[Sticker]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickersTable.tableName}")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def getAll(page: Int, pageSize: Int, searchTerm: Option[String]): Try[Seq[Sticker]] = Try {
    DB readOnly { implicit session =>
      val optionalSearchTermPartialQuery = searchTerm
        .map(term => s"WHERE MATCH (NAME, NOTES) AGAINST ('$term' IN NATURAL LANGUAGE MODE) ")
        .getOrElse("")
      SQL(
        s"" +
          s"SELECT * FROM ${StickersTable.tableName} " +
          optionalSearchTermPartialQuery +
          s"ORDER BY ${StickersTable.idColumn} ASC " +
          s"LIMIT $pageSize " +
          s"OFFSET ${page * pageSize}"
      )
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def getById(id: MaybeStickerId): Try[Option[Sticker]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickersTable.tableName} WHERE ${StickersTable.idColumn}=${id.get}")
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  override def upsert(sticker: Sticker): Try[MaybeStickerId] = {
    Try {
      DB localTx { implicit session =>
        SQL(upsertQuery(sticker))
          .updateAndReturnGeneratedKey(StickersTable.idColumn)
          .apply()
      }
    }.map(stickerId => Some(stickerId))
  }

  override def delete(stickerId: MaybeStickerId): Try[Unit] = Try {
    DB localTx { implicit session =>
      SQL(s"DELETE FROM ${StickersTable.tableName} WHERE ${StickersTable.idColumn}=${stickerId.get}")
        .update()
        .apply()
    }
  }

  override def search(searchTerm: String): Try[Seq[Sticker]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM STICKERS WHERE MATCH (NAME, NOTES) AGAINST ('$searchTerm' IN NATURAL LANGUAGE MODE);")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def getRandomNotPublishedAnywhere: Try[Option[Sticker]] = Try {
    DB readOnly { implicit session =>
      val query = s"" +
        s"SELECT * FROM ${StickersTable.tableName} " +
        s"WHERE ${StickersTable.idColumn} NOT IN (SELECT ${PublicationsTable.stickerIdColumn} FROM ${PublicationsTable.tableName}) " +
        s"ORDER BY RAND() " +
        s"LIMIT 1 "
      SQL(query)
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  // Stickers that are not published at no platform
  override def getUnpublished: Try[Seq[Sticker]] = Try {
    DB readOnly { implicit session =>
      val query = s"" +
        s"SELECT * FROM ${StickersTable.tableName} " +
        s"WHERE ${StickersTable.idColumn} NOT IN (SELECT ${PublicationsTable.stickerIdColumn} FROM ${PublicationsTable.tableName}) "
      SQL(query)
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def countAll: Try[Option[Long]] = Try {
    DB readOnly { implicit session =>
      val query = s"SELECT COUNT(*) AS COUNT FROM ${StickersTable.tableName} "
      SQL(query)
        .map(rs => rs.long("COUNT"))
        .toOption()
        .apply()
    }
  }

  override def countUnpublished: Try[Option[Long]] = Try {
    DB readOnly { implicit session =>
      val query = s"" +
        s"SELECT COUNT(*) AS COUNT FROM ${StickersTable.tableName} " +
        s"WHERE ${StickersTable.idColumn} NOT IN (SELECT DISTINCT ${PublicationsTable.stickerIdColumn} FROM ${PublicationsTable.tableName}) "
      SQL(query)
        .map(rs => rs.long("COUNT"))
        .toOption()
        .apply()
    }
  }

  /** Funky syntax, quite obvious.
    * Reason? scalike, when doing interpolation (trying to avoid SQL injection) will interpolate table/columns names as well.
    * Only solution given is enabling SQL injection: https://github.com/scalikejdbc/scalikejdbc/issues/320
    * So, time to manually avoid the injection. Therefore, this ugly code.
    */
  private def upsertQuery(sticker: Sticker): String = {
    val safeIdValue = sticker.id.map(_.toString).getOrElse("null")
    val safeName = "\"" + sticker.name + "\""
    val safeNotes = sticker.notes.map(notes => "\"" + notes + "\"").orNull
    val safeSource = sticker.source.map(source => "\"" + source + "\"").orNull
    val safeLanguage = sticker.language.map(languageCode => "\"" + languageCode.value + "\"").orNull
    val creationTimestamp = "\"" + sticker.creationTimestamp.toString + "\""
    s"INSERT INTO $tableName ( $idColumn, $nameColumn, $notesColumn, $sourceColumn,$languageColumn, $creationTimestampColumn) " +
      s"VALUES " +
      s"( $safeIdValue, $safeName, $safeNotes, $safeSource, $safeLanguage, $creationTimestamp) " +
      s"ON DUPLICATE KEY UPDATE $nameColumn=$safeName, $notesColumn=$safeNotes, $sourceColumn=$safeSource, $languageColumn=$safeLanguage"
  }
}

object StickersTable {
  val tableName: String = "STICKERS"
  val idColumn: String = "ID"
  val nameColumn: String = "NAME"
  val notesColumn: String = "NOTES"
  val sourceColumn: String = "SOURCE"
  val languageColumn: String = "LANGUAGE"
  val creationTimestampColumn: String = "CREATION_TIMESTAMP"
}

class StickerMapper() {
  def apply(rs: WrappedResultSet): Try[Sticker] = Try {
    dtos.Sticker(
      id = rs.longOpt(StickersTable.idColumn),
      name = rs.string(StickersTable.nameColumn),
      notes = rs.stringOpt(StickersTable.notesColumn),
      source = rs.stringOpt(StickersTable.sourceColumn),
      language = LanguageCode.withValueOpt(rs.string(StickersTable.languageColumn)),
      creationTimestamp = rs.localDateTime(StickersTable.creationTimestampColumn)
    )
  }
}
