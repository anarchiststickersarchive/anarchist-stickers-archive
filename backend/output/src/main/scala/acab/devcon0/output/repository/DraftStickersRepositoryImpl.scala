package acab.devcon0.output.repository

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.dtos.DraftSticker
import acab.devcon0.domain.dtos.TypeAliases.DraftStickerId
import acab.devcon0.domain.ports.output.DraftStickersRepository
import acab.devcon0.domain.codecs.CirceCodecs.Decoders.draftStickerDecoder
import acab.devcon0.domain.codecs.CirceCodecs.Encoders.draftStickerEncoder
import com.redis.{RedisClient, RedisClientPool}
import com.typesafe.scalalogging.LazyLogging
import io.circe.jawn.decode
import io.circe.syntax._

import scala.util.{Success, Try}

class DraftStickersRepositoryImpl(val redisClientPool: RedisClientPool)
    extends DraftStickersRepository
    with LazyLogging {

  def getById(id: DraftStickerId): Try[Option[DraftSticker]] = {
    redisClientPool.withClient(redisClient => {
      redisClient
        .get(id) match {
        case Some(jsonString) => decode[DraftSticker](jsonString).toTry.map(Some(_))
        case None             => Success(None)
      }
    })
  }

  def upsert(draftSticker: DraftSticker): Try[DraftStickerId] = {
    Try {
      redisClientPool.withClient(redisClient => {
        val persistingString = EncoderOps[DraftSticker](draftSticker).asJson.noSpaces
        redisClient.set(draftSticker.id, persistingString)
      })
    }.map(_ => draftSticker.id)
  }

  def delete(id: DraftStickerId): Try[Unit] = {
    Try {
      redisClientPool.withClient(redisClient => {
        redisClient.del(id)
      })
    }
      .map(TryUtils.forceOptionExistence)
      .map(_ => ())
  }
}
