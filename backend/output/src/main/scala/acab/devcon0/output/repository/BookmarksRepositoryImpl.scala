package acab.devcon0.output.repository

import acab.devcon0.domain.dtos.{Bookmark, BookmarkId, BookmarkUserId, BookmarkUserPlatforms}
import acab.devcon0.domain.ports.output.BookmarksRepository
import com.typesafe.scalalogging.LazyLogging
import scalikejdbc.{DB, _}

import scala.util.Try

class BookmarksRepositoryImpl() extends BookmarksRepository with LazyLogging {

  private val mapper: BookmarkMapper = new BookmarkMapper()

  override def getByUser(bookmarkUserId: BookmarkUserId): Try[Seq[Bookmark]] = Try {
    DB readOnly { implicit session =>
      SQL(
        s"" +
          s"SELECT * FROM ${BookmarksTable.tableName} " +
          s"WHERE " +
          s"${BookmarksTable.userIdColumn}='${bookmarkUserId.id}' AND " +
          s"${BookmarksTable.userPlatformColumn}='${bookmarkUserId.platform.toString}' "
      )
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def upsert(bookmark: Bookmark): Try[Bookmark] = {
    Try {
      DB localTx { implicit session =>
        val query = upsertQuery(bookmark)
        SQL(query)
          .update()
          .apply()
      }
    }.map(_ => bookmark)
  }

  override def delete(bookmarkId: BookmarkId): Try[Unit] =
    Try {
      DB localTx { implicit session =>
        SQL(
          s"DELETE FROM ${BookmarksTable.tableName} " +
            s"WHERE " +
            s"${BookmarksTable.stickerIdColumn}=${bookmarkId.stickerId} AND " +
            s"${BookmarksTable.userIdColumn}='${bookmarkId.userId.id}' AND " +
            s"${BookmarksTable.userPlatformColumn}='${bookmarkId.userId.platform.toString}' "
        )
          .update()
          .apply()
      }
    }

  private def upsertQuery(bookmark: Bookmark): String = {
    s"INSERT IGNORE INTO ${BookmarksTable.tableName} " +
      s"(" +
      s"${BookmarksTable.stickerIdColumn}, " +
      s"${BookmarksTable.userIdColumn}, " +
      s"${BookmarksTable.userPlatformColumn}, " +
      s"${BookmarksTable.creationTimestampColumn}) " +
      s"VALUES (" +
      s"${bookmark.id.stickerId}, " +
      s"'${bookmark.id.userId.id}'," +
      s"'${bookmark.id.userId.platform}'," +
      s"'${bookmark.creationTimestamp}'" +
      s")" +
      s"ON DUPLICATE KEY UPDATE ${BookmarksTable.stickerIdColumn}=${bookmark.id.stickerId}"
  }
}

object BookmarksTable {
  val tableName: String = "BOOKMARKS"
  val stickerIdColumn: String = "STICKER_ID"
  val userIdColumn: String = "USER_ID"
  val userPlatformColumn: String = "USER_PLATFORM"
  val creationTimestampColumn: String = "CREATION_TIMESTAMP"
}

class BookmarkMapper() {
  def apply(rs: WrappedResultSet): Try[Bookmark] = Try {
    val stickerId = rs.long(BookmarksTable.stickerIdColumn)
    val userId = rs.string(BookmarksTable.userIdColumn)
    val userPlatform = BookmarkUserPlatforms.withName(rs.string(BookmarksTable.userPlatformColumn))
    Bookmark(
      id = BookmarkId(stickerId, BookmarkUserId(userId, userPlatform)),
      creationTimestamp = rs.localDateTime(BookmarksTable.creationTimestampColumn)
    )
  }
}
