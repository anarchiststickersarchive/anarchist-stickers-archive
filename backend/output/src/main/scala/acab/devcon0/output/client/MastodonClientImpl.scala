package acab.devcon0.output.client

import acab.devcon0.configuration.MastodonConfiguration
import acab.devcon0.domain.codecs.CirceCodecs.Decoders.{
  mastodonAddMediaResponseDecoder,
  mastodonPostStatusResponseDecoder
}
import acab.devcon0.domain.codecs.CirceCodecs.Encoders.mastodonPostStatusRequestEncoder
import acab.devcon0.domain.dtos.{MastodonAddMediaResponse, MastodonPostStatusRequest, MastodonPostStatusResponse}
import acab.devcon0.domain.ports.output.MastodonClient
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import sttp.client3.{Identity, Response, SttpBackend, UriContext, basicRequest, multipartFile}

import java.io.File
import scala.util.{Failure, Try}

class MastodonClientImpl(configuration: MastodonConfiguration, backend: SttpBackend[Identity, Any])
    extends MastodonClient {

  override def addMedia(file: File): Try[MastodonAddMediaResponse] = {
    Try {
      basicRequest
        .multipartBody(multipartFile("file", file.toPath))
        .header("Authorization", s"Bearer ${configuration.token}")
        .post(uri"${configuration.url}/api/v2/media")
        .send(backend)
    }.flatMap(decodeAddMedia)
  }

  override def createPost(status: String, mediaId: String): Try[MastodonPostStatusResponse] = {
    Try {
      val requestBody: MastodonPostStatusRequest = MastodonPostStatusRequest(status, Seq(mediaId))
      val requestBodyStr = requestBody.asJson.noSpaces
      basicRequest
        .header("Authorization", s"Bearer ${configuration.token}")
        .contentType("application/json")
        .post(uri"${configuration.url}/api/v1/statuses")
        .body(requestBodyStr)
        .send(backend)
    }.flatMap(decodePostStatus)
  }

  private def decodeAddMedia(response: Identity[Response[Either[String, String]]]): Try[MastodonAddMediaResponse] =
    Try {
      response.body match {
        case Left(value) => Failure(new Throwable(value))
        case Right(body) => decode[MastodonAddMediaResponse](body).toTry
      }
    }.flatten

  private def decodePostStatus(response: Identity[Response[Either[String, String]]]): Try[MastodonPostStatusResponse] =
    Try {
      response.body match {
        case Left(value) => Failure(new Throwable(value))
        case Right(body) => decode[MastodonPostStatusResponse](body).toTry
      }
    }.flatten

}
