package acab.devcon0.output.client

import acab.devcon0.commons.{FunctionUtils, TryUtils}
import acab.devcon0.domain.dtos.TelegramUserId
import acab.devcon0.domain.ports.output.TelegramClient
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.methods.ParseMode.ParseMode
import com.bot4s.telegram.methods.{GetFile, Request, SendMessage, SendPhoto}
import com.bot4s.telegram.models.{File, InputFile}
import com.typesafe.scalalogging.LazyLogging

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Try

class TelegramClientImpl(val client: RequestHandler[Future], val telegramToken: String)
    extends TelegramClient
    with LazyLogging {

  override def sendMessage(to: TelegramUserId, text: String): Try[Unit] = {
    val requestDto: SendMessage = SendMessage(to.id, text)
    request(requestDto)
  }

  override def sendPhoto(
      to: TelegramUserId,
      text: String,
      inputFile: InputFile,
      parseMode: Option[ParseMode]
  ): Try[Unit] = {
    val requestDto: SendPhoto =
      SendPhoto(chatId = to.id, photo = inputFile, caption = Some(text), parseMode = parseMode)
    request(requestDto)
  }

  override def getFile(fileId: String): Try[File] = {
    val requestDto: GetFile = GetFile(fileId)
    Try(Await.result(client.apply(requestDto), Duration(100, TimeUnit.SECONDS)))
  }

  override def getFileBytes(path: String): Try[Array[Byte]] = Try {
    scalaj.http.Http(s"https://api.telegram.org/file/bot$telegramToken/$path").asBytes.body
  }

  private def request[T](request: Request[_]): Try[Unit] = {
    Try(
      Await.result(
        client.apply(request),
        Duration(100, TimeUnit.SECONDS)
      )
    ).map(FunctionUtils.flushResult)
      .transform(TryUtils.doNothing(), TryUtils.peekException(logger))
  }
}
