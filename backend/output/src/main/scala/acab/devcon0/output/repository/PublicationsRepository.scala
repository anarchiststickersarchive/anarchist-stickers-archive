package acab.devcon0.output.repository

import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, PublicationId}
import acab.devcon0.domain.dtos.{Publication, PublicationPlatforms}
import acab.devcon0.domain.ports.output.PublicationsRepository
import com.typesafe.scalalogging.LazyLogging
import scalikejdbc.{DB, _}

import scala.util.Try

class PublicationsRepositoryImpl() extends PublicationsRepository with LazyLogging {

  private val mapper: PublicationMapper = new PublicationMapper()

  override def getAll: Try[Seq[Publication]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${PublicationsTable.tableName}")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def getById(id: PublicationId): Try[Option[Publication]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${PublicationsTable.tableName} WHERE ${PublicationsTable.idColumn}=${id.get}")
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  override def upsert(publication: Publication): Try[PublicationId] = {
    Try {
      DB localTx { implicit session =>
        val query = upsertQuery(publication)
        SQL(query)
          .updateAndReturnGeneratedKey(PublicationsTable.idColumn)
          .apply()
      }
    }.map(publicationId => Some(publicationId))
  }

  override def delete(publicationId: PublicationId): Try[Unit] = Try {
    DB localTx { implicit session =>
      SQL(
        s"DELETE FROM ${PublicationsTable.tableName} WHERE ${PublicationsTable.stickerIdColumn}=${publicationId.get}"
      )
        .update()
        .apply()
    }
  }

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit] = Try {
    DB localTx { implicit session =>
      SQL(s"DELETE FROM ${PublicationsTable.tableName} WHERE ${PublicationsTable.stickerIdColumn}=${stickerId.get}")
        .update()
        .apply()
    }
  }

  private def upsertQuery(publication: Publication): String = {
    s"INSERT INTO ${PublicationsTable.tableName} " +
      s"(" +
      publication.id.map(_ => s"${PublicationsTable.idColumn}, ").getOrElse("") +
      s"${PublicationsTable.stickerIdColumn}, " +
      s"${PublicationsTable.platformColumn}, " +
      s"${PublicationsTable.creationTimestampColumn}) " +
      s"VALUES (" +
      publication.id.map(id => s"$id, ").getOrElse("") +
      s"${publication.stickerId.get}, " +
      s"'${publication.platform}'," +
      s"'${publication.creationTimestamp}'" +
      s")"
  }
}

object PublicationsTable {
  val tableName: String = "PUBLICATIONS"
  val idColumn: String = "ID"
  val stickerIdColumn: String = "STICKER_ID"
  val platformColumn: String = "PLATFORM"
  val creationTimestampColumn: String = "CREATION_TIMESTAMP"
}

class PublicationMapper() {
  def apply(rs: WrappedResultSet): Try[Publication] = Try {
    Publication(
      id = rs.longOpt(PublicationsTable.idColumn),
      stickerId = rs.longOpt(PublicationsTable.stickerIdColumn),
      platform = PublicationPlatforms.withName(rs.string(PublicationsTable.platformColumn)),
      creationTimestamp = rs.localDateTime(PublicationsTable.creationTimestampColumn)
    )
  }
}
