package acab.devcon0.output.repository

import acab.devcon0.commons.TryUtils
import acab.devcon0.domain.codecs.CirceCodecs.Decoders.telegramFSMInstanceDecoder
import acab.devcon0.domain.codecs.CirceCodecs.Encoders.telegramFSMInstanceEncoder
import acab.devcon0.domain.ports.output.FiniteStateMachineRepository
import acab.devcon0.domain.service.TelegramFiniteStateMachine.{TelegramFSMId, TelegramFSMInstance}
import com.redis.RedisClientPool
import io.circe.jawn.decode
import io.circe.syntax._

import scala.util.{Success, Try}

class TelegramFSMRepository(val redisClientPool: RedisClientPool) extends FiniteStateMachineRepository[TelegramFSMId] {

  def get(id: TelegramFSMId): Try[Option[TelegramFSMInstance]] = {
    redisClientPool.withClient(redisClient => {
      redisClient.get(id) match {
        case Some(jsonString) =>
          val decodedValue: Try[TelegramFSMInstance] = decode[TelegramFSMInstance](jsonString).toTry
          decodedValue.map(Some(_))
        case None => Success(None)
      }
    })
  }

  def upsert(instance: TelegramFSMInstance): Try[TelegramFSMId] = {
    Try {
      val persistingString = EncoderOps[TelegramFSMInstance](instance).asJson.noSpaces
      redisClientPool.withClient(redisClient => {
        redisClient.set(instance.id, persistingString)
      })
    }.map(_ => instance.id)
  }

  def delete(id: TelegramFSMId): Try[Unit] = {
    Try {
      redisClientPool.withClient(redisClient => {
        redisClient.del(id)
      })
    }.map(TryUtils.forceOptionExistence)
      .map(_ => ())
  }
}
