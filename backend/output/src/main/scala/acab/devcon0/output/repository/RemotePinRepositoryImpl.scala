package acab.devcon0.output.repository

import acab.devcon0.domain.ports.output.RemotePinRepository
import com.redis.RedisClient
import com.typesafe.scalalogging.LazyLogging

import scala.util.Try

class RemotePinRepositoryImpl(val redisClient: RedisClient) extends RemotePinRepository with LazyLogging {

  private val key: String = "LATEST_PINNED_HASH"

  override def get: Try[Option[String]] = Try {
    redisClient.get(key)
  }

  override def upsert(cidHash: String): Try[Unit] = Try {
    redisClient.set(key, cidHash)
  }
}
