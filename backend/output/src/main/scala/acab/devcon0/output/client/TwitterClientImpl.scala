package acab.devcon0.output.client

import acab.devcon0.domain.ports.output.TwitterClient
import com.danielasfregola.twitter4s.TwitterRestClient
import com.danielasfregola.twitter4s.entities.{MediaDetails, Tweet}

import java.io.File
import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Try

class TwitterClientImpl(twitterRestClient: TwitterRestClient) extends TwitterClient {
  override def uploadMediaFromFile(file: File): Try[MediaDetails] = {
    Try(Await.result(twitterRestClient.uploadMediaFromFile(file), Duration(100, TimeUnit.SECONDS)))
  }
  override def createTweet(status: String, mediaIds: Seq[Long] = Seq.empty): Try[Tweet] = {
    Try(
      Await.result(
        twitterRestClient.createTweet(status = status, media_ids = mediaIds),
        Duration(100, TimeUnit.SECONDS)
      )
    )
  }
}
