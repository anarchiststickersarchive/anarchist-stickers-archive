package acab.devcon0.output.repository

import acab.devcon0.domain.dtos
import acab.devcon0.domain.dtos.Tag
import acab.devcon0.domain.dtos.TypeAliases.TagId
import com.typesafe.scalalogging.LazyLogging
import scalikejdbc.{DB, _}

import scala.util.Try

class TagsRepository() extends LazyLogging {

  private val mapper: TagMapper = new TagMapper()

  def getAll: Try[Seq[Tag]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${TagsTable.tableName}")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  def getById(id: TagId): Try[Option[Tag]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${TagsTable.tableName} WHERE ID=$id")
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }
}

object TagsTable {
  val tableName: String = "TAGS"
  val idColumn: String = "ID"
  val stickerIdColumn: String = "STICKER_ID"
  val valueColumn: String = "VALUE"
  val creationTimestampColumn: String = "CREATION_TIMESTAMP"
}

class TagMapper() {
  def apply(rs: WrappedResultSet): Try[Tag] = Try {
    dtos.Tag(
      id = rs.longOpt(TagsTable.idColumn),
      stickerId = rs.longOpt(TagsTable.stickerIdColumn),
      value = rs.string(TagsTable.valueColumn),
      creationTimestamp = rs.localDateTime(TagsTable.creationTimestampColumn)
    )
  }
}
