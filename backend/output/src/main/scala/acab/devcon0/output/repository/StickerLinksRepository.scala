package acab.devcon0.output.repository

import acab.devcon0.domain.dtos.TypeAliases.{MaybeStickerId, StickerLinkId, StickerLinkIpfsCid}
import acab.devcon0.domain.dtos.{StickerLink, StickerLinkTypes}
import acab.devcon0.domain.ports.output.StickerLinksRepository
import com.typesafe.scalalogging.LazyLogging
import scalikejdbc.{DB, _}

import scala.util.Try

class StickerLinksRepositoryImpl() extends StickerLinksRepository with LazyLogging {

  private val mapper: StickerLinkMapper = new StickerLinkMapper()

  def getAll: Try[Seq[StickerLink]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickerLinksTable.tableName}")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  def getById(id: StickerLinkId): Try[Option[StickerLink]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickerLinksTable.tableName} WHERE ${StickerLinksTable.idColumn}=${id.get}")
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  def getByStickerId(stickerId: MaybeStickerId): Try[Seq[StickerLink]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickerLinksTable.tableName} WHERE ${StickerLinksTable.stickerIdColumn}=${stickerId.get}")
        .map(rs => mapper.apply(rs).get)
        .toList()
        .apply()
    }
  }

  override def getByIpfsCID(ipfsCid: StickerLinkIpfsCid): Try[Option[StickerLink]] = Try {
    DB readOnly { implicit session =>
      SQL(s"SELECT * FROM ${StickerLinksTable.tableName} WHERE ${StickerLinksTable.ipfsCidColumn}='${ipfsCid}'")
        .map(rs => mapper.apply(rs).get)
        .toOption()
        .apply()
    }
  }

  def upsert(stickerLink: StickerLink): Try[StickerLinkId] = {
    Try {
      DB localTx { implicit session =>
        val query = upsertQuery(stickerLink)
        SQL(query)
          .updateAndReturnGeneratedKey(StickerLinksTable.idColumn)
          .apply()
      }
    }.map(stickerLinkId => Some(stickerLinkId))
  }

  def delete(stickerLinkId: StickerLinkId): Try[Unit] = Try {
    DB localTx { implicit session =>
      SQL(s"DELETE FROM ${StickerLinksTable.tableName} WHERE ${StickerLinksTable.idColumn}=${stickerLinkId.get}")
        .update()
        .apply()
    }
  }

  def deleteByStickerId(stickerId: MaybeStickerId): Try[Unit] = Try {
    DB localTx { implicit session =>
      SQL(s"DELETE FROM ${StickerLinksTable.tableName} WHERE ${StickerLinksTable.stickerIdColumn}=${stickerId.get}")
        .update()
        .apply()
    }
  }

  private def upsertQuery(stickerLink: StickerLink): String = {
    s"INSERT INTO ${StickerLinksTable.tableName} " +
      s"(" +
      stickerLink.id.map(_ => s"${StickerLinksTable.idColumn}, ").getOrElse("") +
      s"${StickerLinksTable.stickerIdColumn}, " +
      s"${StickerLinksTable.filenameColumn}, " +
      s"${StickerLinksTable.ipfsCidColumn}, " +
      s"${StickerLinksTable.typeColumn}, " +
      s"${StickerLinksTable.creationTimestampColumn}) " +
      s"VALUES (" +
      stickerLink.id.map(id => s"$id, ").getOrElse("") +
      s"${stickerLink.stickerId.get}, " +
      s"'${stickerLink.filename}', " +
      s"'${stickerLink.ipfsCID}', " +
      s"'${stickerLink.`type`}', " +
      s"'${stickerLink.creationTimestamp}'" +
      s") ON DUPLICATE KEY UPDATE ${StickerLinksTable.filenameColumn}='${stickerLink.filename}', ${StickerLinksTable.ipfsCidColumn}='${stickerLink.ipfsCID}'"
  }
}

object StickerLinksTable {
  val tableName: String = "STICKER_LINKS"
  val idColumn: String = "ID"
  val stickerIdColumn: String = "STICKER_ID"
  val filenameColumn: String = "FILENAME"
  val ipfsCidColumn: String = "IPFS_CID"
  val typeColumn: String = "TYPE"
  val creationTimestampColumn: String = "CREATION_TIMESTAMP"
}

class StickerLinkMapper() {
  def apply(rs: WrappedResultSet): Try[StickerLink] = Try {
    StickerLink(
      id = rs.longOpt(StickerLinksTable.idColumn),
      stickerId = rs.longOpt(StickerLinksTable.stickerIdColumn),
      filename = rs.string(StickerLinksTable.filenameColumn),
      ipfsCID = rs.string(StickerLinksTable.ipfsCidColumn),
      `type` = StickerLinkTypes.withName(rs.string(StickerLinksTable.typeColumn)),
      creationTimestamp = rs.localDateTime(StickerLinksTable.creationTimestampColumn)
    )
  }
}
