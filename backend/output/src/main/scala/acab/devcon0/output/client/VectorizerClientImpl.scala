package acab.devcon0.output.client

import acab.devcon0.domain.ports.output.VectorizerClient
import com.typesafe.scalalogging.LazyLogging
import sttp.client3._
import sttp.model.Part

import scala.util.Try

class VectorizerClientImpl(
    apiKey: String,
    backend: SttpBackend[Identity, Any]
) extends VectorizerClient
    with LazyLogging {

  override def get(
      desiredWidth: Int,
      desiredHeight: Int,
      model: String,
      colors: String,
      imageInputAbsolutePath: String,
      stickerRasterizeBytes: Array[Byte]
  ): Try[Array[Byte]] = Try {
    val metadata = Map(
      "format" -> "svg",
      "colors" -> colors,
      "model" -> model,
      "algorithm" -> "auto",
      "details" -> "auto",
      "antialiasing" -> "off",
      "minarea" -> "5",
      "colormergefactor" -> "5",
      "unit" -> "auto",
      "width" -> desiredWidth.toString,
      "height" -> desiredHeight.toString,
      "roundness" -> "default"
    )

    val fileMultiPart = multipart("image", stickerRasterizeBytes)
      .contentType("application/octet-stream")
      .fileName(imageInputAbsolutePath)

    val multipartParts: Seq[Part[BasicRequestBody]] = Seq(
      fileMultiPart
    ) ++ metadata.map { case (key, value) =>
      multipart(key, value)
    }

    val request = basicRequest
      .post(uri"https://api.vectorizer.io/v4.0/vectorize")
      .header("X-CREDITS-CODE", apiKey)
      .multipartBody(
        multipartParts.head,
        multipartParts.tail: _*
      )

    val response: Identity[Response[Either[String, String]]] = request.send(backend)

    backend.close()

    response.body match {
      case Right(bytes) => bytes.getBytes
      case Left(error)  =>
        // Handle the error here
        throw new Exception(s"Failed request. Error: $error")
    }
  }
}
