package acab.devcon0.output.client

import acab.devcon0.configuration.PixelfedConfiguration
import acab.devcon0.domain.codecs.CirceCodecs.Decoders.{
  pixelfedAddMediaResponseDecoder,
  pixelfedPostStatusResponseDecoder
}
import acab.devcon0.domain.codecs.CirceCodecs.Encoders.pixelfedPostStatusRequestEncoder
import acab.devcon0.domain.dtos.{PixelfedAddMediaResponse, PixelfedPostStatusRequest, PixelfedPostStatusResponse}
import acab.devcon0.domain.ports.output.PixelfedClient
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import sttp.client3.{Identity, Response, SttpBackend, UriContext, basicRequest, multipartFile}

import java.io.File
import scala.util.{Failure, Try}

class PixelfedClientImpl(configuration: PixelfedConfiguration, backend: SttpBackend[Identity, Any])
    extends PixelfedClient {

  override def addMedia(file: File): Try[PixelfedAddMediaResponse] = {
    Try {
      basicRequest
        .multipartBody(multipartFile("file", file.toPath))
        .header("Authorization", s"Bearer ${configuration.token}")
        .post(uri"${configuration.url}/api/v2/media")
        .send(backend)
    }.flatMap(decodeAddMedia)
  }

  private def decodeAddMedia(response: Identity[Response[Either[String, String]]]): Try[PixelfedAddMediaResponse] =
    Try {
      response.body match {
        case Left(value) => Failure(new Throwable(value))
        case Right(body) => decode[PixelfedAddMediaResponse](body).toTry
      }
    }.flatten

  override def createPost(status: String, mediaId: String): Try[PixelfedPostStatusResponse] = {
    Try {
      val requestBody: PixelfedPostStatusRequest = PixelfedPostStatusRequest(status, Seq(mediaId))
      val requestBodyStr = requestBody.asJson.noSpaces
      basicRequest
        .header("Authorization", s"Bearer ${configuration.token}")
        .contentType("application/json")
        .post(uri"${configuration.url}/api/v1/statuses")
        .body(requestBodyStr)
        .send(backend)
    }.flatMap(decodePostStatus)
  }

  private def decodePostStatus(response: Identity[Response[Either[String, String]]]): Try[PixelfedPostStatusResponse] =
    Try {
      response.body match {
        case Left(value) => Failure(new Throwable(value))
        case Right(body) => decode[PixelfedPostStatusResponse](body).toTry
      }
    }.flatten

}
