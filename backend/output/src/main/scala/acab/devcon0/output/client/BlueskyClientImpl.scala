package acab.devcon0.output.client

import acab.devcon0.configuration.BlueskyConfiguration
import acab.devcon0.domain.codecs.CirceCodecs.Decoders.{
  blueskyAddMediaResponseDecoder,
  blueskyLoginResponseDecoder,
  blueskyPostStatusResponseDecoder
}
import acab.devcon0.domain.codecs.CirceCodecs.Encoders.{blueskyCreatePostRequestEncoder, blueskyLoginRequestEncoder}
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.BlueskyClient
import akka.http.javadsl.model.MediaTypes
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import sttp.client3.{Identity, Response, SttpBackend, UriContext, basicRequest}

import java.io.File
import java.nio.file.Files
import scala.util.{Failure, Try}

class BlueskyClientImpl(configuration: BlueskyConfiguration, backend: SttpBackend[Identity, Any])
    extends BlueskyClient {

  override def addMedia(file: File)(implicit login: BlueskyLoginResponse): Try[BlueskyAddMediaResponse] = {
    Try {
      val fileBytes = Files.readAllBytes(file.toPath)
      basicRequest
        .header("Authorization", s"Bearer ${login.accessJwt}")
        .contentType("image/jpeg")
        .post(uri"${configuration.url}/xrpc/com.atproto.repo.uploadBlob")
        .body(fileBytes)
        .send(backend)
    }.flatMap(response => {
      decodeAddMedia(response)
    })
  }
  override def createPost(text: String, blob: BlueskyBlob)(implicit
      login: BlueskyLoginResponse
  ): Try[BlueskyPostStatusResponse] = {
    Try {
      val postRequest = BlueskyCreatePostRequest(
        repo = login.did,
        collection = "app.bsky.feed.post",
        record = BlueskyRecord(
          text = text,
          createdAt = java.time.Instant.now.toString,
          embed = BlueskyEmbed(
            `$type` = "app.bsky.embed.images",
            images = List(
              BlueskyEmbedImage(
                image = blob,
                alt = ""
              )
            )
          )
        )
      )

      basicRequest
        .header("Authorization", s"Bearer ${login.accessJwt}")
        .contentType(MediaTypes.APPLICATION_JSON.toContentType.toString)
        .post(uri"${configuration.url}/xrpc/com.atproto.repo.createRecord")
        .body(postRequest.asJson.noSpaces)
        .send(backend)
    }.flatMap(response => {
      decodeCreatePost(response)
    })
  }

  override def login(): Try[BlueskyLoginResponse] = {
    val loginRequest = BlueskyLoginRequest(configuration.username, configuration.password)
    val response: Identity[Response[Either[String, String]]] = basicRequest
      .contentType(MediaTypes.APPLICATION_JSON.toContentType.toString)
      .post(uri"${configuration.url}/xrpc/com.atproto.server.createSession")
      .body(loginRequest.asJson.noSpaces)
      .send(backend)

    response.body match {
      case Right(body) => decode[BlueskyLoginResponse](body).toTry
      case Left(error) => Failure(new Exception(s"Bluesky login failed: $error"))
    }
  }

  private def decodeAddMedia(response: Response[Either[String, String]]): Try[BlueskyAddMediaResponse] =
    Try {
      response.body match {
        case Left(error) => Failure(new Exception(error))
        case Right(body) => decode[BlueskyAddMediaResponse](body).toTry
      }
    }.flatten

  private def decodeCreatePost(response: Response[Either[String, String]]): Try[BlueskyPostStatusResponse] =
    Try {
      response.body match {
        case Left(error) => Failure(new Exception(error))
        case Right(body) => decode[BlueskyPostStatusResponse](body).toTry
      }
    }.flatten
}
