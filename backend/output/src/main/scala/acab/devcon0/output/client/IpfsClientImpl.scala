package acab.devcon0.output.client

import acab.devcon0.configuration.Configuration
import acab.devcon0.domain.codecs.CirceCodecs.Decoders.ipfsAddResponseDecoder
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.IpfsClient
import com.typesafe.scalalogging.LazyLogging
import io.circe.jawn.decode
import sttp.client3.{Identity, Response, SttpBackend, UriContext, basicRequest, multipart}
import sttp.model.Uri

import scala.language.postfixOps
import scala.util.{Failure, Try}

class IpfsClientImpl(
    configuration: Configuration,
    backend: SttpBackend[Identity, Any]
) extends IpfsClient
    with LazyLogging {

  private val ADD_URI: Uri = uri"${configuration.ipfs.apiUrl}/add?nocopy=true&pin=true&cid-version=1"
  private val FILE_KEY: String = "file"
  private val FILENAME_KEY: String = "filename"
  private val ABSOLUTE_PATH_HEADER_KEY: String = "Abspath"

  /** Tip: the Abspath header is given to the multipart, not to the root request. */
  override def addNoCopy(
      bytes: Array[Byte],
      fileName: String,
      remoteAbsolutePath: String
  ): Try[IpfsAddResponse] = {
    requestAdd(bytes, fileName, remoteAbsolutePath)
      .flatMap(decodeAddResponse)
  }

  private def requestAdd(
      bytes: Array[Byte],
      fileName: String,
      remoteAbsolutePath: String
  ): Try[Identity[Response[Either[String, String]]]] = Try {
    basicRequest
      .post(uri = ADD_URI)
      .multipartBody(
        multipart(FILE_KEY, bytes)
          .header(ABSOLUTE_PATH_HEADER_KEY, remoteAbsolutePath)
          .dispositionParam(FILENAME_KEY, fileName)
      )
      .send(backend)
  }

  private def decodeAddResponse(response: Identity[Response[Either[String, String]]]): Try[IpfsAddResponse] = Try {
    response.body match {
      case Left(value) => Failure(new Throwable(value))
      case Right(body) => decode[IpfsAddResponse](body).toTry
    }
  }.flatten
}
