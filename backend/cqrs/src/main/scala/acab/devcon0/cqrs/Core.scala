package acab.devcon0.cqrs

import scala.util.Try

abstract class Command[+RETURN_TYPE]
abstract class Query[+RETURN_TYPE]
abstract class Event[RETURN_TYPE]

abstract class CommandHandler[COMMAND <: Command[COMMAND_TARGET_TYPE], COMMAND_TARGET_TYPE, +EVENT <: Event[_]] {
  def handle(cmd: COMMAND): Try[EVENT]
}

trait QueryHandler[QUERY <: Query[TARGET_TYPE], TARGET_TYPE] {
  def handle(query: QUERY): Try[TARGET_TYPE]
}
