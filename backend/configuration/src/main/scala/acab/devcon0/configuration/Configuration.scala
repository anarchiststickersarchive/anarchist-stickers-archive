package acab.devcon0.configuration

import com.typesafe.config.{Config, ConfigFactory}

import scala.jdk.CollectionConverters.ListHasAsScala

class BotConfigFactory {

  private val rawConfig: Config = ConfigFactory.load("application.conf")
  private val conf: Config = rawConfig.getConfig("app")
  private val dbConfig: Config = rawConfig.getConfig("db.default")
  private val instagramConfig: Config = rawConfig.getConfig("instagram")
  private val ipfsConfig: Config = conf.getConfig("ipfs")
  private val httpConfig: Config = conf.getConfig("http")
  private val vectorizerConfig: Config = conf.getConfig("vectorizer")
  private val redisConfig: Config = rawConfig.getConfig("redis")
  private val mastodonConfig: Config = rawConfig.getConfig("mastodon")
  private val blueskyConfig: Config = rawConfig.getConfig("bluesky")
  private val pixelfedConfig: Config = rawConfig.getConfig("pixelfed")

  def build(): Configuration = {
    val flywayDataSource = FlywayDataSource(
      dbConfig.getString("url"),
      dbConfig.getString("user"),
      dbConfig.getString("password")
    )
    val telegramConfiguration = TelegramConfiguration(
      token = conf.getString("telegram.token"),
      channelChatId = conf.getString("telegram.channel-chat-id"),
      adminUserId = TelegramUserId(conf.getLong("telegram.user-id").toString)
    )
    val publicationPlatforms: Seq[String] = conf.getStringList("publication-platforms").asScala.toList
    val instagramConfiguration = InstagramConfiguration(
      instagramConfig.getString("username"),
      instagramConfig.getString("password"),
      instagramConfig.getString("hashtags")
    )

    val ipfsConfiguration: IpfsConfiguration = IpfsConfiguration(
      remoteGateway = ipfsConfig.getString("remote-gateway"),
      apiUrl = ipfsConfig.getString("api-url"),
      noteDataPath = ipfsConfig.getString("node-data-path")
    )

    val redisConfiguration: RedisConfiguration = RedisConfiguration(
      host = redisConfig.getString("host"),
      port = redisConfig.getInt("port")
    )

    val mastodonConfiguration: MastodonConfiguration = MastodonConfiguration(
      url = mastodonConfig.getString("url"),
      token = mastodonConfig.getString("token")
    )

    val blueskyConfiguration: BlueskyConfiguration = BlueskyConfiguration(
      url = blueskyConfig.getString("url"),
      username = blueskyConfig.getString("username"),
      password = blueskyConfig.getString("password")
    )

    val pixelfedConfiguration: PixelfedConfiguration = PixelfedConfiguration(
      url = pixelfedConfig.getString("url"),
      token = pixelfedConfig.getString("token")
    )

    val httpConfiguration: HttpConfiguration = HttpConfiguration(
      port = httpConfig.getInt("port")
    )

    val vectorizerConfiguration: VectorizerConfiguration = VectorizerConfiguration(
      apiKey = vectorizerConfig.getString("api-key")
    )

    val refreshLockAbsolutePath: String = conf.getString("refresh-lock-absolute-path")

    Configuration(
      telegram = telegramConfiguration,
      ingestionFinalDirectory = conf.getString("ingestion-final-directory"),
      archiveDirectory = conf.getString("archive-directory"),
      publicationPlatforms = publicationPlatforms,
      postingFrequencyHours = conf.getLong("posting-frequency-hours"),
      postingInitialDelayHours = conf.getLong("posting-initial-delay-hours"),
      ipfsGateway = conf.getString("ipfs-gateway"),
      websiteDomainName = conf.getString("website-domain-name"),
      flywayDataSource = flywayDataSource,
      instagramConfiguration = instagramConfiguration,
      ipfs = ipfsConfiguration,
      http = httpConfiguration,
      redis = redisConfiguration,
      mastodon = mastodonConfiguration,
      bluesky = blueskyConfiguration,
      pixelfed = pixelfedConfiguration,
      refreshLockAbsolutePath = refreshLockAbsolutePath,
      vectorizerConfiguration = vectorizerConfiguration
    )
  }
}

case class TelegramUserId(id: String)
case class RedisConfiguration(host: String, port: Int)
case class FlywayDataSource(url: String, user: String, password: String)
case class InstagramConfiguration(username: String, password: String, hashtags: String)
case class TelegramConfiguration(token: String, channelChatId: String, adminUserId: TelegramUserId)
case class MastodonConfiguration(url: String, token: String)
case class BlueskyConfiguration(url: String, username: String, password: String)
case class PixelfedConfiguration(url: String, token: String)
case class IpfsConfiguration(remoteGateway: String, apiUrl: String, noteDataPath: String)
case class HttpConfiguration(port: Int)
case class VectorizerConfiguration(apiKey: String)
case class Configuration(
    telegram: TelegramConfiguration,
    ingestionFinalDirectory: String,
    archiveDirectory: String,
    publicationPlatforms: Seq[String],
    postingFrequencyHours: Long,
    postingInitialDelayHours: Long,
    ipfsGateway: String,
    websiteDomainName: String,
    flywayDataSource: FlywayDataSource,
    instagramConfiguration: InstagramConfiguration,
    ipfs: IpfsConfiguration,
    http: HttpConfiguration,
    redis: RedisConfiguration,
    mastodon: MastodonConfiguration,
    bluesky: BlueskyConfiguration,
    pixelfed: PixelfedConfiguration,
    vectorizerConfiguration: VectorizerConfiguration,
    refreshLockAbsolutePath: String
)
