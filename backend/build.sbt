import sbt.Keys.{mainClass, version}
// Root
val projectName = "anarchist-stickers-archive-backend"
val versionValue = "0.1"
val scalaVersionValue = "2.13.12"

// Dependencies
// Alphabetical
lazy val dependencyVersions =
  new {
    val akkaVersion = "2.8.0"
    val akkaHttpVersion = "10.5.2"
    val akkaHttpCors = "1.2.0"
    val apacheCommons = "2.9.0"

    val bot4s = "5.7.1"
    val circe = "0.14.6"
    val docker = "3.3.0"
    val flyway = "9.16.0"
    val logback = "1.4.12"
    val instagram4j = "2.0.7"

    val mariaDb = "3.3.2"
    val redis = "3.42"
    val scalaIso = "0.1.3"
    val scalikeJdbc = "3.5.0"
    val sttp = "3.8.13"
    val twitter4s = "8.0"
    val typeSafeConfig = "1.4.3"
    val typeSafeScalaLogging = "3.9.5"
  }

// Alphabetical
lazy val dependencies =
  new {
    val akkaActorTyped = "com.typesafe.akka" %% "akka-actor-typed" % dependencyVersions.akkaVersion
    val akkaStream = "com.typesafe.akka" %% "akka-stream" % dependencyVersions.akkaVersion
    val akkaHttp = "com.typesafe.akka" %% "akka-http" % dependencyVersions.akkaHttpVersion
    val akkaHttpCors = "ch.megard" %% "akka-http-cors" % dependencyVersions.akkaHttpCors
    val apacheCommons = "org.apache.commons" % "commons-dbcp2" % dependencyVersions.apacheCommons
    val circeCore = "io.circe" %% "circe-core" % dependencyVersions.circe
    val circeGeneric = "io.circe" %% "circe-generic" % dependencyVersions.circe
    val circeParser = "io.circe" %% "circe-parser" % dependencyVersions.circe
    val docker = "com.github.docker-java" % "docker-java-core" % dependencyVersions.docker
    val dockerTransport = "com.github.docker-java" % "docker-java-transport-okhttp" % dependencyVersions.docker
    val flywayMysql = "org.flywaydb" % "flyway-mysql" % dependencyVersions.flyway
    val instagram4j = "com.github.instagram4j" % "instagram4j" % dependencyVersions.instagram4j
    val logback = "ch.qos.logback" % "logback-classic" % dependencyVersions.logback
    val mariaDbJavaClient = "org.mariadb.jdbc" % "mariadb-java-client" % dependencyVersions.mariaDb
    val redis = "net.debasishg" %% "redisclient" % dependencyVersions.redis
    val scalaIso = "io.bartholomews" %% "scala-iso" % dependencyVersions.scalaIso
    val scalike = "org.scalikejdbc" %% "scalikejdbc" % dependencyVersions.scalikeJdbc
    val scalikeConfig = "org.scalikejdbc" %% "scalikejdbc-config" % dependencyVersions.scalikeJdbc
    val sttp = "com.softwaremill.sttp.client3" %% "core" % dependencyVersions.sttp
    val telegramAkka = "com.bot4s" %% "telegram-akka" % dependencyVersions.bot4s
    val telegramCore = "com.bot4s" %% "telegram-core" % dependencyVersions.bot4s
    val twitter4s = "com.danielasfregola" %% "twitter4s" % dependencyVersions.twitter4s
    val typeSafeConfig = "com.typesafe" % "config" % dependencyVersions.typeSafeConfig
    val typeSafeScalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % dependencyVersions.typeSafeScalaLogging
  }

// Modules
lazy val commons = (project in file("commons")).settings(
  Seq(),
  libraryDependencies ++= Seq(dependencies.typeSafeScalaLogging),
  Compile / mainClass := Some("acab.devcon0.Main"),
  scalaVersion := scalaVersionValue
)
lazy val fsm = (project in file("fsm"))
  .dependsOn(commons)
  .settings(
    Seq(),
    libraryDependencies ++= Seq(),
    Compile / mainClass := Some("acab.devcon0.Main"),
    scalaVersion := scalaVersionValue
  )
lazy val cqrs = (project in file("cqrs")).settings(
  Seq(),
  libraryDependencies ++= Seq(),
  Compile / mainClass := Some("acab.devcon0.Main"),
  scalaVersion := scalaVersionValue
)
lazy val configuration = (project in file("configuration")).settings(
  Seq(),
  libraryDependencies ++= Seq(
    // Alphabetical
    dependencies.typeSafeConfig
  ),
  Compile / mainClass := Some("acab.devcon0.Main"),
  scalaVersion := scalaVersionValue
)
lazy val domain = (project in file("domain"))
  .dependsOn(commons, fsm, cqrs, configuration)
  .settings(
    Seq(),
    libraryDependencies ++= Seq(
      // Alphabetical
      dependencies.circeCore,
      dependencies.circeGeneric,
      dependencies.instagram4j,
      dependencies.scalaIso,
      dependencies.telegramCore,
      dependencies.twitter4s,
      dependencies.typeSafeScalaLogging
    ),
    Compile / mainClass := Some("acab.devcon0.Main"),
    scalaVersion := scalaVersionValue
  )
lazy val input = (project in file("input"))
  .dependsOn(commons, configuration, domain)
  .settings(
    Seq(),
    libraryDependencies ++= Seq(
      // Alphabetical
      dependencies.akkaActorTyped,
      dependencies.akkaStream,
      dependencies.akkaHttp,
      dependencies.akkaHttpCors,
      dependencies.telegramCore,
      dependencies.typeSafeScalaLogging
    ),
    Compile / mainClass := Some("acab.devcon0.Main"),
    scalaVersion := scalaVersionValue
  )
lazy val output = (project in file("output"))
  .dependsOn(commons, configuration, domain)
  .settings(
    Seq(),
    libraryDependencies ++= Seq(
      // Alphabetical
      dependencies.circeCore,
      dependencies.circeGeneric,
      dependencies.circeParser,
      dependencies.docker,
      dependencies.dockerTransport,
      dependencies.redis,
      dependencies.scalaIso,
      dependencies.scalike,
      dependencies.sttp,
      dependencies.telegramCore,
      dependencies.twitter4s,
      dependencies.typeSafeScalaLogging
    ),
    Compile / mainClass := Some("acab.devcon0.Main"),
    scalaVersion := scalaVersionValue
  )

lazy val boot = (project in file("boot"))
  .aggregate(commons, fsm, domain, input, output, configuration, cqrs)
  .dependsOn(commons, fsm, domain, input, output, configuration, cqrs)
  .settings(
    update / aggregate := false,
    libraryDependencies ++= Seq(
      // Alphabetical
      dependencies.apacheCommons,
      dependencies.circeCore,
      dependencies.flywayMysql,
      dependencies.instagram4j,
      dependencies.logback,
      dependencies.mariaDbJavaClient,
      dependencies.scalikeConfig,
      dependencies.sttp,
      dependencies.telegramCore,
      dependencies.twitter4s,
      dependencies.typeSafeScalaLogging
    ),
    Compile / mainClass := Some("acab.devcon0.Main"),
    dockerBaseImage := "openjdk:21-jdk",
    name := projectName,
    version := versionValue,
    scalaVersion := scalaVersionValue
  )
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)
