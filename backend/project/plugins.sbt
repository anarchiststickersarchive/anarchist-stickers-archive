addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.16")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "2.1.1")
addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.16")
addDependencyTreePlugin
