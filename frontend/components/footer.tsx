import * as React from 'react';
import Typography from '@mui/material/Typography';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import CodeIcon from '@mui/icons-material/Code';
import TelegramIcon from '@mui/icons-material/Telegram';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';

export default function StickyFooter() {
  return (
  <Box sx={{ p: 6 }} component="footer" style={{color: "gray", position: "fixed", bottom: 0}}>
    <Link href="https://www.instagram.com/anarchiststickersarchive/" target="_blank" style={{minWidth: '40px'}} >
      <InstagramIcon />
    </Link>
    <Link href="https://twitter.com/anarchosticky" target="_blank" style={{minWidth: '40px'}} >
      <TwitterIcon/>
    </Link>
    <Link href="https://t.me/anarchist_stickers_archive" target="_blank" style={{minWidth: '40px'}} >
      <TelegramIcon/>
    </Link>
    <Link href="" target="_blank" style={{minWidth: '40px'}} >
      <CodeIcon/>
    </Link>
    <Link href="mailto:anarchiststickersarchive@riseup.net" target="_blank" style={{minWidth: '40px'}} >
      <MailOutlineIcon/>
    </Link>
  </Box>
  );
}


