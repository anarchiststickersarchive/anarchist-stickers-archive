import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import CodeIcon from '@mui/icons-material/Code';
import TelegramIcon from '@mui/icons-material/Telegram';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import Divider from '@mui/material/Divider';
import ListItemIcon from '@mui/material/ListItemIcon';
import HomeIcon from '@mui/icons-material/Home';
import InfoIcon from '@mui/icons-material/Info';
import OutlinedInput from '@mui/material/OutlinedInput';
import ArchiveIcon from '@mui/icons-material/Archive';
import RssFeedIcon from '@mui/icons-material/RssFeed';
import { useDebounce } from '../hooks/useDebounce';
import { useState, useEffect } from 'react';

export async function getStaticProps() {
  const pageSizeRes = await fetch('https://api.anarchiststickersarchive.org/api/stickers/_count')
  const count = (await pageSizeRes.json()).count
  return {
    props: {
      allStickersCount: count
    }
  }
}

export default function Header({ parentQueryCallback, allStickersCount }: { parentQueryCallback: any, allStickersCount: any }) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const queryCallback = parentQueryCallback;
  const [input, setInput] = useState('');
  const debouncedInput = useDebounce(input, 200);
  useEffect(() => {
    queryCallback?.(debouncedInput);
  }, [debouncedInput]);

  return (
    <AppBar position="sticky">
      <Toolbar>
        <IconButton
          style={{ minWidth: '40px', color: '#ffffff' }}
          id="basic-button"
          aria-controls={open ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
          component={'a' as any}
        ><MenuIcon />
        </IconButton>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          <MenuItem onClick={handleClose} href="/" component="a">
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <Typography variant="inherit">Home</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="/about" component="a">
            <ListItemIcon>
              <InfoIcon />
            </ListItemIcon>
            <Typography variant="inherit">About</Typography>
          </MenuItem>
          <Divider />
          <MenuItem onClick={handleClose} href="https://api.anarchiststickersarchive.org/api/.rss" target="_blank" component="a">
            <ListItemIcon>
              <RssFeedIcon />
            </ListItemIcon>
            <Typography variant="inherit">RSS</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="mailto:anarchiststickersarchive@riseup.net" target="_blank" component="a">
            <ListItemIcon>
              <MailOutlineIcon />
            </ListItemIcon>
            <Typography variant="inherit">Email</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://0xacab.org/anarchiststickersarchive/anarchist-stickers-archive" target="_blank" component="a">
            <ListItemIcon>
              <CodeIcon />
            </ListItemIcon>
            <Typography variant="inherit">Source code</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://gateway.ipfs.anarchiststickersarchive.org/ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ" target="_blank" component="a">
            <ListItemIcon>
              <ArchiveIcon />
            </ListItemIcon>
            <Typography variant="inherit">IPFS archive</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://kolektiva.social/@anarchist_stickers_archive" target="_blank" component="a">
            <ListItemIcon>
              <TwitterIcon />
            </ListItemIcon>
            <Typography variant="inherit">Mastodon</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://pixelfed.social/@anarchiststickersarchive" target="_blank" component="a">
            <ListItemIcon>
              <InstagramIcon />
            </ListItemIcon>
            <Typography variant="inherit">Pixelfed</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://www.instagram.com/anarchiststickersarchive/" target="_blank" component="a">
            <ListItemIcon>
              <InstagramIcon />
            </ListItemIcon>
            <Typography variant="inherit">Instagram</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://bsky.app/profile/anarchosticky.bsky.social" target="_blank" component="a">
            <ListItemIcon>
              <TwitterIcon />
            </ListItemIcon>
            <Typography variant="inherit">Bluesky</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://twitter.com/anarchosticky" target="_blank" component="a">
            <ListItemIcon>
              <TwitterIcon />
            </ListItemIcon>
            <Typography variant="inherit">Twitter</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://t.me/anarchist_stickers_archive" target="_blank" component="a">
            <ListItemIcon>
              <TelegramIcon />
            </ListItemIcon>
            <Typography variant="inherit">Telegram</Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} href="https://t.me/joinchat/bUUzmRCLzLUzOGQ0" target="_blank" component="a">
            <ListItemIcon>
              <TelegramIcon />
            </ListItemIcon>
            <Typography variant="inherit">Telegram (group)</Typography>
          </MenuItem>
        </Menu>
        <Typography variant="h6" color="inherit" noWrap>
          <Link variant="h6" color="inherit" noWrap href="https://anarchiststickersarchive.org" style={{ textDecoration: 'none' }}>
            [Anarchist Stickers Archive]
          </Link>
        </Typography>
        {parentQueryCallback ?
          <OutlinedInput
            style={{ flex: 1 }}
            size="small"
            placeholder={"Search among " + allStickersCount + " stickers"}
            onChange={e => {
              setInput(e.target.value)
            }}
            sx={{
              ml: 2,
              bgcolor: 'white',
              justifyContent: 'flex-end'
            }}
          />
          :
          null
        }
      </Toolbar>
    </AppBar>
  );
}


