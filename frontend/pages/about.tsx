import * as React from 'react';
import fs from 'fs';
import CssBaseline from '@mui/material/CssBaseline';
import matter from 'gray-matter';
import Image from 'next/image';
import Link from 'next/link';
import md from 'markdown-it';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Header from "../components/header"
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

const theme = createTheme({
  palette: {
    primary: {
      main: '#8d0000',
    },
    secondary: {
      main: '#f50057',
    },
  } 
});

export async function getStaticProps() {
  const fileName = fs.readFileSync('markdown/about.md', 'utf-8');
  const { data: frontmatter, content } = matter(fileName);
  return {
    props: {
      frontmatter,
      content,
    },
  };
}

export default function PostPage({ frontmatter, content } : { frontmatter:any, content:any }) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header parentQueryCallback={null} allStickersCount={null}/>
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="md">
	      	<div dangerouslySetInnerHTML={{ __html: md().render(content) }} />
          </Container>
        </Box>
      </main>
    </ThemeProvider>
  );
}