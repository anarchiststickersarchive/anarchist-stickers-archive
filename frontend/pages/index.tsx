import * as React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CardActionArea from '@mui/material/CardActionArea';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import InfiniteScroll from "react-infinite-scroll-component";
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import CodeIcon from '@mui/icons-material/Code';
import TelegramIcon from '@mui/icons-material/Telegram';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import Header from "../components/header"
import Fuse from 'fuse.js'
import TextField from '@mui/material/TextField';

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const theme = createTheme({
  palette: {
    primary: {
      main: '#8d0000',
    },
    secondary: {
      main: '#f50057',
    },
    background: {
      default: '#e0e0e0',
    },
  },
});

var fuseOptions = {
  // Sort by score (metrix of similarity)
  shouldSort: true,
  //threshold: 0.3,
  threshold: 1,
  location: 0,
  distance: 10,
  maxPatternLength: 10,
  minMatchCharLength: 1,
  keys: [
    "name",
    "notes",
    "source",
    "language.name",
    "language.nativeNames",
  ]
};

export async function getStaticProps() {
  const pageSizeRes = await fetch('https://api.anarchiststickersarchive.org/api/stickers/_count')
  const pageSize = (await pageSizeRes.json()).count
  const res = await fetch('https://api.anarchiststickersarchive.org/api/stickers?pageSize=' + pageSize + '&page=0')
  const allStickers = await res.json()
  return {
    props: {
      allStickers: allStickers.sort((a: any, b: any) => b.id - a.id),
      allStickersCount: pageSize
    }
  }
}

export default function Album({ allStickers, allStickersCount }: { allStickers: any, allStickersCount: any }) {

  const fuse = new Fuse(allStickers, fuseOptions)

  const pageSize = 10;
  const [query, setQuery] = React.useState('')
  const [nextPage, setNextPage] = React.useState(1);
  const [stickers, setStickers] = React.useState<any>(allStickers.slice(0, pageSize));
  const [hasMore, setHasMore] = React.useState(allStickers.slice(0, pageSize).length % pageSize == 0);

  const fetchMoreData = () => {
    setNextPage(oldNextPage => oldNextPage + 1);
  };

  const queryCallback = (queryValue: any) => {
    setQuery(queryValue)
  }

  React.useEffect(() => {
    if (query === '' || query === undefined || query === null || query === ' ') {
      setStickers(allStickers.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    } else {
      const queryResults = fuse.search(query).map((result: any) => result.item)
      setStickers(queryResults.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    }
  }, [nextPage])

  React.useEffect(() => {
    if (query === '' || query === undefined || query === null || query === ' ') {
      setNextPage(1);
      setStickers(allStickers.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    } else {
      const queryResults = fuse.search(query).map((result: any) => result.item)
      setNextPage(1);
      setStickers(queryResults.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    }
  }, [query])

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header parentQueryCallback={queryCallback} allStickersCount={allStickersCount} />
      <main>
        <Container sx={{ py: 4 }} maxWidth="md">
          {/* End hero unit */}
          <InfiniteScroll
            dataLength={stickers.length}
            next={fetchMoreData}
            hasMore={hasMore}
            loader={<h4>Loading...</h4>}
            endMessage={<h4>Nothing more to show</h4>}
          >
            <Grid
              container
              spacing={1}
              justifyContent="center"
              alignItems="flex-start"
            >
              {stickers.map((aggregatedSticker: any) => (
                <Grid item key={aggregatedSticker.id} xs={12} sm={6} md={4}>
                  <Card sx={{ maxWidth: 345 }}>
                    <CardActionArea href={"/stickers/" + aggregatedSticker.id}>
                      <CardMedia
                        component="img"
                        title={aggregatedSticker.name}
                        alt={aggregatedSticker.name}
                        style={{ height: '100%', width: '100%' }}
                        image={"https://gateway.ipfs.anarchiststickersarchive.org/ipfs/" + aggregatedSticker.links['ORIGINAL'].ipfsCID}
                      />
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                          {aggregatedSticker.name}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </InfiniteScroll>
        </Container>
      </main>
    </ThemeProvider>
  );
}
