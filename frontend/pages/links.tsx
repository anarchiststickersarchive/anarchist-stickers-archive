import * as React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CardActionArea from '@mui/material/CardActionArea';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import InfiniteScroll from "react-infinite-scroll-component";
import { useRouter } from "next/router";
import Header from "../components/header"
import Fuse from 'fuse.js'
import MenuIcon from '@mui/icons-material/Menu';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import CodeIcon from '@mui/icons-material/Code';
import TelegramIcon from '@mui/icons-material/Telegram';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import Divider from '@mui/material/Divider';
import ListItemIcon from '@mui/material/ListItemIcon';
import HomeIcon from '@mui/icons-material/Home';
import InfoIcon from '@mui/icons-material/Info';
import TextField from '@mui/material/TextField';
import OutlinedInput from '@mui/material/OutlinedInput';
import FilledInput from '@mui/material/FilledInput';
import ArchiveIcon from '@mui/icons-material/Archive';
import RssFeedIcon from '@mui/icons-material/RssFeed';
import ShareIcon from '@mui/icons-material/Share';

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const theme = createTheme({
  palette: {
    primary: {
      main: '#8d0000',
    },
    secondary: {
      main: '#f50057',
    },
    background: {
      default: '#e0e0e0',
    },
  },
});

var fuseOptions = {
  // Sort by score (metrix of similarity)
  shouldSort: true,
  //threshold: 0.3,
  threshold: 1,
  location: 0,
  distance: 10,
  maxPatternLength: 10,
  minMatchCharLength: 1,
  keys: [
    "name",
    "notes",
    "source",
    "language.name",
    "language.nativeNames",
  ]
};

export async function getStaticProps() {
  const pageSizeRes = await fetch('https://api.anarchiststickersarchive.org/api/stickers/_count')
  const pageSize = (await pageSizeRes.json()).count
  const res = await fetch('https://api.anarchiststickersarchive.org/api/stickers?pageSize=' + pageSize + '&page=0')
  const allStickers = await res.json()
  return {
    props: {
      allStickers: allStickers.sort((a: any, b: any) => b.id - a.id),
      allStickersCount: pageSize
    }
  }
}

export default function Album({ allStickers, allStickersCount }: { allStickers: any, allStickersCount: any }) {

  const fuse = new Fuse(allStickers, fuseOptions)

  const pageSize = 10;
  const [query, setQuery] = React.useState('')
  const [nextPage, setNextPage] = React.useState(1);
  const [stickers, setStickers] = React.useState<any>(allStickers.slice(0, pageSize));
  const [hasMore, setHasMore] = React.useState(allStickers.slice(0, pageSize).length % pageSize == 0);

  const fetchMoreData = () => {
    setNextPage(oldNextPage => oldNextPage + 1);
  };

  const queryCallback = (queryValue: any) => {
    setQuery(queryValue)
  }

  React.useEffect(() => {
    if (query === '' || query === undefined || query === null || query === ' ') {
      setStickers(allStickers.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    } else {
      const queryResults = fuse.search(query).map((result: any) => result.item)
      setStickers(queryResults.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    }
  }, [nextPage])

  React.useEffect(() => {
    if (query === '' || query === undefined || query === null || query === ' ') {
      setNextPage(1);
      setStickers(allStickers.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    } else {
      const queryResults = fuse.search(query).map((result: any) => result.item)
      setNextPage(1);
      setStickers(queryResults.slice(0, (nextPage) * pageSize));
      setHasMore(oldHasMore => stickers.length % pageSize == 0)
    }
  }, [query])

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header parentQueryCallback={null} allStickersCount={null} />
      <main>
        <Container sx={{ py: 4 }} maxWidth="md">
          <Grid
            container
            spacing={1}
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item key="home" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="/">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <HomeIcon /> Home
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="about" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="/about">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <InfoIcon /> About (Manifesto)
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="submissions" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="/submissions">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <ShareIcon /> Submissions
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="rss" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://api.anarchiststickersarchive.org/api/.rss">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <RssFeedIcon /> RSS
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="email" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="mailto:anarchiststickersarchive@riseup.net">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <MailOutlineIcon /> Email
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="code" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://0xacab.org/anarchiststickersarchive/anarchist-stickers-archive">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <CodeIcon /> Source code
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="archive" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://gateway.ipfs.anarchiststickersarchive.org/ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ" target="_blank">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <ArchiveIcon /> IPFS archive
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="instagram" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://www.instagram.com/anarchiststickersarchive/">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <InstagramIcon /> Instagram
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="bluesky" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://bsky.app/profile/anarchosticky.bsky.social">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <TwitterIcon /> Bluesky
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="twitter" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://twitter.com/anarchosticky">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <TwitterIcon /> Twitter
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="telegram-chanel" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://t.me/anarchist_stickers_archive">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <TelegramIcon /> Telegram channel
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="telegram-group" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://t.me/joinchat/bUUzmRCLzLUzOGQ0">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <TelegramIcon /> Telegram group
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="telegram-group" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://pixelfed.social/@anarchiststickersarchive">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <InstagramIcon /> Pixelfed
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item key="telegram-group" xs={12} sm={6} md={4}>
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea href="https://kolektiva.social/@anarchist_stickers_archive">
                  <CardMedia
                    component="img"
                    style={{ height: '100%', width: '100%' }}
                    image=""
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <InstagramIcon /> Mastodon
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </main>
    </ThemeProvider>
  );
}
