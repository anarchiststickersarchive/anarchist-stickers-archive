import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CardActionArea from '@mui/material/CardActionArea';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import InfiniteScroll from "react-infinite-scroll-component";
import Header from "../../components/header"
import Head from 'next/head'

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const theme = createTheme({
  palette: {
    primary: {
      main: '#8d0000',
    },
    secondary: {
      main: '#f50057',
    },
  },
});

export async function getStaticPaths() {
  const pageSizeRes = await fetch('https://api.anarchiststickersarchive.org/api/stickers/_count')
  const pageSize = (await pageSizeRes.json()).count
  const res = await fetch('https://api.anarchiststickersarchive.org/api/stickers?pageSize='+pageSize+'&page=0')
  const allStickers = await res.json()

  const paths = allStickers.map((sticker:any) => ({
    params: { 
      id: sticker.id.toString()
    },
  }))
  return { paths, fallback: false }
}

export async function getStaticProps({ params } : {params:any}) {
  const res = await fetch('https://api.anarchiststickersarchive.org/api/stickers/' + params.id)
  const sticker = await res.json()
  return {
    props: {
      sticker
    }
  }
}

export default function Sticker({ sticker }: { sticker:any }) {

  return (
      <>
      <Head>
        <title>{ sticker.name} | Anarchist Sticker Archive </title>
      </Head>
      <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header parentQueryCallback={null} allStickersCount={null}/>
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="md">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              {sticker.name}
            </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={6}> 
            <CardMedia
              component="img"
              style={{ height: '100%', width: '100%' }}
              image={ "https://gateway.ipfs.anarchiststickersarchive.org/ipfs/" + sticker.links['ORIGINAL'].ipfsCID }
              title={ sticker.name}
            />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Typography variant="h5" align="left" color="text.secondary" paragraph>
                Notes: {
                  sticker.notes ?
                  "" + sticker.notes :
                  "-"
                }
              </Typography>
              <Typography variant="h5" align="left" color="text.secondary" paragraph>
                Source: {
                  sticker.source ?
                  "" + sticker.source :
                  "-"
                }
              </Typography>
              <Typography variant="h5" align="left" color="text.secondary" paragraph>
                Language: {
                  sticker.language ?
                  "" + sticker.language.name + " (" + sticker.language.nativeNames + ")" :
                  "-"
                }
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={12}> 
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            >
              <Button variant="contained" href={ "https://gateway.ipfs.anarchiststickersarchive.org/ipfs/" + sticker.links['ORIGINAL'].ipfsCID }  target="_blank">Original</Button>
              {"VECTOR" in sticker.links &&
                  <Button variant="contained" href={ "https://gateway.ipfs.anarchiststickersarchive.org/ipfs/" + sticker.links['VECTOR'].ipfsCID }  target="_blank">Vector</Button>
                
              }
            </Stack>
            </Grid>
          </Grid>
          </Container>
        </Box>
      </main>
    </ThemeProvider>
    </>
  );
}
