import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head'

function MyApp({ Component, pageProps }: AppProps) {
  const title = "Anarchist Sticker Archive";
  const description = "This project on its surface is as simple as it sounds: it's an archive, of stickers, from the anarchists' scenes around the world. That's it.";
  const imageIcon = "https://anarchiststickersarchive.org/icon.png";
  const url = "https://anarchiststickersarchive.org";

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="og:image" content={imageIcon} />
        <meta property="og:url" content={url} />
        <meta name="twitter:title" content={title} />
        <meta name="twitter:description" content={description} />
        <meta name="twitter:url" content={url} />
        <meta name="twitter:card" content={description} />
        <meta httpEquiv="content-language" content="en" />
        <link rel="icon" type="image/png" href="/favicon.ico" />
      </Head>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
