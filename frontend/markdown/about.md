---
title: About
slug: about
---

# Welcome to the Anarchist Stickers Archive!

This project on its surface is as simple as it sounds: it's an archive, of stickers, from the anarchists' scenes around the world. That's it.

On a bit more deep level, this project was founded with 3 main goals in mind: the stickers themselves, the technical approach and an anarchist praxis.

## TL;DR

### I just want to send you some stickers

This project aims to be a reference point for stickers material, so either if you are an individual or a collective, it does not matter, send your stuff over!

You can send your material via [email](mailto:anarchiststickersarchive@riseup.net). Or submit suggestions via the [telegram bot](https://t.me/anarchist_stickers_archive_bot). Or via DMs at Twitter and Instagram.  
As you prefer. Mail and Telegram are the preferred way since it's the only option that does not compress and lowers the quality of the pictures.

### I just want to download all the stickers

Install IPFS on your machine, by following the [this post](https://docs.ipfs.io/install/command-line/#official-distributions).  
Later on, in a terminal or your desktop application, download the archive that's sitting at this [URL](https://gateway.ipfs.anarchiststickersarchive.org/ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ):

As an alternative hacky way, you can download all the pictures of the main gallery using Firefox, [check this post out](https://www.maketecheasier.com/saving-multiple-images-in-firefox/).

### I just want to download all the metadata of the stickers

Simply visit this [URL](https://api.anarchiststickersarchive.org/api/stickers?page=0&pageSize=20000)


### I just want to help in keeping a copy of the archive

You can help the project by running an IPFS instance and pinning the [IPNS](https://gateway.ipfs.anarchiststickersarchive.org/ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ) where the archive lives.


---

## The milestones of the project

From here, the rest of the text gets 50% geeky talk and the other 50% Unabomber-ish. It might be interesting to read, but you've been warned.


### The stickers

It's quite simple in reality. Stickers are nice, fun and an easy way of putting our messages out there in the street. Everyone loves stickers!  

However, at the time this project started to become an idea, there was nothing similar existing yet or there were other similar projects that would lack certain desired features and traits.  
For instance, whereas there are many webshops from different collectives where you can order stickers, none of them functions as an archive.  
It was also noticed that other platforms like Instagram or Twitter profiles that would post interesting material, apart from the barrier such platforms tend to represent (mandatory accounts, limited public visibility, hard to download, etc), the pictures themselves would not be suitable for later printing due to the low quality the websites and apps enforce.  

Given the above, the will to create this project and fill this gap was born!


### The political motivation

Once the what was defined, then came the question of why this project should start.  

It was clear from the beginning that a project like this would have an anarchist print on it. It should be focus on collaboration, be as open as possible and the material should be extremely easy to access, copy, download, print and even modify. A shorter way, a bit of a GNU licenses but applied and rooted in the anarchist praxis.  
There are also aspects to consider such as censorship, which could come from the State when it blocks certain websites, or from the SSMM companies that would ban certain posts or profiles if they don't like the content.  

Also, it was seen the need to start building our political projects and collectives with a deeper sense of collective memory, ownership and sense of history. It's our ideology, our vision, it's our story. We and no one else will take care of preserving it.    
It's not simply stickers that we design, publish, print and paste. The stickers, but also different material we create (books, music, etc) are a live memory of the political struggles we face on a daily basis, how we see the problems, where do we position ourselves and what are the solutions we propose.   
In the end, when we do develop projects that enable free and easy access to culture on one hand, but also do tackle other principles like sovereignty, autonomy, cooperation and confederalism, we are building stronger patterns and practices that strengthen the whole anarchist movement.   

Some tiny disclaimer. This project laughs and shits at the idea of copyright or private property. If a cool sticker is seen anywhere, it will be 'expropriated' and added to the archive with no further questioning. However, authors will be acknowledged as much as possible (and when called out for missing authority, it will be amended asap).

As a short summary, it could be said that although the idea of having a good archive of stickers is much needed and nice to have, it's also a way of practicing anarchist principles and ideas and even, maybe maybe, pushing them a bit further.  


### 01101011 - The technical part

With the what and why already resolved, then came the question of how to do this. The technical vision was set on 4 main ideas:
1. IPFS as the center of the project.
2. Don't feed the algorithm.
3. Collect as much material as possible, publish it on a steady basis.
4. Warranty the quality of the material.


#### IPFS

For those who hear about it for the 1st time, IPFS is the next P2P version that comes after eMule/Kad and BitTorrent.  
It's a technology that allows you to save and share any file globally with no central point of failure. It's 100% distributed. There are no central servers involved in the process and the network cannot be censored. Ever.    

Any file added to the IPFS network gets an unique and universal identifier (called CIDs), that's why the URLs of the pictures have such long and weird names.   
If another person anywhere else in the world adds the exact same file to the network, the identifier will be the same and both people will be serving the file to anyone requesting it.    

This project has the IPFS persistence at the center. The IPFS archive is the main source of truth and the main and very first place where material gets added. The fact that later on gets published on the website or SSMM it's just a showroom, but the real job is done there.  

The political vision states that the project should rely on autonomy, sovereignty, openness, collaboration and accessibility. IPFS is the milestone that provides that. The material is always available in IPFS, regardless of the website or the SSMM profiles being active. Also, within minutes, anyone can get a full copy of the archive and use it for printing or can simply make their IPFS instance expose a copy of the archive, making it more accessible, sharing the bandwidth load of serving the images, keeping the project alive, collaborating in having a common sense of memory as a political movement and avoiding any form of censorship that could come.  

Even in the case a different collective would branch/fork the project, when the same IPFS file identifiers would get used, that would impact on the material getting better accessibility. Cooperation for the win.


#### Don't feed the algorithm

Twitter, Instagram, Facebook, Telegram and whatever other social network out there are evil. Such sites are extremely precisely designed places where it's really hard to make a project or account benefit from the platform more than what they take from you. The norm here is almost every single time the latter.

The average account from our surrounding struggles will mostly bite the bait for exposure and traction and will follow the designed paths at Silicon Valley to make you spend your time at their apps. In the vision of this project, this approach is malicious in the long run as we act as enablers for a posse within our political communities that do not end up in concrete changes in the real world and also because we can act as cooperators of void evasion mechanisms for people out there.

This does not mean such platforms aren't nice showrooms and broadcasting spaces for a project that help get public attention. It simply means that we need to be sharp, aware and cautious on our way of interacting there.  
That's why the accounts at the different SSMM sites publish automatically, even on the cost of lower traction and shadow banning. That's also why they do not engage much or follow other accounts.  
That's also the reason for having this website and the reason for having RSS as an option for reaching the newest items added to the archive.  
In short, this project follows the [POSSE practice](https://indieweb.org/POSSE).  
In the end, pull people from the mainstream to the autonomy. Digital radicalization if you prefer. Don't use enemies' tools without critical and deep understanding on what's at stake.
  
Give the previous, it's important to remark and give visibility to every other form of digital social spaces that do not sniff on our data or get designed to make us fall for the zombie scrolling.  
In this regard, big shout out to the peeps out there developing, maintaining and working over at projects like [radar.squat](https://radar.squat.net), [Kolektiva](https://kolektiva.media), [Mastodon](https://mastodon.social), [Indymedia](https://indymedia.org/), etc, etc, etc.  
Wanna watch a video? Youtube is lame, visit Kolektiva. Twitter? Nitter/Mastodon. News? RSS the fuck out of every site you visit and stay away from the ads, doomscrolling and data collection. Google is evil? Certain thing, [de-google](https://github.com/tycrek/degoogle) your digital life with the support of the OSS community. Scape the cloud, [host your self](https://yunohost.org/#/).  

The Internet is a beautiful place and an amazing piece of technological engineering, really, it's the disgusting comercial side of it which makes us sick.  


#### Adding & Publishing

Let's say that for instance one collective sends their whole repository (a couple of dozens of stickers) that would be added in one go to the archive. Even though the archive is right away updated, when publishing in SSMM would happen at the time of addition, some days the account would go bananas with too many publications whereas there would be days or even weeks would go silent.  
This unstable way of publishing would not be for the SSMM sites which would perform bans out of spam or flooding reasons.  

That's why in the early phases of writing the code, it was decided to use automatic publishing on SSMM using a periodic publishing pattern, as in one post every X hours.    
This approach is a nice way of separating the process of adding new material to the archive and the propaganda action that's publishing in SSMM.  
And as it was said previously, the center of the project it's the IPFS repo, that place, along the website and the RSS, the real tools that matter because are the ones that are under our control. That's why such places will always make the new material visible right away and SSMM accounts won't.


#### Quality of the material

Every sticker in the archive is meant to be ready to be printed up to an A6 size. This means it should have a sharp definition and be big enough in its dimensions.  
This means that any file in the archive, on regular conditions, should be at least 1200px on its smaller axis (width, height).  
Sometimes, the material found or received does not meet those requirements. In such cases, it's needed to use a couple of different tools to make it meet quality requirements.  
If the picture is more like a drawing or has a graphic style, then it's better to create a vectorized (SVG) version that enables infinite scaling. Due to the lack of better options, the solution used there is [vectorizer.io](vectorizer.io).  
When the picture in question is more like a real photograph, then it's better to use [waifu2x.udp.jp](waifu2x.udp.jp), this website would enable duplicating the size of any picture with virtually 0 distortion by using AI.  

Regardless of the option used in each case, every option implies some small modification of the original picture. Sometimes little details get lost, some letters lose definition, the grain of a background gets swept, etc.  
These problems are compromises acquired in order to preserve the principle of warranting every sticker to be ready to be printed and pasted with sufficient quality.
If it ever happens that you see at the archive a sticker that you have in better quality and you observe that the scaling process came with some detail loss, do not hesitate to contact and send over the improved version.


---

### Final word: open source the anarchy, anarchyfy the open source

Or when politics and tech finally meets and entangles.

It's been a long long journey since the internet was invented. On the way many people ambitioned and visioned an internet where freedom (especially of information) would be an absolute, radical and non negotiable principle.  
Since the beginning until our days philosophies like the open source or the hacker movements have experienced waves of popularity, tranformation and decay over time.

At the present moment, at the moment where massive amounts of code get open sourced, the amount of projects that reject the modern vision of a commercial & corporative internet are more scarce by the day. 

Nowadays, when it's the easiest to have access to different technical solutions that lower the barrier for founding massively complex projects, we found ourselves in the anarchist realm having twitter wars, participating in the data farming from the capitalist dystopia that the neo-feudalist Silicon Valley's philosophy represents.  
We experiment every day the consecuences of an internet controlled by the MegaCorp. Sadly the utopian visions on internet so popular a few decades ago now seem impossible, likewise overcoming the capitalist society. But in both cases, we should not be fooled, change is possible and a need.  

This is, among many other reasons are the consequence of de-politicing the open source, the capitalist machinery adapted to it, co-opted it and exploited against us. The example of [Microsoft's EEE](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish) strategy and [Linux mileau situation](https://www.youtube.com/watch?v=WtJ9T_IJOPE) would be the best summary of the situation. It's needed to re-politice the open source, entangle it with our anarchist praxis and put an end to the current loony trajectory the digital world is taking (someone mentioned web3? NFTs? Bitcoin?)

The way of reversing this situation it's not easy, nor simple. It implies discipline, analysis, hard work and the right actions at the right moment.  
But we, the anarchists, have never been afraid of a gig as hard as that. Durruti supposedly said that "we should not be afraid of the ruins of the war, we the workers are the ones that built everything you see, we could and will do it again".

Then so be it. In order to build a stronger anarchist movement, especially in the digital realm, that also feeds with examples the real world (TM), a new set of practices should be onboarded on a daily basis, some are already part of us and should never be forgotten, some others might be new. To be known:
+ Do not only fund projects, always open source your code, material and documents as much as possible. Think of it as the difference between food and recipes. It's not only about having the result of cooking (the food), but also understanding how it got made (the recipe) and why it happened in a certain manner (cooking tips).
+ Make sure to always talk about what, how and why. It's not only having an archive of stickers (the what), it's not only about the code behind the archive (the how), it's also the decisions made, the political principles it gets born from (the why).
+ Do not only publish in SSMM or sites you are not in control of. Rely on autonomous projects, keep ownership of your project and when in need of reaching a massive audience in mainstream SSMM, be sharp, use it as a vector of making more people get in touch with the alternatives and the real core of what benefit us: the tools and platforms that belong to our communities. In other words, using other's tools is a mean for radicalising the mainstream, not as a way of surrending to the current Status Quo.
+ Do not only make the material easily accessible. It's also important to make it easy to modify, republish and maintain it. In case of doubt, use the philosophy behind [the GNU v3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
+ Do not only have a project that provides a way of archiving our culture and ideas. Also promote an open culture about decisions and history that enables a low-key way of transmitting knowledge among communities and generations. The faster we can transmit knowledge among our groups, the faster and better we move and the more chances we get to succeed in our quest for Utopia.

This project is not naive on its position over the complexity of the quest described above. In the end, this project is about stickers and solely aims to be a simple but nicely bundled try out on how we could be doing things to get better.

 
Last but not least: PRINT, SHARE AND PASTE!
