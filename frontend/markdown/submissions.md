---
title: Submissions
slug: submissions
---

# How to submit stickers

When you or your collective have your a collection of stickers you want to add to the archive, there are two main ways, either via email or via the Telegram bot.  
Submitting material via Twitter or Instagram is less ideal and not desired at all, mostly because in the majority of the cases, the size & quality of the links over there goes against the standards of the site.

## Option A: email

Simply and straight forward, drop a line to [anarchiststickersarchive@riseup.net](mailto:anarchiststickersarchive@riseup.net).


## Option B: Telegram bot

If you have a Telegram account, consider submitting via the [telegram bot](https://t.me/anarchist_stickers_archive_bot).  
At this bot it's possible to perform several operations like previewing material, checking the index, searching and so on.  

On of the most powerful features is the capacity to submit your material directly from there.  
You can start your submission by sending an `/add` message to the bot and follow the instructions.  
The bot works with shortcuts, so you can for instance type `language` or simply `l` in order to set that value.  
  
The available fields for edition are:  
+ **source or s** - Some link to the original author. A website or a social media account.  
+ **notes or n** - If the sticker is not in English, consider adding an English translation.  
+ **vector or v** - Add a SVG file to the sticker.  
+ **language or l** - Set the main language of the text in the sticker. Use 2 digit codes (`en`, `de`, `fr` and so on) for this. Check the full code list [here](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).  
+ **save or s** - Sends the sticker for final review. When approved by the admin, you'll get a notification message.  
+ **cancel or .** - Quit, abort. It destroy your changes but you can start over any time.

Please note that images should be sent as files, not as photos.  
This means that the image gets sent uncompressed

---
## Guidelines. What's important when submitting stickers

When sending your material, please consider the following aspects:


### The files

The main image should be at least 2000 pixels in one of the axis and have a sharp definition.  
Do not manually scale images for the sake of meeting the 2K pixels guidelines, instead, consider using AI-powered websites like [waifu2x](https://waifu2x.udp.jp/).  
  
When possible, submit the vector version of the sticker. This helps in giving higher warranties when scaling up and preparing the binaries for printing.
When you do not have the vector version of the sticker but it's the kind of picture that could be vectorized, consider using projects like [vectorizer.io](https://www.vectorizer.io)


### The language & the alphabet

At this project is preferred to respect the original language and alphabet of the material uploaded.  
So, when sending material it truly helps if the title of the sticker in its original language and alphabet is sent as well.  
  
In the cases where the title of the sticker is non-english, please consider including the translation.
Without a translation it's needed to make use of an online translator and depending on the source language, the result may not be acurated.


### The source

When sending data and when you know this information, include referrals to the authors, either individuals or collectives, does not matter.  
If you can properly appoint the author of the material then it's acceptable to submit anonymous content.  

The reference should be a website link as first option or alternatively a social media handler.  
The principle is that websites tend to survive longer than social media, specially considering the censorship ecosystem our groups and accounts suffer.
