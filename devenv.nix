{ pkgs, lib, config, inputs, ... }:

{
  dotenv.enable = true;

  languages.python = {
    version = "3.13";
    enable = true;
    venv.enable = true;
    venv.requirements = ''
      instaloader
    '';
  };
  packages = [
    pkgs.git
    pkgs.zsh
  ];

  enterShell = ''
    # echo "Entering Nix development shell..."
    # git --version
    # python3 --version
    # instaloader --version
  '';

  scripts.scrape-instagram.exec = ''
    scripts/scrappers/instagram_scrapper/run.sh
  '';
}
