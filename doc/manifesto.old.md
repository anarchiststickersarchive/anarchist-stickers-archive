Welcome to the Anarchist Stickers Archive!

This place publishes stickers from anarchism scene and surrounding struggles and aims to be a reference point for stickers material in the scene, as well as providing a long-lasting archive mechanism by using IPFS technology.


How does it work?

Media gets placed in a publishing queue and a publishing bot randomly picks one from time to time.
At this moment, stickers get automatically published every 4 hours.

Are you present in other platforms?

Sure, check the linktree for a complete overview of this project presence over different platforms. So far the project is also publishing at Instagram and Twitter social media sites.

Can I send my stuff?

Certain thing! You can send your stickers via the group
Always send uncompressed pictures >1000px.
Alternatively, SVGs, PSDs, XCFs, AIs and similar are welcome.
Also, material is hosted in IPFS, if you run a node yourself, consider pinning the archive yourself.
Also, you can send a bulk of stickers over the email anarchiststickersarchive@riseup.net


Can I visit a URL where I can see all the material at once?

Yes! Here


Can I download the whole archive in a single click?

Yes. You will need IPFS for that:

ipfs get --output anarchist-stickers-archive /ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ


How can I contribute to keep the archive alive?

Since the material is sitting in IPFS, you only need to pin it to your local IPFS node (or remote pinning service) and you'll be hosting it as well.

# Download the whole archive
ipfs get --output anarchist-stickers-archive /ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ
# Pin the result to your IPFS node, now you are contributing to keep the archive alive worldwide.
ipfs add --pin=true --recursive anarchist-stickers-archive


Is is possible to interact with the bot?

Yes! The bot (@anarchist_stickers_archive_bot) allows certain commands to be used by any user. To be known:

+ /index - List all the stickers in the archive.
+ /indexNonPublished - List all the stickers that are not published in Social Media yet, but you can check them in advance by using the preview command
+ /publicationsQueueStats - It gives some information about the publication queue size
+ /search - Look up terms against the title, authors and English translated captions.
+ /preview - Honestly, the best of all. Allows you to get any sticker of the archive. Simply type /preview 10 and you will get the sticker with id 10. This is specially useful for peeking unpublished stuff. Also, the archive metadata is getting get expanded with new information: authors, dimensions, vectorized versions of the stickers, etc.
So, over time, it's worth checking if your fav ones got updates.

There's one restriction so far, the bot only works when called in a private chat, so do not add it to groups and try to use it from there, it won't work.

Last but not least: PRINT, SHARE AND PASTE!