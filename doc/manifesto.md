# Welcome to the Anarchist Stickers Archive!

This place publishes stickers from the anarchist scene and surrounding struggles.
It aims to be a reference point for stickers material in the scene as well as providing a long-lasting storing mechanism of material for our collectives by using IPFS technology.


## How does it work?

Stickers get placed in a publishing queue and some automated process randomly picks and publish one from time to time.
At this moment, stickers get published every 4 hours.

A bit more technically (optional): there is a scala backend living somewhere in the dark of the cloud. This backend can ingest new stickers via the telegram bot, persist in SQL & IPFS. Later on, via some scheduler, makes the publishing to the different social media platforms. 


## Are you present in other platforms?

Sure, check the [linktree](https://linktr.ee/anarchiststickersarchive) for a complete overview of this project presence over different platforms. 


## I have a bunch of stickers you don't have. Can I send you my stuff?

Certain thing! You can send your stickers via the [group](https://t.me/joinchat/bUUzmRCLzLUzOGQ0) 
Always send uncompressed pictures >1000px.
Alternatively, SVGs, PSDs, XCFs, AIs and similar are welcome.
Also, material is hosted in IPFS, if you run a node yourself, consider pinning the archive yourself.
Also, you can send a bulk of stickers over the email [anarchiststickersarchive@riseup.net](mailto:anarchiststickersarchive@riseup.net)


## Is there any URL where I can see all the material at once?

Yes! [Here](https://gateway.pinata.cloud/ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ)


## Can I download the whole archive in a single click?

Yes. You will need IPFS for that:

`ipfs get --output anarchist-stickers-archive /ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ`


## How can I contribute to keep the archive alive?

Since the material is sitting in IPFS, you only need to pin it to your local IPFS node (or remote pinning service) and you'll be hosting it as well.

`ipfs pin add -r /ipns/12D3KooWRYPzqB5V7MWraWkJ32DEx46CnUmFV1j4PHEAEkJmKzGZ`


## Is is possible to interact with the bot?

Yes! The bot (@anarchist_stickers_archive_bot) allows certain commands to be used by any user. To be known:

+ /index - List all the stickers in the archive.
+ /indexNonPublished - List all the stickers that are not published in Social Media yet, but you can check them in advance by using the preview command
+ /publicationsQueueStats - It gives some information about the publication queue size
+ /search - Look up terms against the title, authors and English translated captions.
+ /preview - Honestly, the best of all. Allows you to get any sticker of the archive. Simply type /preview 10 and you will get the sticker with id 10. This is specially useful for peeking unpublished stuff. Also, the archive metadata is getting get expanded with new information: authors, dimensions, vectorized versions of the stickers, etc.
So, over time, it's worth checking if your fav ones got updates.

There's one restriction so far, the bot only works when called in a private chat, so do not add it to groups and try to use it from there, it won't work.

Last but not least: PRINT, SHARE AND PASTE!