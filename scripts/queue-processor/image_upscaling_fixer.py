import shutil

from PIL import Image
from video2x import Upscaler


class ImageUpscaler:

    def __init__(self, logger, image_input_absolute_path, image_output_absolute_path, error_image_output_absolute_path, desired_width, desired_height):
        self.image_input_absolute_path = image_input_absolute_path
        self.image_output_absolute_path = image_output_absolute_path
        self.error_image_output_absolute_path = error_image_output_absolute_path
        self.desired_width = desired_width
        self.desired_height = desired_height
        self.logger = logger

    def run(self):
        try:
            image = Image.open(self.image_input_absolute_path)
            upscaled_image = Upscaler().upscale_image(image, self.desired_width, self.desired_height, "realcugan", 3)
            upscaled_image.save(self.image_output_absolute_path)
        except Exception as exception:
            shutil.move(self.image_input_absolute_path, self.error_image_output_absolute_path)
            self.logger.error(f"{exception}")
            self.logger.error(f"Upscaled failed, moving the picture to {self.error_image_output_absolute_path}")
            pass
