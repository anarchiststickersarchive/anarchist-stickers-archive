import random
from glob import glob
from pathlib import Path
from typing import List
from logger import logger

from files_picker import get_files_in_path_recursive
from image_vectorizer import ImageVectorizer
from path_mapper_handler import *
from utils import move_to_trash


def get_model_number_of_colors(image_input_absolute_path: str, project_path):
    dirname = image_input_absolute_path.removeprefix(project_path + '/').removeprefix(vector_folder_name + '/').split('/')[0]
    return dirname.split('-')


def vectorize_process_image(image_input_absolute_path):
    vectorizer_io_api_key = os.environ.get("VECTORIZER_IO_API_KEY")
    root_path = os.environ.get("PROJECT_PATH")
    logger.info(f"vectorizer_io_api_key={vectorizer_io_api_key} root_path={root_path}")
    model, number_of_colors = get_model_number_of_colors(image_input_absolute_path, root_path)
    image_output_absolute_path = to_vector_to_final_path_mapping(image_input_absolute_path, model, number_of_colors)
    has_vector = Path(image_output_absolute_path).exists()
    if has_vector:
        logger.info(f"** Skipping already vectorized image: {image_input_absolute_path}")
        pass
    else:
        image_vectorizer = ImageVectorizer(
            logger, image_input_absolute_path, image_output_absolute_path, target_size, vectorizer_io_api_key
        )
        image_vectorizer.run(model, number_of_colors)
        image_trash_absolute_path = to_vector_to_trash_path_mapping(image_input_absolute_path, model, number_of_colors)
        move_to_trash(image_input_absolute_path, image_trash_absolute_path, logger)


def run_vectorization() -> None:
    folders = [y for x in os.walk(vector_path) for y in glob(os.path.join(x[0]))]
    random.shuffle(folders)
    for folder in folders:
        folder_as_path = Path(folder)
        if folder_as_path.is_dir():
            logger.info(f"* Vectorizing {folder}")
            vectorize_process_folder(folder)
        else:
            logger.info(f"* Skipping dir: {folder_as_path.name}")


def vectorize_process_folder(folder_name: str):
    images: List[str] = get_files_in_path_recursive(folder_name, False, None)
    for image_absolute_path in images:
        vectorize_process_image(image_absolute_path)
