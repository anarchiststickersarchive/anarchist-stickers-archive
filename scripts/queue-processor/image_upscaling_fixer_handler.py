import shutil
from typing import List

from PIL import Image

from files_picker import get_files_in_path_recursive
from logger import logger
from path_mapper_handler import *

root_path = os.environ.get("PROJECT_PATH")


def run_upscaler_fixing() -> None:
    images: List[str] = get_files_in_path_recursive(upscaled_path, False, filtering_function=None)
    logger.debug(f"Picked {len(images)} files from path={upscaled_path} shuffled?={False} filtered?={True}")
    counter = 0
    for image_absolute_path in images:
        counter = counter + 1
        try:
            if image_has_only_one_color(image_absolute_path):
                image_at_trash_path = upscaled_to_trash_path_mapping(image_absolute_path)
                image_at_workbench_path = upscaled_to_workbench_path_mapping(image_absolute_path)
                log_image_restored_at_workbench(image_at_trash_path, image_at_workbench_path)
                shutil.copy(image_at_trash_path, image_at_workbench_path)
                os.remove(image_absolute_path)
            else:
                logger.info(f"{counter}")
        except Exception as e:
            logger.error(f"Could not find the image at trash, sorry path={upscaled_to_trash_path_mapping(image_absolute_path)} exception={str(e)}")


def image_has_only_one_color(image_absolute_path):
    im = Image.open(image_absolute_path)

    has_only_one_color = len(im.getcolors(im.size[0] * im.size[1])) == 1
    return has_only_one_color


def log_image_restored_at_workbench(source_absolute_path, destination_absolute_path):
    logger.info(f"** Image restored at workbench.")
    logger.info(f"*** source={source_absolute_path}")
    logger.info(f"*** destination={destination_absolute_path}")
    logger.info(f"****")
