from glob import glob
from pathlib import Path

from image_rasterizer import ImageRasterizer
from path_mapper_handler import *
from logger import logger


def run_rasterization() -> None:
    folders = [y for x in os.walk(final_path) for y in glob(os.path.join(x[0]))]

    for folder in folders:
        folder_as_path = Path(folder)
        if folder_as_path.is_dir():
            rasterize_process_folder(folder)
        else:
            logger.error(f"* Skipping dir: {folder_as_path.name}")


def rasterize_process_image(image_input_absolute_path):
    image_output_absolute_path = final_vector_to_final_jpg_path_mapping(image_input_absolute_path)
    image_rasterizer = ImageRasterizer(image_input_absolute_path, image_output_absolute_path,)
    image_rasterizer.run()


def rasterize_process_folder(folder_name: str):
    vectors = [y for x in os.walk(folder_name) for y in glob(os.path.join(x[0], '*.svg'))]
    for vector in vectors:
        rasterize_process_image(vector)
