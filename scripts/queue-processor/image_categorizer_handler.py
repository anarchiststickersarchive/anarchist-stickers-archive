import shutil
from pathlib import Path
from typing import List

import inquirer
import imageio.v3 as iio

from files_picker import get_files_in_path_recursive
from path_mapper_handler import *
from logger import logger


def ignore_instagram(image_absolute_path):
    return "instagram" not in image_absolute_path


def filter_in_telegram_and_twitter(image_absolute_path):
    return ("twitter" in image_absolute_path) or ("telegram" in image_absolute_path)


def filter_out_telegram_and_twitter(image_absolute_path):
    return ("twitter" not in image_absolute_path) and ("telegram" not in image_absolute_path)


def run_manual_categorization() -> None:
    images_tw_and_tg: List[str] = get_files_in_path_recursive(upscaled_path, shuffle=True, filtering_function=filter_in_telegram_and_twitter)
    images_not_tw_and_tg: List[str] = get_files_in_path_recursive(upscaled_path, shuffle=True, filtering_function=filter_out_telegram_and_twitter)
    #images: List[str] = get_files_in_path_recursive(upscaled_path, shuffle=True)
    images: List[str] = images_tw_and_tg + images_not_tw_and_tg
    for image_absolute_path in images:
        image_at_trash_path = upscaled_to_trash_path_mapping(image_absolute_path)
        open_image_with_default_app(image_absolute_path)
        log_original_dimensions(image_at_trash_path)
        questions = [
            inquirer.List(
                "action",
                message="What to do?",
                choices=[
                    ("vectorize", 1),
                    ("final", 2),
                    ("remove", 3),
                    ("skip", 4),
                    ("problem", 5),
                    ("quit", 6),
                ],
            ),
        ]
        action_answers = inquirer.prompt(questions)
        if action_answers['action'] == 1:
            questions = [
                inquirer.List(
                    "model",
                    message="What type of vector?",
                    choices=[
                        ("clipart - Few colors", "clipart"),
                        ("photo - Many colors", "photo"),
                        ("drawing - Black&White", "drawing")
                    ],
                ),
                inquirer.Text("number_of_colors", message="How many colors? (2-32 or auto by default)", default="a")
            ]
            answers = inquirer.prompt(questions)
            model = answers['model']
            number_of_colors = "auto" if answers['number_of_colors'] == "a" else answers['number_of_colors']
            image_output_absolute_path = upscaled_to_to_vector_path_mapping(image_absolute_path, model, number_of_colors)
            logger.info(f"Moving {image_absolute_path} to {image_output_absolute_path}")
            shutil.move(image_absolute_path, image_output_absolute_path)
            image_at_workbench_path = upscaled_to_workbench_path_mapping(image_absolute_path)
            if Path(image_at_workbench_path).exists():
                image_at_trash_path = upscaled_to_trash_path_mapping(image_absolute_path)
                shutil.move(image_at_workbench_path, image_at_trash_path)
                logger.info(f"Making sure workbench copy is gone {image_at_workbench_path}")
        elif action_answers['action'] == 2:
            image_output_absolute_path = upscaled_to_final_path_mapping(image_absolute_path)
            logger.info(f"Moving {image_absolute_path} to {image_output_absolute_path}")
            shutil.move(image_absolute_path, image_output_absolute_path)
            image_at_workbench_path = upscaled_to_workbench_path_mapping(image_absolute_path)
            if Path(image_at_workbench_path).exists():
                image_at_trash_path = upscaled_to_trash_path_mapping(image_absolute_path)
                shutil.move(image_at_workbench_path, image_at_trash_path)
                logger.info(f"Making sure upscaled copy is gone {image_at_workbench_path}")
        elif action_answers['action'] == 3:
            image_at_workbench_path = upscaled_to_workbench_path_mapping(image_absolute_path)
            os.remove(image_absolute_path)
            logger.info(f"Making sure upscaled copy is gone {image_absolute_path}")
            if Path(image_at_workbench_path).exists():
                os.remove(image_at_workbench_path)
                logger.info(f"Making sure workbench copy is gone {image_at_workbench_path}")
        elif action_answers['action'] == 5:
            open_image_with_default_app(image_at_trash_path)
            sub_command_questions = [
                inquirer.List(
                    "action",
                    message="What to do?",
                    choices=[
                        ("restore from trash", 1),
                        ("remove", 2),
                    ],
                ),
            ]
            sub_command_answers = inquirer.prompt(sub_command_questions)
            if sub_command_answers['action'] == 1:
                image_at_workbench_path = upscaled_to_workbench_path_mapping(image_absolute_path)
                log_image_restored_at_workbench(image_at_trash_path, image_at_workbench_path)
                shutil.move(image_at_trash_path, image_at_workbench_path)
                os.remove(image_absolute_path)
            elif sub_command_answers['action'] == 2:
                os.remove(image_absolute_path)
        elif action_answers['action'] == 6:
            exit(0)
        else:
            pass


def log_original_dimensions(image_at_trash_path):
    try:
        shape = iio.imread(image_at_trash_path).shape
        height = shape[0]
        width = shape[1]
        logger.info(f"*** original dimensions={width}x{height}")
    except Exception as e:
        logger.info(f"{e}")
    logger.info(f"*** at trash={image_at_trash_path}")


def log_image_restored_at_workbench(source_absolute_path, destination_absolute_path):
    logger.info(f"** Image restored at workbench.")
    logger.info(f"*** source={source_absolute_path}")
    logger.info(f"*** destination={destination_absolute_path}")
    logger.info(f"****")


def open_image_with_default_app(image_absolute_path):
    cmd = f"open \"{image_absolute_path}\""
    logger.debug(f"command={cmd}")
    os.system(cmd)
