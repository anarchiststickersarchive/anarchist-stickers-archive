import shutil


def move_to_trash(source_absolute_path, destination_absolute_path, logger):
    log_move_to_trash(destination_absolute_path, source_absolute_path, logger)
    shutil.move(source_absolute_path, destination_absolute_path)


def log_move_to_trash(destination_absolute_path, source_absolute_path, logger):
    logger.info(f"** Moving to trash.")
    logger.info(f"*** source={source_absolute_path}")
    logger.info(f"*** destination={destination_absolute_path}")
    logger.info(f"****")
