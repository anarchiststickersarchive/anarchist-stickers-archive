import os

import inquirer

from image_categorizer_handler import run_manual_categorization
from image_rasterizer_handler import run_rasterization
from image_upscaler_handler import run_upscaling
from image_stats_handler import run_stats
from image_upscaling_fixer_handler import run_upscaler_fixing
from image_vectorizer_handler import run_vectorization
from logger import logger
import argparse


def run() -> None:
    questions = [
        inquirer.List(
            "action",
            message="What to do?",
            choices=[
                ("Upscale", 1),
                ("Categorize", 2),
                ("Vectorize", 3),
                ("Rasterize", 4),
                ("Stats", 5),
                ("Fix", 6),
            ],
        ),
    ]
    action_answers = inquirer.prompt(questions)
    response = action_answers['action']
    if response == 1:
        run_upscaling()
    elif response == 2:
        run_manual_categorization()
    elif response == 3:
        run_vectorization()
    elif response == 4:
        run_rasterization()
    elif response == 5:
        run_stats()
    elif response == 6:
        run_upscaler_fixing()
    else:
        pass


def get_action(all_config):
    maybe_action = all_config['action']
    if not maybe_action:
        return maybe_action
    else:
        return str(maybe_action).upper()


parser = argparse.ArgumentParser(description="Queue processor",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-a", "--action", help="[UPSCALE | CATEGORIZE |VECTORIZE | RASTERIZE | STATS]")
args = parser.parse_args()
action = get_action(vars(args))

vectorizer_io_api_key = os.environ.get("VECTORIZER_IO_API_KEY")
project_path = os.environ.get("PROJECT_PATH")
logger.info(f"configuration: project_path={project_path}")
logger.info(f"invocation: action={action}")

match action:
    case None:
        run()
    case 'UPSCALE':
        run_upscaling()
    case 'CATEGORIZE':
        run_manual_categorization()
    case 'VECTORIZE':
        run_vectorization()
    case 'RASTERIZE':
        run_rasterization()
    case 'STATS':
        run_stats()
