from pathlib import Path

from wand.image import Image
from logger import logger


class ImageRasterizer:

    def __init__(self, image_input_absolute_path, image_output_absolute_path):
        self.image_input_absolute_path = image_input_absolute_path
        self.image_input_absolute_path_as_path = Path(self.image_input_absolute_path)
        self.image_output_absolute_path = image_output_absolute_path

    def run(self):
        is_rasterized = Path(self.image_output_absolute_path).exists()
        if is_rasterized:
            # logger.debug(f"** Skipping already rasterized image: {self.image_input_absolute_path}")
            pass
        else:
            try:
                ny = Image(filename=self.image_input_absolute_path)
                ny_convert = ny.convert('jpg')
                ny_convert.save(filename=self.image_output_absolute_path)
                logger.info(f"** Image rasterized: {self.image_output_absolute_path} ")
            except Exception as e:
                logger.error(f"Something went wrong with {self.image_input_absolute_path}. exception={e}")

    def __save_binary(self, binary_response):
        with open(self.image_output_absolute_path, 'wb') as f:
            f.write(binary_response)
