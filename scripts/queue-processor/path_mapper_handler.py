import os

from constants import *
from path_mapper import WorkbenchToUpscaledPathMapper, UpscaledToWorkbenchPathMapper, UpscaledToTrashPathMapper, \
    WorkbenchToErrorPathMapper, FinalVectorToFinalJpgPathMapper, UpscaledToFinalPathMapper, ToVectorToTrashPathMapper, \
    ToVectorToFinalPathMapper, UpscaledToToVectorPathMapper, WorkbenchToTrashPathMapper

root_path = os.environ.get("PROJECT_PATH")

workbench_path = f"{root_path}/{workbench_folder_name}"
upscaled_path = f"{root_path}/{upscaled_folder_name}"
vector_path = f"{root_path}/{vector_folder_name}"
final_path = f"{root_path}/{final_folder_name}"
trash_path = f"{root_path}/{trash_folder_name}"
error_root_path = f"{root_path}/{error_folder_name}"
error_upscaling_path = f"{root_path}/{upscaled_folder_name}"

upscaled_to_workbench_path_mapper = UpscaledToWorkbenchPathMapper(root_path)
upscaled_to_trash_path_mapper = UpscaledToTrashPathMapper(root_path)
upscaled_to_final_path_mapper = UpscaledToFinalPathMapper(root_path)
upscaled_to_to_vector_path_mapper = UpscaledToToVectorPathMapper(root_path)
workbench_to_upscaled_path_mapper = WorkbenchToUpscaledPathMapper(root_path)
workbench_to_error_path_mapper = WorkbenchToErrorPathMapper(root_path)
workbench_to_trash_path_mapper = WorkbenchToTrashPathMapper(root_path)
final_vector_to_final_jpg_path_mapper = FinalVectorToFinalJpgPathMapper(root_path)
to_vector_to_trash_path_mapper = ToVectorToTrashPathMapper(root_path)
to_vector_to_final_path_mapper = ToVectorToFinalPathMapper(root_path)


def upscaled_to_workbench_path_mapping(source_absolute_path):
    return upscaled_to_workbench_path_mapper.get(source_absolute_path)


def upscaled_to_trash_path_mapping(source_absolute_path):
    return upscaled_to_trash_path_mapper.get(source_absolute_path)


def workbench_to_upscaled_path_mapping(source_absolute_path):
    return workbench_to_upscaled_path_mapper.get(source_absolute_path)


def workbench_to_error_path_mapping(source_absolute_path):
    return workbench_to_error_path_mapper.get(source_absolute_path)


def workbench_to_trash_path_mapping(source_absolute_path):
    return workbench_to_trash_path_mapper.get(source_absolute_path)


def final_vector_to_final_jpg_path_mapping(source_absolute_path):
    return final_vector_to_final_jpg_path_mapper.get(source_absolute_path)


def upscaled_to_final_path_mapping(source_absolute_path):
    return upscaled_to_final_path_mapper.get(source_absolute_path)


def to_vector_to_trash_path_mapping(source_absolute_path, model, number_of_colors):
    return to_vector_to_trash_path_mapper.get(source_absolute_path, model, number_of_colors)


def to_vector_to_final_path_mapping(source_absolute_path, model, number_of_colors):
    return to_vector_to_final_path_mapper.get(source_absolute_path, model, number_of_colors)


def upscaled_to_to_vector_path_mapping(source_absolute_path, model, number_of_colors):
    return upscaled_to_to_vector_path_mapper.get(source_absolute_path, model, number_of_colors)
