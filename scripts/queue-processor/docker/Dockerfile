FROM ubuntu:24.10@sha256:c62f1babc85f8756f395e6aabda682acd7c58a1b0c3bea250713cd0184a93efa

SHELL ["/bin/bash", "-c"]

# Install dependencies
RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository universe
RUN apt-get update && apt-get install -y \
    build-essential \
    cmake \
    ffmpeg \
    git \
    glslang-dev \
    glslang-tools \
    libcairo2 \
    libcairo2-dev \
    libsuitesparse-dev \
    libvulkan-dev \
    libxml2-dev \
    libxmlsec1-dev \
    mesa-vulkan-drivers \
    ninja-build \
    pkg-config \
    python3-dev \
    python3-opencv \
    python3-pil \
    python3-pip \
    python3-tqdm \
    python3.12 \
    python3.12-dev \
    python3.12-venv \
    sox \
    swig \
    texlive-full \
    curl \
    clang \
    pkg-config \
    libavcodec-dev \
    libavdevice-dev \
    libavfilter-dev \
    libavformat-dev \
    libavutil-dev \
    libswscale-dev \
    libvulkan-dev \
    glslang-tools \
    libomp-dev

WORKDIR /opt/anarchist-stickers-archive-queue-processor
RUN curl -LsSf https://astral.sh/uv/install.sh | sh;

COPY requirements.txt .
RUN source $HOME/.cargo/env; uv venv; source .venv/bin/activate; uv pip install -r requirements.txt;

RUN cd /opt/; \
    git clone https://github.com/k4yt3x/video2x.git; \
    cd video2x; \
    git checkout 5.0.0-beta6; \
    sed -i 's/license-expression = "AGPL-3.0-or-later"/license = "AGPL-3.0-or-later"/' pyproject.toml; \
    git diff; \
    source $HOME/.cargo/env; \
    source /opt/anarchist-stickers-archive-queue-processor/.venv/bin/activate; \
    uv pip install -e .; \
    cd /opt/anarchist-stickers-archive-queue-processor

RUN ls -l
COPY *.py /opt/anarchist-stickers-archive-queue-processor/
COPY docker/*.sh /opt/anarchist-stickers-archive-queue-processor/

RUN chmod +x /opt/anarchist-stickers-archive-queue-processor/docker-entrypoint.sh

ENTRYPOINT ["/opt/anarchist-stickers-archive-queue-processor/docker-entrypoint.sh"]
