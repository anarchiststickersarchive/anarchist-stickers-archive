#!/bin/bash

set -e

source /opt/anarchist-stickers-archive-queue-processor/.venv/bin/activate;
pwd
exec "$@"
