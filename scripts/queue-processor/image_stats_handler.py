from typing import List

from files_picker import get_files_in_path_recursive
from logger import logger
from path_mapper_handler import *

root_path = os.environ.get("PROJECT_PATH")


def run_stats() -> None:
    images_workbench: List[str] = get_files_in_path_recursive(workbench_path, False)
    images_upscaled: List[str] = get_files_in_path_recursive(upscaled_path, False)
    images_vector: List[str] = get_files_in_path_recursive(vector_path, False)
    images_error: List[str] = get_files_in_path_recursive(error_root_path, False)
    images_final: List[str] = get_files_in_path_recursive(final_path, False)
    images_trash: List[str] = get_files_in_path_recursive(trash_path, False)

    logger.info(f"{len(images_workbench)} files from path={workbench_path} ")
    logger.info(f"{len(images_upscaled)} files from path={upscaled_path} ")
    logger.info(f"{len(images_vector)} files from path={vector_path} ")
    logger.info(f"{len(images_error)} files from path={error_root_path} ")
    logger.info(f"{len(images_final)} files from path={final_path} ")
    logger.info(f"{len(images_trash)} files from path={trash_path} ")

