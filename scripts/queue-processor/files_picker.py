import os
import random
from glob import glob
from typing import List
from logger import logger


def get_files_in_path_recursive(path: str, shuffle: bool, filtering_function=None) -> List[str]:
    files_jpg = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.jpg'))]
    files_jpg_uc = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.JPG'))]
    files_jpeg = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.jpeg'))]
    files_jpeg_uc = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.JPEG'))]
    files_png = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.png'))]
    files_png_uc = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.PNG'))]
    files_gif = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.gif'))]
    files_gif_uc = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.GIF'))]
    files: List[str]
    files = files_jpg + files_jpeg + files_png + files_gif + files_jpg_uc + files_jpeg_uc + files_png_uc + files_gif_uc
    if shuffle:
        random.shuffle(files)
    if filtering_function:
        filtered_files = list(filter(lambda file: filtering_function(file), files))
        filtered = True
    else:
        filtered_files = files
        filtered = False
    logger.debug(f"Picked {len(filtered_files)} files from path={path} shuffled?={shuffle} filtered?={filtered}")
    return filtered_files
