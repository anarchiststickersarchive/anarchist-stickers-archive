import logging
import os

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger("root")
logger.setLevel(os.environ.get("LOGLEVEL", "INFO"))
