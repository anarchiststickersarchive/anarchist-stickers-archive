import shutil
import time
import traceback
from pathlib import Path
from typing import List
from logger import logger

from PIL.Image import DecompressionBombError

from files_picker import get_files_in_path_recursive
from image_upscaler import ImageUpscaler
from path_mapper_handler import *
from utils import move_to_trash
import imageio.v3 as iio

root_path = os.environ.get("PROJECT_PATH")


def run_upscaling() -> None:
    images: List[str] = get_files_in_path_recursive(workbench_path, False, image_should_be_scaled)
    for image_absolute_path in images:
        upscale_process_image(image_absolute_path)

    logger.info(f"** Up to clean up the workbench from already upscaled images.")
    upscaled_images: List[str] = get_files_in_path_recursive(workbench_path, False, image_has_already_scaled)
    for image_absolute_path in upscaled_images:
        trash_absolute_path = workbench_to_trash_path_mapping(image_absolute_path)
        move_to_trash(image_absolute_path, trash_absolute_path, logger)
    logger.info(f"** Workbench clean up for already upscaled images finished")


def image_should_be_scaled(image_absolute_path):
    has_scaled = image_has_already_scaled(image_absolute_path)
    return not has_scaled


def image_has_already_scaled(image_absolute_path):
    image_output_absolute_path = workbench_to_upscaled_path_mapping(image_absolute_path)
    has_scaled = Path(image_output_absolute_path).exists()
    return has_scaled


def upscale_process_image(image_input_absolute_path):
    image_output_absolute_path = workbench_to_upscaled_path_mapping(image_input_absolute_path)
    if Path(image_output_absolute_path).exists():
        logger.info(f"Skipping already scaled image: {image_input_absolute_path} at {image_output_absolute_path}")
    else:
        try:
            shape = iio.imread(image_input_absolute_path).shape
            height = shape[0]
            width = shape[1]
            if height < target_size or width < target_size:
                logger.info(f"** Up to scale source={image_input_absolute_path}")
                start_time = time.time()
                desired_width, desired_height = (target_size, int(target_size*height/width)) if width < height else (int(target_size*width/height), target_size)
                error_image_output_absolute_path = workbench_to_error_path_mapping(image_input_absolute_path)
                image_upscaler = ImageUpscaler(logger, image_input_absolute_path, image_output_absolute_path, error_image_output_absolute_path, desired_width, desired_height)
                image_upscaler.run()
                delta = (time.time() - start_time)
                log_scaled_image(
                    delta, desired_height, desired_width, height, image_input_absolute_path,
                    image_output_absolute_path, width
                )
                trash_absolute_path = workbench_to_trash_path_mapping(image_input_absolute_path)
                move_to_trash(image_input_absolute_path, trash_absolute_path, logger)
            else:
                shutil.copy(image_input_absolute_path, image_output_absolute_path)
                logger.info(f"{image_input_absolute_path} is already too big, copying instead of upscaling")
                pass
        except DecompressionBombError as e:
            shutil.copy(image_input_absolute_path, image_output_absolute_path)
            logger.info(f"{image_input_absolute_path} is already too big, copying instead of upscaling")
            pass
        except Exception as e:
            logger.error(f"{e}")
            logger.error(traceback.format_exc())
            pass


def log_scaled_image(delta, desired_height, desired_width, height, source_absolute_path,
                     destination_absolute_path, width):
    logger.info(f"** Image scaled.")
    logger.info(f"*** source={source_absolute_path}")
    logger.info(f"*** source dimensions={width}x{height}")
    logger.info(f"*** destination={destination_absolute_path}")
    logger.info(f"*** destination dimensions={desired_width}x{desired_height}")
    logger.info(f"*** processing time={delta}s")
    logger.info(f"****")

