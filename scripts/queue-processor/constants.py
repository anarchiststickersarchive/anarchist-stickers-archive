workbench_folder_name = "workbench"
upscaled_folder_name = "upscaled"
vector_folder_name = "2-vector"
final_folder_name = "final"
error_folder_name = "error"
trash_folder_name = "trash"

vectorization_types = ['clipart', 'drawing']
target_size = 2500
