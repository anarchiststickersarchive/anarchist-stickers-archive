import os
from enum import Enum
from pathlib import Path
from constants import *
from logger import logger


class ExtensionType(Enum):
    JPG = 1
    SVG = 2


class Folder:
    def __init__(self, root_path: str, name_path: str):
        self.root_path = root_path
        self.name_path = name_path
        self.absolute_path = os.path.join(root_path, name_path)
        self.absolute_path_path = Path(self.absolute_path)


class PathMapper:
    def __init__(self, source_folder: Folder, destination_folder: Folder, destination_extension: ExtensionType):
        self.source_folder = source_folder
        self.destination_folder = destination_folder
        self.destination_extension = destination_extension

    def get(self, source_absolute_path) -> str:
        filename = PathMapper.filename(source_absolute_path)
        filename_no_extension = PathMapper.filename_no_extension(source_absolute_path)
        path_infix = self.path_infix(source_absolute_path, filename)

        output_folder = os.path.join(self.source_folder.root_path, self.destination_folder.name_path, path_infix)
        Path(output_folder).mkdir(parents=True, exist_ok=True)
        return_value = os.path.join(output_folder, f"{filename_no_extension}.{self.destination_extension.name.lower()}")
        logger.debug(f"mapper={self.__class__.__name__} input={source_absolute_path}, output={return_value}")
        return return_value

    @staticmethod
    def extension(source_absolute_path) -> str:
        filename = PathMapper.filename(source_absolute_path)
        return os.path.splitext(filename)[1]

    @staticmethod
    def filename(source_absolute_path) -> str:
        source_absolute_path_path = Path(source_absolute_path)
        return source_absolute_path_path.name

    @staticmethod
    def filename_no_extension(source_absolute_path) -> str:
        filename = PathMapper.filename(source_absolute_path)
        return os.path.splitext(filename)[0]

    def path_infix(self, source_absolute_path, filename) -> str:
        return source_absolute_path \
            .removeprefix(self.source_folder.absolute_path + '/') \
            .removesuffix(filename) \
            .removesuffix('/')


class WorkbenchToUpscaledPathMapper(PathMapper):

    def __init__(self, root_path):
        workbench_folder = Folder(root_path, workbench_folder_name)
        upscaled_folder = Folder(root_path, upscaled_folder_name)
        super().__init__(workbench_folder, upscaled_folder, ExtensionType.JPG)


class WorkbenchToErrorPathMapper(PathMapper):

    def __init__(self, root_path):
        workbench_folder = Folder(root_path, workbench_folder_name)
        error_folder = Folder(root_path, error_folder_name)
        super().__init__(workbench_folder, error_folder, ExtensionType.JPG)


class WorkbenchToTrashPathMapper(PathMapper):

    def __init__(self, root_path):
        workbench_folder = Folder(root_path, workbench_folder_name)
        trash_folder = Folder(root_path, trash_folder_name)
        super().__init__(workbench_folder, trash_folder, ExtensionType.JPG)


class UpscaledToWorkbenchPathMapper(PathMapper):

    def __init__(self, root_path):
        upscaled_folder = Folder(root_path, upscaled_folder_name)
        workbench_folder = Folder(root_path, workbench_folder_name)
        super().__init__(upscaled_folder, workbench_folder, ExtensionType.JPG)


class UpscaledToTrashPathMapper(PathMapper):

    def __init__(self, root_path):
        upscaled_folder = Folder(root_path, upscaled_folder_name)
        trash_folder = Folder(root_path, trash_folder_name)
        super().__init__(upscaled_folder, trash_folder, ExtensionType.JPG)


class UpscaledToFinalPathMapper(PathMapper):

    def __init__(self, root_path):
        upscaled_folder = Folder(root_path, upscaled_folder_name)
        final_folder = Folder(root_path, final_folder_name)
        super().__init__(upscaled_folder, final_folder, ExtensionType.JPG)


class UpscaledToToVectorPathMapper(PathMapper):

    def __init__(self, root_path):
        upscaled_folder = Folder(root_path, upscaled_folder_name)
        vector_folder = Folder(root_path, vector_folder_name)
        super().__init__(upscaled_folder, vector_folder, ExtensionType.JPG)

    def get(self, source_absolute_path, model, number_of_colors) -> str:
        filename = PathMapper.filename(source_absolute_path)
        filename_no_extension = PathMapper.filename_no_extension(source_absolute_path)
        extension = PathMapper.extension(source_absolute_path)
        path_infix = self.path_infix(source_absolute_path, filename)

        output_folder = os.path.join(
            self.source_folder.root_path, self.destination_folder.name_path, f"{model}-{number_of_colors}/",
            path_infix
        )
        Path(output_folder).mkdir(parents=True, exist_ok=True)
        output_folder = os.path.join(output_folder, f"{filename_no_extension}{extension}")
        logger.debug(f"mapper={self.__class__.__name__} input={source_absolute_path}, output={output_folder}")
        return output_folder


class ToVectorToTrashPathMapper(PathMapper):

    def __init__(self, root_path):
        vector_folder = Folder(root_path, vector_folder_name)
        trash_folder = Folder(root_path, trash_folder_name)
        super().__init__(vector_folder, trash_folder, ExtensionType.JPG)

    def get(self, source_absolute_path, model, number_of_colors) -> str:
        filename = PathMapper.filename(source_absolute_path)
        filename_no_extension = PathMapper.filename_no_extension(source_absolute_path)
        extension = PathMapper.extension(source_absolute_path)
        path_infix = self.path_infix(source_absolute_path, filename).removeprefix(f"{model}-{number_of_colors}/")

        output_folder = os.path.join(self.source_folder.root_path, self.destination_folder.name_path, path_infix)
        Path(output_folder).mkdir(parents=True, exist_ok=True)
        return_value = os.path.join(output_folder, f"{filename_no_extension}{extension}")
        return return_value


class ToVectorToFinalPathMapper(PathMapper):

    def __init__(self, root_path):
        vector_folder = Folder(root_path, vector_folder_name)
        final_folder = Folder(root_path, final_folder_name)
        super().__init__(vector_folder, final_folder, ExtensionType.SVG)

    def get(self, source_absolute_path, model, number_of_colors) -> str:
        filename = PathMapper.filename(source_absolute_path)
        filename_no_extension = PathMapper.filename_no_extension(source_absolute_path)
        path_infix = self.path_infix(source_absolute_path, filename).removeprefix(f"{model}-{number_of_colors}/")

        output_folder = os.path.join(self.source_folder.root_path, self.destination_folder.name_path, path_infix)
        Path(output_folder).mkdir(parents=True, exist_ok=True)
        return_value = os.path.join(output_folder, f"{filename_no_extension}.{self.destination_extension.name.lower()}")
        return return_value


class FinalVectorToFinalJpgPathMapper(PathMapper):

    def __init__(self, root_path):
        final_folder = Folder(root_path, final_folder_name)
        super().__init__(final_folder, final_folder, ExtensionType.JPG)

    def get(self, source_absolute_path) -> str:
        return super().get(source_absolute_path)
