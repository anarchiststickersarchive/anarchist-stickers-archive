import time
import requests
import imageio.v3 as iio
from pathlib import Path


class ImageVectorizer:

    def __init__(self, logger, image_input_absolute_path, image_output_absolute_path, target_size, api_key):
        self.api_key = api_key
        self.image_input_absolute_path = image_input_absolute_path
        self.image_output_absolute_path = image_output_absolute_path
        self.target_size = target_size
        self.logger = logger

    def run(self, model, number_of_colors):
        has_vector = Path(self.image_output_absolute_path).exists()
        if has_vector:
            self.logger.info(f"** Skipping already scaled image: {self.image_input_absolute_path}")
            pass
        else:
            shape = iio.imread(self.image_input_absolute_path).shape
            height = shape[0]
            width = shape[1]
            desired_width, desired_height = self.__get_desired_dimensions(height, width)
            start_time = time.time()
            binary_response = self.__request_image_to_svg(desired_width, desired_height, model, number_of_colors)
            self.__save_binary(binary_response)
            delta = (time.time() - start_time)
            self.logger.info(f"** Image vectorized:")
            self.logger.info(f"*** source={self.image_input_absolute_path}")
            self.logger.info(f"*** dimensions={width}x{height}")
            self.logger.info(f"*** destination={self.image_output_absolute_path}")
            self.logger.info(f"*** time={delta}s")
            self.logger.info(f"****")

    def __save_binary(self, binary_response):
        with open(self.image_output_absolute_path, 'wb') as f:
            f.write(binary_response)

    def __request_image_to_svg(self, desired_width, desired_height, model, colors):
        headers = {
            'X-CREDITS-CODE': self.api_key,
        }
        metadata = {
            'format': (None, 'svg'),
            'colors': (None, colors),
            'model': (None, model),
            'algorithm': (None, 'auto'),
            'details': (None, 'auto'),
            'antialiasing': (None, 'off'),
            'minarea': (None, '5'),
            'colormergefactor': (None, '5'),
            'unit': (None, 'auto'),
            'width': (None, desired_width),
            'height': (None, desired_height),
            'roundness': (None, 'default'),
            'image': open(self.image_input_absolute_path, 'rb'),
        }

        response = requests.post('https://api.vectorizer.io/v4.0/vectorize', headers=headers, files=metadata)
        if response.status_code == 200:
            return response.content
        else:
            self.logger.error(f"** Failed request. status_code={response.status_code}, "
                              f"image_input_absolute_path={self.image_input_absolute_path} "
                              f"request.url={response.request.url}")
            raise Exception

    def __get_desired_dimensions(self, height, width):
        return (self.target_size, int(self.target_size * height / width)) \
            if width < height \
            else (int(self.target_size * width / height), self.target_size)
