import unittest
from logger import logger
from path_mapper import WorkbenchToUpscaledPathMapper, UpscaledToWorkbenchPathMapper, UpscaledToTrashPathMapper, \
    WorkbenchToErrorPathMapper, FinalVectorToFinalJpgPathMapper, UpscaledToFinalPathMapper, ToVectorToTrashPathMapper, \
    ToVectorToFinalPathMapper, UpscaledToToVectorPathMapper, WorkbenchToTrashPathMapper

root_path = "/tmp/queue-processor/test"


class Testing(unittest.TestCase):

    def test_workbench_to_upscaled_path_mapper(self):
        path_mapper = WorkbenchToUpscaledPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/workbench/instagram/file.jpg")
        assert result == "/tmp/queue-processor/test/upscaled/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/workbench/instagram/account/file.jpg")
        assert result_nested == "/tmp/queue-processor/test/upscaled/instagram/account/file.jpg"

    def test_workbench_to_error_path_mapper(self):
        path_mapper = WorkbenchToErrorPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/workbench/instagram/file.jpg")
        assert result == "/tmp/queue-processor/test/error/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/workbench/instagram/account/file.jpg")
        assert result_nested == "/tmp/queue-processor/test/error/instagram/account/file.jpg"

    def test_workbench_to_trash_path_mapper(self):
        path_mapper = WorkbenchToTrashPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/workbench/instagram/file.jpg")
        assert result == "/tmp/queue-processor/test/trash/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/workbench/instagram/account/file.jpg")
        assert result_nested == "/tmp/queue-processor/test/trash/instagram/account/file.jpg"

    def test_upscaled_to_workbench_path_mapper(self):
        path_mapper = UpscaledToWorkbenchPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/file.jpg")
        assert result == "/tmp/queue-processor/test/workbench/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/account/file.jpg")
        assert result_nested == "/tmp/queue-processor/test/workbench/instagram/account/file.jpg"

    def test_upscaled_to_trash_path_mapper(self):
        path_mapper = UpscaledToTrashPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/file.jpg")
        assert result == "/tmp/queue-processor/test/trash/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/account/file.jpg")
        assert result_nested == "/tmp/queue-processor/test/trash/instagram/account/file.jpg"

    def test_upscaled_to_final_path_mapper(self):
        path_mapper = UpscaledToFinalPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/file.jpg")
        assert result == "/tmp/queue-processor/test/final/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/account/file.jpg")
        assert result_nested == "/tmp/queue-processor/test/final/instagram/account/file.jpg"

    def test_upscaled_to_to_vector_path_mapper(self):
        path_mapper = UpscaledToToVectorPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/file.jpg", "clipart", "2")
        assert result == "/tmp/queue-processor/test/2-vector/clipart-2/instagram/file.jpg"
        result_nested = path_mapper.get("/tmp/queue-processor/test/upscaled/instagram/account/file.jpg", "clipart", "2")
        assert result_nested == "/tmp/queue-processor/test/2-vector/clipart-2/instagram/account/file.jpg"

    def test_final_vector_to_final_jpg_path_mapper(self):
        path_mapper = FinalVectorToFinalJpgPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/final/instagram/file.svg")
        assert result == "/tmp/queue-processor/test/final/instagram/file.jpg"

    def test_to_vector_to_trash_path_mapper(self):
        path_mapper = ToVectorToTrashPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/2-vector/clipart-2/instagram/file.jpg", "clipart", "2")
        assert result == "/tmp/queue-processor/test/trash/instagram/file.jpg"

    def test_to_vector_to_final_path_mapper(self):
        path_mapper = ToVectorToFinalPathMapper(logger, root_path)
        result = path_mapper.get("/tmp/queue-processor/test/2-vector/clipart-2/instagram/file.jpg", "clipart", "2")
        assert result == "/tmp/queue-processor/test/final/instagram/file.svg"
