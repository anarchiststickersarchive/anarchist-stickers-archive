#!/bin/bash

# project_path="../../";
project_path_absolute_path=$( realpath "$(pwd)/../../" );
lock_file="refresh.lock";
logfile_absolute_path="${project_path_absolute_path}/scripts/automatic-refresh/refresh.log";

# Check if the file exists and store the result in a variable
if [ -e "${lock_file}" ]; then
    file_exists=true;
else
    file_exists=false;
fi

touch "$lock_file";
chmod 777 ${lock_file}
# chown $(whoami):$(whoami) ${lock_file}

if [ "$file_exists" = false ]; then
  echo "$(date) ${lock_file} was missing, time to refresh." >> ${logfile_absolute_path};
  cd "${project_path_absolute_path}";
  pwd >> ${logfile_absolute_path};
  ls -lsa . >> ${logfile_absolute_path};
  echo "$(date) make refresh" >> ${logfile_absolute_path};
  make refresh >> ${logfile_absolute_path} 2>&1;
  echo "$(date) make refresh exit_code=$?" >> ${logfile_absolute_path};
fi

